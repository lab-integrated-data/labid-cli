# -*- coding: utf-8 -*-
"""
The 'config' module of the CLI
"""
import logging
import sys
import traceback
from datetime import datetime
from pathlib import Path
from typing import List, Tuple
from urllib.parse import urljoin

import typer

from cli import get_default_config_file_path, CURRENT_USERNAME, get_default_job_database_path, get_defaut_job_dbname
from cli.config import get_config
from cli.database import init_database, DatabaseHandler, Job, JobType, JobStatus
from cli.utils import ObjectState, mail_admin_error, send_email_from_admin, is_less_than_a_week
from cli.register_utils import ProcessedAssayDescriptor, validate_study_input, create_sniffer
from stocks import AssayStructureError

from stocks.models import User, Assay, InstrumentRun, Study
from stocksapi.client import StocksClient
from stocksapi.exceptions import MultipleObjectMatchedError
from stocksapi.manager import StocksManager
from stocksapi.models import PyAssayValidate

logger = logging.getLogger(__name__)

# name of this module (as appearing on the command line) is the last part of the __name__ e.g. cli.config -> config
_MODULE_NAME = __name__.rsplit(".", 1)[-1]
# list of command names offered in this module
_CMD_VALIDATE_RUNDIR = "rundir"
_CMD_VALIDATE_BATCH = "batch"

# create the CLI app
app = typer.Typer()

def email_validation_error_at_init(sniffer_name: str, study_id: str, error: Exception | str,
                                   trace: str | None = None, cc: List[str] | None = None):
    subj = f"LabID validate {_CMD_VALIDATE_BATCH} error"
    mess = f""" 
    The following error occurred at initialization of a labid validate {_CMD_VALIDATE_BATCH} command:
        - executing user: {CURRENT_USERNAME}
        - sniffer: {sniffer_name}
        - study: {study_id}

    Error: 
    {error}  
    """

    if trace:
        mess = mess + f"""
        
        Error trace: 
        {trace}
        """

    mail_admin_error(subj, mess, cc_recipients=cc)

def get_assay_owner_and_email(assay: Assay) -> Tuple[str, str]:
    username = assay.owner
    email = None
    if isinstance(assay.owner, User):
        username = assay.owner.username
        email = assay.owner.email
    return username, email

def email_validation_success(default_labid_url: str, assay: Assay, sniffer_name: str, study_id: str, cc: List[str] | None = None):
    (owner, owner_email) = get_assay_owner_and_email(assay)

    subj = f"LabID assay {assay.name} successfully validated"
    mess = f""" 
    Dear {owner},
    
    The assay {assay.name} ({urljoin(default_labid_url, assay.id)}) was successfully validated by user {CURRENT_USERNAME} using the 
     {sniffer_name} sniffer.
    All datasets have been added to the study: {urljoin(default_labid_url, study_id)}
    
    LabID
    """

    send_email_from_admin(subj=subj, mess=mess, cc_recipients=cc)


def email_validation_error(default_labid_url: str, assay: Assay, sniffer_name: str, study_id: str,
                           error: Exception | str, trace: str | None = None, cc: List[str] | None = None):

    owner = assay.owner
    if isinstance(owner, User):
        owner = owner.username
    subj = f"LabID assay validation error"
    mess = f""" 
    Dear {owner}
    
    An error occurred while trying to validate the assay {assay.name} ({urljoin(default_labid_url, assay.id)}):
        - Executing user: {CURRENT_USERNAME}
        - Sniffer: {sniffer_name}
        - Study: {study_id}

    Error message: 
    {error}  
    """

    if trace:
        mess = mess + f"""

        Error trace: 
        {trace}
        """

    send_email_from_admin(subj=subj, mess=mess, cc_recipients=cc)


def get_cc_list(assay_owner_email: str | None, cc: List[str] | None, dry_run: bool) -> List[str]:
    """
    return the list of emails with assay_owner_email in first position unless dry_run (assay_owner_email is not added)
    """
    l: List[str] = cc if cc else list()
    if not dry_run and assay_owner_email:
        l.append(assay_owner_email)
        l.reverse()
    elif dry_run and assay_owner_email:
        logger.warning(f"dry-run => Will NOT add assay owner email {assay_owner_email} in recipient list")
    return l

@app.command(_CMD_VALIDATE_BATCH,
             short_help="Loop over all 'initialized' assay and validate them against the provided sniffer.",
             help="""
             Loop over all 'initialized' assay (given the user connecting LabID) and validate them against the provided
              sniffer. 
             Assays for which sniffer's dir_qualifies() return False are silently ignored; automated validation 
             is attempted on other assays. An email is sent to assay's owner upon successful or errored validation, 
             provided that the assay's owner email is available in LabID.
             This method is particularly suited to call automated validation using a periodic task (systemd, cron...). 
             In this case we suggest to:
              - use a technical user per group (with an permanent API key) and use its configuration file
              - use email_reporting and set both on_error_email and on_success_email params
              - to route assays in different studies, use different calls and sniffers to adapt dir_qualifies() 
              - redirect the output to a log file
              - use the job_db_file parameter to enable persistence. This will avoid to e.g. report an error email for 
              failing assays at each wake up of the periodic task.  
              
             """
             )
def validate_batch(
        sniffer_name: str = typer.Option(
            ...,
            "--name",
            "-n",
            help="Name of the sniffer to use to extract the dataset and sample information."
        ),
        study_id: str = typer.Option(
            None,
            "--study",
            "-s",
            help="The UUID or name of the study to which all the datasets will be associated. When a name is given, it "
                 "must be unique. If multiple studies match the name, the command will fail."
                 "If not given, --study-token is expected."
        ),
        study_match_token: str = typer.Option(
            None,
            "--study-token",
            "-t",
            help="A token to dynamically look up the study. In this situation, samples are expected to exist in LabID. "
                 "For each dataset, the input sample is fetched, its primary project identified and the dataset is"
                 "associated to the project's study which name matches the token. If multiple studies match the token, "
                 "no guarantee is given on the study to which the dataset will be associated."
                 "If not given, --study is expected."
        ),
        dry_run: bool = typer.Option(
            False,
            "--dry-run / --submit",
            help="only prints the JSON payload without submitting it to the server API. "
                 "In dry run mode, the local job database is still being used but will be deleted right "
                 "before the process ends."
        ),
        sniffer_params: List[str] = typer.Option(
            None,
            "--sniffer_param", "-p",
            help="optional sniffer parameters e.g. --sniffer_param 'name=blah'."
        ),
        email_reporting: bool = typer.Option(
            True,
            "--email-reporting / --no-email-reporting",
            help="Should activity (success/error) be reported by email? "
                 "We recommend --email-reporting when configuring periodic jobs e.g. cron tasks"
                 "When --dry-run is used, emails are only sent to emails listed in --error-email and --success-email"
        ),
        on_error_email: List[str] = typer.Option(
            None,
            "--error-email",
            "-e",
            help="Additional email(s) to report error(s); ignored if --no-email-reporting is used. "
                 "By default the assay's owner gets an email unless no email is available in the owner's profile or "
                 "--dry-run is used"
        ),
        on_success_email: List[str] = typer.Option(
            None,
            "--success-email",
            "-m",
            help="Additional email(s) to report successful validation; ignored if --no-email-reporting is used. "
                 "By default the assay's owner gets an email unless no email is available in the owner's profile or "
                 "--dry-run is used"
        ),
        db_name: str = typer.Option(
            None,
            "--db-name",
            help="Name (not a path!) of a database to remember which assays were processed, when and their success"
                 " or error status. This name will be used to create (if not existing) a sqlite database file stored "
                 "in the home dir of the user used to connect to LabID (as configured in --config-path)."
                 "This is particularly important to avoid getting error emails each time the command is launched while "
                 "an error was not fixed."
        ),
        wipe_db: bool = typer.Option(
            False,
            "--wipe-db / --keep-db",
            help="Should we force create a fresh database?"
        ),
        max_assay_num: int = typer.Option(
            100,
            "--max-jobs",
            help="Maximum number of assays to process. Keep this low when setting up services."
        ),
        conf_file_path: str = typer.Option(
            get_default_config_file_path(),
            "--config-path",
            help="Config file absolute path")
) -> None:
    # list of assays to loop over
    assays: List[Assay] = list()
    db_handler: DatabaseHandler

    if not db_name:
        # create a default one named after this sniffer
        db_name = get_defaut_job_dbname(jobtype="validate", sniffer_name=sniffer_name)

    db_path = get_default_job_database_path(db_name)
    default_labid_url = ""
    try:
        _config = get_config(Path(conf_file_path))
        client: StocksClient = StocksClient(_config)
        stocks_manager: StocksManager = StocksManager(client)
        default_labid_url = _config["default"]

        if not db_path.exists():
            init_database(db_path)
        elif wipe_db:
            init_database(db_path, force_new=True, backup_before_wipe=True)

        db_handler = DatabaseHandler(db_path)

        # Check given id is really a study
        study: Study = None
        if study_id:
            study = validate_study_input(study_id, stocks_manager)
        elif not study_match_token:
            raise typer.BadParameter("Either --study or --study-token must be provided")

        sniffer = create_sniffer(sniffer_name, sniffer_params, stocks_manager)

        assays = stocks_manager.list_assays(state=ObjectState.INITIALIZED)

    except Exception as error:
        # failing here means a problem with the command line itself.
        # we need to report to admin
        trace = traceback.format_exc()
        logger.error(trace)
        logger.error(error)
        if email_reporting:
            email_validation_error_at_init(sniffer_name=sniffer_name,
                                           study_id=study_id if study_id else study_match_token,
                                           error=error,
                                           trace=trace,
                                           cc=on_error_email)
        raise typer.Exit(code=1)

    # now we loop over initialized assays
    assay: Assay = None
    processed_assays: List[ProcessedAssayDescriptor] = list()

    for assay in assays:
        logger.info(f"Looking at {assay.state.name} : {assay.name} ({assay.id}) => {assay.run_dir} ")
        # sniffed_assay will store the assay are returned by the sniffer
        sniffed_assay: Assay = None
        try:
            if not sniffer.dir_qualifies(dir_path=assay.run_dir):
                logger.info(f"SKIP {assay.name} ({assay.id}) => {assay.run_dir} ")
                continue

            # have reached the max_assay_num limit?
            if len(processed_assays) >= max_assay_num:
                logger.info(f"Max number of assays to process in a run reached : {max_assay_num}")
                break

            run_dir = assay.run_dir
            # check if this assay is already in the Job Db
            job: Job = db_handler.get_job_for_assay_id(assay_id=assay.id)
            if not job:
                logger.info(f"Creating new job for assay {assay.id} [{run_dir}]")
                job = db_handler.create_job(JobType.VALIDATE, dir_path=run_dir, assay_id=assay.id, sniffer=sniffer_name)
            if job.status == JobStatus.IGNORE:
                logger.info(f"Ignoring assay {assay.id} [{run_dir}]: Job exists with {job.status.value} status")
                continue
            elif job.status == JobStatus.ERROR and job.mail_sent and is_less_than_a_week(job.mail_sent):
                # we only skip errored jobs less than a week. If these are older than a week we try to validate them
                # again (which may raise a new error email). This is to make sure they dont go under the radar
                logger.info(f"Ignoring assay {assay.id} [{run_dir}]: Errored job recently reported to user")
                logger.info(str(job))
                continue

            # at this point either it a new assay, or it is errored but older than a week
            pad: ProcessedAssayDescriptor = ProcessedAssayDescriptor(assay=assay, job_id=job.id)
            processed_assays.append(pad)

            group = stocks_manager.logged_in_user.get_primary_group().name
            # looks good, try to register
            try:
                original_ownername: str = assay.owner.username if isinstance(assay.owner, User) else assay.owner
                runs = sniffer.sniff_instrument_run_assays(dir_path=run_dir, group=group, username=original_ownername)
            except AssayStructureError as err:
                _mess = f"Sniffer {sniffer_name} cannot validate data in {str(run_dir)}: {str(err)}"
                _trace = traceback.format_exc()
                logger.error(_mess)
                logger.error(_trace)
                pad.set_error(_mess, _trace)
                continue

            # note that we dont check len(runs) == 1 here (as in validate rundir) since here we are looping
            # on assays already in the db so they must contain a single run
            the_run: InstrumentRun = runs[0]
            if len(the_run.assays) != 1:
                _mess = (f"The sniffer '{sniffer_name}' detected {len(the_run.assays)} assay(s) while a unique "
                         f"one is expected per run directory.")
                logger.error(_mess)
                pad.set_error(_mess, "")
                continue

            sniffed_assay = the_run.assays[0]
            note_txt: str = (f"This assay was automatically validated by {stocks_manager.logged_in_user.username}  "
                             f"using the {sniffer_name} validator")

            if dry_run:
                put_obj: PyAssayValidate = stocks_manager._prepare_py_assay_validate_object(
                    sniffed_assay,
                    study_id=study.id if study_id else study_match_token
                )
                logger.info(f"Would validate assay {sniffed_assay.id}")
            else:
                resp = stocks_manager.validate_assay(
                    sniffed_assay,
                    study_id=study.id if study_id else study_match_token
                )
                logger.info(f"Assay {sniffed_assay.id} successfully validated.")
                logger.debug(f"Response was \n {resp}")

            # this assay is a success
            pad.is_failed = False

        except Exception as err:
            _mess = f"Unexpected error while processing assay {assay.name}: {str(err)}"
            _trace = traceback.format_exc()
            if processed_assays[-1].assay.id != assay.id:
                # error happened before we even create the pad & job id
                processed_assays.append(ProcessedAssayDescriptor(assay=assay, job_id=None))

            processed_assays[-1].set_error(_mess, _trace)
            # next assay
            continue

        # add a note if owner is not user
        if sniffed_assay.owner.username != stocks_manager.logged_in_user.username:
            if dry_run:
                logger.info(f"Would add note '{note_txt}' to assay with UUID {sniffed_assay.id}")
            else:
                logger.debug(f"adding note {note_txt}")
                try:
                    stocks_manager.save_note(note_txt, sniffed_assay.stocks_model_type, sniffed_assay.id)
                except Exception as err:
                    # not so important, just log this. The assay is still a success
                    logger.error(f"Failed to add note to assay {assay.name}: {str(err)}")
                    logger.error(traceback.format_exc())

    # Reporting
    #
    for pad in processed_assays:
        is_successful = not pad.is_failed
        _assay = pad.assay
        # owners email
        assay_owner_email = None
        if isinstance(_assay.owner, User) and _assay.owner.email:
            assay_owner_email = _assay.owner.email
        if not assay_owner_email:
            logger.warning(f"No email available for owner of assay {_assay.id}")

        # update job info
        now = datetime.now()
        job_id = pad.job_id
        if not job_id and email_reporting:
            email_validation_error(
                default_labid_url,
                _assay,
                sniffer_name,
                study_id if study_id else study_match_token,
                pad.error_message,
                pad.error_trace,
                cc=get_cc_list(assay_owner_email, on_error_email, dry_run=dry_run))
            continue

        j: Job = db_handler.load_job(job_id)
        j.modified = now
        try:
            if is_successful:
                j.status = JobStatus.COMPLETED
                j.message = f"Successfully validated assay in LabID using {sniffer_name}"
                if email_reporting:
                    email_validation_success(
                        default_labid_url,
                        _assay,
                        sniffer_name,
                        study_id if study_id else study_match_token,
                        cc=get_cc_list(assay_owner_email, on_success_email, dry_run=dry_run)
                    )
                    j.mail_sent = now
                else:
                    j.mail_sent = None
            else:
                j.status = JobStatus.ERROR
                j.message = pad.error_message
                if email_reporting:
                    email_validation_error(
                        default_labid_url,
                        _assay,
                        sniffer_name,
                        study_id if study_id else study_match_token,
                        pad.error_message,
                        pad.error_trace,
                        cc=get_cc_list(assay_owner_email, on_error_email, dry_run=dry_run))
                    j.mail_sent = now
                else:
                    j.mail_sent = None
        except Exception as err:
            # email failed
            logger.error(f"Failed to send email : {str(err)}")
            logger.error(traceback.format_exc())

        # save the job
        logger.info("Updating job:" + str(j))
        db_handler.update_job(j)

    if dry_run:
        # we clean up the job database
        logger.info("Cleaning up job database (dry-run)")
        for pad in processed_assays:
            if pad.job_id:
                try:
                    logger.info(f"Deleting {pad.job_id}...")
                    db_handler.delete_job(pad.job_id)
                except Exception as err:
                    logger.error(f"Failed to delete job {pad.job_id} from {db_path}. "
                                 f"Manual clean up needed! Error: {str(err)}")


@app.command(_CMD_VALIDATE_RUNDIR,
             short_help="Validate an 'initialized' assay identified by its run dir path.",
             help="""
             Longer help
             """
             )
def validate_assay(
        run_dir: Path = typer.Option(
            ...,
            "--run-dir",
            "-r",
            help="Path to the assay run directory to validate."
        ),
        sniffer_name: str = typer.Option(
            ...,
            "--name",
            "-n",
            help="Name of the sniffer to use to extract the dataset and sample information."
        ),
        study_id: str = typer.Option(
            None,
            "--study",
            "-s",
            help="The UUID or name of the study to which all the datasets will be associated. When a name is given, it "
                 "must be unique. If multiple studies match the name, the command will fail."
                 "If not given, --study-token is expected."
        ),
        study_match_token: str = typer.Option(
            None,
            "--study-token",
            "-t",
            help="A token to dynamically look up the study. In this situation, samples are expected to exist in LabID. "
                 "For each dataset, the input sample is fetched, its primary project identified and the dataset is"
                 "associated to the project's study which name matches the token. If multiple studies match the token, "
                 "no guarantee is given on the study to which the dataset will be associated."
                 "If not given, --study is expected."
        ),
        sniffer_params: List[str] = typer.Option(
            None,
            "--sniffer-param", "-p",
            help="optional sniffer parameters e.g. --sniffer_param 'name=blah'."
        ),
        dry_run: bool = typer.Option(
            False,
            "--dry-run / --submit",
            help="only prints the JSON payload without submitting it to the server API"
        ),
        conf_file_path: str = typer.Option(
            get_default_config_file_path(),
            "--config-path",
            help="Config file absolute path")
) -> None:
    """
    Register one or more assay and associated raw data to LabID. All assays must be of the same type i.e. same
    technology (e.g. 'sequencing') and platform (e.g. Illumina or Nanopore) and produced by a unique instrument run.
    The assay(s) are parsed from a run directory obeying a structure recognized by a 'sniffer'.
    """
    _config = get_config(Path(conf_file_path))
    client: StocksClient = StocksClient(_config)
    stocks_manager: StocksManager = StocksManager(client)

    study: Study = None
    # Check given id is really a study
    if study_id:
        study = validate_study_input(study_id, stocks_manager)
    elif not study_match_token:
        raise typer.BadParameter("Either --study or --study-token must be provided")

    sniffer = create_sniffer(sniffer_name=sniffer_name, sniffer_params=sniffer_params, stocks_manager=stocks_manager)
    # looks good, snif the dir_path
    try:
        runs = sniffer.sniff_instrument_run_assays(
            dir_path=run_dir, group=stocks_manager.logged_in_user.get_primary_group().name)
    except AssayStructureError as err:
        logger.error(str(err))
        print(f"The data in {str(run_dir)} does not comply to the sniffer {sniffer_name} expectations!")
        sys.exit(1)
    except MultipleObjectMatchedError as e:
        raise typer.BadParameter(f"{len(e.results)} assays returned for --run-dir {run_dir}")

    if not runs or len(runs) == 0:
        raise typer.BadParameter(f"Sniffer '{sniffer_name}' could not detect any assay i.e. data in {str(run_dir)} "
                                 f"is not recognized by this sniffer.")
    if len(runs) > 1:
        raise typer.BadParameter(f"Sniffer '{sniffer_name}' detected {len(runs)} instrument runs in the directory"
                                 f" {str(run_dir)}. Loading mutliple runs at once is not supported; please call"
                                 f" the {_CMD_VALIDATE_RUNDIR} command on a folder containing results for a unique run")

    the_run: InstrumentRun = runs[0]
    if len(the_run.assays) != 1:
        raise typer.BadParameter(f"The sniffer '{sniffer_name}' detected {len(the_run.assays)} assay(s) while a unique"
                                 f" one is expected in the {_CMD_VALIDATE_RUNDIR} command.")
    the_assay: Assay = the_run.assays[0]

    note_txt: str = f"This assay was automatically validated by {stocks_manager.logged_in_user.username} using " \
                    f"the {sniffer_name} validator"
    if dry_run:
        put_obj: PyAssayValidate = stocks_manager._prepare_py_assay_validate_object(
            the_assay,
            study_id=study.id if study_id else study_match_token
        )
        print(put_obj.json())
        print(f"would add note '{note_txt}' to modeltype {the_assay.stocks_model_type} with UUID {the_assay.id}")

    else:
        resp = stocks_manager.validate_assay(
            the_assay,
            study_id=study.id if study_id else study_match_token
        )
        # add a note if owner is not user
        if the_assay.owner.username != stocks_manager.logged_in_user.username:
            logger.debug(f"adding note {note_txt}")
            stocks_manager.save_note(note_txt, the_assay.stocks_model_type, the_assay.id)
        print(resp)

