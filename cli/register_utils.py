import json
import logging
from pathlib import Path
from typing import Dict, List

import typer

from cli.utils import is_uuid, ModelType
from stocks.assaysniffer import AssaySniffer
from stocks.assaysniffer.registry import registry
from stocks.models import Assay, Study
from stocksapi.exceptions import MultipleObjectMatchedError, HTTPException
from stocksapi.manager import StocksManager

logger = logging.getLogger(__name__)


############################
# Constants and common models
#############################

class ProcessedAssayDescriptor:
    def __init__(self, job_id: int | None, assay: Assay | None = None, dir: str | Path | None = None):
        self.assay = assay
        self.dir = dir
        self.job_id = job_id
        self.is_failed = None
        self.error_message = None
        self.error_trace = None

    def set_error(self, message: str, trace: str | None):
        self.is_failed = True
        self.error_message = message
        self.error_trace = trace


def validate_study_input(study_name_or_uuid: str, stocks_manager: StocksManager, param_name: str = "--study") -> Study:
    """
    load a Study from server
    raises typer.BadParameter for identified issues or if no Study is identified
    """
    study: Study = None
    try:
        if not is_uuid(study_name_or_uuid):
            try:
                # try fetch by name
                o: dict | None = stocks_manager.fetch_item_by_name(name=study_name_or_uuid, model=ModelType.STUDY)
                if o and 'id' in o:
                    logging.debug(f"Fetched study {o} for name {study_name_or_uuid}")
                    study_name_or_uuid = o['id']
                else:
                    raise typer.BadParameter(
                        f"No studies returned for {param_name} {study_name_or_uuid} ; please use UUID")
            except MultipleObjectMatchedError as e:
                raise typer.BadParameter(f"{len(e.results)} studies returned for {param_name} {study_name_or_uuid}")

        study = stocks_manager.fetch_study(study_name_or_uuid)

    except HTTPException as e:
        logger.error(f"Error while fetching study with id {study_name_or_uuid}: {e.detail}")

    if not study:
        raise typer.BadParameter(f"No study returned for {param_name} {study_name_or_uuid}")

    return study


def parse_sniffer_params(sniffer_params) -> Dict[str, str]:
    if not sniffer_params:
        return {}
    if isinstance(sniffer_params, dict):
        return sniffer_params

    sniff_param_map: dict = {}
    if sniffer_params:
        for param in sniffer_params:
            k, v = param.split("=") if '=' in param else param.split(":")
            sniff_param_map[k] = v
        logger.debug(sniff_param_map)
    return sniff_param_map


def create_sniffer(sniffer_name: str, sniffer_params: List[str] | Dict[str,str], stocks_manager: StocksManager) \
        -> AssaySniffer:
    """
    raise BadParameter if no sniffer is found for sniffer_name or sniffer_params dont validate
    """
    sniff_param_map: Dict[str, str] = parse_sniffer_params(sniffer_params)
    sniffer: AssaySniffer = registry.get_sniffer_instance(sniffer_name, **sniff_param_map)
    if not sniffer:
        raise typer.BadParameter(f"Unknown sniffer name: {sniffer_name}. "
                                 f"Registered sniffers: {registry.get_registered_sniffer_names()}")

    sniffer.set_stocks_manager(stocks_manager)

    if sniff_param_map:
        # check input
        supported = sniffer.get_sniffer_param_names()
        for key, value in sniff_param_map.items():
            if key not in supported:
                raise typer.BadParameter(
                    f"Unknown parameter passed to --sniffer_param: {key} for sniffer {sniffer_name}. "
                    f"Supported sniffer parameters: {json.dumps(supported, indent=4)}"
                )

    return sniffer
