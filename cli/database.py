"""
This module is an interface to the database storing the import jobs.

The error management is to not catch sqlite3.Error and let the client decide what needs to happen upon error
"""

# watchdog/database.py

import datetime
import sqlite3
import time
import logging
from enum import Enum

from typing import List, Optional
from pathlib import Path

logger = logging.getLogger(__name__)


class JobType(Enum):
    VALIDATE = 'VALIDATE'
    REGISTER = 'REGISTER'


class JobStatus(Enum):
    IGNORE = 'IGNORE'  # to be used when a job is not suitable and should be ignored
    INIT = 'INIT'  # to be used at job creation
    PROCESSING = 'PROCESSING'  # when jobs span many iterations (like register batch)
    COMPLETED = 'COMPLETED'  # when the job has finished successfully
    ERROR = 'ERROR'  # when the job has failed and should be fixed


def init_database(db_path: Path, force_new: bool = False, backup_before_wipe: bool = True,
                  backup_name: str = None):
    """
    Create the job database
    :param db_path: path to the sqlite db file
    :param force_new: recreate a db even if db_path already exists
    :param backup_before_wipe: should a backup of the db_path be made before re-create
    :param backup_name: optional, path for the backup

    The message stores the success or error message, the status governs if the Job is in ERROR state or not
    mail_sent should store the last time the user got the email
    modified should be set to indicate when was the last time the job was tried. Logic can thus be added to skip


    raise sqlite3.Error if something goes wrong
    """
    if force_new and db_path.exists():
        if backup_before_wipe:
            if backup_name is None:
                since_epoch: int = int(time.time())
                backup_name = f"{db_path.stem}_{str(since_epoch)}{db_path.suffix}"
            logger.debug(
                f"Database file already exists. Making backup before database fresh initialization : {backup_name}")
            db_path.rename(Path(db_path.parent, backup_name))
        else:
            db_path.unlink()
    conn = None
    cursor = None
    try:
        # this creates the file if not existing and open connection
        conn = sqlite3.connect(db_path,
                               detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
                               )
        sqlite_create_job_table_query = '''CREATE TABLE job (
                                        id INTEGER PRIMARY KEY,
                                        job_type TEXT NOT NULL,
                                        dirpath TEXT NOT NULL UNIQUE,
                                        assay_id TEXT,
                                        sniffer TEXT,
                                        status TEXT NOT NULL,
                                        message TEXT NOT NULL,
                                        mail_sent TIMESTAMP,
                                        md5sum TEXT,
                                        submitted TIMESTAMP,
                                        dir_removed TIMESTAMP,
                                        created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                        modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                                        );'''

        cursor = conn.cursor()
        cursor.execute(sqlite_create_job_table_query)
        conn.commit()
        logger.debug(f"Database initialized in file {str(db_path)}.")
    except sqlite3.Error as error:
        logger.error("Error while connecting to sqlite", error)
        raise
    finally:
        if cursor:
            cursor.close()
        if conn:
            conn.close()


class Job:
    """A simple class to wrap a data Job import and associated business logic"""

    def __init__(self, jobid: int, job_type: JobType, dirpath: Path, assay_id: str, sniffer: str, status: JobStatus,
                 message: str, mail_sent: datetime, md5sum: str,
                 submitted: datetime, dir_removed: datetime, created: datetime, modified: datetime
                 ) -> None:
        self.id: int = jobid
        self.job_type: JobType = job_type
        self.dirpath: Path = dirpath
        self.md5sum = md5sum
        self.assay_id: str = assay_id
        self.sniffer: str = sniffer
        self.status: JobStatus = status
        self.message: str = message
        self.mail_sent: datetime = mail_sent
        self.submitted: datetime = submitted
        self.dir_removed: datetime = dir_removed
        self.created: datetime = created
        self.modified: datetime = modified

    def __str__(self):
        sent_str = self.mail_sent.strftime("%m/%d/%Y %H:%M:%S") if self.mail_sent else "not sent yet"
        submitted_str = self.submitted.strftime("%m/%d/%Y %H:%M:%S") if self.submitted else "not submitted yet"
        dir_removed_str = self.dir_removed.strftime("%m/%d/%Y %H:%M:%S") if self.dir_removed else "not removed yet"

        return f"""
        Job {self.job_type.value} id={self.id} Created:{self.created.strftime("%m/%d/%Y %H:%M:%S")} Modified:{self.modified.strftime("%m/%d/%Y %H:%M:%S")}
            Status={self.status.value}; Run Dir={str(self.dirpath)}; Assay={self.assay_id}; Sniffer={self.sniffer}; Hash={self.md5sum}
            Message: {self.message}
            Message mailed on: {sent_str}
            Job Submitted on: {submitted_str}
            Data dir cleanup on: {dir_removed_str}
            """


class DatabaseHandler:
    """A facade to the job database"""""

    def __init__(self, db_path: Path) -> None:
        self._db_path = db_path

    @staticmethod
    def _dbrow_to_job(row: []) -> Job:
        return Job(
            jobid=row[0],
            job_type=JobType(row[1]),
            dirpath=Path(row[2]),
            assay_id=row[3],
            sniffer=row[4],
            status=JobStatus(row[5]),
            message=row[6],
            mail_sent=row[7],
            md5sum=row[8],
            submitted=row[9],
            dir_removed=row[10],
            created=row[11],
            modified=row[12]
        )

    def list_jobs(self, statuses: List[JobStatus] = list(JobStatus)) \
            -> List[Job]:
        """
        Fetch all the jobs which status and steps match provided lists

        :exception sqlite3.Error
        """

        conn = None
        cursor = None
        try:
            sqlite_select_query = """
            SELECT *
            FROM job 
            WHERE status IN ({0}) 
            """.format(', '.join('?' for _ in statuses))

            conn = sqlite3.connect(self._db_path,
                                   detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
                                   )
            cursor = conn.cursor()
            values: List[str] = [e.value for e in statuses]
            cursor.execute(sqlite_select_query, values)
            records = cursor.fetchall()
            jobs = []
            for row in records:
                job: Job = DatabaseHandler._dbrow_to_job(row)
                jobs.append(job)

            return jobs
        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.close()

    def get_job_for_path(self, path: str | Path) -> Optional[Job]:
        """
        Checks if a Job already exists for the given path and returns it, or None

        :exception sqlite3.Error
        """
        conn = None
        cursor = None
        try:
            sqlite_get_job_id = "SELECT id FROM job WHERE dirpath=:dirpath "
            conn = sqlite3.connect(self._db_path,
                                   detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
            cursor = conn.cursor()
            cursor.execute(sqlite_get_job_id, {"dirpath": str(path)})
            row = cursor.fetchone()
            if row is None:
                return None
            return self.load_job(row[0])

        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.close()

    def get_job_for_assay_id(self, assay_id: str) -> Optional[Job]:
        """
        Checks if a Job already exists for the given path and returns it, or None

        :exception sqlite3.Error
        """
        conn = None
        cursor = None
        try:
            sqlite_get_job_id = "SELECT id FROM job WHERE assay_id=:assay_id "
            conn = sqlite3.connect(self._db_path,
                                   detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
            cursor = conn.cursor()
            cursor.execute(sqlite_get_job_id, {"assay_id": assay_id})
            row = cursor.fetchone()
            if row is None:
                return None
            return self.load_job(row[0])

        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.close()

    def load_job(self, jobid: int) -> Job | None:
        """
        Reads a Job from id. This also loads associated datasets

        :exception sqlite3.Error
        """
        conn = None
        cursor = None
        try:
            sqlite_load_job = "SELECT * " \
                              " FROM job" \
                              " WHERE id=:jobid "

            conn = sqlite3.connect(self._db_path,
                                   detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
            cursor = conn.cursor()
            cursor.execute(sqlite_load_job, {"jobid": jobid})
            row = cursor.fetchone()
            if row is None:
                return None
            job: Job = DatabaseHandler._dbrow_to_job(row)

            return job
        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.close()

    def create_job(self, job_type: JobType, dir_path: Path, assay_id: str = "", sniffer: str = "", md5sum: str = "") \
            -> Job:
        """
        Creates a job and return the created object.

        :param dir_path:
        :param assay_id:
        :param sniffer:
        :return:
        :exception sqlite3.Error
        """
        conn = None
        cursor = None

        try:

            conn = sqlite3.connect(self._db_path,
                                   detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
                                   )
            # sqlite3 automatically assign the id
            sqlite_insert = """
            INSERT INTO job
            ('job_type', 'dirpath', 'assay_id', 'sniffer', 'status', 'message', 'mail_sent', 'md5sum', 'submitted',
             'dir_removed', 'created', 'modified') 
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
            """
            cursor = conn.cursor()
            now = datetime.datetime.now()
            cursor.execute(
                sqlite_insert,
                (job_type.value, str(dir_path), assay_id, sniffer,
                 JobStatus.INIT.value, "", "", md5sum, "", "", now, now))
            conn.commit()
            # assemble the created job
            jobid: int = cursor.lastrowid
            return Job(jobid, job_type, dir_path, assay_id, sniffer, JobStatus.INIT, "", "", md5sum, "", "", now, now)

        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.close()

    def update_job(self, job: Job) -> None:
        """
        Update an existing job.
        Only mutable fields ie assay_id, sniffer, status, message, mail_sent & modified are considered

        :exception sqlite3.Error
        """
        conn = None
        cursor = None
        try:
            conn = sqlite3.connect(self._db_path,
                                   detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
                                   )
            # sqlite3 automatically assign the id
            sqlite_update_job = """UPDATE job 
                            SET assay_id=?, sniffer=?, status=?, message=?, mail_sent=?, modified=?, md5sum=?, 
                            submitted=?, dir_removed=? 
                            WHERE id=?
                            """

            cursor = conn.cursor()
            cursor.execute(sqlite_update_job,
                           (
                               job.assay_id, job.sniffer, job.status.value, job.message, job.mail_sent, job.modified,
                               job.md5sum, job.submitted, job.dir_removed,
                               job.id))

            conn.commit()

        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.close()

    def delete_job(self, jobid: str) -> None:
        """
        Delete a job

        :exception sqlite3.Error
        """
        conn = None
        cursor = None
        try:
            conn = sqlite3.connect(self._db_path,
                                   detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
                                   )
            # sqlite3 automatically assign the id
            sqlite_delete_job = """DELETE from job WHERE id=:jobid"""

            cursor = conn.cursor()
            cursor.execute(sqlite_delete_job, {"jobid": jobid})
            conn.commit()

        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.close()
