import logging
from pathlib import Path
import sys
import os
from typing import List

import typer
import getpass
import configparser

import cli

logger = logging.getLogger(__name__)

# user calling cmd line
CURRENT_USERNAME = getpass.getuser()

FILE_DIR = Path(__file__).parent

# init the version from version.py; this adds the __version__ in main_ns
main_ns = {}
with open(os.path.join(FILE_DIR, 'version.py')) as f:
    exec(f.read(), main_ns)
with open(os.path.join(FILE_DIR, 'appname.py')) as f:
    exec(f.read(), main_ns)

__app_name__ = main_ns['__app_name__']
__version__ = main_ns['__version__']

# read the global conf file
_GLOBAL_CONFIG_FILE_NAME = "labidcli.ini"
_GLOBAL_CONFIG_FILE_TEMPLATE_NAME = "labidcli.ini.template"
config_parser = configparser.ConfigParser()
_conf_file_path: Path = Path(FILE_DIR.parent, _GLOBAL_CONFIG_FILE_NAME)
_conf_template_file_path: Path = Path(FILE_DIR.parent, _GLOBAL_CONFIG_FILE_TEMPLATE_NAME)

if _conf_file_path.exists():
    # read up custom config
    config_parser.read(_conf_file_path)
elif _conf_template_file_path.exists():
    # read up default config, will not work unless at embl prod
    config_parser.read(_conf_template_file_path)
else:
    typer.echo(f"Could not find the global config file: {str(_conf_file_path)}")
    sys.exit(1)

# options found in the global config file
_CONFIG_SECTION_STOCKS = 'labid'
_CONFIG_OPTION_STOCKS_API_URL = 'api_url'
_CONFIG_OPTION_STOCKS_USER = 'labid_user'
_CONFIG_OPTION_STOCKS_SNIFFER_PLUGIN_DIRS = 'sniffer_plugin_dirs'

_CONFIG_SECTION_MAIL = 'mail'
_CONFIG_OPTION_MAIL_HOST = 'host'
_CONFIG_OPTION_MAIL_SMTP = 'smtp'

_CONFIG_SECTION_ADMIN = 'admin'
_CONFIG_OPTION_ADMIN_NAME = 'name'
_CONFIG_OPTION_ADMIN_EMAIL = 'email'

_CONFIG_SECTION_MAGETAB = 'magetab'
_CONFIG_OPTION_MAGETAB_LASTNAME = 'submitter_lastname'
_CONFIG_OPTION_MAGETAB_MIDDLENAME = 'submitter_middlename'
_CONFIG_OPTION_MAGETAB_FIRSTNAME = 'submitter_firstname'
_CONFIG_OPTION_MAGETAB_EMAIL = 'submitter_email'
_CONFIG_OPTION_MAGETAB_AFFILIATION = 'default_affiliation'

_CONFIG_SECTION_INSTITUTION = 'institution'
_CONFIG_OPTION_DEFAULT_INSTITUTION = 'default_institution'

_CONFIG_SECTION_PLUGIN = 'plugins'
_CONFIG_OPTION_DMA_PLUGIN_ENABLE = 'plugin_dma_enable'
_CONFIG_OPTION_DMA_URL = 'plugin_dma_url'

############################################
# User specific setup
# where to save configs by default ; this is :
#   - /Users/<user>/Library/Application\ Support/watchdog/ on mac
#   - /home/user/.config/watchdog/ on ubuntu ...
###########################################
_DEFAULT_CONFIG_DIR_PATH = Path(typer.get_app_dir(app_name=__app_name__, force_posix=True))
# default name of the config file
_DEFAULT_CONFIG_FILE_PATH: Path = _DEFAULT_CONFIG_DIR_PATH / "labidapi.yml"


def get_defaut_job_dbname(jobtype: str, sniffer_name: str) -> str:
    return jobtype+ "_" + sniffer_name.replace(" ", "_")+".sqlite"

def get_sniffer_plugin_dir_list() -> List[Path]:
    """
    read the list of registered sniffer plugin dirs from the config file
    :return: a list of Path or an empty list
    """
    path_list: List[Path] = []
    value: str = config_parser.get(_CONFIG_SECTION_STOCKS, _CONFIG_OPTION_STOCKS_SNIFFER_PLUGIN_DIRS)
    if not value:
        logger.warning("No value defined for config option %s", _CONFIG_OPTION_STOCKS_SNIFFER_PLUGIN_DIRS)
        return path_list

    logger.debug("config value for %s: %s", _CONFIG_OPTION_STOCKS_SNIFFER_PLUGIN_DIRS, value)

    for p in value.split(sep=","):
        plugin_dir_path: Path = Path(p)
        if plugin_dir_path.exists() and plugin_dir_path.is_dir():
            logger.debug("keeping plugin_dir_path: %s", p)
            path_list.append(plugin_dir_path)
        else:
            logger.warning("ignoring invalid plugin_dir_path: %s", p)

    return path_list


def get_default_config_file_path() -> Path:
    """
    Show current configuration information
    """
    return _DEFAULT_CONFIG_FILE_PATH

def get_default_job_database_path(db_name: str) -> Path:
    return Path(_DEFAULT_CONFIG_DIR_PATH, db_name)

def get_labid_server_user() -> str:
    return config_parser.get(_CONFIG_SECTION_STOCKS, _CONFIG_OPTION_STOCKS_USER)

def get_default_stocks_api_url() -> str:
    """
    :return: the default LabID API URL
    """
    return config_parser.get(_CONFIG_SECTION_STOCKS, _CONFIG_OPTION_STOCKS_API_URL)

def get_default_mail_host() -> str:
    return config_parser.get(_CONFIG_SECTION_MAIL, _CONFIG_OPTION_MAIL_HOST)

def get_default_mail_smtp() -> str:
    return config_parser.get(_CONFIG_SECTION_MAIL, _CONFIG_OPTION_MAIL_SMTP)

def get_default_admin_name() -> str:
    return config_parser.get(_CONFIG_SECTION_ADMIN, _CONFIG_OPTION_ADMIN_NAME)

def get_default_admin_email() -> str:
    return config_parser.get(_CONFIG_SECTION_ADMIN, _CONFIG_OPTION_ADMIN_EMAIL)

def get_mailing_details() -> List[str]:
    return [get_default_mail_host(), get_default_mail_smtp(), get_default_admin_name(), get_default_admin_email()]

def get_default_submitter_lastname() -> str:
    return config_parser.get(_CONFIG_SECTION_MAGETAB, _CONFIG_OPTION_MAGETAB_LASTNAME)

def get_default_submitter_middlename() -> str:
    return config_parser.get(_CONFIG_SECTION_MAGETAB, _CONFIG_OPTION_MAGETAB_MIDDLENAME)

def get_default_submitter_firstname() -> str:
    return config_parser.get(_CONFIG_SECTION_MAGETAB, _CONFIG_OPTION_MAGETAB_FIRSTNAME)

def get_default_submitter_email() -> str:
    return config_parser.get(_CONFIG_SECTION_MAGETAB, _CONFIG_OPTION_MAGETAB_EMAIL)

def get_default_affiliation() -> str:
    return config_parser.get(_CONFIG_SECTION_INSTITUTION, _CONFIG_OPTION_DEFAULT_INSTITUTION)

##########################################################
# DMA. set those live to overwrite config file values
##########################################################
DMA_URL: str = None
DMA_SCOPE: str = None

def set_dma_plugin_options(url: str, scope:str):
    cli.DMA_URL = url
    cli.DMA_SCOPE = scope

# we return the DMA_URL (which is set using global cli options) if set no matter what the config says;
# else we follow the config
def get_dma_plugin_api_url() -> str:
    """
    :return: the default API URL of the DMA plugin
    """
    if DMA_URL:
        return DMA_URL
    elif is_dma_plugin_enabled():
        return config_parser.get(_CONFIG_SECTION_PLUGIN, _CONFIG_OPTION_DMA_URL)
    return None

def get_dma_plugin_scope() -> str:
    """
    :return: the default API URL of the DMA plugin
    """
    if DMA_SCOPE:
        return DMA_SCOPE
    return "User"

def is_dma_plugin_enabled() -> bool:
    val = config_parser.get(_CONFIG_SECTION_PLUGIN, _CONFIG_OPTION_DMA_PLUGIN_ENABLE)
    return val.lower() in ['true', 'yes', '1']