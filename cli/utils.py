import hashlib
import re
import os
import smtplib
import logging
import unicodedata
from datetime import datetime
from shutil import copytree
from email.message import EmailMessage
from enum import Enum
from html.parser import HTMLParser
from pathlib import Path
from pwd import getpwuid
import socket
from subprocess import Popen, PIPE
from typing import Union, Iterable, List, Tuple
from uuid import UUID

from jsonpickle import handlers

from cli import get_mailing_details

logger = logging.getLogger(__name__)

############################
# Constants and common models
#############################

class HTMLFilter(HTMLParser):
    text = ""

    def handle_data(self, data):
        self.text += data


class JsonEnumHandler(handlers.BaseHandler):

    def restore(self, obj):
        pass

    def flatten(self, obj: Enum, data):
        return obj.value


class ExtendedEnum(Enum):
    """
    https://stackoverflow.com/questions/29503339/how-to-get-all-values-from-python-enum-class
    """
    @classmethod
    def list(cls):
        return list(map(lambda c: c.value, cls))


class ModelType(ExtendedEnum):
    ANNOTATION = "annotation"
    AVITISEQUENCINGASSAY = "AVITISEQUENCINGASSAY"
    ARCHIVE = "archive"
    ASSAY = "assay"
    ATTACHMENT = "attachment"
    CVCATEGORY = "CATEGORIES"
    CONSUMABLE = "consumable"
    DATAFILE = "datafile"
    DATAFILECOPY = "datafilecopy"
    DATASET = "dataset"
    DATASETCOLLECTION = "datasetcollection"
    DROPBOX = "dropbox"
    EMSAMPLE = 'EMSAMPLE'
    EQUIPMENT = "equipment"
    EXPERIMENT = "experiment"
    GENERICASSAY = "GENERICASSAY"
    GENERICSAMPLE = "GENERICSAMPLE"
    GROUP = "group"
    INSTRUMENTMODEL = "instrumentmodel"
    INSTRUMENTRUN = "instrumentrun"
    LIGHTMICROSCOPYASSAY = "LIGHTMICROSCOPYASSAY"
    LIGHTMICROSCOPYSCREENASSAY = "LIGHTMICROSCOPYSCREENASSAY"
    NANOPOREASSAY = "NANOPOREASSAY"
    NGSILLUMINAASSAY = "NGSILLUMINAASSAY"
    NOTE = "note"
    PROJECT = "project"
    PROTOCOL = "protocol"
    SAMPLE = "sample"
    SEQUENCINGLIBRARY = "SEQUENCINGLIBRARY"
    SPECIMEN = "specimen"
    STORAGE_EQUIPMENT = "storageequipment"
    STORAGE_VOLUME = "storagevolume"
    STUDY = "study"
    TERM = "term"
    TRANSMISSIONEMASSAY = "TRANSMISSIONEMASSAY"
    USER = "user"
    VOLUMEEMASSAY = "VOLUMEEMASSAY"
    WORKFLOW = "workflow"


class ExperimentStatus(ExtendedEnum):
    PLANNED = 'PLANNED'
    IN_PROGRESS = 'IN PROGRESS'
    COMPLETED = 'COMPLETED'
    FAILED = 'FAILED'

class StorageBackend(ExtendedEnum):
    LOCAL = 'LocalStorageBackend'
    DMA_LOCAL = 'EmblDmStorageBackend'
    S3 = 'ObjectStorageBackend'
    ARCHIVE = 'ArchiveStorageBackend'

class UserRole(ExtendedEnum):
    """
    roles from EFO https://www.ebi.ac.uk/ols/ontologies/efo/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBFO_0000023&lang=en&viewMode=All&siblings=false
    """
    SUBMITTER = 'submitter'
    INVESTIGATOR = 'investigator'
    DATA_ANALYST = 'data analyst'
    EXPERIMENT_PERFORMER = 'experiment performer'


class Technology(ExtendedEnum):
    SEQUENCING = 'sequencing'
    LIGHT_MICROSCOPY = 'light microscopy'
    ELECTRON_MICROSCOPY = 'electron microscopy'
    PROTEOMICS = 'proteomics'
    FLOW_CYTOMETRY = 'flow cytometry'
    METABOLOMICS = 'metabolomics'
    OTHER = 'others'
    NA = 'NA'

class InstrumentType(ExtendedEnum):
    SEQUENCER = 'SEQUENCER'
    MICROSCOPE = 'MICROSCOPE'
    SPECTROMETER = 'SPECTROMETER'


class SequencingRunType(ExtendedEnum):
    SINGLE_END = 'single-end'
    PAIRED_END = 'paired-end'
    MULTI_END = 'multi-end'


class SequencingReadType(ExtendedEnum):
    READ1 = 'read_1'
    READ2 = 'read_2'
    READ3 = 'read_3'
    READ4 = 'read_4'
    INDEX1 = 'index_1'
    INDEX2 = 'index_2'
    ALL_READS = 'all'


class NanoporeAdaptiveMode(ExtendedEnum):
    NONE = 'None'
    OTHER = 'Other'
    ENRICH = 'Enrich'
    DEPLETE = 'Deplete'

class NanoporeLiveBaseCallingType(ExtendedEnum):
    FAST = 'fast'
    HIGH_ACCURACY = 'high accuracy'
    SUPER_HIGH_ACCURACY = 'super high accuracy'
    METHYLATION = 'methylation'
    OTHER = 'other'
    NONE = 'None'


class ObjectState(ExtendedEnum):
    INCOMING = 'INCOMING'
    INITIALIZED = 'INITIALIZED'
    NEW = 'NEW'
    REGISTERED = 'REGISTERED'
    TEMPLATE = 'TEMPLATE'

def looks_like_windows_path(absolute_path: str) -> bool:
    """
    checks if the given path looks like a windows path
    """
    return '\\' in absolute_path

def is_running_on_windows() -> bool:
    import platform
    return platform.system() == "Windows"

def convert_to_unix_path(absolute_path: str, prefix: str | None = None) -> Path:
    """
    turns a windows path into a unix path, optionally adding the given prefix
    eg convert_to_unix_path("c:\\Users\\foo\\bar", prefix="/cygwin") -> Path("/cygwin/c/users/foo/bar")

    Note that unix path are left untouched (even the prefix is ignored)

    :param absolute_path: a valid absolute windows path
    :param prefix: a prefix to add to the path, must start with a '/'
    """
    if looks_like_windows_path(absolute_path):
        from pathlib import PureWindowsPath, PurePosixPath
        path: PureWindowsPath = PureWindowsPath(absolute_path)
        drive_letter = path.parts[0].lower().replace(':', '').replace('\\', '')
        if prefix and not prefix.startswith('/'):
            raise ValueError(f"prefix must start with a '/': {prefix}")
        elif prefix:
            return Path(prefix, drive_letter, *path.parts[1:])
        else:
            return Path("/", drive_letter, *path.parts[1:])

    return Path(absolute_path)


def find_owner(path: Path|str) -> str:
    """gets the owner name of a file or dir_path"""
    if isinstance(path, str):
        path = Path(path)
    return getpwuid(path.stat().st_uid).pw_name


def rsync(source: str, dest: str, safe: bool = True):
    logger.info(f"rsync {source} to {dest}")
    if safe:
        cmd = ["rsync", "--safe-links", "--one-file-system", "-r", source, dest]
    else:
        cmd = ["rsync", "--copy-links", "-r", source, dest]
    logger.info("%s", " ".join(cmd))
    process = Popen(cmd, stdout=PIPE, stderr=PIPE)
    std_out, std_err = process.communicate()
    if process.returncode != 0:
        logger.debug(std_out)
        logger.exception(std_err)
        raise Exception(f"Failed to rsync {source} -> {dest}")


def is_tool(name):
    """Check whether `name` is on PATH and marked as executable."""

    # from whichcraft import which
    from shutil import which

    return which(name) is not None


def copy_dir(src: str, dst: str):
    if is_tool('rsync'):
        rsync(source=src + "/", dest=dst)  # we make sure source as a trailing '/'
    else:
        logger.info(f"rsync not avail => python copy_tree {src} to {dst}")
        copytree(src=src, dst=dst)


def send_email(subj:str, mess: str, recipients: List[str], sender: str):
    (mail_host, mail_smtp, admin_name, admin_email) = get_mailing_details()
    msg = EmailMessage()
    msg.set_content(mess)

    to = ', '.join(recipients)
    msg['Subject'] = subj
    msg['From'] = sender
    msg['To'] = to
    # Send the message
    s = smtplib.SMTP(mail_smtp)
    s.send_message(msg)
    logger.info(f"Sending email to {to}: {subj}")
    s.quit()


def send_email_from_admin(subj:str, mess: str, cc_recipients: List[str] | None = None, include_admin=False):
    """
    send email to recipients using the configured admin as sender
    if recipients is empty, send the email to configured admin; else admin is not cced
    """
    (mail_host, mail_smtp, admin_name, admin_email) = get_mailing_details()
    if not cc_recipients:
        cc_recipients = [admin_email]
    if include_admin and admin_email not in cc_recipients:
        cc_recipients.append(admin_email)
    if admin_email in cc_recipients:
        subj = subj + f" [host: {socket.gethostname()}]"
    send_email(subj=subj, mess=mess, recipients=cc_recipients, sender=[admin_email])


def mail_admin_error(subj:str, mess: str, cc_recipients: List[str] | None = None):
    """
    mail an error message to admin and cc_recipients if any.
    """
    (mail_host, mail_smtp, admin_name, admin_email) = get_mailing_details()

    mess=f"""
        Dear LabID {admin_name},
        
        The following issue occurred:
        
        {mess}

        Action from your part is most likely needed.
        NB: Email sent from host : {socket.gethostname()}
        
        LabID WatchDog
        """
    if not cc_recipients:
        cc_recipients = [admin_email]
    if admin_email not in cc_recipients:
        cc_recipients.append(admin_email)
        cc_recipients.reverse()

    send_email(subj=subj, mess=mess, recipients=cc_recipients, sender=[admin_email])

def is_less_than_a_week(a_date: datetime):
    now = datetime.now()
    delta = now - a_date
    return delta.days <= 7

def older_than_x_days(a_date: datetime, x: int):
    now = datetime.now()
    delta = now - a_date
    return delta.days > x

def older_than_x_hours(a_date: datetime, x: int):
    now = datetime.now()
    delta = now - a_date
    delta_hours = delta.days * 24 + delta.seconds // 3600
    return delta_hours >= x

def is_within_labid_data_library(path: str|Path) -> bool:
    # TODO: we should improve this with a query to server...
    return bool( re.match(r".*/(STOCKS|LABID)/Data/(Assay|Other)/.*", str(path), re.IGNORECASE) )

def is_uuid(value: Union[str, int, UUID]) -> bool:
    try:
        UUID(value)
        return True
    except (ValueError, AttributeError):  # str or int, but not uuid
        return False

def longest_common_prefix(words: Iterable[str]) -> str :
    """
    Finds the longest common prefix among a set of words
    :return: the prefix or empty string if there is no common prefix
    """
    if not words:
        return ''

    words.sort()
    prefix = []
    for i in range(len(words[0])):
        char = words[0][i]
        for j in words:
            if j[i] != char:
                return ''.join(prefix)
        prefix.append(char)

    return ''.join(prefix)

def get_checksum_dir(dir: str | Path, checksum_type: str = "md5"):
    """
    Here we'll accumulate file descriptors and hash this.
    A proper implementation would be to hash each file recursively, and compute a md5sum from all these
    file's md5. But this may reveal extremely expensive
    """
    # a list to accumulate strings like <file_name><size_in_byte>
    files: List[str] = list()
    for dirpath, dirnames, filenames in os.walk(dir):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            rel_path = os.path.relpath(dirpath, str(dir))
            # skip if it is symbolic link
            if not os.path.islink(fp):
                size = os.path.getsize(fp)
                files.append(f"{os.path.join(rel_path, f)}:" + str(size))

    total_string = "\n".join(sorted(files))
    logger.debug(total_string)

    if checksum_type == "md5":
        return hashlib.md5(total_string.encode("utf-8")).hexdigest()
    raise NotImplementedError(f"unsupported checksum_type : {checksum_type}")

def get_creation_time_range(dir: str | Path) -> Tuple:
    """
    Each file is scanned for last modif time and we return [oldest, youngest] datetime for the whole dir
    """
    # a list to accumulate strings like <file_name><size_in_byte>
    oldest: datetime = datetime.now()
    youngest: datetime = None
    for dirpath, dirnames, filenames in os.walk(dir):
        for f in filenames:
            fp = Path(dirpath, f)
            # skip if it is symbolic link
            if not fp.is_symlink():
                dt = datetime.fromtimestamp( fp.stat().st_mtime )
                if dt < oldest:
                    oldest = dt
                if not youngest or dt > youngest:
                    youngest = dt

    return [oldest, youngest]


def slugify(value, allow_unicode=False):
    """
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize("NFKC", value)
    else:
        value = (
            unicodedata.normalize("NFKD", value)
            .encode("ascii", "ignore")
            .decode("ascii")
        )
    value = re.sub(r"[^\w\s-]", "", value.lower())
    return re.sub(r"[-\s]+", "-", value).strip("-_")