# -*- coding: utf-8 -*-
"""
The 'transfer' module of the CLI
"""
import getpass
import logging
import os
import re
from datetime import datetime
from pathlib import Path
from typing import List, Dict, Set, Tuple, Any
from urllib.parse import urljoin, urlencode

import typer

from cli import get_labid_server_user, get_default_stocks_api_url, get_dma_plugin_api_url, \
    is_dma_plugin_enabled, get_dma_plugin_scope
from cli.register import _lookup_instrument_by_uuid_code_or_name, _grab_previously_created_assay
from cli.utils import ObjectState, ExtendedEnum, is_within_labid_data_library, Technology, ModelType, is_uuid, \
    convert_to_unix_path, is_running_on_windows
from stocks.models import Assay, InstrumentRun, Instrument, StorageVolume, UserGroup, GenericAssay, \
    SequencingAssay, VolumeEMAssay, TransmissionEMAssay, NanoporeAssay, LightMicroscopyScreenAssay, \
    LightMicroscopyAssay, AnnotationType, SimpleInstrumentRun, InstrumentModel, AvitiSequencingAssay, StocksCVTerm, \
    StocksCVCategory
from stocksapi.client import StocksClient
from stocksapi.exceptions import AuthenticationError
from stocksapi.manager import StocksManager

logger = logging.getLogger(__name__)

# name of this module (as appearing on the command line) is the last part of the __name__ e.g. cli.config -> config
_MODULE_NAME = __name__.rsplit(".", 1)[-1]
# list of command names offered in this module
_CMD_TRANSFER_ASSAY = "assay"
_CMD_TRANSFER_TEM_ASSAY = "tem"
_CMD_TRANSFER_VEM_ASSAY = "vem"
_CMD_TRANSFER_AVITI_ASSAY = "aviti"
_CMD_TRANSFER_ILLUMINA_ASSAY = "illumina"
_CMD_TRANSFER_NANOPORE_ASSAY = "nanopore"
_CMD_TRANSFER_MICROSCOPY_SCREEN_ASSAY = "hcs"
_CMD_TRANSFER_MICROSCOPY_ASSAY = "lm"
_CMD_TRANSFER_GENERIC_ASSAY = "generic"


class TransferMethod(ExtendedEnum):
    RSYNC = 'rsync'
    DMA = 'dma'


def _validate_transfer_method(transfer_method: TransferMethod):
    """
    check availability of the transfer method
    raise typer.BadParameter if transfer method is not avail / not configured properly
    """
    if transfer_method == TransferMethod.DMA:
        if not is_dma_plugin_enabled():
            raise typer.BadParameter(
                message="The DMA transfer method is not available for your installation. Please contact your admin.")
        elif not get_dma_plugin_api_url():
            raise typer.BadParameter(
                message="The DMA connection URL is not globally set for your installation. Please contact your admin "
                        "or use the global --dma-url option.")
        else:
            logger.info(f"Will use DMA transfer method with URL: {get_dma_plugin_api_url()}")
    else:
        raise typer.BadParameter(message=f"Transfer method not yet supported: {transfer_method.value}")


class AssayType(ExtendedEnum):
    AVITI = 'AVITI'
    GENERIC = 'Generic'
    ILLUMINA = 'Illumina'
    LM = 'LightMicroscopy'
    LMS = 'LightMicroscopyScreen'
    NANOPORE = 'Nanopore'
    TEM = 'TransmissionEM'
    VEM = 'VolumeEM'


# TODO: cant this be done by looping over the Assay classes and getting their techno / platform?
assay_type_technology_map: Dict[AssayType, Technology] = \
    {
        AssayType.AVITI: Technology.SEQUENCING,
        AssayType.ILLUMINA: Technology.SEQUENCING,
        AssayType.NANOPORE: Technology.SEQUENCING,
        AssayType.GENERIC: Technology.OTHER,
        AssayType.TEM: Technology.ELECTRON_MICROSCOPY,
        AssayType.VEM: Technology.ELECTRON_MICROSCOPY,
        AssayType.LM: Technology.LIGHT_MICROSCOPY,
        AssayType.LMS: Technology.LIGHT_MICROSCOPY
    }

assay_type_model_type_map: Dict[AssayType, ModelType] = \
    {
        AssayType.AVITI: ModelType.AVITISEQUENCINGASSAY,
        AssayType.ILLUMINA: ModelType.NGSILLUMINAASSAY,
        AssayType.NANOPORE: ModelType.NANOPOREASSAY,
        AssayType.GENERIC: ModelType.GENERICASSAY,
        AssayType.TEM: ModelType.TRANSMISSIONEMASSAY,
        AssayType.VEM: ModelType.VOLUMEEMASSAY,
        AssayType.LM: ModelType.LIGHTMICROSCOPYASSAY,
        AssayType.LMS: ModelType.LIGHTMICROSCOPYSCREENASSAY
    }
# create the CLI app
app = typer.Typer()


def _create_or_reuse_assay_from_type(stocks_manager: StocksManager,
                                     assay_type: AssayType,
                                     assay_name: str,
                                     instrument_run: InstrumentRun,
                                     assay_run_dir_path: Path,
                                     group: UserGroup,
                                     assay_state: ObjectState = ObjectState.INCOMING,
                                     **kwargs):
    """
    Creates the instrument run and an assay of type 'assay_type' if not existing.
    To prevent the multiplication of orphan objects i.e. the method first attempts at re-using :
    - an assay with same name, state and run_dir
    - an instrument run if a run with the same name, the same instrument and the same owner already exists
    """

    if assay_type == AssayType.GENERIC:
        assay = GenericAssay(name=assay_name,
                             technology=assay_type_technology_map[assay_type],
                             generic_assay_type="unspecified",
                             **kwargs)
    elif assay_type == AssayType.AVITI:
        assay = AvitiSequencingAssay(name=assay_name, flowcell="?", runtype=None, **kwargs)
    elif assay_type == AssayType.ILLUMINA:
        assay = SequencingAssay(name=assay_name, flowcell="?", runtype=None, **kwargs)
    elif assay_type == AssayType.NANOPORE:
        assay = NanoporeAssay(name=assay_name, flowcell="?", flowcell_version=None, run_mode="?", **kwargs)
    elif assay_type == AssayType.LM:
        assay = LightMicroscopyAssay(
            name=assay_name,
            imaging_method=[
                StocksCVTerm(
                    name="Other",
                    category=StocksCVCategory(
                        name=LightMicroscopyAssay.get_property_to_cvcategory()['imaging_method'])
                )],
            **kwargs)
    elif assay_type == AssayType.LMS:
        assay = LightMicroscopyScreenAssay(name=assay_name, **kwargs)
    elif assay_type == AssayType.VEM:
        assay = VolumeEMAssay(name=assay_name, **kwargs)
    elif assay_type == AssayType.TEM:
        assay = TransmissionEMAssay(name=assay_name, **kwargs)
    else:
        raise NotImplementedError(f"Unsupported assay type: {assay_type}")

    assay.state = assay_state
    assay.run_dir = assay_run_dir_path
    instrument_run.owned_by = group.name
    assay.owned_by = group.name

    # is user trying to transfer same dir after an error?
    # look for candidate_assay with same name and state
    # note that run dir won't match between 2 subsequent calls as we use the current time in the run dir name
    candidate_assay: Assay = _grab_previously_created_assay(
        assay=assay, stocks_manager=stocks_manager, state=assay_state, check_run_dir_path=False)

    existing_assay: Assay | None = candidate_assay
    if candidate_assay:
        # if the user executed the same command in a short time interval, it could be that previous successful call
        # is still in progress; therefore candidate_assay is in incoming state as the DMA is still transferring
        # i.e. the callback has not been executed yet. In this case, candidate_assay must NOT be re-used.
        # We can check if a note reflecting a previous DMA call is present in the candidate_assay notes to check this.
        for note in stocks_manager.list_notes(item_id=candidate_assay.id):
            if str(candidate_assay.run_dir) in note.content:
                # a note mentions the candidate_assay's run_dir => a DMA transfer is ongoing, we do NOT reuse this assay
                existing_assay = None
                logger.info(f"Will not re-use {assay_state.value} assay {candidate_assay.name} [{candidate_assay.id}]"
                            f" as a DMA transfer is ongoing")
                break
        if existing_assay:
            # If re-using the assay => its run_dir must be patched to the new one
            logger.info(f"Setting rundir={assay_run_dir_path}, state={assay_state.value} to assay {existing_assay.id}")
            stocks_manager.set_run_dir_to_assay(
                assay_id=existing_assay.id, run_dir=assay_run_dir_path, new_state=assay_state)
    else:
        existing_assay = None

    existing_run: SimpleInstrumentRun = None
    if not existing_assay or existing_assay.stocks_model_type != assay.stocks_model_type:
        # create or fetch existing run
        existing_runs: List[InstrumentRun] = stocks_manager.list_instrument_runs(
            name=instrument_run.name, instrument_name=instrument_run.instrument.name,
            owner=stocks_manager.client.username)
        if existing_runs and len(existing_runs) > 0:
            existing_run = existing_runs[0]
        else:
            logger.debug("creating new run")
            existing_run = stocks_manager.save_instrument_run(
                name=instrument_run.name, instrument=instrument_run.instrument,
                description=instrument_run.description,
                start_datetime=instrument_run.start_datetime, end_datetime=instrument_run.end_datetime,
                producer=instrument_run.producer
            )
        logger.debug(assay.as_simple_json())
        assay.id = stocks_manager.save_assay(assay=assay, instrument_run=existing_run)
    else:
        assay = existing_assay

    return [assay.id, assay.stocks_model_type]


def _create_assay_change_post_action_url(stocks_manager: StocksManager, assay_id: str,
                                         new_state: ObjectState = ObjectState.INITIALIZED):
    # assays/<assay-id>/callback/?status = INITIALIZED &...
    q_params = {
        "state": new_state.value.lower(),
        "service": "DMA",
        "service_job_id_param": "action_id"
    }
    # export q_params to a query string
    end_point = f"assays/{assay_id}/callback/?{urlencode(q_params)}"
    return urljoin(stocks_manager.client.url, end_point)


TRANSFER_HELP = ("Transfers a directory containing primary data generated by a sequencing, microscopy or other type "
                 "of assay into the LabID data repository structure of your group e.g. under"
                 " 'LabID_repo_path/Data/Assay/light_microscopy/2024/<new_assay_run_dir>' "
                 "where <new_assay_run_dir> is a newly created assay run directory which name follows LabID internal "
                 "naming convention. "
                 "The transfer command is relevant to transfer data from a local storage where the primary "
                 "data has been saved by the instrument (i.e. sequencer, microscope...) to LabID. "
                 "Importantly, the transfer method must have permission to write to the LabID data repository and "
                 "should therefore be executed with the relevant user or call a third party service having required "
                 "privileges. In absence of relevant third party service custom to your infrastructure, this command is"
                 " therefore reserved to administrators able to execute the command as the user managing the LabID "
                 "repository. "
                 "Before starting the data transfer, the transfer command will create an assay of the relevant type ("
                 "and associated instrument run/session) in LabID and preset its run directory to the transferred data"
                 " (i.e. '<new_assay_run_dir>'). Alternatively, an existing assay can be used provided that its state"
                 " is NEW or REGISTERED."
                 "At creation, the assay is assigned the INCOMING state. This state will switch to INITIALISED after "
                 "data transfer has successfully completed, and you will then be able to complete the data "
                 "registration using the data import wizard available in the LabID UI (you will be notified by "
                 "email at the end of the data transfer process i.e. when the assay's state switch happens). "
                 "A similar approach is taken when transferring to an existing assay witch state is NEW i.e. the assay"
                 "state is switched to INCOMING and its run directory is set to the transfer location. "
                 "When transferring data to an assay in the REGISTERED state, the transfer destination will be forced "
                 "to a dropbox to avoid overwriting registered datasets present in the assay run directory."
                 "Importantly, the transfer command will not delete the source directory after transfer unless "
                 "--delete-original is used (and supported by the transfer method). "
                 "If you belong to multiple groups, the command will let you select the relevant group destination."
                 )


@app.command(_CMD_TRANSFER_ASSAY,
             help=TRANSFER_HELP,
             short_help="Transfer an assay directory to LabID data repository using specified transfer method")
def transfer_assay_dir(
        input_dir_path: str = typer.Option(
            ...,
            "--dir",
            "-d",
            help="Absolute path to the directory to transfer."
        ),
        hostname: str = typer.Option(
            "select",
            "--host",
            "-h",
            help="Host name where the dir is located. This must be a host known/supported by the transfer method."
                 "If 'localhost', the dir must be on the local machine. "
                 "If 'select', the command will let you choose the host. "
        ),
        username: str = typer.Option(
            str(getpass.getuser()),
            "--user",
            "-u",
            prompt="Username to use?",
        ),
        password: str = typer.Option(
            ...,
            "--password",
            "-p",
            prompt="Password to use?",
            hide_input=True,
        ),
        assay_id: str = typer.Option(
            None,
            "--assay",
            "-a",
            help="The uuid of an existing assay in NEW or REGISTERED state. "
                 "If the assay is in the registered state, the transfer can only happen to a dropbox; to register "
                 "the data after successful transfer, please use the 'Add data' button available on the assay detail"
                 " page."
        ),
        assay_type: AssayType = typer.Option(
            None,
            "--assay-type",
            "-t",
            case_sensitive=False,
            help=f"The assay type of the transferred data; one of {AssayType.list()}. "
                 f"Mandatory if --assay is not given."
        ),
        instrument_key: str = typer.Option(
            None,
            "--instrument",
            "-i",
            help="Name, code or UUID of an existing instrument (e.g. sequencer, microscope...) used to generate the "
                 "assay. Optional when either --assay or --instrument-type is given."
        ),
        instrument_type: str = typer.Option(
            None,
            "--instrument-type",
            "-x",
            help="The type of the instrument used to generate the data (e.g. sequencer, microscope...). "
                 "Ignored if --instrument or --assay are provided."
        ),
        transfer_method: TransferMethod = typer.Option(
            TransferMethod.DMA.value,
            "--transfer-method",
            case_sensitive=False,
            help=f"The transfer method to copy the assay directory; one of {TransferMethod.list()}"
        ),
        delete_original: bool = typer.Option(
            False,
            "--delete-original / --keep-original",
            help="Should the source dir (--dir) be deleted after transfer?"
        ),
        url: str = typer.Option(
            get_default_stocks_api_url(),
            "--url",
            help="LabID server URL")
) -> None:
    #
    # check params concerning assay are consistent
    #
    if not assay_id:
        if not assay_type:
            raise typer.BadParameter(
                message="You must provide an assay uuid (--assay) or type (--assay-type)")
        if not instrument_key and not instrument_type:
            raise typer.BadParameter(
                message="You must provide an instrument key (--instrument) or type (--instrument-type)")
    elif not is_uuid(assay_id):
        logger.error(f"Invalid assay uuid: '{assay_id}'")
        raise typer.BadParameter(
            message=f"Invalid assay uuid: {assay_id}", param_hint="--assay")

    ###
    # check method availability
    ###
    _validate_transfer_method(transfer_method)

    if transfer_method == TransferMethod.DMA and not is_running_on_windows():
        # path may be a windows path, convert it to unix path with the usual DMA prefix
        dir = convert_to_unix_path(input_dir_path, "/cygdrive")
    else:
        dir = Path(input_dir_path)

    ###
    # check input dir is not in LabID storage
    ###

    # we want only abspath to avoid path issue with mounted volumes
    if not dir.is_absolute():
        raise typer.BadParameter(
            message=f"Please provide an absolute path to the directory to transfer: {input_dir_path}",
            param_hint="--dir")
    # we don't want any weird name in the directory name to avoid problematic path in the labid storage
    if re.search(r"[^a-zA-Z0-9_-]", dir.name):
        raise typer.BadParameter(
            message=f"Invalid directory name: '{dir.name}'. "
                    f"Please avoid special characters or spaces in the directory name: "
                    f"use only letters, numbers, '_' & '-' ",
            param_hint="--dir")

    ###
    # LabID connection check
    ###
    labid_client: StocksClient = StocksClient(url=url, username=username)
    try:
        labid_client.authenticate(password)
    except AuthenticationError as ae:
        raise typer.BadParameter(message=f"Could not connect to LabID: {ae}")

    stocks_manager: StocksManager = StocksManager(labid_client)
    print(f"Connection to LabID ({url}): OK")

    # if the given path is reachable from the local machine, we can use localhost as the hostname
    try:
        if hostname.lower() == "select" and dir.exists():
            hostname = "localhost"
    except FileNotFoundError:
        pass

    if hostname.lower() == "select" or hostname.lower() != "localhost":
        hostname = validate_hostname_and_path(transfer_method, dir, username, password, machine_name=hostname,
                                              include_localhost=False)

    if hostname.lower() == "localhost":
        if not dir.exists():
            raise typer.BadParameter(message=f"Path does not exist: {str(dir)}", param_hint="root-dir")

        if is_within_labid_data_library(dir):
            raise typer.BadParameter(message=f"The directory to transfer ({dir}) must be outside the LabID Data"
                                             f" Library.", param_hint="root-dir")

    ####
    # assemble the destination run_dir
    # ie <LABID_ROOT>/Data/Assay/<technology>/<year>/<run_dir_name>/
    ####

    # init assay if --assay was provided
    assay: Assay | None = None
    instrument: Instrument | InstrumentModel
    technology: Technology
    if assay_id:
        assay: Assay = stocks_manager.fetch_assay(uuid=assay_id)
        # we only allow transfer to assay in NEW oe REGISTERED states ie we forbid the INCOMING or INITIALISED states
        if assay.state != ObjectState.REGISTERED and assay.state != ObjectState.NEW:
            raise typer.BadParameter(f"The provided assay (id={assay_id}) is in the '{assay.state.value}' state. "
                                     f"Transfer is only allowed for assays in the 'NEW' or 'REGISTERED' states.",
                                     param_hint="--assay")

        # init technology and assay_type from assay
        technology = assay.technology
        for _assay_type, _techno in assay_type_technology_map.items():
            if _techno == technology:
                assay_type = _assay_type
                break
        if assay.instrumentrun:
            instrument = assay.instrumentrun.instrument
        else:
            instrument = assay.instrumentmodel

    else:
        technology = assay_type_technology_map.get(assay_type)
        if instrument_key:
            # get instrument, this succeeds or raise a typer.BadParameter
            instrument = _lookup_instrument_by_uuid_code_or_name(stocks_manager=stocks_manager,
                                                                 instrument_key=instrument_key,
                                                                 technology=technology)
        elif instrument_type:
            instruments: List[Instrument] = stocks_manager.list_instruments_for_type(
                model_type=instrument_type,
                technology=technology if (technology != technology.NA and technology != technology.OTHER) else None
            )
            if len(instruments) == 0:
                raise typer.BadParameter(
                    message=f"No instrument of type {instrument_type} found for technology {technology}")

            # ask the user which one to use
            print(f"Available {instrument_type} (please contact your admin if you don't see the expected instrument):")
            default_idx = None
            for idx, ins in enumerate(instruments):
                print(f"   ({idx}): {ins.name}")
            idx_choice = typer.prompt(text="Select the instrument used to generate the data")
            try:
                idx_choice = int(idx_choice)
            except ValueError:
                raise typer.BadParameter(f"Invalid choice ({idx_choice}): you must enter an integer number!")

            instrument = instruments[idx_choice]
        else:
            raise typer.BadParameter(
                message="You must provide an instrument key (--instrument) or type (--instrument-type)")

    ###
    # init transfer
    ###
    if transfer_method == TransferMethod.DMA:
        transfer_with_dma(hostname=hostname, dir_to_transfer=dir, delete_original=delete_original,
                          assay_type=assay_type, instrument=instrument, technology=technology,
                          stocks_manager=stocks_manager, url=url, username=username, password=password,
                          assay=assay)
    else:
        print(f"Unsupported method:{transfer_method.value}")


def validate_hostname_and_path(transfer_method: str, dir_to_transfer: Path, username: str, password: str,
                               machine_name: str | None = None, include_localhost: bool = False,
                               verbose: bool = False) -> str:
    """
    Selects the remote host where the data is located.
    @param transfer_method: The transfer method to use.
    @param dir_to_transfer: Path of the directory to transfer, can be a remote path.
    @param username: The username to use for the transfer.
    @param password: The password to use for the transfer.
    @param machine_name: The name of the remote machine, when given on the command line ; can be 'select', None
    or a remote machine name
    @param include_localhost: If True, include 'localhost' as a possible choice.
    @param verbose: If True, print verbose output.

    """
    if machine_name == "localhost":
        return "localhost"

    if transfer_method != TransferMethod.DMA:
        raise typer.BadParameter(f"Unsupported transfer method: {transfer_method}")

    from dma_client import RESTClient, ClientUnathorized, ClientException
    from dma_client.models import Volume, Machine

    # get client and authenticate
    dma_client: RESTClient
    try:
        dma_client = RESTClient(get_dma_plugin_api_url())
        dma_client.authenticate(username, password, scope=get_dma_plugin_scope())
        if verbose:
            print(f"Connection to DMA [{get_dma_plugin_api_url()}]: OK")

        dma_machines: List[Machine] = dma_client.list_remote_machines()
        if not dma_machines or len(dma_machines) == 0:
            raise typer.BadParameter(message=f"No remote machines found, please contact the DMA admins in ITS")

        # if a machine name was provided, validate it
        machine: Machine | None = None
        if machine_name and machine_name != "select":
            for m in dma_machines:
                if m.name == machine_name:
                    machine = m
                    break
            if not machine:
                raise typer.BadParameter(message=f"Machine {machine_name} is not a valid remote machines")
        else:
            # else we ask the user
            print(f"Available transfer locations (please contact your admin if you don't see the expected ones):")
            localhost_idx = None
            for idx, machin in enumerate(dma_machines):
                print(f"   ({idx}): [remote] {machin.name} ({machin.user})")
            # add localhost
            if include_localhost:
                idx = idx + 1
                localhost_idx = idx
                print(f"   ({idx}): [local] localhost")

            idx_choice = typer.prompt(text="Select the host where the data is located")
            try:
                idx_choice = int(idx_choice)
            except ValueError:
                raise typer.BadParameter(f"Invalid choice ({idx_choice}): you must enter an integer number!")
            if localhost_idx and idx_choice == localhost_idx:
                return "localhost"

            # a remote host was selected, validate dir path
            machine = dma_machines[idx_choice]
            logger.debug(f"Selected machine: {machine.name}")
            remote_volumes: List[Volume] = dma_client.list_volumes_for_remote_machine(machine_id=machine.id)
            if not remote_volumes or len(remote_volumes) == 0:
                raise typer.BadParameter(message=f"No accessible volume found for machine {machine.name}, "
                                                 f"please contact the DMA admins in ITS")
            # we check that dir_to_transfer is a valid path on the remote machine
            is_valid_path: bool = False
            # iterate over remote_volumes to check if dir_to_transfer is a valid path on the remote machine
            for v in remote_volumes:
                logger.debug(f"Valid remote path for {machine.name}: {str(v.path)}")
                if str(dir_to_transfer).startswith(str(v.path)):
                    is_valid_path = True
                    break
            if not is_valid_path:
                raise typer.BadParameter(
                    message=f"Path {str(dir_to_transfer)} is not a valid path on machine {machine.name}.{os.linesep}"
                            f"Allowed root paths are:"
                            f" {os.linesep}{os.linesep.join([str(v.path) for v in remote_volumes])}")

            return machine.name

    except ClientUnathorized as cu:
        raise typer.BadParameter(message=f"{username} not connect to the DMA ({get_dma_plugin_api_url()})"
                                         f" with scope {get_dma_plugin_scope()}: {cu}")
    except ClientException as ce:
        raise typer.BadParameter(message=f"Could not connect to the DMA with URL: {get_dma_plugin_api_url()}")
    except IndexError:
        raise typer.BadParameter(f"Invalid choice ({idx_choice}): no machine at this index!")

def transfer_with_dma(hostname: str, dir_to_transfer: Path, assay_type: AssayType, technology: Technology,
                      instrument: Instrument | InstrumentModel, delete_original: bool,
                      stocks_manager: StocksManager, url: str, username: str, password: str,
                      assay: Assay | None = None, verbose: bool = True):
    """
    Transfers a directory to the LabID storage library matching provided technology using the DMA service and
    register an assay in incoming state.

    Parameters:
    hostname (str): The hostname where the directory is located.
    dir_to_transfer (Path): The directory to be transferred.
    assay_type (AssayType): The type of the assay to create.
    technology (Technology): The technology used for the assay.
    instrument (Instrument): The instrument used for the assay.
    delete_original (bool): If True, the original directory will be deleted after transfer.
    stocks_manager (StocksManager): The StocksManager instance to manage stocks.
    url (str): The URL of the LabID server.
    username (str): The username for authentication.
    password (str): The password for authentication.
    assay (Assay): The assay to use for the transfer. If None, a new assay will be created.
    verbose (bool): If True, print verbose output (using print() not the logger).

    Returns:
    None

    Raises:
    typer.BadParameter: if the directory is not found, the user is not allowed to transfer data on any volume,
    or DMA connection fails
    """

    # tech dir in labid library
    technology_dir_name: str = technology.value.replace(' ', '_')
    if technology == Technology.NA:
        technology_dir_name: str = Technology.OTHER.value.replace(' ', '_')
    # dma imports are concentrated here, so they are only exec if DMA is avail and used
    from dma_client import RESTClient, ClientUnathorized, ClientException, ClientActionError
    from dma_client.models import Volume, Collection
    # get client and authenticate
    dma_client: RESTClient
    # list of allowed DMA root path
    allowed_destination_root_paths: Set[str] = set()
    try:
        dma_client = RESTClient(get_dma_plugin_api_url())
        dma_client.authenticate(username, password, scope=get_dma_plugin_scope())
        dma_volumes: List[Volume] = dma_client.list_volumes()
        if verbose:
            print(f"Connection to DMA [{get_dma_plugin_api_url()}]: OK")
        if not dma_volumes or len(dma_volumes) == 0:
            raise typer.BadParameter(message=f"User {username} is not allowed to transfer data on any volume, "
                                             f"please contact the DMA admins in ITS")
        for v in dma_volumes:
            allowed_destination_root_paths.add(str(v.path))

    except ClientUnathorized as cu:
        raise typer.BadParameter(message=f"{username} not connect to the DMA ({get_dma_plugin_api_url()})"
                                         f" with scope {get_dma_plugin_scope()}: {cu}")
    except ClientException as ce:
        raise typer.BadParameter(message=f"Could not connect to the DMA with URL: {get_dma_plugin_api_url()}")

    # final run dir
    _exclude_dropboxes = True
    _only_dropboxes = False

    if assay and assay.state == ObjectState.REGISTERED:
        # we force transfer to a dropbox
        _exclude_dropboxes = False
        _only_dropboxes = True

    final_run_dir_path, storage_volume = _assemble_final_dir_from_user_choice(
        stocks_manager=stocks_manager,
        source_dir=dir_to_transfer,
        instrument_name=instrument.name,
        technology_dir_name=technology_dir_name,
        username=username,
        allowed_destination_root_paths=allowed_destination_root_paths,
        exclude_dropboxes=_exclude_dropboxes,
        only_dropboxes=_only_dropboxes
    )

    logger.debug(f"Will transfer {dir_to_transfer} to {final_run_dir_path} using DMA")

    dma_collection_id = None
    assay_id = None
    dma_annotation_content = None
    try:
        dma_collection_name: str = dir_to_transfer.name
        dma_collection: Collection = dma_client.register_collection(
            name=dma_collection_name,
            comment=f"Managed by LabID - Auto-registered from labid CLI {_CMD_TRANSFER_ASSAY} from host={hostname}")
        dma_collection_id = dma_collection.id
        logger.debug(f"DMA collection id {dma_collection_id} ")

        assay_model_type: ModelType
        if assay:
            assay_id = assay.id
            assay_model_type = assay.stocks_model_type

        ###
        # post action callback for third-party transfer services
        ###
        post_action_url: str = ""
        if hostname == "localhost":
            if not assay:
                ###
                # create or reuse assay
                ###
                end_time = datetime.fromtimestamp(dir_to_transfer.stat().st_mtime).strftime('%Y-%m-%d %H:%M:%S')
                name = f"{instrument.name} session {end_time}"
                instrument_run: InstrumentRun = _create_instrument_run(
                    name=f"{instrument.name} session {end_time}",
                    instrument=instrument,
                    technology=technology,
                    end_time=end_time)

                assay_id, assay_model_type = _create_or_reuse_assay_from_type(
                    stocks_manager=stocks_manager,
                    assay_type=assay_type,
                    assay_name=dir_to_transfer.name,
                    instrument_run=instrument_run,
                    assay_run_dir_path=final_run_dir_path,
                    group=storage_volume.owned_by,
                    description=f"Registered using labid CLI {_CMD_TRANSFER_ASSAY} from {str(dir_to_transfer)}"
                )
                assay_url = urljoin(url, assay_id)
                logger.info(f"{assay_type.value} assay successfully created in LabID: {assay_url}")
                post_action_url = _create_assay_change_post_action_url(stocks_manager, assay_id)
            elif assay.state == ObjectState.REGISTERED:
                # no callback if assay is
                post_action_url = _create_assay_change_post_action_url(stocks_manager, assay_id,
                                                                       new_state=ObjectState.REGISTERED)
            else:
                # rest assay in initialized state
                post_action_url = _create_assay_change_post_action_url(stocks_manager, assay_id)

            logger.debug(post_action_url)
            assay_id, action = transfer_with_dma_handover(
                dir_to_transfer=dir_to_transfer, final_run_dir_path=final_run_dir_path,
                assay_type=assay_type, assay_id=assay_id,
                delete_original=delete_original,
                dma_client=dma_client, dma_collection=dma_collection,
                stocks_manager=stocks_manager, callback_url=post_action_url)

            dma_annotation_content = {'id': action.id, 'type': 'action'}
        else:
            # we use pullover
            if not assay:
                ###
                # create or reuse assay
                ###
                # the session name should include the remote host name and the remote dir name
                # we still need to make it somehow unique in case users re-use the same dir name on the remote host
                # we can use the current day to make it unique while still avoiding creating many runs from different
                # attempts in the same day
                date_str = datetime.now().strftime('%Y_%m_%d')
                name = f"{instrument.name} {date_str} session {hostname} {dir_to_transfer.name}"
                instrument_run: InstrumentRun = _create_instrument_run(name=name, technology=technology,
                                                                       instrument=instrument)

                assay_id, assay_model_type = _create_or_reuse_assay_from_type(
                    stocks_manager=stocks_manager,
                    assay_type=assay_type,
                    assay_name=dir_to_transfer.name,
                    instrument_run=instrument_run,
                    assay_run_dir_path=final_run_dir_path,
                    group=storage_volume.owned_by,
                    description=f"Registered using labid CLI {_CMD_TRANSFER_ASSAY} "
                                f"from {hostname}:{str(dir_to_transfer)}"
                )
                assay_url = urljoin(url, assay_id)
                logger.info(f"{assay_type.value} assay successfully created in LabID: {assay_url}")
                post_action_url = _create_assay_change_post_action_url(stocks_manager, assay_id)
            elif assay.state == ObjectState.REGISTERED:
                # no callback if assay is
                post_action_url = _create_assay_change_post_action_url(stocks_manager, assay_id,
                                                                       new_state=ObjectState.REGISTERED)
            else:
                # rest assay in initialized state
                post_action_url = _create_assay_change_post_action_url(stocks_manager, assay_id)

            logger.debug(post_action_url)

            assay_id, data = transfer_with_dma_pullover(
                hostname=hostname, dir_to_transfer=dir_to_transfer, final_run_dir_path=final_run_dir_path,
                assay_type=assay_type, assay_id=assay_id,
                dma_client=dma_client, dma_collection=dma_collection, stocks_manager=stocks_manager,
                callback_url=post_action_url, verbose=verbose)

            dma_annotation_content = {'id': data.data_id, 'type': 'data'}

        dma_client.logout()
    except ClientActionError as e:
        raise ValueError(f"Could not register in DMA: {e}")

    # transfer has been init, we set the run dir of the assay for NEW assay
    if assay and assay.state == ObjectState.NEW:
        logging.warning(f"Setting assay run dir to {final_run_dir_path} and switching state to INCOMING")
        stocks_manager.set_run_dir_to_assay(
            assay_id=assay.id,
            run_dir=final_run_dir_path,
            new_state=ObjectState.INCOMING)

    # add DMA annotation to the assay
    if assay_id and dma_annotation_content:
        at: AnnotationType = stocks_manager.fetch_annotationtype_by_name(name="dmapp_annotation_object")
        # TODO : uncomment when DMA annotation format is correct
        # stocks_manager.save_annotation(item_type=ModelType.ASSAY, uuid=assay_id, annotation_type=at,
        #                               content=json.dumps(dma_annotation_content))


def transfer_with_dma_pullover(hostname: str, dir_to_transfer: Path, final_run_dir_path: Path,
                               assay_type: AssayType, assay_id: str, dma_client, dma_collection,
                               stocks_manager: StocksManager,
                               callback_url: str, verbose: bool = True) -> Tuple[str, Any]:
    from dma_client.models import Data

    ###
    # call third-party DMA transfer service
    ###
    handover_to: str = get_labid_server_user()
    if not handover_to:
        raise ValueError("The labid_user (unix user running the LabID server installation) parameter has not been "
                         "configured in this LabID CLI installation (labidcli.ini file), please set it or report "
                         "this issue to the LabID admins. DMA pullover is not possible until this issue is resolved.")
    if verbose:
        print(f"Requesting DMA pullover of {str(dir_to_transfer)} from {hostname} to {str(final_run_dir_path)}")

    result_data: Data = dma_client.request_pullover(
        target_path=final_run_dir_path,
        original_path=dir_to_transfer,
        original_machine=hostname,
        set_owner=handover_to,
        name=final_run_dir_path.name,
        collection_id=dma_collection.id,
        callback_url=callback_url
    )

    note_description = (f"Requested DMA pullover of {str(dir_to_transfer)} from {hostname} to {final_run_dir_path} "
                        f"(Data {result_data.data_id})")
    logger.info(note_description)
    stocks_manager.save_note(note_description, assay_type_model_type_map.get(assay_type).value, assay_id)

    message = (
        f"Transfer of {dir_to_transfer} to {final_run_dir_path} successfully requested to DMA "
        f"(Data {result_data.data_id}). "
        f"You'll get an email once the data transfer is completed. "
        f"At that moment, you should complete the data/assay registration in LabID using the 'Register' "
        f"button on the assay page."
    )
    if verbose:
        print(message)

    return assay_id, result_data


def transfer_with_dma_handover(dir_to_transfer: Path, final_run_dir_path: Path, assay_type: AssayType, assay_id: str,
                               delete_original: bool, dma_client, dma_collection, stocks_manager: StocksManager,
                               callback_url: str, verbose: bool = True) -> Tuple[str, Any]:
    """
    Transfers a directory to the indicated dir using the DMA service and register an assay in incoming state.

    Parameters:
    dir_to_transfer (Path): The directory to be transferred.
    final_run_dir_path (Path): The destination directory.
    assay_type (AssayType): The type of the assay to create.
    assay_id: the assay id to use
    delete_original (bool): If True, the original directory will be deleted after transfer.
    dma_client: The DMA client.
    dma_collection: The DMA collection.
    storage_volume (StorageVolume): The storage volume.
    stocks_manager (StocksManager): The StocksManager instance to manage stocks.
    url (str): The URL of the LabID server.
    verbose (bool): If True, print verbose output (using print() not the logger).

    Returns:
    Tuple[str, Any]: The assay ID and the DMA Action object representing the handover.
    """

    from dma_client.models import Data, Action

    ##
    # register data to use DMA handover
    ##
    dma_dataset: Data = None
    existing_data: List[Data] = dma_client.list_data(path_contains=str(dir_to_transfer))
    if existing_data:
        if len(existing_data) == 1:
            dma_dataset = existing_data[0]
        else:
            for d in existing_data:
                if d.path == dir_to_transfer:
                    dma_dataset = d
                    break
        logger.info(f"grabbed existing data from DMA: {dma_dataset}")
    else:
        dma_dataset = dma_client.register_data(
            path=str(dir_to_transfer),
            name=dir_to_transfer.name,
            collection_id=dma_collection.id,
            comment="Managed by LabID."
        )

    ###
    # call third-party DMA transfer service
    ###
    logger.debug(f"LabID user to handover: {get_labid_server_user()}")
    if verbose:
        print(f"Requesting DMA handover of {str(dir_to_transfer)} to {str(final_run_dir_path)} (delete_original={delete_original})")

    action: Action = dma_client.request_handover_labid(
        data_id=dma_dataset.data_id,
        copy_to=str(final_run_dir_path),
        comment=f"Handover requested by labid {_CMD_TRANSFER_ASSAY} with call back: {callback_url}",
        delete_original=delete_original,
        callback_url=callback_url
    )

    note_description = f"Requested DMA handover of {str(dir_to_transfer)} to {final_run_dir_path} (Action {action.id})"
    logger.info(note_description)
    stocks_manager.save_note(note_description, assay_type_model_type_map.get(assay_type).value, assay_id)

    message = (
        f"Transfer of {dir_to_transfer} to {final_run_dir_path} successfully requested to DMA (Action {action.id}). "
        f"You'll get an email once the data transfer is completed. "
        f"At that moment, you should complete the data/assay registration in LabID using the 'Register' or `Add Data` "
        f"button available on the detail page of {ObjectState.INITIALIZED.value} and {ObjectState.REGISTERED.value} "
        f"assays, respectively ."
    )
    if verbose:
        print(message)

    return assay_id, action


def _create_instrument_run(name: str, technology: Technology, instrument: Instrument, end_time: str = None
                           ) -> InstrumentRun:
    instrument_run: InstrumentRun = InstrumentRun(
        name=name,
        managed=False,
        technology=technology,
        instrument=instrument,
        platform=instrument.model.platform if instrument.model else "UNKNOWN",
        end_datetime=end_time if end_time else None
    )
    return instrument_run


def _assemble_final_dir_from_user_choice(
        stocks_manager: StocksManager,
        source_dir: Path,
        instrument_name: str,
        technology_dir_name: str,
        username: str,
        allowed_destination_root_paths: Set[str] | None = None,
        exclude_dropboxes: bool = True,
        only_dropboxes: bool = False
) -> Tuple[Path, StorageVolume]:
    """
    Assembles the final destination directory path for the assay run based on user choices and
    available storage volumes.

    Parameters:
    stocks_manager (StocksManager): The StocksManager instance to manage stocks.
    source_dir (Path): The source directory to be transferred.
    instrument_name (str): The name of the instrument used to generate the data.
    technology_dir_name (str): The directory name corresponding to the technology used.
    username (str): The username of the person initiating the transfer.
    allowed_destination_root_paths (Set[str] | None): A set of allowed root paths for the destination. If None, all paths are allowed.
    exclude_dropboxes (bool): If True, exclude dropbox volumes from the list of storage volumes.
    only_dropboxes (bool): If True, only include dropbox volumes in the list of storage volumes.

    Returns:
    Tuple[Path, StorageVolume]: A tuple containing the final directory path and the corresponding storage volume.

    Raises:
    BadParameter: If no valid storage volume is found for the transfer or user input is wrong.
    """

    # assemble run_dir_name
    # clip instrument name up to the first space or weird char (exclude []{}'"/\`~!@#$%^&*()_+;:<>?
    token: str = instrument_name.replace(' ', '_')
    match = re.search(r'^[\w-]+[\da-zA-Z]', token)  # make sure we end with a normal char
    if match:
        token = match.group()

    now = datetime.now()
    # we use also the %H%M%S to avoid collisions if the transfer needs to be re-executed
    run_dir_name = now.strftime('%Y-%m-%d-%H%M%S') + "_" + username + "_" + token

    # labid library root
    storage_volumes: List[StorageVolume] = stocks_manager.list_storage_volumes(
        username=username,
        only_primary_volumes=True,
        include_dropbox_volumes=exclude_dropboxes is False)
    if only_dropboxes:
        storage_volumes = [sv for sv in storage_volumes if sv.is_dropbox_volume]
    if allowed_destination_root_paths:
        allowed_storage_volumes: List[StorageVolume] = list()
        for sv in storage_volumes:
            logger.debug(str(sv.base_path))
            for root in allowed_destination_root_paths:
                logger.debug(f"...check against {root} ")
                if str(sv.base_path).lower().startswith(root.lower()):
                    logger.debug(f"...OK !")
                    allowed_storage_volumes.append(sv)
                    break
        storage_volumes = allowed_storage_volumes

    # sort the volumes with so the primary group ones come last
    storage_volumes.sort(key=lambda x: (
        getattr(getattr(x, 'owned_by'), 'is_primary_group'),
        getattr(getattr(x, 'owned_by'), 'name'),
        getattr(x, 'base_path')))
    storage_volume: StorageVolume = storage_volumes[0]
    if len(storage_volumes) > 1:
        # ask the user which one to use
        print("To which LabID storage volume should data be transferred?")
        default_idx = None
        for idx, sv in enumerate(storage_volumes):
            # we don't reset because the real SV comes usually first (dropbox is after since path is longer)
            if not default_idx and sv.owned_by.is_primary_group:
                default_idx = idx
                print(f"  *({idx}): {sv.name} [{sv.base_path}] ({sv.owned_by.name})")
            else:
                print(f"   ({idx}): {sv.name} [{sv.base_path}] ({sv.owned_by.name})")
        idx_choice = typer.prompt(
            text="Select data transfer destination (type number or enter for default)",
            default=default_idx)
        try:
            idx_choice = int(idx_choice)
        except ValueError:
            raise typer.BadParameter(f"Invalid choice ({idx_choice}): you must enter an integer number!")
        storage_volume: StorageVolume = storage_volumes[idx_choice]
    logger.debug(storage_volume.as_simple_json())
    if only_dropboxes:
        # since we transfer to dropbox, we do not create an extra dir in the dropbox
        final_run_dir_path: Path = Path(storage_volume.base_path, username)
    else:
        final_run_dir_path: Path = Path(storage_volume.base_path,
                                        "Data",
                                        "Assay",
                                        technology_dir_name,
                                        str(now.year),
                                        run_dir_name)
        # the run dir should not be already existing
        if final_run_dir_path.exists():
            raise typer.BadParameter(f"The destination Assay Run Directory already exists: {final_run_dir_path}.\n"
                                     f"You can not transfer '{source_dir.name}' twice to the same LabID data library "
                                     f"({storage_volume.base_path})")

    logger.debug(final_run_dir_path)

    return [final_run_dir_path, storage_volume]


@app.command(_CMD_TRANSFER_VEM_ASSAY,
             help=TRANSFER_HELP,
             short_help="Transfer a Volume Electron Microscopy session directory to LabID")
def transfer_vem_assay_dir(
        input_dir_path: str = typer.Option(
            ...,
            "--dir",
            "-d",
            help="Absolute path to the TEM session directory to transfer."
        ),
        hostname: str = typer.Option(
            "select",
            "--host",
            "-h",
            help="Host name where the dir is located. This must be a host known/supported by the transfer method."
                 "If 'localhost', the dir must be on the local machine. "
                 "If 'select', the command will let you choose the host. "
        ),
        username: str = typer.Option(
            str(getpass.getuser()),
            "--user",
            "-u",
            prompt="Username to use?",
        ),
        password: str = typer.Option(
            ...,
            "--password",
            "-p",
            prompt="Password to use?",
            hide_input=True,
        ),
        instrument_key: str = typer.Option(
            None,
            "--instrument",
            "-i",
            help="Name, code or UUID of an existing Electron Microscope"
        ),
        instrument_type: str = typer.Option(
            "microscope",
            "--instrument-type",
            "-x",
            help="The type of the instrument used to generate the data (e.g. sequencer, microscope...). "
                 "Ignored if --instrument is also provided."
        ),
        transfer_method: TransferMethod = typer.Option(
            TransferMethod.DMA.value,
            "--transfer-method",
            case_sensitive=False,
            help=f"The transfer method to copy the assay directory; one of {TransferMethod.list()}"
        ),
        delete_original: bool = typer.Option(
            False,
            "--delete-original / --keep-original",
            help="Should the source dir (--dir) be deleted after transfer?"
        ),
        url: str = typer.Option(
            get_default_stocks_api_url(),
            "--url",
            help="LabID server URL")
) -> None:
    transfer_assay_dir(
        input_dir_path=input_dir_path,
        hostname=hostname,
        username=username,
        password=password,
        instrument_key=instrument_key,
        instrument_type=instrument_type,
        assay_type=AssayType.VEM,
        assay_id=None,
        transfer_method=transfer_method,
        delete_original=delete_original,
        url=url
    )


@app.command(_CMD_TRANSFER_TEM_ASSAY,
             help=TRANSFER_HELP,
             short_help="Transfer a Transmission Electron Microscopy session directory to LabID")
def transfer_tem_assay_dir(
        input_dir_path: str = typer.Option(
            ...,
            "--dir",
            "-d",
            help="Absolute path to the TEM session directory to transfer."
        ),
        hostname: str = typer.Option(
            "select",
            "--host",
            "-h",
            help="Host name where the dir is located. This must be a host known/supported by the transfer method."
                 "If 'localhost', the dir must be on the local machine. "
                 "If 'select', the command will let you choose the host. "
        ),
        username: str = typer.Option(
            str(getpass.getuser()),
            "--user",
            "-u",
            prompt="Username to use?",
        ),
        password: str = typer.Option(
            ...,
            "--password",
            "-p",
            prompt="Password to use?",
            hide_input=True,
        ),
        instrument_key: str = typer.Option(
            None,
            "--instrument",
            "-i",
            help="Name, code or UUID of an existing Electron Microscope"
        ),
        instrument_type: str = typer.Option(
            "microscope",
            "--instrument-type",
            "-x",
            help="The type of the instrument used to generate the data (e.g. sequencer, microscope...). "
                 "Ignored if --instrument is also provided."
        ),
        transfer_method: TransferMethod = typer.Option(
            TransferMethod.DMA.value,
            "--transfer-method",
            case_sensitive=False,
            help=f"The transfer method to copy the assay directory; one of {TransferMethod.list()}"
        ),
        delete_original: bool = typer.Option(
            False,
            "--delete-original / --keep-original",
            help="Should the source dir (--dir) be deleted after transfer?"
        ),
        url: str = typer.Option(
            get_default_stocks_api_url(),
            "--url",
            help="LabID server URL")
) -> None:
    transfer_assay_dir(
        input_dir_path=input_dir_path,
        hostname=hostname,
        username=username,
        password=password,
        instrument_key=instrument_key,
        instrument_type=instrument_type,
        assay_type=AssayType.TEM,
        assay_id=None,
        transfer_method=transfer_method,
        delete_original=delete_original,
        url=url
    )


@app.command(_CMD_TRANSFER_NANOPORE_ASSAY,
             help=TRANSFER_HELP,
             short_help="Transfer a directory containing Nanopore data to LabID")
def transfer_nanopore_assay_dir(
        input_dir_path: str = typer.Option(
            ...,
            "--dir",
            "-d",
            help="Absolute path to the directory containing the sequencing data to transfer."
        ),
        hostname: str = typer.Option(
            "select",
            "--host",
            "-h",
            help="Host name where the dir is located. This must be a host known/supported by the transfer method."
                 "If 'localhost', the dir must be on the local machine. "
                 "If 'select', the command will let you choose the host. "
        ),
        username: str = typer.Option(
            str(getpass.getuser()),
            "--user",
            "-u",
            prompt="Username to use?",
        ),
        password: str = typer.Option(
            ...,
            "--password",
            "-p",
            prompt="Password to use?",
            hide_input=True,
        ),
        instrument_key: str = typer.Option(
            None,
            "--instrument",
            "-i",
            help="Name, code or UUID of an existing Sequencer"
        ),
        instrument_type: str = typer.Option(
            "sequencer",
            "--instrument-type",
            "-x",
            help="The type of the instrument used to generate the data (e.g. sequencer, microscope...). "
                 "Ignored if --instrument is also provided."
        ),
        transfer_method: TransferMethod = typer.Option(
            TransferMethod.DMA.value,
            "--transfer-method",
            case_sensitive=False,
            help=f"The transfer method to copy the assay directory; one of {TransferMethod.list()}"
        ),
        delete_original: bool = typer.Option(
            False,
            "--delete-original / --keep-original",
            help="Should the source dir (--dir) be deleted after transfer?"
        ),
        url: str = typer.Option(
            get_default_stocks_api_url(),
            "--url",
            help="LabID server URL")
) -> None:
    transfer_assay_dir(
        input_dir_path=input_dir_path,
        hostname=hostname,
        username=username,
        password=password,
        instrument_key=instrument_key,
        instrument_type=instrument_type,
        assay_type=AssayType.NANOPORE,
        assay_id=None,
        transfer_method=transfer_method,
        delete_original=delete_original,
        url=url
    )


@app.command(_CMD_TRANSFER_AVITI_ASSAY,
             help=TRANSFER_HELP,
             short_help="Transfer a directory containing Elements Bioscience AVITI data to LabID",
             no_args_is_help=True)
def transfer_aviti_assay_dir(
        input_dir_path: str = typer.Option(
            ...,
            "--dir",
            "-d",
            help="Absolute path to the directory containing the sequencing data to transfer."
        ),
        hostname: str = typer.Option(
            "localhost",
            "--host", "-h",
            help="Host name when the dir is not local."
        ),
        username: str = typer.Option(
            str(getpass.getuser()),
            "--user",
            "-u",
            prompt="Username to use?",
        ),
        password: str = typer.Option(
            ...,
            "--password",
            "-p",
            prompt="Password to use?",
            hide_input=True,
        ),
        instrument_key: str = typer.Option(
            None,
            "--instrument",
            "-i",
            help="Name, code or UUID of an existing Sequencer"
        ),
        instrument_type: str = typer.Option(
            "sequencer",
            "--instrument-type",
            "-x",
            help="The type of the instrument used to generate the data (e.g. sequencer, microscope...). "
                 "Ignored if --instrument is also provided."
        ),
        transfer_method: TransferMethod = typer.Option(
            TransferMethod.DMA.value,
            "--transfer-method",
            case_sensitive=False,
            help=f"The transfer method to copy the assay directory; one of {TransferMethod.list()}"
        ),
        delete_original: bool = typer.Option(
            False,
            "--delete-original / --keep-original",
            help="Should the source dir (--dir) be deleted after transfer?"
        ),
        url: str = typer.Option(
            get_default_stocks_api_url(),
            "--url",
            help="LabID server URL")
) -> None:
    transfer_assay_dir(
        input_dir_path=input_dir_path,
        hostname=hostname,
        username=username,
        password=password,
        instrument_key=instrument_key,
        instrument_type=instrument_type,
        assay_type=AssayType.AVITI,
        assay_id=None,
        transfer_method=transfer_method,
        delete_original=delete_original,
        url=url
    )


@app.command(_CMD_TRANSFER_ILLUMINA_ASSAY,
             help=TRANSFER_HELP,
             short_help="Transfer a directory containing Illumina data to LabID")
def transfer_illumina_assay_dir(
        input_dir_path: str = typer.Option(
            ...,
            "--dir",
            "-d",
            help="Absolute path to the directory containing the sequencing data to transfer."
        ),
        hostname: str = typer.Option(
            "select",
            "--host",
            "-h",
            help="Host name where the dir is located. This must be a host known/supported by the transfer method."
                 "If 'localhost', the dir must be on the local machine. "
                 "If 'select', the command will let you choose the host. "
        ),
        username: str = typer.Option(
            str(getpass.getuser()),
            "--user",
            "-u",
            prompt="Username to use?",
        ),
        password: str = typer.Option(
            ...,
            "--password",
            "-p",
            prompt="Password to use?",
            hide_input=True,
        ),
        instrument_key: str = typer.Option(
            None,
            "--instrument",
            "-i",
            help="Name, code or UUID of an existing Sequencer"
        ),
        instrument_type: str = typer.Option(
            "sequencer",
            "--instrument-type",
            "-x",
            help="The type of the instrument used to generate the data (e.g. sequencer, microscope...). "
                 "Ignored if --instrument is also provided."
        ),
        transfer_method: TransferMethod = typer.Option(
            TransferMethod.DMA.value,
            "--transfer-method",
            case_sensitive=False,
            help=f"The transfer method to copy the assay directory; one of {TransferMethod.list()}"
        ),
        delete_original: bool = typer.Option(
            False,
            "--delete-original / --keep-original",
            help="Should the source dir (--dir) be deleted after transfer?"
        ),
        url: str = typer.Option(
            get_default_stocks_api_url(),
            "--url",
            help="LabID server URL")
) -> None:
    transfer_assay_dir(
        input_dir_path=input_dir_path,
        hostname=hostname,
        username=username,
        password=password,
        instrument_key=instrument_key,
        instrument_type=instrument_type,
        assay_type=AssayType.ILLUMINA,
        assay_id=None,
        transfer_method=transfer_method,
        delete_original=delete_original,
        url=url
    )


@app.command(_CMD_TRANSFER_MICROSCOPY_ASSAY,
             help=TRANSFER_HELP,
             short_help="Transfer a directory containing microscopy data to LabID")
def transfer_lm_assay_dir(
        input_dir_path: str = typer.Option(
            ...,
            "--dir",
            "-d",
            help="Absolute path to the directory containing the microscopy data to transfer."
        ),
        hostname: str = typer.Option(
            "select",
            "--host",
            "-h",
            help="Host name where the dir is located. This must be a host known/supported by the transfer method."
                 "If 'localhost', the dir must be on the local machine. "
                 "If 'select', the command will let you choose the host. "
        ),
        username: str = typer.Option(
            str(getpass.getuser()),
            "--user",
            "-u",
            prompt="Username to use?",
        ),
        password: str = typer.Option(
            ...,
            "--password",
            "-p",
            prompt="Password to use?",
            hide_input=True,
        ),
        instrument_key: str = typer.Option(
            None,
            "--instrument",
            "-i",
            help="Name, code or UUID of an existing Sequencer"
        ),
        instrument_type: str = typer.Option(
            "microscope",
            "--instrument-type",
            "-x",
            help="The type of the instrument used to generate the data (e.g. sequencer, microscope...). "
                 "Ignored if --instrument is also provided."
        ),
        transfer_method: TransferMethod = typer.Option(
            TransferMethod.DMA.value,
            "--transfer-method",
            case_sensitive=False,
            help=f"The transfer method to copy the assay directory; one of {TransferMethod.list()}"
        ),
        delete_original: bool = typer.Option(
            False,
            "--delete-original / --keep-original",
            help="Should the source dir (--dir) be deleted after transfer?"
        ),
        url: str = typer.Option(
            get_default_stocks_api_url(),
            "--url",
            help="LabID server URL")
) -> None:
    transfer_assay_dir(
        input_dir_path=input_dir_path,
        hostname=hostname,
        username=username,
        password=password,
        instrument_key=instrument_key,
        instrument_type=instrument_type,
        assay_type=AssayType.LM,
        assay_id=None,
        transfer_method=transfer_method,
        delete_original=delete_original,
        url=url
    )


@app.command(_CMD_TRANSFER_MICROSCOPY_SCREEN_ASSAY,
             help=TRANSFER_HELP,
             short_help="Transfer a directory containing microscopy-based screen data to LabID")
def transfer_hcs_assay_dir(
        input_dir_path: str = typer.Option(
            ...,
            "--dir",
            "-d",
            help="Absolute path to the directory containing the microscopy data to transfer."
        ),
        hostname: str = typer.Option(
            "select",
            "--host",
            "-h",
            help="Host name where the dir is located. This must be a host known/supported by the transfer method."
                 "If 'localhost', the dir must be on the local machine. "
                 "If 'select', the command will let you choose the host. "
        ),
        username: str = typer.Option(
            str(getpass.getuser()),
            "--user",
            "-u",
            prompt="Username to use?",
        ),
        password: str = typer.Option(
            ...,
            "--password",
            "-p",
            prompt="Password to use?",
            hide_input=True,
        ),
        instrument_key: str = typer.Option(
            None,
            "--instrument",
            "-i",
            help="Name, code or UUID of an existing Sequencer"
        ),
        instrument_type: str = typer.Option(
            "microscope",
            "--instrument-type",
            "-x",
            help="The type of the instrument used to generate the data (e.g. sequencer, microscope...). "
                 "Ignored if --instrument is also provided."
        ),
        transfer_method: TransferMethod = typer.Option(
            TransferMethod.DMA.value,
            "--transfer-method",
            case_sensitive=False,
            help=f"The transfer method to copy the assay directory; one of {TransferMethod.list()}"
        ),
        delete_original: bool = typer.Option(
            False,
            "--delete-original / --keep-original",
            help="Should the source dir (--dir) be deleted after transfer?"
        ),
        url: str = typer.Option(
            get_default_stocks_api_url(),
            "--url",
            help="LabID server URL")
) -> None:
    transfer_assay_dir(
        input_dir_path=input_dir_path,
        hostname=hostname,
        username=username,
        password=password,
        instrument_key=instrument_key,
        instrument_type=instrument_type,
        assay_type=AssayType.LMS,
        assay_id=None,
        transfer_method=transfer_method,
        delete_original=delete_original,
        url=url
    )


@app.command(_CMD_TRANSFER_GENERIC_ASSAY,
             help=TRANSFER_HELP,
             short_help="Transfer a directory containing 'generic' assay data to LabID")
def transfer_generic_assay_dir(
        input_dir_path: str = typer.Option(
            ...,
            "--dir",
            "-d",
            help="Absolute path to the directory containing the assay's data to transfer."
        ),
        hostname: str = typer.Option(
            "select",
            "--host",
            "-h",
            help="Host name where the dir is located. This must be a host known/supported by the transfer method."
                 "If 'localhost', the dir must be on the local machine. "
                 "If 'select', the command will let you choose the host. "
        ),
        username: str = typer.Option(
            str(getpass.getuser()),
            "--user",
            "-u",
            prompt="Username to use?",
        ),
        password: str = typer.Option(
            ...,
            "--password",
            "-p",
            prompt="Password to use?",
            hide_input=True,
        ),
        instrument_key: str = typer.Option(
            None,
            "--instrument",
            "-i",
            help="Name, code or UUID of an existing instrument"
        ),
        instrument_type: str = typer.Option(
            None,
            "--instrument-type",
            "-x",
            help="The type of the instrument used to generate the data (e.g. sequencer, microscope...). "
                 "Ignored if --instrument is also provided."
        ),
        transfer_method: TransferMethod = typer.Option(
            TransferMethod.DMA.value,
            "--transfer-method",
            case_sensitive=False,
            help=f"The transfer method to copy the assay directory; one of {TransferMethod.list()}"
        ),
        delete_original: bool = typer.Option(
            False,
            "--delete-original / --keep-original",
            help="Should the source dir (--dir) be deleted after transfer?"
        ),
        url: str = typer.Option(
            get_default_stocks_api_url(),
            "--url",
            help="LabID server URL")
) -> None:
    transfer_assay_dir(
        input_dir_path=input_dir_path,
        hostname=hostname,
        username=username,
        password=password,
        instrument_key=instrument_key,
        instrument_type=instrument_type,
        assay_type=AssayType.GENERIC,
        assay_id=None,
        transfer_method=transfer_method,
        delete_original=delete_original,
        url=url
    )
