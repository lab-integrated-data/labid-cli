from datetime import datetime
from pathlib import Path
from typing import List

import typer
import logging

from cli import get_default_job_database_path
from cli.database import DatabaseHandler, JobStatus, Job, init_database

logger = logging.getLogger(__name__)

# name of this module (as appearing on the command line) is the last part of the __name__ eg cli.config -> config
_MODULE_NAME = __name__.rsplit(".", 1)[-1]

# list of command names offered in this module
_CMD_DELETE = "delete"
_CMD_LIST = "list"
_CMD_SHOW = "show"
_CMD_UPDATE = "update"
_CMD_WIPE = "wipedb"


# create the CLI app
app = typer.Typer()

@app.command(_CMD_WIPE, help="Wipe and re-init a db")
def wipe(
        db_name: str = typer.Option(
            ...,
            "--db-name",
            "-d",
            help="Name of a database; located in the default location ie the caller's home"
        ),
        backup: bool = typer.Option(
            False,
            "--backup",
            help="Should the existing db be backed up before re-init"
        ),
):
    db_path = get_default_job_database_path(db_name)
    logger.debug(db_path)
    if not db_path.exists():
        raise typer.BadParameter(f"no database found for name: {db_name} with expected location: {db_path}")
    init_database(db_path, force_new=True, backup_before_wipe=backup)


@app.command(_CMD_UPDATE, help="Update job status")
def delete_job(
        ids: List[str] = typer.Option(
            ...,
            "--id",
            "-i",
            help="The job id"
        ),
        status: JobStatus = typer.Option(
            ...,
            "--status",
            "-s",
            help="The job status"
        ),
        db_name: str = typer.Option(
            ...,
            "--db-name",
            "-d",
            help="Name of a database; located in the default location ie the caller's home"
        )
):
    db_path = get_default_job_database_path(db_name)
    db_handler: DatabaseHandler = DatabaseHandler(db_path)
    for id in ids:
        job: Job = db_handler.load_job(id)
        job.status = status
        job.message = f"Status manually set to {status.value} on {datetime.now()}"
        db_handler.update_job(job)

@app.command(_CMD_DELETE, help="Delete a job from database")
def delete_job(
        id: str = typer.Option(
            ...,
            "--id",
            "-i",
            help="The job id to delete"
        ),
        db_name: str = typer.Option(
            ...,
            "--db-name",
            "-d",
            help="Name of a database; located in the default location ie the caller's home"
        )
):
    db_path = get_default_job_database_path(db_name)
    db_handler: DatabaseHandler = DatabaseHandler(db_path)
    db_handler.delete_job(id)


@app.command(_CMD_SHOW, help="Print job details")
def job_detail(
        id: str = typer.Option(
            None,
            "--id",
            "-i",
            help="The job id to delete"
        ),
        rundir: Path = typer.Option(
            None,
            "--rundir",
            "-r",
            help="The assay run dir path"
        ),
        assay_id: str = typer.Option(
            None,
            "--assay",
            "-a",
            help="The assay UUID"
        ),
        db_name: str = typer.Option(
            ...,
            "--db-name",
            "-d",
            help="Name of a database; located in the default location ie the caller's home"
        )
):
    db_path = get_default_job_database_path(db_name)
    db_handler: DatabaseHandler = DatabaseHandler(db_path)
    j: Job = None
    if id:
        j = db_handler.load_job(id)
    elif assay_id:
        j = db_handler.get_job_for_assay_id(assay_id)
    elif rundir:
        j = db_handler.get_job_for_path(str(rundir))

    print(str(j))


@app.command(_CMD_LIST, help="List jobs from database")
def list_jobs(
        status: JobStatus = typer.Option(
            None,
            "--status",
            "-s",
            help="job's status to filter on"
        ),
        db_name: str = typer.Option(
            ...,
            "--db-name",
            "-d",
            help="Name of a database; located in the default location ie the caller's home"
        )
):
    db_path = get_default_job_database_path(db_name)
    db_handler: DatabaseHandler = DatabaseHandler(db_path)
    jobs: List[Job] = None
    if status:
        jobs = db_handler.list_jobs(statuses=status)
    else:
        jobs = db_handler.list_jobs()

    if jobs:
        for j in jobs:
            print(str(j))
    else:
        print(f"No jobs with status {status.value} in database") if status else print("No jobs in database")