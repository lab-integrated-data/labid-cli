import logging
from pathlib import Path

import typer

from cli import get_default_config_file_path, CURRENT_USERNAME, get_default_job_database_path, get_defaut_job_dbname
from cli.config import get_config
from cli.database import init_database, DatabaseHandler, Job, JobType, JobStatus
from cli.utils import ObjectState, mail_admin_error, send_email_from_admin, is_less_than_a_week
from cli.register_utils import ProcessedAssayDescriptor, validate_study_input, create_sniffer
from stocks import AssayStructureError

from stocks.models import User, Assay, InstrumentRun, Study
from stocksapi.client import StocksClient
from stocksapi.exceptions import MultipleObjectMatchedError
from stocksapi.manager import StocksManager
from stocksapi.models import PyAssayValidate

logger = logging.getLogger(__name__)

# name of this module (as appearing on the command line) is the last part of the __name__ e.g. cli.config -> config
_MODULE_NAME = __name__.rsplit(".", 1)[-1]
# list of command names offered in this module
_CMD_ASSAYS_PROTECT = "protect"
_CMD_ASSAYS_UNPROTECT = "unprotect"

# create the CLI app
app = typer.Typer()

_PROTECT_HELP = """
             The dataset(s) produced by an assay are stored in the assay's run directory in the LabID data library of
             the assay's group owner. On the group's primary storage, this is by default a read-only place to ensure
             data is not accidentally deleted, renamed or edited i.e. a run directory is by default 'protected'.
             In specific situations, it may be necessary to edit a run directory content. For example, one may want to
             rename or re-organized files in a run directory of an assay in INITIALIZED state **before** registering its
             content using the UI data import wizard. Another example is when analysis tools require to write in the
             same folder as the loaded data (this last point is particularly bad practices but some key tools e.g. in
             electron microscopy are still expecting write access to the primary data folder).
             
             For these reasons, LabID allows to unprotect an assay's run directory i.e. make the entire run directory
             writable (in the sense of unix permissions) to the group on the primary storage. 
             Un-protecting an assay's run directory is a dangerous operation and must **always be regarded as an 
             exceptional operation**!!! 
             It should always be considered a temporary situation as it puts the managed data at risk of accidental
             deletion, renaming or editing. Assays must be unprotected only for the time necessary to perform the 
             required operations and then protected again. 
             
             ==> LabID logs (username and date) all (un)protect actions.
             
             Important: LabID is not responsible for unprotected data i.e. modification or deletion after registration
             and potential data loss resulting from manipulating data in an unprotected run directory. Changes
             occurring in unprotected run directories are *NOT* traced. In particular, data added to an unprotected run
             directory will *NOT* be known by LabID (i.e. data is *NOT* automatically registered by LabID e.g. upon
             re-protection) and it is currently not possible to register such data using the UI data import wizard.
"""

@app.command(_CMD_ASSAYS_PROTECT,
             short_help="Protect an assay's run directory i.e. makes the entire run directory read-only to the group "
                        "on the primary storage",
             help=_PROTECT_HELP,
             no_args_is_help=True
             )
def protect(
        assay_id: str = typer.Option(
            None,
            "--id",
            "-i",
            help="The UUID of the assay which run directory should be protected. Mutually exclusive with --path."
                 "If not given, --path is expected."
        ),
        run_dir_path: str = typer.Option(
            None,
            "--path",
            "-p",
            help="The absolute path to the run directory of the assay to protect. Mutually exclusive with --id."
                 "If not given, --id is expected."
        ),
        conf_file_path: str = typer.Option(
            get_default_config_file_path(),
            "--config-path",
            help="Config file absolute path")
) -> None:
    """
    Protect an assay's run directory i.e. makes the entire run directory read-only to the group on the primary storage
    """
    stocks_manager: StocksManager = StocksManager(StocksClient(get_config(Path(conf_file_path))))

    # check that only one of the two options is given
    if assay_id is None and run_dir_path is None:
        raise typer.BadParameter("One of --id or --path must be given.")

    assay = None
    try:
        assay = stocks_manager.fetch_assay(uuid=assay_id, run_dir=run_dir_path, load_ownership=False)
    except Exception as e:
        logger.error(f"Error fetching assay: {e}")
        raise typer.Abort()

    try:
        stocks_manager.change_assay_protection(uuid=assay.id, protect=True)
    except Exception as e:
        logger.error(f"Error changing assay protection: {e}")
        raise typer.Abort()

    print(f"Assay {assay.name}(uuid: {assay.id}) is now protected. Run dir path: {str(assay.run_dir)}")

@app.command(_CMD_ASSAYS_UNPROTECT,
             short_help="Unprotect an assay's run directory i.e. makes the entire run directory writable to the group "
                        "on the primary storage",
             help=_PROTECT_HELP,
             no_args_is_help=True
             )
def unprotect(
        assay_id: str = typer.Option(
            None,
            "--id",
            "-i",
            help="The UUID of the assay which run directory should be protected. Mutually exclusive with --path."
                 "If not given, --path is expected."
        ),
        run_dir_path: str = typer.Option(
            None,
            "--path",
            "-p",
            help="The absolute path to the run directory of the assay to protect. Mutually exclusive with --id."
                 "If not given, --id is expected."
        ),
        conf_file_path: str = typer.Option(
            get_default_config_file_path(),
            "--config-path",
            help="Config file absolute path")
) -> None:
    """
    Protect an assay's run directory i.e. makes the entire run directory read-only to the group on the primary storage
    """
    stocks_manager: StocksManager = StocksManager(StocksClient(get_config(Path(conf_file_path))))

    # check that only one of the two options is given
    if assay_id is None and run_dir_path is None:
        raise typer.BadParameter("One of --id or --path must be given.")

    assay = None
    try:
        assay = stocks_manager.fetch_assay(uuid=assay_id, run_dir=run_dir_path, load_ownership=False)
    except Exception as e:
        logger.error(f"Error fetching assay: {e}")
        raise typer.Abort()

    try:
        stocks_manager.change_assay_protection(uuid=assay.id, protect=False)
    except Exception as e:
        logger.error(f"Error changing assay protection: {e}")
        raise typer.Abort()

    print(f"Assay {assay.name}(uuid: {assay.id}) is now unprotected. Run dir path: {str(assay.run_dir)}")