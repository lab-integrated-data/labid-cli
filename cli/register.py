# -*- coding: utf-8 -*-
"""
The 'register' module of the CLI
"""
import os.path
import sys
import traceback
from typing import Set

import typer
from cli import get_default_config_file_path, get_defaut_job_dbname, CURRENT_USERNAME, \
    get_default_job_database_path, get_labid_server_user
from cli.database import DatabaseHandler, init_database, Job, JobType, JobStatus
from stocks.assaysniffer.sniffers import *
from stocks.assaysniffer.registry import registry
from stocks.models import InstrumentModel, Instrument, InstrumentRun, SimpleInstrumentRun, Study, Assay
from stocksapi.client import StocksClient
from stocksapi.exceptions import HTTPException
from stocksapi.manager import StocksManager
from cli.config import get_config
from stocksapi.models import PyDatasetListPost
from cli.utils import ExtendedEnum, is_uuid, mail_admin_error, is_less_than_a_week, get_checksum_dir, find_owner, \
    send_email_from_admin, is_within_labid_data_library, older_than_x_hours
from cli.register_utils import ProcessedAssayDescriptor, validate_study_input, create_sniffer, parse_sniffer_params

DELETE_GRACE_PERIOD_HOURS = 48

logger = logging.getLogger(__name__)

# name of this module (as appearing on the command line) is the last part of the __name__ eg cli.config -> config
_MODULE_NAME = __name__.rsplit(".", 1)[-1]
# list of command names offered in this module
_CMD_REGISTER_ASSAYS = "assays"
_CMD_REGISTER_BATCH = "batch"
_CMD_REGISTER_DATASET_COLLECTION = "collection"


# enums for fixed choices
class SidecarMetadataFormats(ExtendedEnum):
    KeyValue = "KeyValue"


# create the CLI app
app = typer.Typer()


def email_validation_error_at_init(root_dir: Path, sniffer_name: str, study_id: str, error: Exception | str,
                                   trace: str | None = None, cc: List[str] | None = None):
    subj = f"LabID register {_CMD_REGISTER_BATCH} error"
    mess = f""" 
    The following error occurred at initialization of a labid register {_CMD_REGISTER_BATCH} command:
        - executing user: {CURRENT_USERNAME}
        - sniffer: {sniffer_name}
        - study: {study_id}
        - root dir: {str(root_dir)}

    Error: 
    {error}  
    """

    if trace:
        mess = mess + f"""

        Error trace: 
        {trace}
        """

    mail_admin_error(subj, mess, cc_recipients=cc)


def email_registration_success(dir_path: str | Path, sniffer_name: str, study_id: str, cc: List[str] | None = None):
    subj = f"Data folder successfully submitted to LabID: {dir_path} "
    mess = f""" 
    Dear LabID user,

    The following data folder was successfully submitted to LabID:
    
    - {dir_path}
    - Sniffer: {sniffer_name} 
    - Submitted by: {CURRENT_USERNAME}
    
    The data will be copied to the LabID Data Library and you will be notified upon success. 
    
    IMPORTANT: Do not delete, edit or rename this data before it is fully registered in LabID!

    LabID
    """

    send_email_from_admin(subj=subj, mess=mess, cc_recipients=cc)


def email_ingestion_success(dir_path: str | Path, sniffer_name: str, cc: List[str] | None = None,
                            grace_period_hrs: str | int | None = None, is_auto_delete: bool = False):
    subj = f"Data successfully copied into LabID: {dir_path} "
    if is_auto_delete:
        mess = f""" 
        Dear LabID user,
        
        The following data folder was successfully copied into LabID Data Library:
        
        {dir_path} 
        
        The original data will be deleted automatically deleted in {str(grace_period_hrs)} hrs.   
        
        Best wishes
        
        LabID
        """
    else:
        mess = f"""
        Dear LabID user,
        
        The following data folder was successfully copied into LabID Data Library:
        
        {dir_path} 
        
        The original data will not be deleted. Please take care of cleaning up space.  
        
        Best wishes
        
        LabID
        """

    send_email_from_admin(subj=subj, mess=mess, cc_recipients=cc)


def email_data_deletion_success(dir_path: str | Path, assay_id: str, cc: List[str] | None = None):
    subj = f"Dropbox data deletion after successful registration to LabID: {dir_path}"
    mess = f""" 
    Dear LabID user,

    The following data folder was successfully deleted after successful registration to LabID under assay {assay_id}: 
    
    {dir_path} 
    
    LabID
    """

    send_email_from_admin(subj=subj, mess=mess, cc_recipients=cc)


def email_registration_error(dir_path: str | Path, sniffer_name: str, study_id: str,
                             error: Exception | str, trace: str | None = None, cc: List[str] | None = None):
    subj = f"LabID batch registration error for {str(dir_path)}"
    mess = f""" 
    Dear LabID user,

    An error occurred while trying to register the data in {str(dir_path)} :
        - Executing user: {CURRENT_USERNAME}
        - Sniffer: {sniffer_name}
        - Study: {study_id}

    Error message: 
    {error}  
    """

    if trace:
        mess = mess + f"""

        Error trace: 
        {trace}
        """

    send_email_from_admin(subj=subj, mess=mess, cc_recipients=cc)


@app.command(_CMD_REGISTER_BATCH,
             short_help="Batch register sub-directories",
             help=f"""
             Given a root dir, this command loops over sub-directories and attempts to register each subdir using the 
             provided sniffer. All sub-directories are expected to validate against the provided sniffer i.e. an error 
             is sent for each sub-directory that fails validation.
             An assay registration job is created for each subdir and a permanent storage (sqlite db) is used to 
             remember which subdir was processed, the job result status and the processing step.
             The behavior of the batch processing depends on the --daemon option. 
             
             ** When the --daemon option is used (i.e. when this command is run automatically to register new data using 
             periodic jobs eg cron), the following procedure applies:
             
             - 1. When a subdir is found, a job is created (INIT status) and an overall md5sum for the subdir is
             computed using all file paths and sizes. This sub-dir will not be further considered until the next
             call.
             
             - 2. When the md5sum for a subdir does not change between 2 runs and the job is in INIT state, the subdir
             is further processed and the assay submitted to LabID server. The job's assay id is saved together with
             status set to PROCESSING. This avoids processing a subdir while data is still being copied. 
             Note that to avoid failed jobs to go unnoticed, jobs in ERROR state are re-processed if the error email
             was sent more than a week ago; eventually re-raising an error email.  
             
             - 3. All PROCESSING jobs are checked for completion i.e. the assay's state on the server changed from
             NEW to REGISTERED upon successful data ingestion. When a subdir has been processed, the job is flagged
             as COMPLETED. 
             
             - 4. At the end of the run, jobs in COMPLETED states are deleted if --delete is set and the 'delete grace
             period time' ({DELETE_GRACE_PERIOD_HOURS} hrs) is over.
             
             ** When the command is not run in daemon mode, all directories are processed immediately and flagged 
             COMPLETED if server's submission is successful, else ERROR state is used. At the end of the run, jobs in
             COMPLETED states are deleted if --delete is set and the 'delete grace period time' 
             ({DELETE_GRACE_PERIOD_HOURS} hrs) is over i.e. file deletion wont happen unless the command is called a 
             second time after data ingestion by the server has completed.
             
             Note that:
             
             - Upon error, jobs status are flagged ERROR and an email is sent to --error-email recipients
             
             - emails are sent to --success-email upon successful completion of main steps 
             
             - the IGNORE state can be used to ignore jobs (see labid job --help)
             
                """)
def register_batch(
        root_dir: Path = typer.Option(
            ...,
            "--root-dir",
            "-r",
            help="Path to the root dir; this **must** be in a valid user dropbox (the one of the user used in the "
                 "LabID configuration). Each sub-dir is expected to validate against the sniffer."
        ),
        sniffer_name: str = typer.Option(
            ...,
            "--name",
            "-n",
            help="Name of the sniffer to use to extract the dataset and sample information."
        ),
        study_id: str = typer.Option(
            ...,
            "--study",
            "-s",
            help="The UUID of the study to which all the datasets will be associated "
        ),
        instrument_key: Optional[str] = typer.Option(
            None,
            "--instrument",
            help="Name, code or UUID of an existing instrument (e.g. sequencer, microscope...) used to generate the "
                 "assay(s). Only relevant for sniffer of type AssaySniffer (option ignored for DatasetSniffer). "
                 "When given, this is used to replace the instrument slot of the run returned by the sniffer."

        ),
        instrument_model_key: Optional[str] = typer.Option(
            None,
            "--instrument-model",
            help="Name or UUID of an existing instrument model (e.g. sequencer, microscope...) used to generate the "
                 "assay(s). This is ignored when --instrument is provided. Only relevant for sniffer of type "
                 "AssaySniffer (option ignored for DatasetSniffer). When given (and not ignored), this is used to "
                 "replace the instrument_model slot of the run returned by the sniffer."
        ),
        daemon: bool = typer.Option(
            False,
            "--daemon",
            help="activates daemon mode"
        ),
        delete_on_completed: bool = typer.Option(
            False,
            "--delete",
            help=f"Should data be deleted upon successful data transfer. Deletion only happens after a grace period "
                 f"{DELETE_GRACE_PERIOD_HOURS} hrs by default and for data which registration is completed."
        ),
        delete_grace_period: int = typer.Option(
            DELETE_GRACE_PERIOD_HOURS,
            "--delete-after-h",
            help="Grace period, in hours, after which successfully ingested data is deleted from disk. Only relevant "
                 "when --delete is true."
        ),
        dry_run: bool = typer.Option(
            False,
            "--dry-run / --submit",
            help="only prints the JSON payload without submitting it to the server API. "
                 "In dry run mode, the local job database is still being used but created jobs will be deleted right "
                 "before the process ends. Data is never removed from disk in dry run mode."
        ),
        allow_pooled_samples: bool = typer.Option(
            True,
            "--allow-pooled/--reject-pooled",
            help="When pooled is allowed, the same sample can be the input of multiple datasets (within the same "
                 "dataset collection)"),
        transfer_whole_input_dir: bool = typer.Option(
            True,
            "--whole-dir_path/--dataset-only",
            help="Transfer the whole run dir_path. If false (--dataset-only), only dataset files are imported i.e. "
                 "any other file will be ignored"),
        sniffer_params: List[str] = typer.Option(
            None,
            "--sniffer_param", "-p",
            help="Optional sniffer parameters e.g. --sniffer_param 'name=blah'."
        ),
        assay_protocol_ids: List[str] = typer.Option(
            None,
            "--assay-protocol",
            help="Protocol UUID to link to created assays. Multiple protocol UUID can be passed in which case the order"
                 " on the command line is preserved in the corresponding protocol workflow "
                 "e.g. --assay-protocol uuid1 --assay-protocol uuid2 results in a Workflow:[uuid1, uuid2]"
        ),
        sample_protocol_ids: List[str] = typer.Option(
            None,
            "--sample-protocol",
            help="Protocol UUID to link to samples, even when already existing. When samples are already existing and "
                 "already have a workflow, the workflow will be replaced with this one (i.e. protocols are **not** "
                 "added to the existing workflow). Multiple protocol UUID can be passed in which case the order on "
                 "the command line is preserved in the corresponding protocol workflow "
                 "e.g. --sample-protocol uuid1 --sample-protocol uuid2 results in a Workflow:[uuid1, uuid2]"
        ),
        email_reporting: bool = typer.Option(
            True,
            "--email-reporting / --no-email-reporting",
            help="Should activity (success/error) be reported by email? "
                 "We recommend --email-reporting when using --daemon"
        ),
        on_error_email: List[str] = typer.Option(
            None,
            "--error-email",
            "-e",
            help="Email(s) to report error(s); ignored if --no-email-reporting is used. "
        ),
        on_success_email: List[str] = typer.Option(
            None,
            "--success-email",
            "-m",
            help="Email(s) to report successful registration; ignored if --no-email-reporting is used. "
        ),
        db_name: str = typer.Option(
            None,
            "--db-name",
            help="Name (not a path!) of a database to remember which assays were processed, when and their success"
                 " or error status. This name will be used to create (if not existing) a sqlite database file stored "
                 "in the home dir of the user used to connect to LabID (as configured in --config-path)."
                 "This is particularly important to avoid getting error emails each time the command is launched while "
                 "an error was not fixed."
        ),
        wipe_db: bool = typer.Option(
            False,
            "--wipe-db / --keep-db",
            help="Should we force create a fresh database? Never do this in production unless you know what you do!"
        ),
        max_num: int = typer.Option(
            10,
            "--max-jobs",
            help="Maximum number of subdirs to process. Keep this low when setting up services."
        ),
        conf_file_path: str = typer.Option(
            get_default_config_file_path(),
            "--config-path",
            help="Config file absolute path")
) -> None:
    if not root_dir.exists():
        raise typer.BadParameter(message=f"Path does not exist: {str(root_dir)}", param_hint="root-dir")

    if is_within_labid_data_library(root_dir):
        raise typer.BadParameter(
            message=f"You cannot use this feature to monitor LabID Data Library sub-folders: the monitored "
                    f"directory ({root_dir}) must be outside the LabID Data Library.", param_hint="root-dir")

    db_handler: DatabaseHandler
    study: Study
    instrument: Instrument
    instrument_model: InstrumentModel

    if not db_name:
        # create a default one named after this sniffer
        db_name = get_defaut_job_dbname(jobtype="register", sniffer_name=sniffer_name)

    db_path = get_default_job_database_path(db_name)
    default_labid_url = ""
    logged_in_user: User
    stocks_group_name: str
    try:
        _config = get_config(Path(conf_file_path))
        client: StocksClient = StocksClient(_config)
        stocks_manager: StocksManager = StocksManager(client)
        default_labid_url = _config["default"]
        logged_in_user = stocks_manager.logged_in_user
        stocks_group_name = logged_in_user.get_primary_group().name

        if not db_path.exists():
            init_database(db_path)
        elif wipe_db:
            # we always keep a backup !
            init_database(db_path, force_new=True, backup_before_wipe=True)

        db_handler = DatabaseHandler(db_path)

        # Check given id is really a study or fails
        study = validate_study_input(study_id, stocks_manager)

        # raise BadParameter if invalid
        validate_protocol_ids(stocks_manager, sample_protocol_ids)
        validate_protocol_ids(stocks_manager, assay_protocol_ids)

        # init the sniffer or fails ; this is only meant for validation ie
        # a new sniffer instance must be created for each sniffing
        sniffer: AssaySniffer | DatasetSniffer = create_sniffer(sniffer_name, sniffer_params, stocks_manager)

        if isinstance(sniffer, AssaySniffer):
            # we may also have an instrument or an instrument model
            if instrument_key:
                instrument = _lookup_instrument_by_uuid_code_or_name(
                    stocks_manager=stocks_manager, instrument_key=instrument_key)
            elif instrument_model_key:
                instrument_model = _lookup_instrument_model_by_uuid_or_name(
                    stocks_manager=stocks_manager, instrument_model_key=instrument_model_key,
                    technology=sniffer.get_supported_technology()
                )
        elif not isinstance(sniffer, DatasetSniffer):
            raise NotImplemented

    except Exception as error:
        # failing here means a problem with the command line itself.
        # we need to report to admin
        trace = traceback.format_exc()
        logger.error(trace)
        logger.error(error)
        if email_reporting:
            email_validation_error_at_init(root_dir=root_dir, sniffer_name=sniffer_name,
                                           study_id=study_id, error=error, trace=trace, cc=on_error_email)
        raise typer.Exit(code=1)

    sub_dir_paths = [f.path for f in os.scandir(root_dir) if f.is_dir()]
    # we first assess which subdirs should be processed
    to_process_paths = list()
    dir_path: Path
    for dir_path in sub_dir_paths:
        try:
            md5sum: str = ""
            # check if this dir is already in the Job Db
            job: Job = db_handler.get_job_for_path(path=dir_path)
            if not job:
                # first time we see this dir, we register the job in INIT state and continue
                md5sum = get_checksum_dir(dir_path)
                logger.info(f"Creating new job for dir {dir_path} with hash {md5sum}")
                job = db_handler.create_job(JobType.REGISTER, dir_path=dir_path, sniffer=sniffer_name, md5sum=md5sum)
                logger.info(f"Creating job {job.id} for {str(dir_path)}")
                if daemon and not dry_run:
                    # we dont want to process this dir in this round
                    continue
            elif job.status == JobStatus.IGNORE:
                # no matter the daemon mode, we always ignore
                logger.info(f"Ignoring dir {dir_path}: Job exists with {job.status.value} status")
                continue
            elif job.status == JobStatus.ERROR:
                if job.mail_sent and is_less_than_a_week(job.mail_sent):
                    # we only skip errored jobs less than a week. If these are older than a week we try to validate them
                    # again (which may raise a new error email). This is to make sure they dont go under the radar
                    # here again, no difference given daemon mode
                    logger.info(f"Ignoring dir {dir_path}: Errored job recently reported to user")
                    logger.info(str(job))
                    continue

                # we want to reprocess it, we need to reset its state accordingly
                # if the job was previously submitted (and errored after successfull submission),
                # we need to reset to PROCESSING else to INIT to try re-submitting
                job.status = JobStatus.PROCESSING if job.submitted else JobStatus.INIT
                # reset the message / mail sent fields
                job.message = ""
                job.mail_sent = None
                db_handler.update_job(job)

            if daemon and job.status == JobStatus.INIT:
                # it is not the first round we look at this dir, is it ready to process? ie md5sum did not change
                if not md5sum:
                    md5sum = get_checksum_dir(dir_path)
                if job.md5sum != md5sum:
                    # not ready, update the dir_hash
                    logger.info(f"Dir hash changed compared to previous round, while now ignore {str(dir_path)}")
                    job.md5sum = md5sum
                    db_handler.update_job(job)
                    continue

            # sounds like it is ready to process
            to_process_paths.append(dir_path)

        except Exception as err:
            _mess = f"Unexpected error while looking at dir {str(dir_path)}: {str(err)}"
            _trace = traceback.format_exc()
            logger.error(_mess)
            logger.error(_trace)
            if email_reporting:
                email_validation_error_at_init(root_dir=root_dir, sniffer_name=sniffer_name, study_id=study_id,
                                               error=_mess, trace=_trace, cc=on_error_email)

            # failing here also means something is very wrong => exit
            typer.Exit(1)
    #
    # now we process the to_process_paths:
    # PROCESSING jobs should be checked for completion, and flagged COMPLETED if so
    # COMPLETED jobs should be checked data removal if requested
    # INIT/ERROR jobs should be processed

    # =-> we now process only the INIT/ERROR jobs: these need to be submitted
    # this dict will contain an entry for each dir we try to process in this call
    processed_dirs: Dict[str, ProcessedAssayDescriptor] = dict()
    for dir_path in to_process_paths:
        # here we want a ProcessedAssayDescriptor for each processed dir to keep the success/error status
        # no error should interupt this so we have a global try catch
        try:
            # have we reached the max_assay_num limit?
            if len(processed_dirs) >= max_num:
                logger.info(f"Max number of subdirs to process in a run reached : {max_num}")
                break

            job: Job = db_handler.get_job_for_path(path=dir_path)
            logger.info(str(job))
            if job.status != JobStatus.INIT:
                continue

            # every single dir to process should have a ProcessedAssayDescriptor
            pad: ProcessedAssayDescriptor = ProcessedAssayDescriptor(dir=dir_path, job_id=job.id)
            processed_dirs[dir_path] = pad
            logger.info(f"Trying to sniff {dir_path}")
            if not os.access(str(dir_path), os.R_OK):
                _mess = f"Input dir {str(dir_path)} cannot be read"
                logger.error(_mess)
                pad.set_error(_mess)
                continue

            # sniff
            data_owner = find_owner(dir_path)
            if data_owner == get_labid_server_user():
                # the data already belongs to the labid technical user, we'll make the data owned by the logged in user
                data_owner = logged_in_user.username
            # make sure data_owner is valid in LAbID
            try:
                stocks_manager.fetch_user(username_or_uuid=data_owner)
            except ValueError as ve:
                _mess = f"{str(err)}"
                logger.error(_mess)
                logger.warning(f"File owner {data_owner} is not a valid LabID user, data owner will default to "
                               f"the logged in user: {logged_in_user.username}")
                data_owner = logged_in_user.username

            # we always re-create a new sniffer
            _sniffer: AssaySniffer | DatasetSniffer = create_sniffer(sniffer_name, sniffer_params, stocks_manager)
            res = _batch_register_for_assaysniffer(
                sniffer=_sniffer,
                sniffer_name=sniffer_name,
                dir_path=dir_path,
                job=job,
                db_handler=db_handler,
                stocks_manager=stocks_manager,
                logged_in_user=logged_in_user,
                instrument=instrument,
                instrument_model=instrument_model,
                data_owner=data_owner,
                stocks_group_name=stocks_group_name,
                allow_pooled_samples=allow_pooled_samples,
                transfer_whole_input_dir=transfer_whole_input_dir,
                study=study,
                dry_run=dry_run,
                assay_protocol_ids=assay_protocol_ids
            )

            if isinstance(_sniffer, AssaySniffer):
                try:

                    job.status = JobStatus.PROCESSING
                    job.submitted = datetime.now()
                    db_handler.update_job(job)
                    logger.info(f"Submitted data for {str(dir_path)}")

                    if not dry_run and sample_protocol_ids:
                        o: PyDatasetListPost = PyDatasetListPost(**res)
                        for _s in o.samples:
                            resp = stocks_manager.add_protocols_to_sample(protocol_ids=sample_protocol_ids,
                                                                          sample_id=_s.id)
                            logger.debug(resp)

                except Exception as err:
                    logger.warning(f"data from {str(dir_path)} raised an error")
                    _mess = f"{str(err)}"
                    _trace = traceback.format_exc()
                    logger.error(_mess)
                    logger.error(_trace)
                    pad.set_error(_mess, _trace)
                    continue

            elif isinstance(_sniffer, DatasetSniffer):
                logger.error("DatasetSniffer support Needed!")
                raise NotImplemented

        except Exception as err:
            # most likely error with the db access
            _mess = f"Unexpected error while processing dir {str(dir_path)}: {str(err)}"
            _trace = traceback.format_exc()
            logger.error(_mess)
            logger.error(_trace)
            if dir_path not in processed_dirs:
                # error happened before we even create the pad & job id
                processed_dirs[dir_path] = ProcessedAssayDescriptor(dir=dir_path, job_id=None)

            processed_dirs[dir_path].set_error(_mess, _trace)

    # =-> we now check if PROCESSING jobs have completed
    logger.info("Now Checking for Jobs in PROCESSING status")
    for dir_path in to_process_paths:
        # if we just processed it, we can ignore as it wont be ready
        if dir_path in processed_dirs:
            continue
        # get job
        job: Job = db_handler.get_job_for_path(path=dir_path)

        if job.status != JobStatus.PROCESSING:
            continue

        # we check if the job has been fully processed
        job_assay_ids: Set[str] = set()
        if job.assay_id:
            job_assay_ids = set(job.assay_id.split(","))
        else:
            # this is not normal, put this job in error
            processed_dirs[dir_path] = ProcessedAssayDescriptor(dir=dir_path, job_id=job.id)
            processed_dirs[dir_path].set_error(f"Job has no assay id set while in {job.status} status", "")
            continue

        logger.info(f"   -> Checking if data has been processed by LabID: {str(dir_path)} (job {job.id})")
        try:
            all_assays_registered = True
            for _assay_id in job_assay_ids:
                # fetch assay
                _assay = stocks_manager.fetch_assay(uuid=_assay_id)
                if _assay.state != ObjectState.REGISTERED:
                    logger.info(f"still ingesting assay {_assay.name} ({_assay_id})")
                    all_assays_registered = False
                    break

            if all_assays_registered:
                # all assays are completed, we change the job status
                job.status = JobStatus.COMPLETED
                db_handler.update_job(job)
                if email_reporting:
                    email_ingestion_success(dir_path=dir_path, sniffer_name=sniffer_name, cc=on_success_email,
                                            grace_period_hrs=delete_grace_period, is_auto_delete=delete_on_completed)
                logger.info(f"    Job {job.id} completed !")
            else:
                logger.info(f"    Job {job.id} still processing")

        except Exception as err:
            # db access error or md5sum not matching
            _mess = f"Unexpected error while processing dir {str(dir_path)}: {str(err)}"
            _trace = traceback.format_exc()
            logger.error(_mess)
            logger.error(_trace)
            if dir_path not in processed_dirs:
                processed_dirs[dir_path] = ProcessedAssayDescriptor(dir=dir_path, job_id=job.id)
            processed_dirs[dir_path].set_error(_mess, _trace)
            # next dir
            continue

    # REPORTING on ProcessedAssayDescriptor objects => either error or processing SINCE we did not check the newly
    # submitted jobs for COMPLETION
    #
    logger.info("Now reporting about processed dirs...")
    for pad in processed_dirs.values():
        is_successful = not pad.is_failed
        dir_path = pad.dir
        # update job info
        now = datetime.now()
        job_id = pad.job_id
        if not job_id:
            logger.error(f"Non job ID in PAD {str(pad)}")
            if email_reporting:
                # error occured even before we can create a job
                email_registration_error(
                    dir_path, sniffer_name, study_id, pad.error_message, pad.error_trace, cc=on_error_email)
            continue

        j: Job = db_handler.load_job(job_id)
        j.modified = now
        try:
            if is_successful:
                j.status = JobStatus.PROCESSING
                j.message = f"Successfully submitted data to LabID using {sniffer_name}"
                if email_reporting:
                    email_registration_success(dir_path, sniffer_name, study_id, cc=on_success_email)
                    j.mail_sent = now
                else:
                    j.mail_sent = None
            else:
                j.status = JobStatus.ERROR
                j.message = pad.error_message
                if email_reporting:
                    email_registration_error(
                        dir_path, sniffer_name, study_id, pad.error_message, pad.error_trace, cc=on_error_email)

                    j.mail_sent = now
                else:
                    j.mail_sent = None
        except Exception as err:
            # email failed
            logger.error(f"Failed to send email : {str(err)}")
            logger.error(traceback.format_exc())

        # save the job
        logger.info("Updating job:" + str(j))
        db_handler.update_job(j)

    if dry_run:
        # we clean up the job database
        logger.info("Cleaning up job database (dry-run)")
        for pad in processed_dirs.values():
            if pad.job_id:
                try:
                    logger.info(f"Deleting {pad.job_id}...")
                    db_handler.delete_job(pad.job_id)
                except Exception as err:
                    logger.error(f"Failed to delete job {pad.job_id} from {db_path}. "
                                 f"Manual clean up needed! Error: {str(err)}")
    else:
        # we now check if COMPLETED job can be cleaned up, we let error propagate ie end the script because this is
        # last operation
        logger.info(f"Checking for input dir to be cleaned...")
        for dir_path in to_process_paths:
            # get job
            job: Job = db_handler.get_job_for_path(path=dir_path)
            if job.status != JobStatus.COMPLETED:
                continue

            # we only delete after a grace period; eg at EMBL this makes sure nightly backups
            # have quicked in so we have a chance to get the data back ...just in case we missed
            # something !
            if delete_on_completed and older_than_x_hours(job.submitted, delete_grace_period):
                import shutil
                try:
                    shutil.rmtree(dir_path)
                    job.dir_removed = datetime.now()
                    db_handler.update_job(job)
                    logger.warning(f"{dir_path} has been loaded in LabID and was deleted from dropbox")
                    email_data_deletion_success(dir_path=dir_path, assay_id=job.assay_id, cc=on_success_email)
                except Exception as e:
                    _mess = (f"Failed to delete directory {dir_path}: {e}. \n"
                             f"Ensure {CURRENT_USERNAME} has privileges to delete the entire folder; or do it yourself.")
                    logger.error(_mess)
                    mail_admin_error("LabID batch register failed to delete dir after data registration", _mess)
            elif delete_on_completed:
                logger.info(f"{dir_path} has been loaded in LabID (submitted on {job.submitted}) but will be deleted "
                            f"later (still in delete grace period of {delete_grace_period} hours)")
            elif not delete_on_completed:
                logger.warning(f"{dir_path} has been loaded in LabID and can be deleted now")

        logger.info(f"End of watch turn for dir: {root_dir}")


@app.command(_CMD_REGISTER_ASSAYS,
             short_help="Register one or more assays and associated raw data to an existing study.",
             help="""
     All assays must be of the same type/technology (e.g. 'sequencing') and platform (e.g. Illumina or Nanopore)
     and produced by a unique instrument run. It is recommended to first execute the command as a dry run to spot 
     potential issues. 
     The assay(s) information is extracted from a run directory obeying a structure recognized by a 'sniffer'. 
     You can either specify the sniffer to use (always preferred) or let the  tool iterate over all the sniffers 
     available for the technology at hand. 
     The optional and mutually exclusive run_id, instrument or instrument model can be provided when the sniffer 
     is not able to extract this information from the run directory.
     Finally, you may decide to only transfer the files/folders identified as dataset or to transfer the whole run 
     folder (--indir). The latter option allows to keep additional information that may be later registered as 
     datasets if need be.     
    """)
def sniff_assays_from_instrument_run(
        indir: Path = typer.Option(
            ...,
            "--indir",
            "-i",
            help="Path to the run directory containing the data to be parsed/sniffed. "
        ),
        technology: Technology = typer.Option(
            ...,
            "--technology",
            "-t",
            case_sensitive=False,
            help=f"The technology that produced this run directory; one of {Technology.list()}"
        ),
        platform: str = typer.Option(
            None,
            "--platform",
            "-p",
            case_sensitive=False,
            help=f"The platform that produced this run directory e.g. NANOPORE, ILLUMINA, ZEISS ..."
        ),
        study_id: str = typer.Option(
            ...,
            "--study",
            "-s",
            help="The Name or UUID of the study to link the datasets to. When passing a name, best quote it "
                 "i.e. -s 'my study' "
        ),
        instrument_session_id: Optional[str] = typer.Option(
            None,
            "--instrument-session",
            help="UUID of an existing instrument session (aka instrument run) to use. When given, the instrument run "
                 "returned by the sniffer is replaced with this given instrument run."
        ),
        instrument_key: Optional[str] = typer.Option(
            None,
            "--instrument",
            help="Name, code or UUID of an existing instrument (e.g. sequencer, microscope...) used to generate the "
                 "assay(s). This is ignored when --instrument-session is provided. "
                 "When given (and not ignored), this is used to replace the instrument slot of the run returned by "
                 "the sniffer."

        ),
        instrument_model_key: Optional[str] = typer.Option(
            None,
            "--instrument-model",
            help="Name or UUID of an existing instrument model (e.g. sequencer, microscope...) used to generate the "
                 "assay(s). This is ignored when --instrument or --instrument-session is provided. "
                 "When given (and not ignored), this is used to replace the instrument_model slot of the run "
                 "returned by the sniffer."
        ),
        dry_run: bool = typer.Option(
            False,
            "--dry",
            "-d",
            help="Do not perform real request. Only checks if the dir_path can be sniffed"),
        allow_pooled_samples: bool = typer.Option(
            True,
            "--allow-pooled/--reject-pooled",
            help="When pooled is allowed, the same sample can be the input of multiple datasets (within the same "
                 "dataset collection)"),
        transfer_whole_input_dir: bool = typer.Option(
            True,
            "--whole-dir_path/--dataset-only",
            help="Transfer the whole run dir_path. If false (--dataset-only), only dataset files are imported "
                 "i.e. any other file will be ignored"),
        sniffer_name: Optional[str] = typer.Option(
            None,
            "--sniffer_name",
            "-n",
            help="Name of the assay sniffer to use, optional. "
                 "If not given, a sniffer matching the given Technology/Platform are used and the first "
                 "returning a valid result is used"
        ),
        sniffer_params: List[str] = typer.Option(
            None,
            "--sniffer_param",
            help="optional sniffer parameters e.g. --sniffer_param 'name=blah' [--sniffer_param 'X=Z']. "
                 "Only considered with a qualified --sniffer_name"
        ),
        assay_protocol_ids: List[str] = typer.Option(
            None,
            "--assay-protocol",
            help="Protocol UUID to link to created assays. Multiple protocol UUID can be passed in which case the order"
                 " on the command line is preserved in the corresponding protocol workflow "
                 "e.g. --assay-protocol uuid1 --assay-protocol uuid2 results in a Workflow:[uuid1, uuid2]"
        ),
        sample_protocol_ids: List[str] = typer.Option(
            None,
            "--sample-protocol",
            help="Protocol UUID to link to samples, even when already existing. When samples are already existing and "
                 "already have a workflow, the workflow will be replaced with this one (i.e. protocols are **not** "
                 "added to the existing workflow). Multiple protocol UUID can be passed in which case the order on "
                 "the command line is preserved in the corresponding protocol workflow "
                 "e.g. --sample-protocol uuid1 --sample-protocol uuid2 results in a Workflow:[uuid1, uuid2]"
        ),
        stocks_group_name: Optional[str] = typer.Option(
            None,
            "--group_name",
            "-g",
            help="Group name to use to load the data. By default, your primary group is used."
                 "Warning: the group name must match one of the groups listed by calling 'stocks-cli config show'. "
        ),
        conf_file_path: str = typer.Option(
            get_default_config_file_path(),
            "--config-path",
            help="Config file absolute path")
) -> None:
    """
    Register one or more assay and associated raw data to LabID. All assays must be of the same type i.e. same
    technology (e.g. 'sequencing') and platform (e.g. Illumina or Nanopore) and produced by a unique instrument run.
    The assay(s) are parsed from a run directory obeying a structure recognized by a 'sniffer'.
    """
    _config = get_config(Path(conf_file_path))
    # _default_unix_group = _config[_config["default"]]["unix_group"]
    client: StocksClient = StocksClient(_config)
    stocks_manager: StocksManager = StocksManager(client)
    logged_in_user: User = stocks_manager.logged_in_user
    if not stocks_group_name:
        stocks_group_name = logged_in_user.get_primary_group().name
    elif stocks_group_name not in stocks_manager.logged_in_user.groups:
        raise typer.BadParameter(f"Invalid group name for user '{logged_in_user.username}' : "
                                 f"'{stocks_group_name}'. The group must be one of "
                                 f"{list(logged_in_user.groups)}")

    logger.debug(indir)
    if not check_valid_directory(indir):
        raise typer.BadParameter(f"Invalid directory path: {str(indir)}")
    if not os.access(str(indir), os.R_OK):
        raise typer.BadParameter(f"Directory cannot be read: {str(indir)}")

    # Check given id is really a study
    study: Study = validate_study_input(study_id, stocks_manager)

    # raise BadParameter if invalid
    validate_protocol_ids(stocks_manager, sample_protocol_ids)
    validate_protocol_ids(stocks_manager, assay_protocol_ids)

    # Check if session, instrument or model is provided, with session superseeding instrument superseeding model
    session: InstrumentRun = None
    instrument: Instrument = None
    instrument_model: InstrumentModel = None
    if instrument_session_id:
        if not is_uuid(instrument_session_id):
            raise typer.BadParameter(f"Provided run/session UUID {instrument_session_id} is not a valid UUID",
                                     param_hint="--instrument-session")
        try:
            session = stocks_manager.fetch_instrument_run(instrument_session_id)
            logger.info(f"Found session {session.name} with UUID {instrument_session_id}")
        except Exception:
            logger.error(f"Error:\n {traceback.format_exc()}")
            raise typer.BadParameter(f"No valid session found with run/session UUID {instrument_session_id}",
                                     param_hint="--instrument-session")
    elif instrument_key:
        instrument = _lookup_instrument_by_uuid_code_or_name(
            stocks_manager=stocks_manager, instrument_key=instrument_key)
        logger.info(f"Found instrument {instrument.name} with key {instrument_key}")
    elif instrument_model_key:
        instrument_model = _lookup_instrument_model_by_uuid_or_name(
            stocks_manager=stocks_manager, instrument_model_key=instrument_model_key, technology=technology
        )
        logger.info(f"Found instrument model {instrument_model.name} with key {instrument_model_key} and "
                    f"technology {technology.value}")
    # end of --instrument-session --instrument and --instrument-model checks

    # runs will store runs extracted by sniffer (or remains None)
    runs: List[InstrumentRun] = None

    if sniffer_name:
        ####################
        # one sniffer name was given
        ####################
        sniffer: AssaySniffer = create_sniffer(sniffer_name, sniffer_params, stocks_manager)
        if not sniffer.is_sniffer_valid_for(technology=technology, platform=platform):
            raise typer.BadParameter(f"Sniffer '{sniffer_name}' is not valid for technology/platform:"
                                     f" {technology}/{platform}.")
        # looks good, snif the dir_path
        try:
            runs = sniffer.sniff_instrument_run_assays(
                dir_path=indir, username=logged_in_user.username, group=stocks_group_name)
            logger.info(f"Sniffer {sniffer_name} returned {len(runs)} run")
        except AssayStructureError as err:
            logger.error(str(err))
            print(f"The data in {str(indir)} does not comply to the sniffer {sniffer_name} expectations!")
            sys.exit(1)

        if not runs or len(runs) == 0:
            raise typer.BadParameter(f"Sniffer '{sniffer_name}' could not detect any assay i.e. data in {str(indir)} "
                                     f"is not recognized by this sniffer.")
        if len(runs) > 1:
            raise typer.BadParameter(f"Sniffer '{sniffer_name}' detected {len(runs)} instrument runs in the directory"
                                     f" {str(indir)}. Loading multiple runs at once is not supported; please call"
                                     f" the {_CMD_REGISTER_ASSAYS} command on a folder containing results for a unique run")

    else:
        ####################
        # loop over sniffers and try !
        ####################
        logger.info("No sniffer specified, trying to identify one...")
        for sniffer_cls in registry.get_sniffers():
            # get instance
            sniffer = sniffer_cls()
            sniffer.set_stocks_manager(stocks_manager=stocks_manager)
            logger.debug(sniffer)
            if sniffer.is_sniffer_valid_for(technology=technology, platform=platform):
                # looks good, snif the dir_path
                try:
                    runs: List[InstrumentRun] = sniffer.sniff_instrument_run_assays(dir_path=indir,
                                                                                    username=logged_in_user.username,
                                                                                    group=stocks_group_name)
                except AssayStructureError as err:
                    logger.debug(str(err))
                    logger.debug(f"Sniffer {sniffer.__class__.__name__} raised an error while sniffing {str(indir)}."
                                 f" Trying next sniffer.")
                    continue

                if not runs or len(runs) == 0:
                    logger.debug(
                        f"Sniffer {sniffer.__class__.__name__} did not find run in {str(indir)}. Trying next sniffer.")
                    continue

                if len(runs) > 1:
                    logger.error(
                        f"Sniffer {sniffer.__class__.__name__} detected {len(runs)} instrument runs in directory"
                        f" {str(indir)}. Loading multiple runs at once is not supported; please call"
                        f" the {_CMD_REGISTER_ASSAYS} command on a folder containing results for a unique run")
                    continue

                # we have a unique run, good!post this and let the server complains if something wrong !
                logger.info(
                    f"Sniffer {sniffer.__class__.__name__} found a run in {str(indir)}, submitting to LabID...")
                break

    submitted: bool = False

    try:
        if runs and len(runs) == 1:
            # we have a unique run in 'runs', good!
            # post this and let the server complains if something wrong !

            # we first need to register the assays and for this, we need to have a run or a model
            the_run: InstrumentRun = runs[0]
            # do we need to look up for instrument (ie user did not pass this as option) ?
            if not session and not instrument and not instrument_model:
                instrument, instrument_model = _fetch_instrument_or_model_info_from_sniffed_data(
                    stocks_manager=stocks_manager, run=the_run, technology=technology)
            # at this point we must have a session, an instrument or a model
            existing_run: SimpleInstrumentRun = None
            if session:
                existing_run = session
            elif instrument:
                the_run.instrument = instrument
            elif instrument_model:
                # we force the instrument model and there is no proper instrument_run for assays
                the_run.instrument = None
                for assay in the_run.assays:
                    assay.instrumentrun = None
                    assay.instrumentmodel = instrument_model

            if not dry_run:
                logger.info(f"will register assay(s) from run named {the_run.name}")
                # let s create a run to use for all assays if needed (if instrument...)
                if not existing_run and instrument:
                    # check if we have this run already. This could happen if eg (1) assays are loaded separately while
                    # from the same run or (2) when re-running code after failure (with run created before).
                    logger.info(f"looking up for existing run named {the_run.name}")
                    existing_runs: List[InstrumentRun] = stocks_manager.list_instrument_runs(
                        name=the_run.name, instrument_name=instrument.name, owner=logged_in_user.username)
                    if existing_runs and len(existing_runs) > 0:
                        existing_run = existing_runs[0]
                        logger.info(f"  => re-using existing run {existing_run.name} for instrument {instrument.name}")
                    else:
                        logger.info("  => creating new run")
                        existing_run = stocks_manager.save_instrument_run(
                            name=the_run.name, instrument=instrument, description=the_run.description,
                            start_datetime=the_run.start_datetime, end_datetime=the_run.end_datetime,
                            producer=the_run.producer
                        )
                # now we save all assays to the run
                for assay in the_run.assays:
                    # from previous CLI call
                    # when needed:
                    # assay.owner = logged_in_user
                    # assay.owned_by = ..
                    # First check if we have an assay with same name and status NEW (ie this is a leftover from previous
                    # aborted run); if so re-use
                    assay.id = _create_or_grab_previously_created_assay(assay=assay, assay_run=existing_run,
                                                                        stocks_manager=stocks_manager)

                    # patch protocols
                    if assay_protocol_ids:
                        logger.info("Adding protocol(s) to assay %s", assay.id)
                        stocks_manager.add_protocols_to_assay(protocol_ids=assay_protocol_ids, assay_id=assay.id)

                logger.info(f"Creating datasets, samples and connecting to the assay...")
                res = stocks_manager.register_raw_assay_datasets(
                    instrument_run=runs[0], run_dir=indir,
                    username=logged_in_user.username, unixgroup=stocks_group_name,
                    allow_pooled_samples=allow_pooled_samples, transfer_whole_input_dir=transfer_whole_input_dir,
                    study=study)
                logger.debug(res)

                o: PyDatasetListPost = PyDatasetListPost(**res)
                if sample_protocol_ids:
                    logger.info("Adding protocol(s) to sample(s)")
                    for _s in o.samples:
                        resp = stocks_manager.add_protocols_to_sample(protocol_ids=sample_protocol_ids, sample_id=_s.id)
                        logger.debug(resp)

                logger.warning(
                    f"Successfully submitted {len(o.assays)} assay(s) totalling to {len(o.datasets)} dataset(s) as"
                    f" {len(o.collections)} collection(s). An email will notify you when data ingestion has completed. "
                    f"Until then, please do NOT touch the data in {str(indir)}.")

                submitted = True
            else:
                o: PyDatasetListPost = stocks_manager._create_instrument_run_post(
                    instrument_run=runs[0], run_dir=indir,
                    owner=logged_in_user.username, owned_by_group=stocks_group_name,
                    allow_pooled_samples=allow_pooled_samples, transfer_whole_input_dir=transfer_whole_input_dir,
                    study_id=study_id, fail_on_missing_assay_id=False)
                logger.warning("Dry-run: would submit payload (JSON):")
                logger.warning("\n" + o.json(exclude_none=True, exclude_unset=False))
                submitted = True
        else:
            logger.warning("NO RUN TO REGISTER !!")
    except Exception:
        logger.error(f"Error:\n {traceback.format_exc()}")
        logger.error(f"An unexpected error occurred, please check error logs.")
        exit(1)

    if not sniffer_name and not submitted:
        logger.warning(f"Could not find a suitable sniffer for {str(indir)}. Sorry. You are welcome to contribute one.")
    elif dry_run:
        logger.warning("Dry-run: looking good!")


def _fetch_instrument_or_model_info_from_sniffed_data(
        stocks_manager: StocksManager, run: InstrumentRun, technology: Technology, platform: str = None
) -> Tuple[Instrument, InstrumentModel]:
    """
    looks in the server to try to identify the instrument or instrument model matching info in the sniffed run
    returns a Tuple (Instrument, InstrumentModel) of which only one is not None
    """
    instrument: Instrument = None
    instrument_model: InstrumentModel = None
    # the user did not provided this info, let's see if we can look this up with sniffed info
    unknown_instrument = run.instrument
    if not unknown_instrument:
        logger.error("No instrument was set by the sniffer, you should provide on in the command line")
    instrument = _lookup_instrument_by_code(stocks_manager, code=unknown_instrument.serial_number)
    if not instrument:
        instrument = _lookup_instrument_by_name(stocks_manager, unknown_instrument.name, technology)

    # if still no instrument, we fall back on model
    if not instrument:
        logging.warning(f"No instrument registered for {unknown_instrument.as_simple_json()}.{os.linesep}"
                        f"We warmly recommend to register this instrument using the Web UI")
        instrument_model = _lookup_instrument_model_by_name(stocks_manager, unknown_instrument.model,
                                                            technology)
    if not instrument_model:
        logging.warning(f"No instrument model registered for {run.instrument.model} and technology "
                        f"{str(technology)}.{os.linesep}"
                        f"We warmly recommend to register this instrument model using the Web UI")
        # we use default model
        try:
            instrument_model = stocks_manager.list_instrument_models(technology=technology,
                                                                     platform="UNKNOWN")[0]
            logging.info(f"Will default to model: {os.linesep} {instrument_model.as_simple_json()}")
        except IndexError as e:
            # no model was returned!
            logging.fatal(f"No instrument model defined in server for technology={technology.value} and the"
                          f" platform=UNKNOWN. Such 'UNKNOWN' instrument models are expected for each "
                          f"technology. Please report this message to admin.")
            sys.exit(1)

    return (instrument, instrument_model)


def _lookup_instrument_model_by_uuid_or_name(stocks_manager: StocksManager, instrument_model_key: str,
                                             technology: Technology, platform: str = None) \
        -> InstrumentModel | None:
    """
    try to find an instrument by UUID, code or name (in this order) using provided key
    :param stocks_manager
    :param instrument_model_key a uuid, code or name. If None the method return None

    """
    if not instrument_model_key:
        return None

    instrument_model: InstrumentModel = None
    if is_uuid(instrument_model_key):
        data = stocks_manager.resolve(uuid=instrument_model_key)
        if not data:
            raise typer.BadParameter(f"UUID provided as --instrument-model is not valid for this server:"
                                     f" {instrument_model_key}")
        resolved_type: str = data['model_name']
        if resolved_type != "instrumentmodel":
            raise typer.BadParameter(f"UUID provided as --instrument-model '{instrument_model_key}' does not point "
                                     f"to a valid equipment model but to {resolved_type}")
        instrument_model = stocks_manager.fetch_instrument_model(data['id'])
        return instrument_model

    # look up by name
    results: List[InstrumentModel] = \
        stocks_manager.list_instrument_models(name=instrument_model_key, technology=technology, platform=platform)
    if results and len(results) == 1:
        instrument_model = results[0]
    elif results and len(results) > 1:
        raise typer.BadParameter(f"key provided as --instrument-model '{instrument_model_key}' matches"
                                 f" {len(results)} instrument models, please use UUID to disambiguate")
    else:
        raise typer.BadParameter(f"key provided as --instrument-model '{instrument_model_key}' did not match "
                                 f"any instrument, please use UUID to disambiguate")

    if instrument_model:
        logger.debug(f"Will use instrument model: {instrument_model.as_simple_json()}")
    return instrument_model


def _lookup_instrument_model_by_name(stocks_manager: StocksManager, name: str, technology: Technology
                                     ) -> Instrument | None:
    """
    only return an InstrumentModel if a unique instrument model matches this name for the given technology
    """
    if not name:
        return None

    results: List[InstrumentModel] = stocks_manager.list_instrument_models(name=name, technology=technology)
    if results and len(results) == 1:
        return results[0]

    return None


def _lookup_instrument_by_uuid_code_or_name(stocks_manager: StocksManager, instrument_key: str,
                                            technology: Technology | None = None) -> Instrument | None:
    """
    try to find an instrument by UUID, code or name (in this order) using provided key or raise a typer.BadParameter
    :param stocks_manager
    :param instrument_key a uuid, code or name. If None the method return None
    :param technology restrict look up on instrument linked to this technology (only relevant when looking up with name
     or code)

    """
    if not instrument_key:
        return None

    instrument: Instrument = None
    if is_uuid(instrument_key):
        data = stocks_manager.resolve(uuid=instrument_key)
        if not data:
            raise typer.BadParameter(f"UUID provided as --instrument is not valid for this server:"
                                     f" {instrument_key}")
        resolved_type: str = data['model_name']
        if resolved_type != "equipment":
            raise typer.BadParameter(f"UUID provided as --instrument '{instrument_key}' does not point "
                                     f"to an equipment item but to {resolved_type}")
        instrument = stocks_manager.fetch_equipment(data['id'])
        return instrument

    # look up by code
    results: List[Instrument] = stocks_manager.list_instruments(code=instrument_key, technology=technology)
    if not results:
        # look up by name
        results = stocks_manager.list_instruments(name=instrument_key, technology=technology)

    if results and len(results) == 1:
        instrument = results[0]
    elif results and len(results) > 1:
        raise typer.BadParameter(f"key provided as --instrument '{instrument_key}' matches {len(results)} "
                                 f"instruments, please use UUID to disambiguate")
    else:
        raise typer.BadParameter(f"key provided as --instrument '{instrument_key}' did not match any instrument"
                                 f", please use UUID to disambiguate")

    if instrument:
        logger.debug(f"Will use instrument: {instrument.as_simple_json()}")
    return instrument


def _lookup_instrument_by_code(stocks_manager: StocksManager, code: str) -> Instrument | None:
    """
    only return an Instrument if a unique instrument matches this code
    """
    if not code:
        return None

    results: List[Instrument] = stocks_manager.list_instruments(code=code)

    if results and len(results) == 1:
        return results[0]

    return None


def _lookup_instrument_by_name(stocks_manager: StocksManager, name: str, technology: Technology) -> Instrument | None:
    """
    only return an Instrument if a unique instrument matches this name for the given technology
    """
    if not name:
        return None

    results: List[Instrument] = stocks_manager.list_instruments(technology=technology, name=name)
    if results and len(results) == 1:
        return results[0]

    return None


def validate_protocol_ids(stocks_manager: StocksManager, protocol_ids: List[str]):
    """
    raises typer.BadParameter if any uuid is invalid
    """
    if protocol_ids:
        for pid in protocol_ids:
            if not is_uuid(pid):
                raise typer.BadParameter(f"Invalid protocol UUID: {pid}")
            try:
                x = stocks_manager.resolve(pid)
                logger.debug(x)
            except Exception as e:
                logger.error(f"Error:\n {traceback.format_exc()}")
                raise typer.BadParameter(f"Error when fetching protocol with UUID: {pid}")


@app.command(_CMD_REGISTER_DATASET_COLLECTION,
             help="Ingest a collection of 'derived' datasets from a folder located in the user's dropbox (or from "
                  "whitelisted paths for trusted submission sources like facilities). All "
                  "datasets are associated to a unique dataset collection and should be of the same type. "
                  "A typical use case is to load all the datasets generated by a workflow step i.e. all datasets "
                  "having the same type and sharing the workflow step parameters metadata. "
             )
def load_dataset_collection(
        in_dir: Path = typer.Option(
            ...,
            "--indir",
            "-i",
            help="Full path to the run directory containing the data to be registered. For security issues, this"
                 " directory must be in one of your dropboxes or a whitelisted pathes for trusted services (in which "
                 "case you must also be using a trusted account); else LabID will refused data ingestion."
        ),
        collection_name: str = typer.Option(
            ...,
            "--collection-name",
            "-c",
            help="The name of the dataset collection e.g. 'Aligned reads with STAR (bam)'"
                 "This parameter is also forwarded to the sniffer init (kwargs) with key 'collection_name'."
        ),
        study_id: str = typer.Option(
            ...,
            "--study",
            "-s",
            help="The UUID of the study to link the datasets to"
        ),
        pattern: str = typer.Option(
            "*",
            "--pattern",
            "-p",
            help="Name pattern e.g. '*.bam', DO NOT forget the simple quotes! "
                 "Every file/folder matching the pattern will be loaded as a new dataset."
                 "This parameter is also forwarded to the sniffer init (kwargs) with key 'pattern'."
        ),
        recursive: bool = typer.Option(
            False,
            "--recursive",
            "-r",
            help="Should we recursively look into sub-folders ?"
                 "This parameter is also forwarded to the sniffer init (kwargs) with key 'recursive'."
        ),
        search_files: bool = typer.Option(
            False,
            "--search-files/--ignore-files",
            help="Are we looking for files? When true, --search-folders must be False."
                 "This parameter is also forwarded to the sniffer init (kwargs) with key 'search_files'."
        ),
        search_folders: bool = typer.Option(
            False,
            "--search-folders/--ignore-folders",
            help="Are we looking for folders? When true, --search-files must be False."
                 "This parameter is also forwarded to the sniffer init (kwargs) with key 'search_folders'."
        ),
        dataset_type: str = typer.Option(
            "generic",
            "--type",
            "-t",
            help="Associates all found datasets (files or folders) to this dataset type. "
                 "This parameter is also forwarded to the sniffer init (kwargs) with key 'dataset_type'."
                 "Note that the dataset type is not the same as the dataset file format. For example a dataset of "
                 "type 'paired-end fastq' is composed of two files which format are 'fastq' "
        ),
        datafile_format: str = typer.Option(
            None,
            "--format",
            "-f",
            help="Associates all found dataset files to this format i.e. no matter their extension."
                 " For files, data file format defaults to the file extension e.g. 'bam' unless this option is given."
                 " This option is mandatory if --search-folders is used. "
                 " Note that the dataset type is not the same as the dataset file format. For example a dataset of "
                 "type 'paired-end fastq' is composed of two files which format are 'fastq' "
        ),
        protocol_ids: List[str] = typer.Option(
            None,
            "--protocol",
            help="Protocol UUID to link to each dataset. Multiple protocol UUID can be passed, in which case the order "
                 "on the command line is preserved in the corresponding protocol workflow "
                 "e.g. --protocol uuid1 --protocol uuid2 results in a Workflow:[uuid1, uuid2]"
        ),
        sniffer_name: str = typer.Option(
            "WildcardDatasetSniffer",
            "--sniffer_name",
            "-n",
            help="Name of the DatasetSniffer to use."
        ),
        sniffer_params: List[str] = typer.Option(
            None,
            "--sniffer_param",
            help="Optional sniffer parameters e.g. --sniffer_param 'name=blah' [--sniffer_param 'X=Z']. "
        ),
        transfer_whole_input_dir: bool = typer.Option(
            True,
            "--whole-dir_path/--dataset-only",
            help="Transfer the whole input dir_path. If false (--dataset-only), only datasets are imported i.e. other"
                 " files & folders will be ignored. Note that relative path to the input dir is always preserved unless"
                 "--flatten is passed."),
        dry_run: bool = typer.Option(
            True,
            "--dry/--no-dry",
            help="Dry run i.e. only tell what would be done."),

        conf_file_path: str = typer.Option(
            get_default_config_file_path(),
            "--config-path",
            help="Config file absolute path")
) -> None:
    """
    Register one or more non-raw datasets to LabID.
    """

    if not search_files and not search_folders:
        raise typer.BadParameter("Provide one of --search-files or --search-folders")

    if search_files and search_folders:
        raise typer.BadParameter(f"Cannot look for file and folder at the same time! "
                                 f"Give only one of --search-files or --search-folders")

    if search_folders and not datafile_format:
        raise typer.BadParameter(f"--format is mandatory with --search-folders option")

    _config = get_config(Path(conf_file_path))
    _username: str = _config[_config["default"]]["username"]

    client: StocksClient = StocksClient(_config)
    stocks_manager: StocksManager = StocksManager(client)

    logger.debug(in_dir)
    if not check_valid_directory(in_dir):
        raise typer.BadParameter(f"Invalid directory path: {str(in_dir)}")
    if not os.access(str(in_dir), os.R_OK):
        raise typer.BadParameter(f"Directory cannot be read: {str(in_dir)}")

    # Check given id is really a study
    invalid_study = False
    try:
        study = stocks_manager.fetch_study(study_id)
        if not study:
            invalid_study = True
    except HTTPException as e:
        logger.error(f"Error while fetching study with id {study_id}: {e.detail}")
        invalid_study = True

    if invalid_study:
        raise typer.BadParameter(f"No study returned for --study {study_id}")

    # check protocols; raise BadParameter if invalid
    validate_protocol_ids(stocks_manager, protocol_ids)

    the_groupname: str = None
    try:
        # validate dropbox and group
        # get dropboxes for user and that in_dir is in one of the user's dropbox
        dropboxes: dict[str, str] = stocks_manager.list_dropboxes(for_username=_username)

        # check in_dir is in a user's dropbox (also validating the user belong to the group)
        for g_name, box_path in dropboxes.items():
            if str(in_dir).startswith(os.path.abspath(box_path) + os.sep):
                the_groupname = g_name
    except Exception:
        logger.error(f"Error:\n {traceback.format_exc()}")
        logger.error(f"An unexpected error occurred, please check error logs.")
        exit(1)

    if not the_groupname:
        dropboxes_multiline = ""
        for k, v in dropboxes.items():
            dropboxes_multiline = dropboxes_multiline + f"{k} -> {v}" + os.linesep
        raise typer.BadParameter(f"You are trying to load data that is not in any of your Dropbox. "
                                 f"This is not supported, please move the data in one of your dropbox first : {os.linesep}"
                                 f"{dropboxes_multiline}")

    # init the sniffer or fails
    # we need the sniffer params as a map to add the command line params into it
    sniffer_params_map = {}
    if sniffer_params:
        sniffer_params_map = parse_sniffer_params(sniffer_params)
    sniffer_params_map['collection_name'] = collection_name
    sniffer_params_map['pattern'] = pattern
    sniffer_params_map['recursive'] = recursive
    sniffer_params_map['dataset_type'] = dataset_type
    sniffer_params_map['search_files'] = search_files
    sniffer_params_map['search_folders'] = search_folders

    sniffer: DatasetSniffer = create_sniffer(
        sniffer_name=sniffer_name,
        sniffer_params=sniffer_params_map,
        stocks_manager=stocks_manager)

    if not isinstance(sniffer, DatasetSniffer):
        raise NotImplemented

    try:
        result: List[Dataset] | List[DatasetCollection] = sniffer.sniff_datasets(
            dir_path=in_dir,
            group=the_groupname)

        if not result:
            raise typer.BadParameter(f"Could not find a single datasets. Please check your input!")

        collections: List[DatasetCollection] = result
        if isinstance(result[0], Dataset):
            # we got datasets only, create the collection with command line options
            collection = DatasetCollection(name=collection_name,
                                           description=f"Datasets batch loaded from {str(in_dir)}")
            collection.datasets = result
            for d in collection.datasets:
                d.collection = collection
            collections = [collection]
        elif len(result) == 1:
            # we have a single datasetcollection, we force the name to be the given one
            collections[0].name = collection_name
        else:
            # we have multiple collections returned, we only set collection name if empty
            i = 1
            for c in collections:
                if not c.name:
                    c.name = collection_name + " " + i
                    i = i+1

        for c in collections:
            if not c.datasets:
                raise typer.BadParameter(f"No dataset in collection! Please check your input!")
            if datafile_format:
                d: Dataset = None
                for d in c.datasets:
                    for df in d.datafiles:
                        df.filetype = datafile_format

        res = stocks_manager.register_derived_datasets(
            collections=collections,
            run_dir=in_dir,
            username=_username,
            transfer_whole_input_dir=transfer_whole_input_dir,
            study=study,
            group=the_groupname
        )
        o: PyDatasetListPost = PyDatasetListPost(**res)

        if protocol_ids:
            for _d in o.datasets:
                resp = stocks_manager.add_protocols_to_dataset(protocol_ids=protocol_ids, dataset_id=_d.id)
                logger.debug(resp)

        logger.warning(f"Successfully submitted {len(o.datasets)} dataset(s) as {len(collections)} collection(s). "
                       f"An email will notify you when data ingestion has completed. Until then, please do NOT touch "
                       f"the data in {str(in_dir)}.")
        # below code is an example how to set dataset parent
        # parent_ids = []
        # if parent_ids:
        #     resp = stocks_manager.add_parents_to_dataset(parent_ids=parent_ids, dataset_id=_d.id)
        #     logger.debug(resp)

    except Exception:
        logger.error(f"Error:\n {traceback.format_exc()}")
        logger.error(f"An unexpected error occurred, please check error logs.")
        exit(1)

def _create_or_grab_previously_created_assay(
        assay: Assay,
        assay_run: InstrumentRun,
        stocks_manager: StocksManager,
        potential_ids: Set[str] | None = None,
        state: ObjectState = ObjectState.NEW,
        allow_datasets: bool = False,
        check_run_dir_path: bool = False,
) -> str:
    """
    This method creates the given assay after checking it does exist to prevent the accumulation of orphan assays in
    the server (created before the CLI fails for some reason).
    The logic is to look for an assay with identical name and state. Optionally conditions on the existing assay's
    run_dir, id or dataset number can be enabled

    :param assay: the assay to create, if nto existing
    :param potential_ids: an optional set of assay UUIDs to restrict the lookup to
    :param state: state of the assays for look up
    :param allow_datasets: an existing assay is only reused if no dataset is connected to it, unless allow_datasets
    :param check_run_dir_path: an existing assay is only reused if its run_dir matches the assay's one when
        check_run_dir_path is True AND assay.run_dir is set.
    """

    existing_assay: Assay = _grab_previously_created_assay(
        assay=assay, stocks_manager=stocks_manager, potential_ids=potential_ids, state=state,
        allow_datasets=allow_datasets, check_run_dir_path=check_run_dir_path)
    if existing_assay:
        logger.info(f"Found existing assay to re-use {existing_assay.name} [{existing_assay.id}] ")
        return existing_assay.id

    # ok we create a new one then
    logger.debug(f"Creating new assay {assay.name}...")
    _id = stocks_manager.save_assay(assay=assay, instrument_run=assay_run)
    logger.info(f"Assay {assay.name} created with id {_id}")
    return _id


def _grab_previously_created_assay(
        assay: Assay,
        stocks_manager: StocksManager,
        potential_ids: Set[str] | None = None,
        state: ObjectState = ObjectState.NEW,
        allow_datasets: bool = False,
        check_run_dir_path: bool = False,
) -> Assay | None:
    """
    This method checks if the assay was already created in the server (created before the CLI fails for some reason).
    The logic is to look for an assay with identical name and state. Optionally conditions on the existing assay's
    run_dir, id or dataset number can be enabled

    :param assay: the assay to create, if not existing
    :param potential_ids: an optional set of assay UUIDs to restrict the lookup to
    :param state: state of the assays for look up
    :param allow_datasets: an existing assay is only reused if no dataset is connected to it, unless allow_datasets
    :param check_run_dir_path: an existing assay is only reused if its run_dir matches the assay's one when
        check_run_dir_path is True AND assay.run_dir is set.
    """
    logger.info(f"Looking for assay with name {assay.name} with state {state} (type: {assay.stocks_model_type})")
    run_dir_path: str | None = None
    if check_run_dir_path and assay.run_dir:
        run_dir_path = str(assay.run_dir)

    _assays = stocks_manager.list_assays(name=assay.name, state=state)
    if not _assays:
        return None

    logger.info(f"got {len(_assays)}")

    # make sure it is the same name (query uses 'contains' I think ) and that no datasets are
    # assoc to the assay unless allow_datasets
    if potential_ids:
        for _a in _assays:
            # if we gave a run_dir then it must match, else we pass
            if run_dir_path and (not _a.run_dir or _a.run_dir != run_dir_path):
                continue
            if _a.name == assay.name and _a.id in potential_ids:
                logger.info(f"Assay {assay.name} [{_a.id}] in state {state.value} already exist...could re-using it")
                return _a

    # potential ids approach did not work or disabled
    for _a in _assays:
        # if we gave a run_dir then it must match, else we pass
        if run_dir_path and (not _a.run_dir or _a.run_dir != run_dir_path):
            continue
        if (_a.name == assay.name and
                _a.stocks_model_type == assay.stocks_model_type and
                (allow_datasets or not _a.datasets)):
            logger.info(f"Assay {assay.name} in state {state.value} already exists...re-using {_a.id}")
            return _a

    # assay does not seem to exist in the server
    return None

def _batch_register_for_assaysniffer(sniffer: AssaySniffer, sniffer_name: str, dir_path: str | Path,
                                     job: Job, db_handler: DatabaseHandler, stocks_manager: StocksManager,
                                     logged_in_user: User,
                                     instrument: Instrument | None, instrument_model: InstrumentModel | None,
                                     data_owner: str, stocks_group_name: str,
                                     allow_pooled_samples: bool, transfer_whole_input_dir: bool,
                                     study: Study, dry_run: bool, assay_protocol_ids: List[str] | None = None):
    """
    every error raises an exception, calling code should catch and react appropriately
    """
    if isinstance(dir_path, str):
        dir_path = Path(dir_path)
    try:
        runs = sniffer.sniff_instrument_run_assays(
            dir_path=dir_path, username=data_owner, group=stocks_group_name)
        logger.debug(f"Sniffer {sniffer_name} returned {len(runs)} run")
    except AssayStructureError as err:
        raise AssayStructureError(f"Data in {str(dir_path)} doesn't comply to sniffer {sniffer_name}: {str(err)}")

    if not runs or len(runs) == 0:
        raise AssayStructureError(f"The sniffer '{sniffer_name}' did not detect assay(s) in {str(dir_path)} ")

    if len(runs) > 1:
        raise AssayStructureError(
            f"Sniffer '{sniffer_name}' detected {len(runs)} instrument runs in the directory"
            f" {str(dir_path)}. Loading multiple runs at once is not supported; please rearrange folder to "
            f"contain results for a unique run")

    # OK! we have one run back from folder
    # we first need to register the assays and for this, we need to have a run or a model
    the_run: InstrumentRun = runs[0]
    # do we need to look up for instrument (ie user did not pass this as option) ?
    if not instrument and not instrument_model:
        instrument, instrument_model = _fetch_instrument_or_model_info_from_sniffed_data(
            stocks_manager=stocks_manager, run=the_run, technology=sniffer.get_supported_technology())
    # at this point we must have an instrument or a model
    if instrument:
        the_run.instrument = instrument
    elif instrument_model:
        # we force the instrument model and there is no proper instrument_run for assays
        the_run.instrument = None
        for assay in the_run.assays:
            assay.instrumentrun = None
            assay.instrumentmodel = instrument_model

    existing_run: SimpleInstrumentRun = None
    if not dry_run:
        logger.info(f"will register assay(s) from run named {the_run.name}")
        # let s create a run to use for all assays if needed (if instrument...)
        if instrument:
            # check if we have this run already. This could happen if eg (1) assays are loaded separately while
            # from the same run or (2) when re-running code after failure (with run created before).
            logger.info(f"looking up for existing run named {the_run.name}")
            existing_runs: List[InstrumentRun] = stocks_manager.list_instrument_runs(
                name=the_run.name, instrument_name=instrument.name, owner=logged_in_user.username)
            if existing_runs and len(existing_runs) > 0:
                existing_run = existing_runs[0]
                logger.info(f"re-using run {existing_run.id}")
            else:
                logger.info("creating new run")
                # we dont pass data_owner as new object can only be created by logged in user
                existing_run = stocks_manager.save_instrument_run(
                    name=the_run.name, instrument=instrument, description=the_run.description,
                    start_datetime=the_run.start_datetime, end_datetime=the_run.end_datetime,
                    producer=the_run.producer
                )
        # now we save all assays to the run, sets their id
        # in case of processing after failure, we may already have saved this assay before...check!
        job_assay_ids: Set[str] = set()
        if job.assay_id:
            job_assay_ids = set(job.assay_id.split(","))
        restored_assay_ids = job_assay_ids.copy()
        for assay in the_run.assays:
            assay.id = _create_or_grab_previously_created_assay(
                assay=assay, assay_run=existing_run, stocks_manager=stocks_manager,
                potential_ids=restored_assay_ids)
            job_assay_ids.add(assay.id)
            # update the job info
            job.assay_id = ",".join(job_assay_ids)
            db_handler.update_job(job)

            # patch protocols
            if assay_protocol_ids:
                stocks_manager.add_protocols_to_assay(protocol_ids=assay_protocol_ids, assay_id=assay.id)

        logger.info(f"Creating datasets, samples and connecting to the assay...")
        return stocks_manager.register_raw_assay_datasets(
            instrument_run=runs[0], run_dir=dir_path,
            username=data_owner, unixgroup=stocks_group_name,
            allow_pooled_samples=allow_pooled_samples, transfer_whole_input_dir=transfer_whole_input_dir,
            study=study)

    else:
        o: PyDatasetListPost = stocks_manager._create_instrument_run_post(
            instrument_run=runs[0], run_dir=dir_path,
            owner=data_owner, owned_by_group=stocks_group_name,
            allow_pooled_samples=allow_pooled_samples, transfer_whole_input_dir=transfer_whole_input_dir,
            study_id=study.id, fail_on_missing_assay_id=False)
        logger.info(f">>>> Directory {dir_path}  <<<<<< Would submit JSON payload (use debug mode to see it)")
        logger.debug("\n" + o.json(exclude_none=True, exclude_unset=False))
