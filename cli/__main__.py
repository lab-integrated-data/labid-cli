import logging
from enum import Enum
from typing import Optional
import typer
from cli import __app_name__, __version__, get_sniffer_plugin_dir_list, is_dma_plugin_enabled, get_dma_plugin_api_url, \
    set_dma_plugin_options
from cli import assays, config, export, get, register, validate, job, transfer
from stocks.assaysniffer.registry import registry


class LogLevel(str, Enum):
    DEBUG = "DEBUG"
    INFO = "INFO"
    WARNING = "WARNING"
    ERROR = "ERROR"
    CRITICAL = "CRITICAL"


# The main app
app = typer.Typer()
# app the different module, each one file in the cli package
app.add_typer(assays.app, name="assays", help="Manage assays")
app.add_typer(config.app, name="config", help="Create and manage your connection configurations")
app.add_typer(export.app, name="export", help="Export links and dataset metadata")
app.add_typer(get.app, name="get", help="Get items of any types as JSON representation")
app.add_typer(register.app, name="register", help="Register raw and derived datasets")
app.add_typer(validate.app, name="validate", help="Validate 'initialized' assays")
app.add_typer(job.app, name="job", help="Manage jobs")
app.add_typer(transfer.app, name="transfer", help="Transfer assay data to LabID library with assay pre-registration")

# init the sniffer registry with potential plugins
for p in get_sniffer_plugin_dir_list():
    registry.load_custom_plugins_from_plugin_base_dir(p)


# a method to print version and exit directly
def _version_callback(value: bool) -> None:
    if value:
        typer.echo(f"{__app_name__} v{__version__}")
        raise typer.Exit()

def _init_logging(log_level: LogLevel, log_file: str | None = None ):
    if log_file:
        logging.basicConfig(
            filename=log_file,
            filemode='a',
            level=str(log_level.value),
            format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S'
        )
    else:
        logging.basicConfig(
            level=str(log_level.value),
            format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S'
        )

@app.callback()
def add_global_options_callback(
        log_level: LogLevel = typer.Option(
            "WARNING",
            "--log-level",
            case_sensitive=False,
            help="Sets the verbose level"),
        log_file: str = typer.Option(
            None,
            "--log-file",
            help="file path to redirect logging events (by default logging goes to stderr)"),
        version: Optional[bool] = typer.Option(
            None,
            "--version",
            "-v",
            help="Show the application's version and exit.",
            callback=_version_callback,
            is_eager=True
        )
) -> None:
    _init_logging(log_level=log_level, log_file=log_file)

if is_dma_plugin_enabled():
    @app.callback()
    def add_global_options_callback(
            log_level: LogLevel = typer.Option(
                "WARNING",
                "--log-level",
                case_sensitive=False,
                help="Sets the verbose level"),
            log_file: str = typer.Option(
                None,
                "--log-file",
                help="file path to redirect logging events (by default logging goes to stderr)"),
            dma_url: str = typer.Option(
                get_dma_plugin_api_url(),
                "--dma-url",
                help=f"DMA API Server URL"),
            dma_scope: str = typer.Option(
                "User",
                "--dma-scope",
                help="The DMA scope to use to login the DMA e.g. GBCS or GeneCore. Your user must of course be part "
                     "of the given scope"),
            version: Optional[bool] = typer.Option(
                None,
                "--version",
                "-v",
                help="Show the application's version and exit.",
                callback=_version_callback,
                is_eager=True
            )
    ) -> None:
        _init_logging(log_level=log_level, log_file=log_file)
        set_dma_plugin_options(url=dma_url, scope=dma_scope)


def main():
    try:
        app(prog_name=__app_name__)
    except Exception as err:
        logging.exception("An unexpected error occurred during execution")
        exit(1)


if __name__ == "__main__":
    main()
