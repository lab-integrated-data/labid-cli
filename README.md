# LabID Python Client

This repo contains a command line interface (CLI) as interface to the Lab Integrated Data (LabID) API.

## Development

How we set this up globally at embl, see https://git.embl.de/grp-gbcs/stocks-cli/-/issues/36 and https://git.embl.de/grp-gbcs/stocks-cli/-/issues/55 for update


```bash
# create a virtualenv/conda env 
conda create -n labid-cli python=3.10.12 && conda activate labid-cli

git clone https://git.embl.de/grp-gbcs/labid-cli.git
cd labid-cli
pip install -e .

# Display global help i.e. list modules
labid --help
# Display module help i.e. list modules commands
labid <module_name> --help 
# Display command help 
labid <module_name> <command_name> --help
```

## Deployment
We've developed some awesome features and they're all pushed to the `main` branch. Now, we're ready to deploy
these changes in production i.e. pypi.org. We're gonna follow these steps to do so -

1. Merge everything to `staging` 
```bash
# considering we're in "main" branch and make sure everything's up to date
$ git pull
$ git checkout staging
$ git merge main
```
2. Update the version under `cli/version.py`, commit and push
```bash
# Update version in version.py (cli module) following the YY.MM.# format with # a number starting from 1 
$ git commit -am "Bump version to v24.1.1"
$ git push
```
2.1. Follow the [pipeline](https://git.embl.de/grp-gbcs/labid-cli/-/pipelines) and wait till it's passed
3. Merge everything to `production` 
```bash
$ git checkout production
$ git merge staging
```
4. Create a tag by the version 
```bash
# change the version according to the latest that you put in setup.py
$ git tag -a "v24.1.1" -m "v24.1.1"
```
5. Push everything to `production`
```bash
$ git push && git push --tags
```
6. Follow the [pipeline](https://git.embl.de/grp-gbcs/labid-cli/-/pipelines) 🚀

7. Merge back everything
```bash
$ git checkout main
$ git merge staging
$ git push
```

## Configure
To use the CLI, you must first setup a user configuration file containing important connection information; this is by 
default stored in your home (`/my/home/.labid/labidapi.yml`).

For setup the LabID server to connect to, we use the `config` sub-module.  

Note for developers: you can set more than one server, and switch between those as needed

```bash
# Calling config setup will prompt you with defaults offered
labid config setup
# Alternatively one can provide values in the command line, your pwd will be prompted
labid config setup --api-url https://stocks.embl.de --labid_user <username> --group-name <group>

# You can check the content of your config with 
labid config show
# Or reset (wipe it altogether)
labid config clean
# Or switch to another configuration i.e. a locally running server or a test server (useful for developpers)
labid config switch --server-api-url http://127.0.0.1:8000/

```

## Global properties
Other configuration files are available to globally configure properties common to a LabID server 
- `cli/labidcli.ini` (email, institution name...)
- `cli/log_config.ini` (loggers)

## The LabID modules 

You can learn all available modules using `labid --help` and module commands using `labid <module> --help`


## Going further with `jq`

`jq` can be used to format and parse the returned json. 
Please note that these commands also now have `labid` commands  

### Fetch a list of datafile locations for a study

and format to a table for further processing

```bash
labid list datafilecopies study_uuid | jq -r '.[]|[.shortname, .uri, .readtype] | @tsv'
```

### Create symbolic links to files stored in LabID

A similar approach as above can be used to easily make symbolic links to the files in the current directory
The last xargs will prompt for every symbolic link that is going to be made due to the `-p`.
```bash
labid list datafilecopies study_uuid | jq -r '.[]|[.uri, .shortname] | @tsv' | xargs -p -n 2 ln -s
```

### Fetch all datafile copies belonging to a flowcell

and list them as a table, including the datafilecopy checksum and the status.

```bash
labid list assay --query_param "flowcell=000000000-AUG58" --query_param "fields=id" | jq -r '.[]|[.id] | @tsv' | xargs -I{} -n 1 stocks list datafilecopies {} | jq -r '.results[]|[.uri, .checksum, .status.value] | @tsv'
```


## For Developers
NB: `STOCKS` is the former name of LabID, code was not changed to reflect this renaming

The tool is organized in packages:
- The `cli` module defines the different command line modules and commads
   - The entry point is the `cli/__main__.py` at the root
- The `stocks` module defines the LabID object model 
- The `stocks.assaysniffer` module defines the plugin framework for assay sniffers (see below)
   - New sniffers should be dropped in the `plugins` directory   
- The `stocksapi` module defines the LabID API and exposes it to the rest of the application via the `StocksManager`
   - The `StocksManager` accepts objects from the `stocks` object model
   - Internally, the `StocksManager` uses a low level `StocksClient` and encode/decode the API requests/responses using `Pydantic` objects
   - Pydantic objects should not be exposed to the rest of the app; they are only short term data objects 
- The `plugins` module contains sniffers that we ship with the main code to be used elsewhere and/or demonstrate how to write sniffers
- The `test` module regroups the unit tests
- The `data` folder contains data files either used for tests or actually used by the code 


### Assay Sniffer Framework & Plugin
t.b.d
