import os
import unittest
from typing import List

import pandas as pd
from io import StringIO, BytesIO

import cli
from cli.export import add_default_submitter, \
    create_protocol_ref, extract_final_annotations, create_annofactor_df, create_idf_design
from cli.export_utils import create_idf_users, create_idf_protocol, stocks_factor_format_name, check_assay_type, \
    arrayexpress_export_to_dataframe
from stocks.models import Study, UserMember, Protocol, OntologyTerm, Ontology, Assay, AnnotationType, StocksCVTerm, \
    User, SequencingAssay, InstrumentRun, Instrument
from cli.utils import UserRole, Technology


class MyExportTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        os.makedirs('../data/out_ignore', exist_ok = True)

    def test_arrayexpress_curating(self):
        # create test_table
        table = bytes(
            """ref,Organism,Organism (Annotation)
        val1,None,mouse
        val2, ,mouse
        val3,    ,mouse
        val4,,mouse
        val5,human,None
        val6,human,
        val7,human, 
        val8,None,
        val9,,None
        val10,human,mouse
        val11,human,mouse
        """, 'utf-8')
        expected_res = bytes(
            """ref,Organism
        val1,mouse
        val2,mouse
        val3,mouse
        val4,mouse
        val5,human
        val6,human
        val7,human
        val8,
        val9,None
        val10,human
        val11,human
        """, 'utf-8')

        expected_res = pd.read_table(BytesIO(expected_res), dtype=str, sep=',',
                                     keep_default_na=False).fillna('').astype(str)

        try:
            result = arrayexpress_export_to_dataframe(table)
        except Exception as err:
            self.fail(str(err))
        self.assertIsNotNone(result)
        self.assertTrue(isinstance(result, pd.DataFrame))
        self.assertTrue("Organism" in result.columns)
        self.assertTrue("ref" in result.columns)
        self.assertFalse("Organism (Annotation)" in result.columns)
        self.assertTrue(expected_res.equals(result))

    def test_add_default_submitter(self):
        # Test depends on configuration file
        empty_study = Study("testname", "testdescription")

        # Insures study initial state
        self.assertIsNotNone(empty_study)
        self.assertTrue(empty_study.user_roles is None)

        try:
            add_default_submitter(empty_study)
            user = list(empty_study.user_roles.values())[0]
        except Exception as err:
            self.fail(str(err))

        self.assertTrue(len(empty_study.user_roles) == 1)
        self.assertTrue(user.email == cli.get_default_submitter_email())

    def test_add_user(self):
        empty_study = Study("testname", "testdescription")
        user1 = UserMember(user=User(username="name1"), roles={UserRole.SUBMITTER})

        # Insure study initial state
        self.assertIsNotNone(empty_study)
        self.assertTrue(empty_study.user_roles is None)

        # Tests if user is added
        try:
            empty_study.add_user(user1)
        except Exception as err:
            self.fail(str(err))

        self.assertTrue(len(empty_study.user_roles.keys()) == 1)
        self.assertTrue(empty_study.user_roles['name1'].roles == {UserRole.SUBMITTER})
        self.assertTrue(empty_study.user_roles['name1'].username == 'name1')

        # Tests if new user appends
        user2 = UserMember(user=User(username="name2"), roles={UserRole.INVESTIGATOR})
        try:
            empty_study.add_user(user2)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(len(empty_study.user_roles.keys()) == 2)
        self.assertTrue(empty_study.user_roles['name2'].roles == {UserRole.INVESTIGATOR})
        self.assertTrue(empty_study.user_roles['name2'].username == 'name2')

        # Tests if UserMember instance with same full_name only updates roles and not append new user on list
        user3 = UserMember(user=User(username="name1"), roles={UserRole.SUBMITTER, UserRole.INVESTIGATOR})
        try:
            empty_study.add_user(user3)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(len(empty_study.user_roles.keys()) == 2)
        self.assertTrue(empty_study.user_roles['name1'].roles == {UserRole.SUBMITTER, UserRole.INVESTIGATOR})
        self.assertTrue(empty_study.user_roles['name1'].username == 'name1')

    def test_check_assay_type(self):
        assay1 = Assay(name='assay_name_1', technology=Technology.SEQUENCING, platform="ILLUMINA")
        assay2 = Assay(name='assay_name_2', technology=Technology.SEQUENCING, platform="ILLUMINA")
        assay3 = Assay(name='assay_name_3', technology=Technology.SEQUENCING, platform="OTHER")
        assay_list = [assay1, assay2]

        # Tests if returns technology type if all types are the same.
        try:
            res = check_assay_type(assay_list)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(res == "ILLUMINA")

        # Tests if returns None if types differ
        assay_list = [assay1, assay2, assay3]
        try:
            res = check_assay_type(assay_list)
        except Exception as err:
            self.fail(str(err))
        self.assertFalse(res)

        # Tests if raises ValueError if empty list
        with self.assertRaises(ValueError) as context:
            res = check_assay_type([])
        self.assertTrue("Error with assay list: list null or empty." in str(context.exception))

    def test_extract_annofactor_list(self):
        # Tests annotation and factor list creation
        table = StringIO(
            """samples,Sample[annotation_name_1],Sample[annotation_name_4],notannotation,Sample[annotation_name_2],Dataset[annotation_name_2]
        sample1,dog,1,x,fluffy,more fluffy
        sample2,dog,2,x,fluffy,more fluffy
        """)
        df = pd.read_table(table, dtype=str, sep=',').fillna('').astype(str)
        annotation1 = AnnotationType(name='annotation_name_1')
        annotation2 = AnnotationType(name='annotation_name_2')
        annotation3 = AnnotationType(name='annotation_name_3')
        annotation4 = AnnotationType(name='annotation_name_4')
        all_annotations = [annotation4, annotation2, annotation3]
        expected_annotation_dic = {
            "annotation_name_1": {"obj": None, "is_factor": False, "data": ["dog", "dog"]},
            "annotation_name_4": {"obj": annotation4, "is_factor": True, "data": ["1", "2"]},
            "annotation_name_2": {"obj": annotation2, "is_factor": False, "data": ["more fluffy", "more fluffy"]}
        }

        try:
            annotation_dic = extract_final_annotations(df, all_annotations)
        except Exception as err:
            self.fail(str(err))
        self.assertIsNotNone(annotation_dic)
        self.assertTrue(annotation_dic == expected_annotation_dic)

    def test_create_annofactor_df(self):
        annotation1 = AnnotationType(name='annotation_name_1')
        annotation2 = AnnotationType(name='annotation_name_2')
        annotation3 = AnnotationType(name='annotation_name_3')
        annotation4 = AnnotationType(name='annotation_name_4')
        annotations = [annotation1, annotation4, annotation2]
        factors = ['annotation_name_4']
        annotation_dic = {
            'annotation_name_1': {'data': ['dog', 'dog'], 'obj': None, 'is_factor': False},
            'annotation_name_4': {'data': ['1', '2'], 'obj': annotation4, 'is_factor': True},
            'annotation_name_2': {'data': ['more fluffy', 'more fluffy'], 'obj': annotation2, 'is_factor': False}
        }
        table_res_factors = StringIO(
            """Factor Value[annotation_name_4]
        1
        2
        """)
        expected_factors = pd.read_table(table_res_factors, skipinitialspace=True,
                                         dtype=str, sep=',').fillna('').astype(str)
        table_res_ann = StringIO(
            """Characteristics[annotation_name_1],Characteristics[annotation_name_4],Characteristics[annotation_name_2]
        dog,1,more fluffy
        dog,2,more fluffy
        """)
        expected_annotations = pd.read_table(table_res_ann, skipinitialspace=True,
                                             dtype=str, sep=',').fillna('').astype(str)

        try:
            annotation_df_res, factor_df_res = create_annofactor_df(annotation_dic)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(annotation_df_res.equals(expected_annotations))
        self.assertTrue(factor_df_res.equals(expected_factors))

    def test_create_idf_protocol(self):
        onto_term1 = OntologyTerm(name='onto_term_name1', term_id='onto_term_id1', ontology=Ontology(name='onto1'))
        protocol1 = Protocol(name='protocol1', protocol_type=onto_term1, summary='protocol_summary1')
        run1 = InstrumentRun(name='instrument_run_name1', managed=True, technology=Technology.SEQUENCING,
                             platform="ILLUMINA", instrument=Instrument(name='instrument_name1',
                                                                               model='instrument_model1'))
        seq_assay1 = SequencingAssay(name='seq_assay_name1', flowcell='seq_assay_flowcell1',
                                     id='id1', instrumentrun=run1)

        table_res = StringIO(
            """header,0,0
        Protocol Name,Standard instrument_model1 single-end Sequencing,protocol1
        Protocole Type,nucleic acid sequencing protocol,onto_term_name1
        Protocol Description,single-end sequencing on illumina instrument_model1,protocol_summary1
        Protocol Hardware,instrument_model1,
        Protocol Term Source REF,EFO,onto1
        Protocol Term Accession Number,EFO_0004170,onto_term_id1
        Protocol Comment[stocks_uuid],id1,
        """)
        expected_res = pd.read_table(table_res, skipinitialspace=True, dtype=str, sep=',').fillna('').astype(str)
        expected_res.columns = ['header', 0, 0]

        # Positive test
        try:
            df_res = create_idf_protocol([protocol1], [seq_assay1])
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(df_res.equals(expected_res))

        # Test with assay and protocol instances containing only required fields
        min_onto = OntologyTerm(name='onto_term_name2', term_id='onto_term_id2', ontology=Ontology(name='onto2'))
        min_protocol = Protocol(name='protocol2', protocol_type=min_onto)
        min_seq_assay = SequencingAssay(name='seq_assay_name2', flowcell='seq_assay_flowcell2')

        table_res = StringIO(
            """header,0,0
        Protocol Name,ERROR: No instrument run found,protocol2
        Protocole Type,,onto_term_name2
        Protocol Description,,ERROR: no summary was found
        Protocol Hardware,,
        Protocol Term Source REF,,onto2
        Protocol Term Accession Number,,onto_term_id2
        Protocol Comment[stocks_uuid],,
        """)
        expected_res = pd.read_table(table_res, skipinitialspace=True, dtype=str, sep=',').fillna('').astype(str)
        expected_res.columns = ['header', 0, 0]

        try:
            df_res = create_idf_protocol([min_protocol], [min_seq_assay])
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(df_res.equals(expected_res))

    def test_create_idf_users(self):
        user1 = UserMember(user=User(username='user1', last_name='last_name1', first_name='first_name1',
                                     middle_name='mn1', email='email1'),
                           roles={UserRole.SUBMITTER})
        study = Study(name='study_name')

        expected_table = StringIO("""
        header,0
        Person Last Name,last_name1
        Person First Name,first_name1
        Person Middle Name,mn1
        Person Email,email1
        Person Affiliation,
        Person Roles,submitter
        """)
        expected_res = pd.read_table(expected_table, skipinitialspace=True,
                                     dtype=str, sep=',').fillna('').astype(str)
        expected_res.columns = ['header', 0]

        try:
            study.add_user(user1)
            df_res = create_idf_users(study)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(df_res.equals(expected_res))

    def test_create_idf_design(self):
        ontology_efo = Ontology(name='EFO')
        ontology_other = Ontology(name='OTHER')
        onto_term_1 = OntologyTerm(name='name_onto_term_1', term_id='EFO_onto_term_1', ontology=ontology_efo)
        onto_term_2 = OntologyTerm(name='name_onto_term_2', term_id='EFO_onto_term_2', ontology=ontology_efo)
        onto_term_3 = OntologyTerm(name='name_onto_term_3', term_id='OTHER_onto_term_3', ontology=ontology_other)
        stocks_cv_term_1 = StocksCVTerm(name=onto_term_1.name, ontology_mappings={ontology_efo.name: onto_term_1})
        stocks_cv_term_2 = StocksCVTerm(name=onto_term_2.name, ontology_mappings={ontology_other.name: onto_term_2,
                                                                                  ontology_efo.name: onto_term_2})
        stocks_cv_term_3 = StocksCVTerm(name=onto_term_3.name, ontology_mappings={ontology_other.name: onto_term_3})
        experimental_design_terms: List[StocksCVTerm] = [stocks_cv_term_1, stocks_cv_term_2, stocks_cv_term_3]
        study = Study(name='study_name', description='study description',
                      experimental_design_terms=experimental_design_terms)

        # First column positive test
        # Second column tests if selected ontology is EFO if several exist
        # Third column tests if other ontology is taken if not EFO.
        # Header is an artefact of manipulation and gets discarded later in the code.
        table_res = StringIO(
            """header,0,0,0
        Experimental Design,name_onto_term_1,name_onto_term_2,name_onto_term_3
        Experimental Design Term Source REF,EFO,EFO,OTHER
        Experimental Design Term Accession Number,EFO_onto_term_1,EFO_onto_term_2,OTHER_onto_term_3
        """)
        expected_res = pd.read_table(table_res, skipinitialspace=True, dtype=str, sep=',').fillna('').astype(str)
        expected_res.columns = ['header', 0, 0, 0]
        try:
            res = create_idf_design(study)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(res.equals(expected_res))

    def test_create_protocol_ref(self):

        onto_term1 = OntologyTerm(name='onto1', term_id='onto_id1', ontology=Ontology(name=''))
        protocol1 = Protocol(name='protocol1', id='protocol_id1', protocol_type=onto_term1)
        protocol2 = Protocol(name='protocol2', id='protocol_id2', protocol_type=onto_term1)
        onto_term2 = OntologyTerm(name='onto2', term_id='onto_id2', ontology=Ontology(name=''))
        onto_term3 = OntologyTerm(name='onto3', term_id='onto_id3', ontology=Ontology(name=''))
        protocol3 = Protocol(name='protocol3', id='protocol_id3', protocol_type=onto_term3)
        onto_term4 = OntologyTerm(name='onto4', term_id='onto_id4', ontology=Ontology(name=''))
        protocol4 = Protocol(name='protocol4', id='protocol_id4', protocol_type=onto_term4)
        protocol0 = Protocol(name='protocol0', id='protocol_id0', protocol_type=onto_term1)
        protocol5 = Protocol(name='protocol5', id='protocol_id5', protocol_type=onto_term2)
        protocol6 = Protocol(name='protocol6', id='protocol_id6', protocol_type=onto_term2)
        protocol7 = Protocol(name='protocol7', id='protocol_id7', protocol_type=onto_term2)
        protocol8 = Protocol(name='protocol8', id='protocol_id8', protocol_type=onto_term2)
        protocol_list = [protocol1, protocol2, protocol3, protocol4, protocol0,
                         protocol5, protocol6, protocol7, protocol8]

        # Tests if different protocols of same type only create one column if they arent used on the same sample.
        input_table = StringIO("""Samples;Protocols
        sample1;protocol_id2,protocol_id3,protocol_id4
        sample2;protocol_id1,protocol_id3,protocol_id4
        sample3;protocol_id1,protocol_id3,protocol_id4
        """)
        df = pd.read_table(input_table, sep=';', dtype=str).fillna('').astype(str)

        expected_table = StringIO("""Protocol REF;Protocol REF;Protocol REF
        protocol2;protocol3;protocol4
        protocol1;protocol3;protocol4
        protocol1;protocol3;protocol4
        """)
        expected_res = pd.read_table(expected_table, sep=';', skipinitialspace=True).fillna('').astype(str)
        expected_res.columns = ['Protocol REF', 'Protocol REF', 'Protocol REF']

        try:
            df_res = create_protocol_ref(df, protocol_list)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(df_res.equals(expected_res))

        # Tests if different protocols of the same type create several columns of the same type if used on same sample.
        input_table = StringIO("""Samples;Protocols
        sample1;protocol_id1,protocol_id2,protocol_id4
        sample2;protocol_id0,protocol_id1,protocol_id3
        """)
        df = pd.read_table(input_table, sep=';', dtype=str).fillna('').astype(str)
        expected_table = StringIO("""Protocol REF;Protocol REF;Protocol REF;Protocol REF;Protocol REF
        ;protocol1;protocol2;protocol4;
        protocol0;protocol1;;;protocol3
        """)
        expected_res = pd.read_table(expected_table, sep=';', skipinitialspace=True).fillna('').astype(str)
        expected_res.columns = ['Protocol REF', 'Protocol REF', 'Protocol REF', 'Protocol REF', 'Protocol REF']

        try:
            df_res = create_protocol_ref(df, protocol_list)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(df_res.equals(expected_res))

        # Tests bad input
        input_table = StringIO("""Samples;Protocols
        sample1;
        sample2;protocol_id1,protocol_id4
        """)
        df = pd.read_table(input_table, sep=';', dtype=str).fillna('').astype(str)

        expected_table = StringIO("""Protocol REF;Protocol REF
        ;
        protocol1;protocol4
        """)
        expected_res = pd.read_table(expected_table, sep=';', skipinitialspace=True).fillna('').astype(str)
        expected_res.columns = ['Protocol REF', 'Protocol REF']

        try:
            df_res = create_protocol_ref(df, protocol_list)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(df_res.equals(expected_res))

        # Test protocols never presents on same sample. At least one ontology type must be common in all the samples.
        # protocol_id1 and protocol_id2 both belong to the ontology term 1 in this case.
        input_table = StringIO("""Samples;Protocols
        sample1;protocol_id1,protocol_id3
        sample2;protocol_id2,protocol_id4
        """)
        df = pd.read_table(input_table, sep=';', dtype=str).fillna('').astype(str)
        expected_table = StringIO("""Protocol REF;Protocol REF;Protocol REF
        protocol1;protocol3;
        protocol2;;protocol4
        """)
        expected_res = pd.read_table(expected_table, sep=';', skipinitialspace=True).fillna('').astype(str)
        expected_res.columns = ['Protocol REF', 'Protocol REF', 'Protocol REF']

        try:
            df_res = create_protocol_ref(df, protocol_list)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(df_res.equals(expected_res))

        # Tests if ontology types are well ordered
        input_table = StringIO("""Samples;Protocols
        sample1;protocol_id5,protocol_id7,protocol_id4
        sample2;protocol_id2,protocol_id4
        sample3;protocol_id2,protocol_id6
        """)
        df = pd.read_table(input_table, sep=';', dtype=str).fillna('').astype(str)
        expected_table = StringIO("""Protocol REF;Protocol REF;Protocol REF;Protocol REF
        ;protocol5;protocol7;protocol4
        protocol2;;;protocol4
        protocol2;;protocol6;
        """)
        expected_res = pd.read_table(expected_table, sep=';', skipinitialspace=True).fillna('').astype(str)
        expected_res.columns = ['Protocol REF', 'Protocol REF', 'Protocol REF', 'Protocol REF']

        try:
            df_res = create_protocol_ref(df, protocol_list)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(df_res.equals(expected_res))

        # Tests merging on more than 2 columns. Second column protocols 5, 6, 7, 8 are all the same ontology term.
        input_table = StringIO("""Samples;Protocols
        sample1;protocol_id1,protocol_id5,protocol_id4
        sample2;protocol_id1,protocol_id6,protocol_id4
        sample3;protocol_id2,protocol_id7,protocol_id4
        sample3;protocol_id2,protocol_id8,protocol_id3
        """)
        df = pd.read_table(input_table, sep=';', dtype=str).fillna('').astype(str)
        expected_table = StringIO("""Protocol REF;Protocol REF;Protocol REF;Protocol REF
        protocol1;protocol5;protocol4;
        protocol1;protocol6;protocol4;
        protocol2;protocol7;protocol4;
        protocol2;protocol8;;protocol3
        """)
        expected_res = pd.read_table(expected_table, sep=';', skipinitialspace=True).fillna('').astype(str)
        expected_res.columns = ['Protocol REF', 'Protocol REF', 'Protocol REF', 'Protocol REF']

        try:
            df_res = create_protocol_ref(df, protocol_list)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(df_res.equals(expected_res))

        # Tests merging on more than 2 columns. Second column protocols 5, 6, 7, 8 are all the same ontology term.
        input_table = StringIO("""Samples;Protocols
        sample1;protocol_id2,protocol_id3
        sample2;protocol_id4,protocol_id5
        sample3;protocol_id2,protocol_id5
        sample3;protocol_id4,protocol_id3
        """)
        df = pd.read_table(input_table, sep=';', dtype=str).fillna('').astype(str)
        expected_table = StringIO("""Protocol REF;Protocol REF;Protocol REF;Protocol REF
        protocol2;;protocol3;
        ;protocol4;;protocol5
        protocol2;;;protocol5
        ;protocol4;protocol3;
        """)
        expected_res = pd.read_table(expected_table, sep=';', skipinitialspace=True).fillna('').astype(str)
        expected_res.columns = ['Protocol REF', 'Protocol REF', 'Protocol REF', 'Protocol REF']

        try:
            df_res = create_protocol_ref(df, protocol_list)
        except Exception as err:
            self.fail(str(err))
        print(df_res.to_string())
        print(expected_res.to_string())
        self.assertTrue(df_res.equals(expected_res))

    def test_create_protocol_ref_upload(self):
        onto_term1 = OntologyTerm(name='onto1', term_id='onto_id1', ontology=Ontology(name=''))
        protocol1 = Protocol(name='protocol1', id='protocol_id1', protocol_type=onto_term1)
        protocol2 = Protocol(name='protocol2', id='protocol_id2', protocol_type=onto_term1)
        onto_term2 = OntologyTerm(name='onto2', term_id='onto_id2', ontology=Ontology(name=''))
        onto_term3 = OntologyTerm(name='onto3', term_id='onto_id3', ontology=Ontology(name=''))
        protocol3 = Protocol(name='protocol3', id='protocol_id3', protocol_type=onto_term3)
        onto_term4 = OntologyTerm(name='onto4', term_id='onto_id4', ontology=Ontology(name=''))
        protocol4 = Protocol(name='protocol4', id='protocol_id4', protocol_type=onto_term4)
        protocol0 = Protocol(name='protocol0', id='protocol_id0', protocol_type=onto_term1)
        protocol5 = Protocol(name='protocol5', id='protocol_id5', protocol_type=onto_term2)
        protocol6 = Protocol(name='protocol6', id='protocol_id6', protocol_type=onto_term2)
        protocol7 = Protocol(name='protocol7', id='protocol_id7', protocol_type=onto_term2)
        protocol8 = Protocol(name='protocol8', id='protocol_id8', protocol_type=onto_term2)
        protocol_list = [protocol1, protocol2, protocol3, protocol4, protocol0,
                         protocol5, protocol6, protocol7, protocol8]
        file = '../data/testdir1/test_protocol_sort.txt'
        outpath = '../data/out_ignore/test_protocol_sort_res.txt'
        df = pd.read_table(file, sep=';')

        expected_table = StringIO("""Protocol REF;Protocol REF;Protocol REF;Protocol REF
                protocol1;protocol5;protocol4;
                protocol1;protocol6;protocol4;
                protocol2;protocol7;protocol4;
                protocol2;protocol8;;protocol3
                """)
        expected_res = pd.read_table(expected_table, sep=';', skipinitialspace=True).fillna('').astype(str)
        expected_res.columns = ['Protocol REF', 'Protocol REF', 'Protocol REF', 'Protocol REF']

        try:
            df_res = create_protocol_ref(df, protocol_list)
            df_res.to_csv(outpath, sep='\t', index=False)
            df_res_reopened = pd.read_table(outpath, sep='\t').fillna('')
            df_res_reopened.columns = ['Protocol REF' for i in range(len(df_res_reopened.columns.values))]
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(df_res_reopened.equals(df_res))
        self.assertTrue(df_res_reopened.equals(expected_res))