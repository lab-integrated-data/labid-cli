import logging
import unittest
from pathlib import Path
from typing import Set, List, Dict, Any

from stocks.assaysniffer.sniffers import NanoporeAssaySniffer, InstrumentRun, AssayStructureError, GeneCoreAssaySniffer, \
    just_one_file_in_dir, list_files_in_dir
from stocks.assaysniffer.registry import registry
from stocks.models import Dataset, SequencingLibrary

logger = logging.getLogger("MySnifferTestCase")

class MySnifferTestCase(unittest.TestCase):

    def test_nanopore_sniffer_sniff(self):
        sniffer = NanoporeAssaySniffer()

        # 1 (multiplexed) sample in 1 flowcell (3 barcoded sample)
        p = Path("../data/nanopore/", "2021-11-16_InvitroMet_S2_dA_TE")
        try:
            runs: List[InstrumentRun] = sniffer.sniff_instrument_run_assays(
                dir_path=p, group="krebs", username="boulange")
            self.assertEqual(len(runs), 1)
            logger.info("\n"+runs[0].as_simple_json())
        except AssayStructureError as err:
            self.fail(str(err))

        # 1 sample in 1 flowcell
        p = Path("../data/nanopore/", "2021-08-05_SMF_GW_emb2-4_nB")
        try:
            runs: List[InstrumentRun] = sniffer.sniff_instrument_run_assays(
                dir_path=p, group="krebs", username="boulange")
            self.assertEqual(len(runs), 1)
        except AssayStructureError as err:
            self.fail(str(err))

        self.assertRaises(AssayStructureError, sniffer.sniff_instrument_run_assays, dir_path=None, group="krebs", username="boulange")

        # 2 samples in 4 flowcells. The 'Analysis' dir_path has been added after
        # one run of the emb2-4 and one fo the emb6-8 ('1739') only have failed fast5
        p = Path("../data/nanopore/", "SMF-Nanopore_GW_emb2-4_6-8_DE_FP_rep1")
        try:
            runs: List[InstrumentRun] = sniffer.sniff_instrument_run_assays(
                dir_path=p, group="krebs", username="boulange")
            self.assertEqual(len(runs), 4)
        except AssayStructureError as err:
            self.fail(str(err))

    def test_nanopore_sniffer_methods(self):
        sniffer = NanoporeAssaySniffer()
        for test_dir_name in ['2021-11-16_InvitroMet_S2_dA_TE', '2021-08-05_SMF_GW_emb2-4_nB',
                              'SMF-Nanopore_GW_emb2-4_6-8_DE_FP_rep1']:
            try:
                params: Dict[str, Any] = sniffer.looks_like_nanopore_project_dir(
                    Path("../data/nanopore/", test_dir_name))
                logger.debug("\n" + str(params))
            except AssayStructureError as err:
                self.fail(str(err))

        run_dir = Path('../data/nanopore/2021-11-16_InvitroMet_S2_dA_TE/S2/20211116_1756_X1_FAR80225_214c3dbd/')

        # test get_param_dict_from_nanopore_report()
        report_file = Path(run_dir, 'report_FAR80225_20211116_1800_214c3dbd.md')
        d = sniffer.get_param_dict_from_nanopore_report(report_file)
        self.assertIsNotNone(d)

        # test just_one_file_in_dir()
        self.assertTrue(just_one_file_in_dir(run_dir, ext='pdf'))
        self.assertFalse(just_one_file_in_dir(run_dir, ext='csv'))

        # test list_files_in_dir()
        self.assertEqual(len(list_files_in_dir(run_dir, ext='pdf')), 1)
        self.assertEqual(len(list_files_in_dir(run_dir, ext='csv')), 2)
        fastq_dir = Path(run_dir, "fastq_fail", "barcode01")
        self.assertEqual(len(list_files_in_dir(fastq_dir, ext='fastq')), 2)
        self.assertEqual(len(list_files_in_dir(fastq_dir, ext='fastq', gz_tolerant=False)), 0)

        run_dir_multi = Path('../data/nanopore/2021-11-16_InvitroMet_S2_dA_TE/S2/20211116_1756_X1_FAR80225_214c3dbd/')
        ds: List[Dataset] = sniffer.get_nanopore_demultiplexed_datasets(
            run_dir_multi, "fast5_pass", sample_base_name="test", file_type="fast5", barcode_number=12)
        self.assertEqual(len(ds), 13)

        run_dir = Path('../data/nanopore/2021-08-05_SMF_GW_emb2-4_nB/emb2-4/20210802_1513_X1_FAP22834_16e3f9e6')

        d: Dataset = sniffer.get_nanopore_dataset(run_dir, "fast5_pass", SequencingLibrary(name="test", barcode=""),
                                                  "fast5")
        logger.info(d.as_simple_json())

    def test_genecore_sniffer(self):
        sniffer = GeneCoreAssaySniffer()
        runs: List[InstrumentRun] = sniffer.sniff_instrument_run_assays(dir_path=Path("../data/genecore/"),
                                                                        group="my_group")
        self.assertEqual(len(runs), 1, "wrong number of runs")
        self.assertEqual(len(runs[0].assays), 1, "wrong number of assays")
        self.assertTrue(len(runs[0].assays[0].datasets) > 0, "wrong number of datasets")

        # logger.info("\n"+encode(runs[0], unpicklable=False, indent=2, make_refs=False))
        logger.info("\n" + runs[0].as_simple_json())
        # logger.info(encode(runs[0].assays[0].datasets))

    def test_registry_sniffer(self):
        registry.load_custom_plugins_from_plugin_base_dir(
            Path("../plugins/assaysniffers")
        )
        names: Set[str] = registry.get_registered_sniffer_names()
        self.assertTrue("GeneCoreAssaySniffer" in names)
        self.assertTrue("TRECSequencingPlateAssayValidator" in names)


if __name__ == '__main__':
    unittest.main()
