import logging
import unittest
from pathlib import Path
from typing import Set, List, Dict, Any

from plugins.assaysniffers.trec_scplate_validator import TRECSequencingPlateAssayValidator
from stocks.assaysniffer.sniffers import NanoporeAssaySniffer, InstrumentRun, AssayStructureError, GeneCoreAssaySniffer
from stocks.assaysniffer.registry import registry
from stocks.models import Dataset, SequencingLibrary

logger = logging.getLogger("TrecScplateValidatorTestCase")

class TrecScplateValidatorTestCase(unittest.TestCase):

    def test_sample_regex(self):
        sniffer = TRECSequencingPlateAssayValidator()

        valid: Dict[str, str] = {
            "11254839220230404SC2B3": "112548392_20230404_SC2",
            "11254839220230404SC1A12": "112548392_20230404_SC1",
            "11254846620240130SC2X3515D3":"112548466_20240130_SC2X3515"
        }

        for k, v in valid.items():
            try:
                extracted = sniffer.extract_sample_name(k)
                self.assertEqual(extracted, v.replace("_", ""), f"wrong sample part extracted from {k}")
                formatted = sniffer.reformat_sample_name(extracted)
                self.assertEqual(formatted, v, f"wrong sample name reformatting from {extracted}")
            except Exception as err:
                self.fail(f"Failed to process {k} into {v}: {err}")


if __name__ == '__main__':
    unittest.main()
