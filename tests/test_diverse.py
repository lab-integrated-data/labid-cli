import logging
import unittest
from pathlib import Path

from cli.utils import longest_common_prefix, convert_to_unix_path

logger = logging.getLogger("MyDiverseTestCase")

class MyDiverseTestCase(unittest.TestCase):

    def test_longest_common_prefix(self):
        self.assertEqual(longest_common_prefix(['bla', 'bi']), 'b')
        self.assertEqual(longest_common_prefix(['bla', 'bla']), 'bla')
        self.assertEqual(longest_common_prefix(['bla', 'bli']), 'bl')
        self.assertEqual(longest_common_prefix(['bla', 'abli']), '')
        self.assertEqual(longest_common_prefix([]), '')
        self.assertEqual(longest_common_prefix(words=None), '')

    def test_convert_to_unix_path(self):
        p: Path = convert_to_unix_path("/g/not/a/win/path/")
        self.assertEqual(p, Path("/g/not/a/win/path"))

        a_win_path: str = "D:\\DMA_Pullover\\test for charles\\20240815\\LuxTest\\"
        p: Path = convert_to_unix_path(a_win_path, prefix="/cygdrive")
        self.assertEqual(p, Path("/cygdrive/d/DMA_Pullover/test for charles/20240815/LuxTest"))


if __name__ == '__main__':
    unittest.main()
