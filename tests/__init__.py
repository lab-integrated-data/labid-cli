import logging

logging.basicConfig(level=logging.DEBUG,
                    format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
                    datefmt='%H:%M:%S'
                    )

# stoc = StocksClient(get_config(get_default_config_file_path()))
# stoc = StocksClient(get_config(get_default_config_file_path()))
# query_params ={"response_format": "flat"}
# query_params["format"] = "csv"
# study_id="3c33f740-0279-4bf2-9194-daf38888df44"
# query_params["study_id"] =study_id
# url = urljoin(model_to_url["datasets"], "arrayexpress_export")
# status, data = stoc.get(url, query_params)
#
# from io import BytesIO
# import numpy
# bdata = BytesIO(data)
# numpy.genfromtxt(bdata, delimiter=",")
#
#
#
# manager = StocksManager(stoc)
# sample: Sample = manager.fecthSample(id='7f027e5e-9cdb-41d0-bae3-15f68f76f29c')
# code, results = stoc.fetch(id='7f027e5e-9cdb-41d0-bae3-15f68f76f29c', model='samples', query_params={'response_format':'flat'})
# res = results.get('results')