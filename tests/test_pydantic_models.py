import unittest
import logging

from stocksapi.models import PydanticStocksBaseItem, PydanticChannelVisualizedEntity

logging.basicConfig(level=logging.DEBUG, format='%(levelname)-8s  %(name)-23s  %(message)s')


class MyPydanticTestCase(unittest.TestCase):

    def test_PydanticStocksBaseItem(self):

        json = {'name': 'yes'}
        try:
            obj = PydanticStocksBaseItem.parse_obj(json)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(obj.name == 'yes')

        json = {'name': {'value': 'yes!'}}
        try:
            obj = PydanticStocksBaseItem.parse_obj(json)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(obj.name == 'yes!')

        json = {'name': {'value': {'name': 'YES!'}}}
        try:
            obj = PydanticStocksBaseItem.parse_obj(json)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(obj.name == 'YES!')

    def test_parse_channel(self):
        json = {
            'id': '5855b811-3a1b-489d-8223-83a4363feef1',
            'created_by': {'id': 270, 'username': 'anonymous', 'full_name': 'Anonymous Name'},
            'modified_by': {'id': 270, 'username': 'anonymous', 'full_name': 'Anonymous Name'},
            'created': '2023-10-18T15:41:08.804532+02:00',
            'modified': '2023-10-18T15:41:08.804673+02:00',
            'model_name': 'imagingchannel',
            'app_name': 'assays',
            'model_type': 'DEFAULT',
            'choice_label': 'HKSTAG1mEGFP-X4CF680_AF647-dUTP',
            'name': 'HKSTAG1mEGFP-X4CF680_AF647-dUTP',
            'number': 1,
            'target': 'STAG1',
            'target_type': {
                'id': 'f58ebe3a-8a7a-400d-bc82-7d0e2353a0f8',
                'name': 'Protein',
                'label': 'Protein',
                'description': '',
                'dbxref_id': 'Protein'
            },
            'label': {
                'id': '458b8848-d9cf-475a-8541-33e49abeff17',
                'name': 'CF680',
                'label': 'CF680 (Fluorophore; Ex/Em max: 681/698 nm)',
                'description': 'Fluorophore; Ex/Em max: 681/698 nm', 'dbxref_id': 'CF680'
            },
            'label_object': {
                'id': '5874ef83-6ab1-4789-919f-6c17b6b97198',
                'name': 'FluoTag®-X4 anti-GFP',
                'deleted': False,
                'model_name': 'consumable',
                'app_name': 'stocks',
                'model_type': 'ANTIBODY'
            },
            'excitation_wavelength': {
                'lower': None,
                'upper': None,
                'bounds': '()'
            },
            'emission_wavelength': {
                'lower': None,
                'upper': None,
                'bounds': '()'},
            'probe_type': {},
            'probe_description': '',
            'probe_object': {}
        }

        try:
            obj = PydanticChannelVisualizedEntity.parse_obj(json)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(obj.name == 'HKSTAG1mEGFP-X4CF680_AF647-dUTP')
