import logging
import random
import unittest
from pathlib import Path
import time
from typing import List
from uuid import uuid4

import cli
from cli.config import get_config
from cli.export import add_default_submitter, \
    create_protocol_ref, extract_final_annotations, create_annofactor_df, create_idf_design
from cli.export_utils import create_idf_users, create_idf_protocol, stocks_factor_format_name, check_assay_type, \
    arrayexpress_export_to_dataframe
from stocks.models import Study, UserMember, Protocol, OntologyTerm, Ontology, Assay, AnnotationType, StocksCVTerm, \
    User, SequencingAssay, InstrumentRun, Instrument, LightMicroscopyAssay, ChannelVisualizedEntity, InstrumentModel, \
    Project, Sample, EMSample, SequencingLibrary, Note, UserGroup, StorageVolume
from cli.utils import UserRole, Technology, is_uuid, ModelType, ObjectState, SequencingRunType
from stocksapi.client import StocksClient
from stocksapi.exceptions import ItemNotFoundException, MultipleObjectMatchedError
from stocksapi.manager import StocksManager


manager: StocksManager
conf_file = Path("testconfig.yml")
if not conf_file.exists():
    raise unittest.SkipTest(f"Test connection config not found: {str(conf_file)}. Skipping all tests in test_online.py")
# try to init the manager
try:
    manager = StocksManager(client=StocksClient(get_config(conf_file)))
except Exception:
    raise unittest.SkipTest("Test server not accessible. Skipping all tests in test_online.py")

logger = logging.getLogger("OnlineTestCase")

logger.warning(f"Running online unit tests against server {manager.client.url} using {manager.client.username}")

ref_study = {
    'id': "a988625c-4656-4dfc-bd1a-e9dc103a05e8",
    'name': "Hi-C in the Drosophila wild-type embryos at 6-8 h and 16-18 hrs after egg-laying",
    'arrayexpress_ann_id': "6256ec6b-1931-4567-8a04-8c53d774e012",
    'arrayexpress_acc': "E-MTAB-13267",
    'project': {
        'id': "db1c386d-94bd-4436-9b20-fbc403ade811",
        'name': "High Depth Hi-C in WT (Yad)",
    },
    'design_terms': ['biological replicate', 'technical replicate', 'time series design']
}

ref_cv_term = {
    'id': "b2abca9f-70d2-4f8d-af02-5711f11c805b",
    'name': 'GENOMIC',
    'category_name': 'sample_library_source',
    'owner': 'admin',
}

class OnlineTestCase(unittest.TestCase):
    ###
    # requires access to test server or skip all tests
    ###

    def test_fetch_user(self):
        # grab an existing user by username
        u: User = manager.fetch_user("girardot")
        self.assertTrue(isinstance(u, User))
        self.assertEqual(u.username, "girardot")
        # check the id is set
        self.assertTrue(u.id)
        # fetch same user by uuid
        u2: User = manager.fetch_user(u.id)
        self.assertEqual(u.as_simple_json(), u2.as_simple_json())

        # try to fetch a non-existing user
        try:
            manager.fetch_user("wronguser")
            self.fail("Should have raised an exception")
        except ItemNotFoundException as e:
            pass

    def test_fetch_annotation_from_item(self):
        # fetch an existing annotation, use published study / sequencing library
        ann_value, ann_id = manager._fetch_annotation_from_item(
            model=ModelType.STUDY,
            id=ref_study['id'],
            ann_type="arrayexpress_id"
        )
        self.assertTrue(ann_id)
        self.assertTrue(is_uuid(ann_id))
        self.assertEqual(ref_study['arrayexpress_ann_id'], ann_id)
        self.assertEqual(ref_study['arrayexpress_acc'], ann_value)
        try:
            res = manager._fetch_annotation_from_item(
                model=ModelType.STUDY,
                id=ref_study['id'],
                ann_type="non_existing"
            )
            self.assertIsNone(res)
        except Exception as e:
            # no exception is expected
            self.fail(e)

    def test_list_annotation_from_item(self):
        # fetch annotations from a published study
        ann_list = manager.list_annotations_from_item(
            model=ModelType.STUDY,
            id=ref_study['id']
        )
        self.assertTrue(isinstance(ann_list, list))
        self.assertTrue(len(ann_list) > 0)
        self.assertTrue(isinstance(ann_list[0], dict))
        self.assertTrue('name' in ann_list[0])
        self.assertTrue('id' in ann_list[0])

        ann_list = manager.list_annotations_from_item(
            model=ModelType.SEQUENCINGLIBRARY,
            id='46f886db-c67b-4870-afe8-4135872481c6'
        )
        # check we got all annotations
        self.assertTrue(len(ann_list) == 9)
        for ann in ann_list:
            self.assertTrue(isinstance(ann, dict) or isinstance(ann, str))

    def test_fetch_stocks_cv_term(self):
        try:
            # grab a non-existing CV term
            res = manager.fetch_stocks_cv_term(uuid=str(uuid4()))
            self.fail("Should have raised an exception")
        except Exception as e:
            # no exception is expected when fetching non-existing CV Term
            pass

        # fetch an existing CV term: GENOMIC from sample_library_source category
        t: StocksCVTerm = manager.fetch_stocks_cv_term(uuid=ref_cv_term['id'])
        self._check_genomic_sample_library_source(t)

    def _check_genomic_sample_library_source(self, t: StocksCVTerm):
        self.assertTrue(isinstance(t, StocksCVTerm))
        self.assertEqual(ref_cv_term['name'], t.name)
        self.assertEqual(ref_cv_term['id'], t.id)
        self.assertEqual(ref_cv_term['owner'], t.owner)
        self.assertIsNotNone(t.owned_by)
        self.assertIsNotNone(t.created_by)
        self.assertIsNotNone(t.created)
        self.assertIsNotNone(t.modified)
        self.assertIsNotNone(t.modified_by)
        self.assertFalse(t.deleted)
        self.assertIsNone(t.deleted_by)
        self.assertIsNone(t.deleted_date)
        self.assertEqual(t.ontology_mappings, {})
        self.assertEqual(ref_cv_term['category_name'], t.category.name)

    def test_fetch_assay(self):
        # fetch an existing illumina
        a = manager.fetch_assay(uuid="f2482b0b-2cc8-4a13-9bdc-bf31129286f4")
        self.assertTrue(isinstance(a, SequencingAssay))
        self.assertEqual(a.technology, Technology.SEQUENCING)
        self.assertEqual(a.stocks_model_type, ModelType.NGSILLUMINAASSAY.value)
        self.assertEqual(a.platform, "ILLUMINA")
        self.assertEqual(a.name, "Hi-C_CA1P2ANXX_1")
        self.assertEqual(a.state, ObjectState.REGISTERED)
        self.assertIsNotNone(a.run_dir)
        self.assertFalse(a.multiplexed)

        self.assertEqual(a.runtype, SequencingRunType.PAIRED_END)
        self.assertEqual(a.flowcell, "CA1P2ANXX")
        self.assertTrue(isinstance(a.lane, int), f"wrong type: {type(a.lane)}")
        self.assertEqual(a.lane, 1)
        self.assertEqual(a.runmode, "125")
        # samples and datasets are not fetched by default
        self.assertIsNone(a.datasets)
        self.assertIsNone(a.samples)

        # fetch an existing Nanopore

        # fetch an existing Imaging
        lma_uuid = "e31024b5-cb2b-49d7-aced-1a43ac206f15"
        a = manager.fetch_assay(uuid=lma_uuid)
        self.assertTrue(isinstance(a, LightMicroscopyAssay))
        self.assertEqual(a.technology, Technology.LIGHT_MICROSCOPY)
        self.assertEqual(a.stocks_model_type, ModelType.LIGHTMICROSCOPYASSAY.value)
        self.assertEqual(a.platform, "Olympus")
        self.assertEqual(a.name, "Tischi Astrocyte Demo")
        self.assertEqual(a.state, ObjectState.REGISTERED)
        self.assertIsNotNone(a.run_dir)
        self.assertTrue(a.multiplexed)
        self.assertEqual(a.imaging_method[0].name, "confocal microscopy")
        self.assertEqual(a.x_size, 2046)
        self.assertEqual(a.y_size, 2048)
        self.assertEqual(a.pixel_size, 0.108)
        self.assertEqual(a.z_planes, 41)
        self.assertEqual(a.z_step, 0.3)
        self.assertEqual(a.time_points, 1)
        self.assertFalse(a.time_step)
        # channels
        self.assertTrue(isinstance(a.channels, list))
        self.assertEqual(len(a.channels), 3)
        self.assertTrue(isinstance(a.channels[0], ChannelVisualizedEntity))

        # we test values of the 3 channels
        self.assertEqual(a.channels[2].number, 3)
        self.assertEqual(a.channels[2].name, "405DAPI")
        self.assertEqual(a.channels[2].target, "DNA")
        self.assertEqual(a.channels[2].target_type.name, "Nucleus")
        self.assertEqual(a.channels[2].label.name, "DAPI")
        self.assertFalse(a.channels[2].label_object)
        self.assertFalse(a.channels[2].probe_type)
        self.assertFalse(a.channels[2].probe_object)
        self.assertFalse(a.channels[2].probe_description)

        # samples and datasets are not fetched by default
        self.assertIsNone(a.datasets)
        self.assertIsNone(a.samples)

        # fetch an existing TEM

        # fetch an existing VEM

    def test_fetch_cvterm_by_name_and_category(self):
        t = manager.fetch_cvterm_by_name_and_category(
            name=ref_cv_term['name'],
            category_name=ref_cv_term['category_name']
        )
        self._check_genomic_sample_library_source(t)

        # no category also works if term is unique
        t = manager.fetch_cvterm_by_name_and_category(name=ref_cv_term['name'])
        self._check_genomic_sample_library_source(t)

        multi_term_name = "AscI"
        t = manager.fetch_cvterm_by_name_and_category(name=multi_term_name, category_name="common_restriction_enzymes")
        self.assertTrue(isinstance(t, StocksCVTerm))
        self.assertEqual(t.id, "1ff1fd4e-6f19-4e9c-b8ed-7a26f80fcef6")
        self.assertEqual(t.category.name, "common_restriction_enzymes")

        t = manager.fetch_cvterm_by_name_and_category(name=multi_term_name, category_name="restriction_enzymes")
        self.assertTrue(isinstance(t, StocksCVTerm))
        self.assertEqual(t.id, "c2bbe300-0f19-413b-8825-391780614a84")
        self.assertEqual(t.category.name, "restriction_enzymes")

        try:
            t = manager.fetch_cvterm_by_name_and_category(name=multi_term_name)
            self.fail("Should have raised an exception")
        except MultipleObjectMatchedError as e:
            pass

    def test_fetch_item_by_name(self):
        d = manager.fetch_item_by_name("Tischi Astrocyte Demo", ModelType.LIGHTMICROSCOPYASSAY)
        self.assertTrue(isinstance(d, dict))
        self.assertEqual(d["name"], "Tischi Astrocyte Demo")
        self.assertEqual(d["id"], "e31024b5-cb2b-49d7-aced-1a43ac206f15")
        self.assertEqual(d["model_type"], ModelType.LIGHTMICROSCOPYASSAY.value)
        self.assertEqual(d["model_name"], "assay")

        multi_term_name = "AscI"
        try:
            t = manager.fetch_item_by_name(multi_term_name, ModelType.TERM)
            self.fail("Should have raised an exception")
        except MultipleObjectMatchedError as e:
            pass

    def test_list_fetch_instrument(self):
        lst: List[Instrument] = manager.list_instruments()
        self.assertTrue(lst)
        self.assertTrue(len(lst) > 0)
        self.assertTrue(isinstance(lst[0], Instrument))

        i: Instrument = manager.fetch_equipment(lst[random.randint(1, len(lst) - 1)].id)
        self.assertTrue(isinstance(i, Instrument))

        lst = manager.list_instruments(technology=Technology.SEQUENCING)
        self.assertTrue(lst)
        self.assertTrue(isinstance(lst[0].model, InstrumentModel))
        for i in lst:
            self.assertTrue(i.model.technology == Technology.SEQUENCING)
            self.assertTrue(isinstance(i.model, InstrumentModel), msg=f"wrong type: {type(i.model)}")

        lst = manager.list_instruments(technology=Technology.SEQUENCING, platform="ILLUMINA")
        for i in lst:
            self.assertTrue(i.model.platform == "ILLUMINA")

        lst = manager.list_instruments(technology=Technology.LIGHT_MICROSCOPY)
        for i in lst:
            self.assertTrue(i.model.technology == Technology.LIGHT_MICROSCOPY)

        # we have 2 of these as of writing
        lst = manager.list_instruments(name="GeneCore PromethION P2")
        self.assertTrue(len(lst) > 1)
        for i in lst:
            self.assertEqual(i.name, "GeneCore PromethION P2")

    def test_list_fetch_instrument_model(self):
        lst: List[InstrumentModel] = manager.list_instrument_models()
        self.assertTrue(lst)
        self.assertTrue(len(lst) > 0)
        self.assertTrue(isinstance(lst[0], InstrumentModel))

        i: InstrumentModel = manager.fetch_instrument_model(lst[random.randint(1, len(lst) - 1)].id)
        self.assertTrue(isinstance(i, InstrumentModel))

        lst = manager.list_instrument_models(technology=Technology.SEQUENCING)
        self.assertTrue(lst)
        for i in lst:
            self.assertEqual(i.technology, Technology.SEQUENCING)

        lst = manager.list_instrument_models(technology=Technology.SEQUENCING, platform="ILLUMINA")
        for i in lst:
            self.assertEqual(i.platform, "ILLUMINA")

        lst = manager.list_instrument_models(technology=Technology.LIGHT_MICROSCOPY)
        for i in lst:
            self.assertEqual(i.technology, Technology.LIGHT_MICROSCOPY)

        # we have 2 instrument of these as of writing, but shoudl have only one model
        lst = manager.list_instrument_models(name="PromethION P2")
        self.assertEqual(len(lst), 1)
        self.assertEqual(lst[0].name, "PromethION P2")

    def test_list_fetch_instrument_run(self):
        ref_run_id = "6e4b4b05-a447-47e9-99ef-4e706e3d2784"
        ref_run_name = "000000000-A59GM:MiSeqSe01"

        i: InstrumentRun = manager.fetch_instrument_run(ref_run_id)
        self._check_ref_instrument_run_attrs(i, ref_run_id, ref_run_name)

        lst: List[InstrumentRun] = manager.list_instrument_runs()
        self.assertTrue(lst)
        self.assertTrue(len(lst) > 0)
        self.assertTrue(isinstance(lst[0], InstrumentRun))
        self.assertTrue(lst[0].name)
        self.assertTrue(lst[0].id)
        self.assertTrue(lst[0].platform)
        self.assertTrue(isinstance(lst[0].instrument, Instrument))
        self.assertTrue(isinstance(lst[0].technology, Technology))

        lst = manager.list_instrument_runs(name=ref_run_name)
        self.assertTrue(lst)
        self.assertEqual(len(lst), 1)
        self._check_ref_instrument_run_attrs(lst[0], ref_run_id, ref_run_name)

    def _check_ref_instrument_run_attrs(self, i: InstrumentRun, ref_run_id: str, ref_run_name: str):
        self.assertTrue(isinstance(i, InstrumentRun))
        self.assertEqual(i.name, ref_run_name)
        self.assertTrue(isinstance(i.instrument, Instrument))
        self.assertTrue(isinstance(i.instrument.model, InstrumentModel))
        self.assertEqual(i.instrument.name, "MiSeqSe01")
        self.assertEqual(i.instrument.id, "06ca46fe-c254-4530-ad66-ebd0a077ad6d")
        self.assertEqual(i.instrument.model.name, "Illumina MiSeq")
        self.assertEqual(i.instrument.model.id, "d650e962-d918-454e-ae82-045234cbc506")

    def test_list_fetch_project(self):
        projects: List[dict] = manager.list_items(ModelType.PROJECT)
        for p in projects:
            self.assertTrue(isinstance(p, dict))
            self.assertTrue(p["name"])
            self.assertTrue(p["id"])

        p: Project = manager.fetch_project(projects[random.randint(1, len(projects) - 1)]['id'])
        self.assertTrue(isinstance(p, Project))
        self.assertTrue(p.name)
        self.assertTrue(p.id)
        self.assertTrue(p.owner)

    def test_list_fetch_protocol(self):
        protocols: List[dict] = manager.list_items(ModelType.PROTOCOL)
        for p in protocols:
            self.assertTrue(isinstance(p, dict))
            self.assertTrue(p["name"])
            self.assertTrue(p["id"])

        p: Protocol = manager.fetch_protocol(protocols[random.randint(1, len(protocols) - 1)]['id'])
        self.assertTrue(isinstance(p, Protocol))
        self.assertTrue(p.name)
        self.assertTrue(p.summary)
        self.assertTrue(p.id)
        self.assertTrue(p.owner)

    def test_list_fetch_sample(self):
        seq_lib = {
            'id': '46f886db-c67b-4870-afe8-4135872481c6',
            'name': 'Hi-C_WE_16-18h_1',
        }
        s: Sample|EMSample|SequencingLibrary = manager.fetch_sample(
            uuid=seq_lib['id'],
            sample_type=ModelType.SEQUENCINGLIBRARY,
            load_ownership=False,
            load_annotations=False
        )
        self.assertTrue(isinstance(s, SequencingLibrary))
        self.assertEqual(s.name, seq_lib['name'])
        self.assertEqual(s.id, seq_lib['id'])
        self.assertFalse(s.deleted)
        self.assertFalse(s.annotations)

        s = manager.fetch_sample(
            uuid=seq_lib['id'],
            sample_type=ModelType.SEQUENCINGLIBRARY,
            load_ownership=False,
            load_annotations=True
        )
        self.assertTrue(s.annotations)
        self.assertTrue(isinstance(s.annotations, dict))
        print(s.annotations)
        self.assertTrue('Sex' in s.annotations)
        self.assertTrue(isinstance( s.annotations['Sex'], dict ))
        self.assertEqual(s.annotations['Sex']['name'], "mixed sex")
        self.assertEqual(len(s.annotations), 9)

        s = manager.fetch_sample(
            uuid=seq_lib['id'],
            sample_type=ModelType.SEQUENCINGLIBRARY,
            load_ownership=True
        )
        self.assertTrue(isinstance( s.owner, User))

    def test_list_fetch_study(self):
        s: Study = manager.fetch_study(ref_study['id'])
        self.assertTrue(isinstance(s, Study))
        self.assertEqual(s.name, ref_study['name'])
        self.assertEqual(s.id, ref_study['id'])

    def test_list_fetch_annotationtype(self):
        a: AnnotationType = manager.fetch_annotationtype_by_name("Sex")
        self.assertTrue(isinstance(a, AnnotationType))
        self.assertEqual(a.name, "Sex")

        lst: List[AnnotationType] = manager.list_annotation_types()
        self.assertTrue(lst)
        self.assertTrue(len(lst) > 10)
        for a in lst:
            self.assertTrue(isinstance(a, AnnotationType))

    def test_list_notes(self):
        notes = manager.list_notes(item_id=ref_study['id'])
        self.assertTrue(notes)
        self.assertTrue(len(notes) > 0)
        for n in notes:
            self.assertTrue(isinstance(n, Note))

    def test_list_groups(self):
        groups = manager.list_groups(as_dict=False)
        self.assertTrue(groups)
        self.assertTrue(len(groups) > 0)
        for g in groups:
            self.assertTrue(isinstance(g, UserGroup))

        groups = manager.list_groups(as_dict=True)
        self.assertTrue(isinstance(groups, dict))
        self.assertTrue(len(groups) > 0)
        # iterate over the dict and check the values are UserGroup instances
        for k, v in groups.items():
            self.assertTrue(isinstance(v, UserGroup))

    def test_list_storage_volumes(self):
        volumes: List[StorageVolume] = manager.list_storage_volumes(username=manager.logged_in_user.username)
        self.assertTrue(volumes)
        self.assertTrue(len(volumes) > 0)
        for v in volumes:
            self.assertTrue(isinstance(v, StorageVolume))

    def test_save_note(self):
        # get epoc as a string
        epoch_time_str = str(int(time.time()))
        note_text = f"This is a test note from unittesting {epoch_time_str}"
        manager.save_note(
            item_id=ref_study['id'],
            note_text=note_text,
            stocks_model_type=ModelType.STUDY
        )
        notes = manager.list_notes(item_id=ref_study['id'])
        found_it = False
        for n in notes:
            if n.content == note_text:
                self.assertFalse(n.is_readonly)
                self.assertEqual(n.object_id, ref_study['id'])
                self.assertEqual(n.owner, manager.logged_in_user.username)
                found_it = True
                break

        if not found_it:
            self.fail("Saved note not found in the list of notes")

    def test_save_annotation(self):
        # save a CV annotation
        manager.save_annotation(
                uuid=ref_study['id'],
                item_type=ModelType.STUDY,
                annotation_type="Sex",
                content=StocksCVTerm(name="mixed sex", id="b43d9337-5560-4c44-a3bf-b36562d51df0")
            )

        # save a free text annotation
        manager.save_annotation(
                uuid=ref_study['id'],
                item_type=ModelType.STUDY,
                annotation_type="Antibody",
                content="free text"
            )

        ann_list: List[dict] = manager.list_annotations_from_item(
            model=ModelType.STUDY,
            id=ref_study['id']
        )

        for ann_dict in ann_list:
            if ann_dict['name'] == "Antibody":
                self.assertEqual(ann_dict['value'], "free text")
            elif ann_dict['name'] == "Sex":
                # value is a dict representing the CVTerm
                self.assertEqual(ann_dict['value']['name'], "mixed sex")
