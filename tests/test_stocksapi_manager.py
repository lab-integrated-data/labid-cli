import logging
import traceback
import unittest
from pathlib import Path
from typing import List
from uuid import uuid4

from pydantic import ValidationError

from stocks import AssayStructureError
# make sure to import sniffers to get them init in the registry
from stocks.assaysniffer.registry import registry
from stocks.models import InstrumentRun, User, LightMicroscopyAssay, StocksCVTerm, StocksCVCategory
from stocksapi.manager import StocksManager
from stocksapi.models import PyDatasetListPost, PydanticCVTerm, PydanticStocksBaseItem, PydanticAssay, PydanticSimpleInstrumentRun

logger = logging.getLogger(__name__)


class MySTOCKSAPIManagerTestCase(unittest.TestCase):

    def test_convert_object_pydantic_cv_fields(self):
        ojbective: PydanticCVTerm = PydanticCVTerm(name="objectiveA", category=PydanticStocksBaseItem(name="obj_cat"))
        imaging_method: List[PydanticCVTerm] = list()
        imaging_methods_cat = PydanticStocksBaseItem(name="imaging_methods")
        imaging_method.append(PydanticCVTerm(name="im1", category=imaging_methods_cat) )
        imaging_method.append(PydanticCVTerm(name="im2", category=imaging_methods_cat))
        imaging_method.append(PydanticCVTerm(name="im3", category=imaging_methods_cat))

        assay: LightMicroscopyAssay = LightMicroscopyAssay(
            name="blah", objective=ojbective, imaging_method=imaging_method)
        assay = StocksManager._convert_object_pydantic_cv_fields(assay)

        self.assertIsNotNone(assay.objective)
        self.assertTrue(isinstance(assay.objective, StocksCVTerm))
        self.assertEqual(assay.objective.name, "objectiveA")
        self.assertTrue(isinstance(assay.objective.category, StocksCVCategory))
        self.assertEqual(assay.objective.category.name, "obj_cat")

        self.assertIsNotNone(assay.imaging_method)
        self.assertTrue(isinstance(assay.imaging_method, list))
        names = []
        for im in assay.imaging_method:
            self.assertTrue(isinstance(im, StocksCVTerm))
            self.assertTrue(isinstance(im.category, StocksCVCategory))
            self.assertEqual(im.category.name, "imaging_methods")
            names.append(im.name)
        self.assertTrue('im1' in names)
        self.assertTrue('im2' in names)
        self.assertTrue('im3' in names)

        ## mix types
        imaging_method: List[PydanticCVTerm] = list()
        imaging_methods_cat = PydanticStocksBaseItem(name="imaging_methods")
        imaging_method.append(PydanticCVTerm(name="im1", category=imaging_methods_cat))
        # a term uuid
        imaging_method.append("18bec1aa-4445-438f-aead-7d741138f294")
        # a plain name
        imaging_method.append("im3")
        assay: LightMicroscopyAssay = LightMicroscopyAssay(
            name="blah", imaging_method=imaging_method)
        assay = StocksManager._convert_object_pydantic_cv_fields(assay)

        self.assertIsNone(assay.objective)
        self.assertIsNotNone(assay.imaging_method)
        self.assertTrue(isinstance(assay.imaging_method, list))

        for im in assay.imaging_method:
            self.assertTrue(isinstance(im, StocksCVTerm))
        self.assertEqual(assay.imaging_method[0].name, "im1")
        self.assertEqual(assay.imaging_method[1].id, "18bec1aa-4445-438f-aead-7d741138f294")
        self.assertFalse(assay.imaging_method[1].name)
        self.assertEqual(assay.imaging_method[2].name, "im3")

    def test_PyAssay_User_Validation(self):
        try:
            a: PydanticAssay = PydanticAssay(name="test", type="GENERICASSAY",
                                             multiplexed=True, owned_by="group_name", owner="bob")
        except Exception as err:
            self.fail(traceback.format_exc())

        try:
            a: PydanticAssay = PydanticAssay(name="test", type="GENERICASSAY",
                                             multiplexed=True, owned_by="group_name",
                                             owner=User(username="bob", last_name="bob"))
            self.assertEqual("bob", a.owner)
        except Exception as err:
            self.fail(traceback.format_exc())

    def test_to_pydantic_run(self):
        sniffer_cls = registry.get_sniffer("NanoporeAssaySniffer")
        sniffer = sniffer_cls()
        # 1 (multiplexed) sample in 1 flowcell (3 barcoded sample)
        p = Path("../data/nanopore/", "2021-11-16_InvitroMet_S2_dA_TE")
        runs: List[InstrumentRun] = sniffer.sniff_instrument_run_assays(
            dir_path=p, group="krebs", username="boulange")

        instrument_run = runs[0]

        try:
            pyrun: PydanticSimpleInstrumentRun = StocksManager._to_pydantic_simple_run(instrument_run)
        except ValidationError as e:
            self.fail(str(e))

        # initial object untouched
        self.assertNotEqual(len(runs[0].assays), 0, "assays prop should still be on original InstrumentRun")
        try:
            self.assertIsNotNone(pyrun.instrument, "instrument not properly initialized")
        except Exception as err:
            self.fail(traceback.format_exc())

    def test_convert_nanopore_run(self):
        registry.load_custom_plugins_from_plugin_base_dir(
            Path("../plugins/assaysniffers")
        )
        sniffer_cls = registry.get_sniffer("NanoporeAssaySniffer")
        sniffer = sniffer_cls()

        # 1 (multiplexed) sample in 1 flowcell (3 barcoded sample)
        p = Path("../data/nanopore/", "2021-11-16_InvitroMet_S2_dA_TE")
        try:
            runs: List[InstrumentRun] = sniffer.sniff_instrument_run_assays(
                dir_path=p, group="krebs", username="boulange")
            self.assertEqual(len(runs), 1)
            self.assertNotEqual(len(runs[0].assays), 0)
            # print("\n"+runs[0].as_simple_json())
        except AssayStructureError as err:
            self.fail(str(err))

        try:
            # assay must be registered, the method below checks for the prsence of uuid in assay.id slot
            for _a in runs[0].assays:
                _a.id = str(uuid4())
            o: PyDatasetListPost = StocksManager(
                client=None
            )._create_instrument_run_post(
                instrument_run=runs[0], run_dir=p, owner="boulange", owned_by_group="krebs",
                allow_pooled_samples=False, transfer_whole_input_dir=True, study_id="A_fake_study_id",
                old_payload=True)
        except Exception as err:
            self.fail(traceback.format_exc())

        print("JSON is:")
        #print(o.json(exclude_none=True, exclude_unset=True, by_alias=False))
        print(o.json())
        #print(o.schema_json(indent=2))


if __name__ == '__main__':
    unittest.main()
