import unittest

from stocks.models import InstrumentRun, User, UserMember, UserGroup, Protocol
from cli.utils import Technology, UserRole


class MySTOCKSModelTestCase(unittest.TestCase):

    def test_hierarchy(self):
        params = {'name': 'a protocol', 'protocol_type': 'extraction', 'owner': 'me', 'deleted': 'true',
                  'modified_by': 'me', 'created_by': 'me', 'annotations': {'ann1': 'val1'},
                  'crap': 'should be ignored'}
        p = Protocol(**params)
        self.assertEqual(p.name, params['name'])
        self.assertEqual(p.protocol_type, params['protocol_type'])
        self.assertEqual(p.owner, params['owner'])
        self.assertTrue(p.deleted)
        self.assertEqual(p.modified_by, params['modified_by'])
        self.assertEqual(p.created_by, params['created_by'])
        self.assertTrue(p.annotations)
        self.assertEqual(p.annotations['ann1'], 'val1')

    def test_annotable_mixin(self):
        run: InstrumentRun = InstrumentRun(name="coucou", managed=True, technology=Technology.SEQUENCING,
                                           platform="ILLUMINA", owned_by="mygroup")

        try:
            run.set_owner(owner=User(username="dad", groups=[UserGroup('zeubi', is_primary_group=True)]),
                          also_set_group=False)
            run.add_annotation("key", "value")
            self.assertEqual(run.annotations['key'], "value")
            self.assertEqual(run.owned_by, "mygroup")
            run.set_owner(owner=User(username="dad", groups=[UserGroup('zeubi', is_primary_group=True)]),
                          also_set_group=True)
            self.assertEqual("zeubi", run.owned_by)
            run.set_owner(owner=User(username="dad", groups=[UserGroup('notzeubi', is_primary_group=False)]),
                          also_set_group=True)
            self.assertEqual("notzeubi", run.owned_by)
        except AttributeError as err:
            self.fail(str(err))

    def test_usermodel(self):
        try:
            User(username="reb")
            u: UserMember = UserMember(user=User(username="reb"), roles={UserRole.SUBMITTER})
        except Exception as err:
            self.fail(str(err))

    def test_list_enum_members(self):
        try:
            s: str = Technology.list()
            print(s)
        except Exception as err:
            self.fail(str(err))


if __name__ == '__main__':
    unittest.main()
