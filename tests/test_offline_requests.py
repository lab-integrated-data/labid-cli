import unittest
from http import HTTPStatus
from uuid import uuid4

import responses
import json
import logging
from stocks.models import Study, User, StocksCVTerm, Ontology, OntologyTerm, Protocol, SequencingAssay, UserGroup, \
    LightMicroscopyAssay, ChannelVisualizedEntity, StocksBaseItem, TransmissionEMAssay, IntegerRange
from stocksapi.client import StocksClient, model_to_url
from stocksapi.manager import StocksManager
from cli.utils import ModelType, SequencingRunType, Technology, ObjectState, is_uuid

logging.basicConfig(level=logging.DEBUG,
                    format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
                    datefmt='%H:%M:%S'
                    )

logger = logging.getLogger(__name__)
def result_json(name: str) -> str:
    """Gets json string from files in /data/api_response folder"""
    if not name.endswith('.json'):
        name = name + '.json'
    path = "../data/api_response/" + name
    with open(path) as f:
        json_data = json.load(f)
    return json_data


@responses.activate
def get_offline_manager() -> StocksManager:
    url = "https://fakeurl.com/"
    username = "username"
    request_url = f"{url}api/v2/{model_to_url[ModelType.USER]}{username}"
    responses.add(responses.GET, request_url, json=result_json("user1"), status=HTTPStatus.OK)
    client: StocksClient = StocksClient(None, url=url, username=username, token="token")
    return StocksManager(client)


stocks_manager = get_offline_manager()


class MySTOCKSRequestsTestCase(unittest.TestCase):

    @responses.activate
    def test_fetch_study(self):
        """
        Fetch study in the context of an export
        """
        uuid = "study_id"
        user_id = "user_id1"
        user_username = "user_username1"
        term1_id = 'term_id1'
        term2_id = "term_id2"

        # Mocks of study, user, cvterm and annotation list
        request_url = stocks_manager.client.url + model_to_url[ModelType.STUDY] + uuid
        user_request_url = stocks_manager.client.url + model_to_url[ModelType.USER] + user_username
        term1_request_url = stocks_manager.client.url + model_to_url[ModelType.TERM] + term1_id
        term2_request_url = stocks_manager.client.url + model_to_url[ModelType.TERM] + term2_id
        annot_request_url = stocks_manager.client.url + model_to_url[ModelType.STUDY] + uuid + "/annotations/"
        responses.add(responses.GET, request_url, json=result_json("study"), status=HTTPStatus.OK)
        responses.add(responses.GET, user_request_url, json=result_json("user1"), status=HTTPStatus.OK)
        responses.add(responses.GET, term1_request_url, json=result_json("cvterm1"), status=HTTPStatus.OK)
        responses.add(responses.GET, term2_request_url, json=result_json("cvterm2"), status=HTTPStatus.OK)
        responses.add(responses.GET, annot_request_url, json=result_json("annotation_list"), status=HTTPStatus.OK)

        # Positive testing
        try:
            test_study: Study = stocks_manager.fetch_study(uuid, load_ownership=True)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(isinstance(test_study, Study))
        self.assertTrue(test_study.description == "Study description")
        self.assertTrue(test_study.name == "Study name")
        self.assertTrue(isinstance(test_study.owner, User))
        self.assertTrue(test_study.owner.full_name == "user1_full_name")
        # Check design terms
        self.assertTrue(isinstance(test_study.experimental_design_terms[0], StocksCVTerm))
        self.assertTrue(test_study.experimental_design_terms[0].name == "term1_name")
        self.assertTrue(isinstance(test_study.experimental_design_terms[0].ontology_mappings["EFO"], OntologyTerm))
        self.assertTrue(isinstance(test_study.experimental_design_terms[0].ontology_mappings["EFO"].ontology, Ontology))
        self.assertTrue(isinstance(test_study.experimental_design_terms[1], StocksCVTerm))
        self.assertTrue(test_study.experimental_design_terms[1].name == "term2_name")
        self.assertTrue(isinstance(test_study.experimental_design_terms[1].ontology_mappings["EFO"], OntologyTerm))
        self.assertTrue(isinstance(test_study.experimental_design_terms[1].ontology_mappings["EFO"].ontology, Ontology))

    @responses.activate
    def test_fetch_protocol(self):

        # Growth protocol test
        uuid = 'growth_p'
        request_url = stocks_manager.client.url + model_to_url[ModelType.PROTOCOL] + uuid
        responses.add(responses.GET, request_url, json=result_json("protocol_growth"), status=HTTPStatus.OK)
        try:
            test_protocol_growth: Protocol = stocks_manager.fetch_protocol(uuid)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(isinstance(test_protocol_growth, Protocol))
        self.assertTrue(isinstance(test_protocol_growth.protocol_type, OntologyTerm))
        self.assertTrue(test_protocol_growth.protocol_type.name == "growth protocol")
        self.assertTrue(isinstance(test_protocol_growth.protocol_type.ontology, Ontology))
        self.assertTrue(test_protocol_growth.protocol_type.ontology.name == "EFO")

        # Extraction protocol test
        uuid = 'extraction_p'
        request_url = stocks_manager.client.url + model_to_url[ModelType.PROTOCOL] + uuid
        responses.add(responses.GET, request_url, json=result_json("protocol_extraction"), status=HTTPStatus.OK)
        try:
            test_protocol_extraction: Protocol = stocks_manager.fetch_protocol(uuid)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(isinstance(test_protocol_extraction, Protocol))
        self.assertTrue(isinstance(test_protocol_extraction.protocol_type, OntologyTerm))
        self.assertTrue(test_protocol_extraction.protocol_type.name == "nucleic acid extraction protocol")
        self.assertTrue(isinstance(test_protocol_extraction.protocol_type.ontology, Ontology))
        self.assertTrue(test_protocol_extraction.protocol_type.ontology.name == "EFO")

        # Construction protocol test
        uuid = 'construction_p'
        request_url = stocks_manager.client.url + model_to_url[ModelType.PROTOCOL] + uuid
        responses.add(responses.GET, request_url, json=result_json("protocol_construction"), status=HTTPStatus.OK)
        try:
            test_protocol_construction: Protocol = stocks_manager.fetch_protocol(uuid)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(isinstance(test_protocol_construction, Protocol))
        self.assertTrue(isinstance(test_protocol_construction.protocol_type, OntologyTerm))
        self.assertTrue(test_protocol_construction.protocol_type.name == "nucleic acid library construction protocol")
        self.assertTrue(isinstance(test_protocol_construction.protocol_type.ontology, Ontology))
        self.assertTrue(test_protocol_construction.protocol_type.ontology.name == "EFO")

    @responses.activate
    def test_fetch_stocks_cv_term(self):

        uuid = '1234'
        request_url = stocks_manager.client.url + model_to_url[ModelType.TERM] + uuid
        responses.add(responses.GET, request_url, json=result_json("cvterm1"), status=HTTPStatus.OK)
        try:
            test_cv: StocksCVTerm = stocks_manager.fetch_stocks_cv_term(uuid)
        except Exception as err:
            self.fail(str(err))
        self.assertTrue(isinstance(test_cv, StocksCVTerm))
        self.assertTrue(test_cv.name == "term1_name")
        self.assertTrue(isinstance(test_cv.ontology_mappings["EFO"], OntologyTerm))
        self.assertTrue(isinstance(test_cv.ontology_mappings["EFO"].ontology, Ontology))
