import json
import logging
import os
import re

from pathlib import Path
from typing import List, Optional, Dict, Tuple

from stocksapi.exceptions import MultipleObjectMatchedError
from stocks import AssayStructureError
from stocks.assaysniffer.sniffers import list_files_in_dir, just_one_file_in_dir, read_md5_from_disk
from stocks.models import InstrumentRun, Sample, LightMicroscopyAssay, StocksCVTerm, Dataset, \
    DatasetCollection, DatasetFile, User, UserGroup, StocksCVCategory, SequencingLibrary
from cli.utils import Technology, ModelType, is_uuid

from stocks.assaysniffer import AssaySniffer
from stocks.assaysniffer.registry import registry
from stocksapi.manager import StocksManager


logger = logging.getLogger(__name__)


def check_valid_directory(path: Path|str) -> bool:
    if isinstance(path, str):
        path = Path(path)
    if not path.exists() or not path.is_dir():
        return False
    return True

@registry.install
class CopasVisionAssayLoader(AssaySniffer):
    """
    This sniffer creates a bright-field microscopy assay (no channels) with 96 images (tiff) corresponding to 96 single
    particles sorted into a plate (one particle per well) using COPAS Vision platform. The original COPAS Vision
    files (.bxr4, .lmd, .txt, .dcimg) are also saved.
    The 96 images actually show the single particles that were sorted into a plate well and are therefore important for
    a posteriori quality check; and potential image analysis.

    The images (manually exported) are named following the default COPAS export setting ie Id_<PictureCopas>_w_<well>.

    where the PLATENAME follows
    the pattern <BARCODE>_YYYYMMDD_SC<NUMBER> e.g. 112548392_20230404_SC2_Id_470_w_A1.Tiff

    This sniffer expects:
     - existing samples with type 'sequencing library' and named after the PLATENAME i.e. <BARCODE>_YYYYMMDD_SC<NUMBER>

    """
    IMAGE_DIR_NAME = "Dispensed_TIFFS"
    PROFILES_DIR_NAME = "All_profiles"
    COPAS_EXP_DIR_NAME = "COPAS_Exp"

    _DEFAULT_PLATFORM: str = "COPAS Vision 250"
    _DEFAULT_SAMPLE_NAME_PATTERN: str = r'^[\w]+_202\d{5,5}_SC\d$'
    _DEFAULT_SAMPLE_MUST_EXIST: bool = True
    _DEFAULT_SAMPLE_TYPE: str = "SEQUENCINGLIBRARY"
    _DEFAULT_REQUIRES_IMAGES_FOLDER: bool = True
    _DEFAULT_REQUIRES_PROFILE_FOLDER: bool = True
    _DEFAULT_IMAGE_NUM: int = 96

    def __init__(self,
                 platform: str = _DEFAULT_PLATFORM,
                 sample_name_pattern: str | None = _DEFAULT_SAMPLE_NAME_PATTERN,
                 sample_must_exist: bool = _DEFAULT_SAMPLE_MUST_EXIST,
                 sample_type: str = _DEFAULT_SAMPLE_TYPE,
                 sample_id: str = None,
                 requires_images_folder: bool = _DEFAULT_REQUIRES_IMAGES_FOLDER,
                 expected_image_number: int = _DEFAULT_IMAGE_NUM,
                 requires_profiles_folder: bool = _DEFAULT_REQUIRES_PROFILE_FOLDER,
                 stocks_manager: StocksManager | None = None,
                 test_mode: bool = False,
                 **kwargs):
        super().__init__(stocks_manager=stocks_manager, **kwargs)
        self.platform = platform
        self.sample_name_pattern: str = sample_name_pattern
        self.sample_must_exist: bool = sample_must_exist
        self.sample_type: ModelType = ModelType(sample_type)
        # optional sample_id, to bypass sample lookup ie use this one
        if sample_id and not is_uuid(sample_id):
            raise AttributeError(f"Given parameter sample_id is not a UUID: {sample_id}")
        self.sample_id: str = sample_id
        # the corresponding Sample object
        self.sample: SequencingLibrary = None

        # if True an 'images' sub-folder is required, with expected_image_number in it, all with same format (ie .ext)
        self.requires_images_folder: bool = requires_images_folder
        self.expected_image_number: int = int(expected_image_number)
        self.requires_profiles_folder = requires_profiles_folder
        self.test_mode: bool = test_mode


    @classmethod
    def get_sniffer_param_names(cls) -> Dict[str, str]:
        params = {
            'platform': f'the platform, matching a LabID model name (default={cls._DEFAULT_PLATFORM})',
            'sample_name_regex': f'regex to validate the sample name i.e. the folder name '
                                 f'(default={cls._DEFAULT_SAMPLE_NAME_PATTERN})',
            'sample_must_exist': f'True or False. If True the sample must exist in LabID '
                                 f'(default={cls._DEFAULT_SAMPLE_MUST_EXIST})',
            'sample_type': f'The sample type for look up or create a new one (default={cls._DEFAULT_SAMPLE_TYPE})',
            'sample_id': f'True or False (default={cls._DEFAULT_SAMPLE_NAME_PATTERN})',
            'requires_images_folder': f'when True, a sub-folder (named {cls.IMAGE_DIR_NAME}) is required and should '
                                      f'contain the expected_image_number images (all with same format as assessed by '
                                      f' extension). True or False (default={cls._DEFAULT_REQUIRES_IMAGES_FOLDER})',
            'requires_profiles_folder': f'when True, a sub-folder (named {cls.PROFILES_DIR_NAME}) is required and '
                                        f'should contain .txt files). '
                                        f'True or False (default={cls._DEFAULT_REQUIRES_PROFILE_FOLDER})',
            'expected_image_number': f'image number to find in the images folder (default={cls._DEFAULT_IMAGE_NUM})',
            'test_mode': 'switch test mode on (True|False). In test mode compliance to regex is ignored.'
        }
        return params

    @classmethod
    def get_sniffer_description(cls) -> str:
        return f"""
            This sniffer creates a Light Microscopy (LM) Assay reflecting the sorting a sample into single cells (e.g. 
            unicellular organism like diatoms) distributed into a 96-well plate using thr COPAS Vision Platform. 
            
            This sniffer expects to run on a folder containing the COPAS Vision results for one plate. 
            Expected content:
            - An 'COPAS_Exp' folder containing the original COPAS Vision files (.bxr4, .lmd, .txt, .dcimg)
            - An 'Dispensed_TIFFS' folder containing 'expected_image_number' of tiff images. All images must be of the same 
             format (e.g. tiff) as indicated by their file extension. 
            - An 'All_profiles' folder containing the exported channel profiles 
            
            Naming convention:
            - The top folder (given as input) name is used as the sample/plate name and is validated using 
             the sample_name_regex. A SequencingLibrary with the name must be found in LabID. 
            - COPAS Vision file (.bxr4, .lmd, .txt, .dcimg) names are expected to start with the sample/plate name (<plate_name>.*.bxr4)
            - Image file names must end with the well position (e.g F4) e.g. ".+<well_position>.Tiff"
            - Channel profile file names have the .txt extension    
            
        """

    def get_supported_technology(cls) -> Technology:
        """
        :return: the technology this sniffer supports
        """
        return Technology.LIGHT_MICROSCOPY

    def get_supported_platforms(cls) -> List[str]:
        """
        :return: platforms: the list of Platform this sniffer supports or None if no constraints apply here
        """
        return []

    def is_multi_run_sniffer(cls) -> bool:
        """
        Tells if the sniffer can sniff more than one run.
        :return: true if more than one instrument run can be sniffed by this sniffer
        """
        return False

    def sniff_instrument_run_assays(self, dir_path: Path, group: str, username: Optional[str] = None) \
            -> List[InstrumentRun]:

        owner: User = None
        if username:
            owner = User(username=username, groups=[UserGroup(name=group)])

        if not check_valid_directory(dir_path):
            mess = f"The dir_path {str(dir_path)} is either empty or does not point to a valid directory."
            raise AssayStructureError(mess)

        subdirs = [f.name for f in os.scandir(dir_path) if f.is_dir() and not f.name.startswith(".")]
        profile_dir_path = Path(dir_path, self.PROFILES_DIR_NAME)
        image_dir_path = Path(dir_path, self.IMAGE_DIR_NAME)
        copas_exp_dir_path = Path(dir_path, self.COPAS_EXP_DIR_NAME)
        # we use this 'in' test to gain case-sensitivity as profile_dir_path.exists() is case insensitive when volume
        # is mounted on a mac
        if self.COPAS_EXP_DIR_NAME not in subdirs:
            raise AssayStructureError(f"COPAS experiment folder {self.COPAS_EXP_DIR_NAME} not found; "
                                      f"subdirs are {subdirs}")
        if self.requires_images_folder and self.IMAGE_DIR_NAME not in subdirs:
            raise AssayStructureError(f"Dispensed image folder {self.IMAGE_DIR_NAME} not found; subdirs are {subdirs}")
        if self.requires_profiles_folder and self.PROFILES_DIR_NAME not in subdirs:
            raise AssayStructureError(f"Profile folder {self.PROFILES_DIR_NAME} not found; subdirs are {subdirs}")

        runs: List[InstrumentRun] = list()

        #
        # Validate dir structure/content.

        plate_name = dir_path.name
        # validate top folder name
        if self.sample_name_pattern:
            sample_pattern = re.compile(self.sample_name_pattern)
            if not sample_pattern.match(plate_name):
                raise AssayStructureError(
                    f"The folder name {plate_name} does not comply to naming convention {self.sample_name_pattern}")

        expected_file_types = ['bxr4', 'lmd', 'txt', 'dcimg']
        copas_type2files: Dict[str, List[Path]] = dict()
        for ext in expected_file_types:
            copas_type2files[ext] = list_files_in_dir(copas_exp_dir_path, ext=ext, gz_tolerant=False)
            if not copas_type2files[ext]:
                raise AssayStructureError(f"No COPAS {ext} file found in {str(copas_exp_dir_path)}.")
            # we also check the copas file name contains the plate name
            for _p in copas_type2files[ext]:
                if self.sample_name_pattern and plate_name not in _p.name:
                    raise AssayStructureError(f"The COPAS file must contain the plate name {plate_name} "
                                              f"in their name. Not the case with : {_p.name} !")
        # look up the sample
        if not self.sample_must_exist:
            raise NotImplementedError(f"sample_must_exist=False is not yet supported.")
        elif not self.sample_id:
            # let's look up the sample
            try:
                self.sample = self.stocks_manager.fetch_item_by_name(name=plate_name, model=self.sample_type)
                if not self.sample:
                    mess = f"No {self.sample_type.name} found for plate name {plate_name}, plate must exist in LabID."
                    raise AssayStructureError(mess)
                else:
                    self.sample_id = self.sample['id']
            except MultipleObjectMatchedError as mome:
                logger.warning(mome)
                # problem might be that the API uses a starts_with and not an exact match
                raise AssayStructureError(f"More than one {self.sample_type.name} match the name {plate_name}!")

        #load the sample
        self.sample = self.stocks_manager.fetch_sample(uuid=self.sample_id, sample_type=self.sample_type)
        logger.debug(self.sample.as_simple_json())

        img_file_pattern = re.compile(r'^.+([A-H]\d{1,2})\.([^\.]+)$')
        image_format: str = None
        image_file2well: Dict[Path, str] = dict()
        if self.requires_images_folder or image_dir_path.exists():
            _files = [f.path for f in os.scandir(image_dir_path)
                      if f.is_file() and not f.name.startswith(".") and not f.name == "Thumbs.db"]
            errors = list()
            for img_f in _files:
                img_f = Path(img_f)
                filename = Path(img_f).name
                # Check if the filename matches the regex pattern
                match = img_file_pattern.match(filename)
                if match:
                    well = match.group(1)
                    _file_format = match.group(2)
                    image_file2well[img_f] = well
                    logger.debug(f"_file2well {filename} => {well}")

                    if image_format is not None and _file_format != image_format:
                        emess = (f"{filename} has type {_file_format} while previous"
                                 f" {dir_path.name} files had {image_format}")
                        errors.append(emess)
                        logger.error(emess)
                    else:
                        image_format = _file_format
                else:
                    errors.append(f"{filename} does not match the expected pattern.")
            if errors:
                mess = f"Image file names do not follow naming convention:\n"
                mess = mess + "\n".join(errors)
                raise AssayStructureError(mess)

        profile_file_paths = list()
        if self.requires_profiles_folder or profile_dir_path.exists():
            profile_file_paths = [f.path for f in os.scandir(profile_dir_path)
                                  if f.is_file() and not f.name.startswith(".")]

        # create a run and a light microscopy assay, the assay must have its info set
        _the_run: InstrumentRun = InstrumentRun(
            name=f"COPAS Vision sorting of plate {plate_name}", managed=False,
            technology=Technology.LIGHT_MICROSCOPY, platform=self.platform, instrument=None,
            owner=owner, owned_by=group)

        _datasets: List[Dataset] = list()
        _samples: List[Sample] = list()

        dc_copas = DatasetCollection(name=f"COPAS {plate_name} Experiment Primary Files", owner=owner, owned_by=group)
        for _ext, _pathes in copas_type2files.items():
            more_than_one_file = len(_pathes) > 1
            for _cnt, _path in enumerate(_pathes):
                _df = DatasetFile(name=_path.name, mime_type=_ext, byte=_path.stat().st_size,
                                  filetype=_ext, is_dir=False, uri=str(_path))
                logger.debug("_df.name => "+_df.name)
                # we can also setup the md5sum if we have access to it
                _df.md5sum = read_md5_from_disk(_df)
                ds_name: str = f"{plate_name} {_ext} COPAS file"
                if more_than_one_file:
                    ds_name = f"{ds_name} {_cnt+1}"
                _d = Dataset(name=ds_name, is_raw=True, samples=[self.sample], collection=dc_copas, datafiles=[_df])
                _d.owner = owner
                _d.owned_by = group
                # do not add in the collection
                _datasets.append(_d)

        dc_images = DatasetCollection(name=f"COPAS {plate_name} Dispensed Images", owner=owner, owned_by=group)
        for filename, well in image_file2well.items():
            # create the dataset
            _df = DatasetFile(name=f"{plate_name}_{filename}", mime_type=f"image/{image_format.lower()}",
                              byte=filename.stat().st_size,
                              filetype=image_format, is_dir=False, uri=str(filename))
            logger.debug("_df.name => " + _df.name)
            # we can also setup the md5sum if we have access to it
            _df.md5sum = read_md5_from_disk(_df)
            ds_name: str = f"{plate_name} {well} {image_format} Image"
            _d = Dataset(name=ds_name, is_raw=True, samples=[self.sample], collection=dc_images, datafiles=[_df])
            _d.owner = owner
            _d.owned_by = group
            # do not add in the collection
            _datasets.append(_d)

        dc_profiles = DatasetCollection(name=f"COPAS {plate_name} Profile Files", owner=owner, owned_by=group)
        for filepath in profile_file_paths:
            # create the dataset
            _f = Path(filepath)
            _df = DatasetFile(name=f"{_f.name}", mime_type="text/plain", byte=filename.stat().st_size,
                              filetype="txt", is_dir=False, uri=filepath)
            logger.debug("_df.name => " + _df.name)
            # we can also setup the md5sum if we have access to it
            _df.md5sum = read_md5_from_disk(_df)
            ds_name: str = f"{_f.name} Profile"
            _d = Dataset(name=ds_name, is_raw=True, samples=[self.sample], collection=dc_profiles, datafiles=[_df])
            _d.owner = owner
            _d.owned_by = group
            # do not add in the collection
            _datasets.append(_d)

        # create the assay
        lm_assay: LightMicroscopyAssay = LightMicroscopyAssay(
            name=f"COPAS {dir_path.name}",
            platform=self.platform,
            instrumentrun=_the_run,
            description="",
            datasets=_datasets,
            samples=_samples,
            multiplexed=True,
            imaging_method=[
                StocksCVTerm(
                    name="bright-field microscopy",
                    category=StocksCVCategory(name=LightMicroscopyAssay.get_property_to_cvcategory()['imaging_method'])
                )
            ],
            channels=None  # no channel in bright field microscopy (== white light)
        )

        if owner:
            lm_assay.set_owner(owner=owner, also_set_group=True)
        else:
            lm_assay.group = group

        _the_run.add_assay(lm_assay)
        # add run in result list
        runs.append(_the_run)

        if self.test_mode:
            with open("vincent_copas_run.json", 'w') as file:
                json.dump(_the_run.as_simple_json(), file, indent=4)  # 'indent' is optional and used for pretty formatting

        return runs


