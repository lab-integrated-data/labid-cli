import json
import logging
from pathlib import Path
from typing import Dict, Optional, List

from stocks import AssayStructureError
from stocks.assaysniffer.registry import registry
from stocks.assaysniffer.sniffers import PooledIlluminaInitializedAssayValidator, GeneCoreAssaySniffer
from stocks.models import InstrumentRun, Assay
from stocksapi.manager import StocksManager

logger = logging.getLogger(__name__)

_DEFAULT_SAMPLE_NAME_REGEX: str = r'^([\w]+202\d{5,5}SC\d+.*)[a-zA-Z]\d{1,2}$'
_DEFAULT_SAMPLE_MUST_EXIST: bool = True
_DEFAULT_SAMPLE_FMT_REGEX: str = r'^([\w]+)(202\d{5,5})(SC\d+.*).*$'
_DEFAULT_FMT_SPACER: str = "_"

@registry.install
class TRECSequencingPlateAssayValidator(PooledIlluminaInitializedAssayValidator):
    """
    This sniffer validates a multiplexed, plate-based, single-cell assay (in the initialized state) transferred from EMBL GeneCore.
    A unique sequencing library representing all wells of the input plate is created and all datasets originating from
    this plate are linked to this 'POOL' library. Optionally, the Plate must exist in LabID.
    This sniffer also expects samples to be named according to a predefined pattern

    Note that regex can be passed in the CLI like : -p "sample_name_regex=^([\w]+202\d{5,5}SC\d).+$"
    """

    def __init__(self, dataset_per_sample: int = 96, **kwargs):
        _sample_name_regex = kwargs.pop('sample_name_regex', _DEFAULT_SAMPLE_NAME_REGEX)
        _sample_must_exist = kwargs.pop('sample_must_exist', _DEFAULT_SAMPLE_MUST_EXIST)
        _sample_format_regex = kwargs.pop('sample_format_regex', _DEFAULT_SAMPLE_FMT_REGEX)
        _sample_format_spacer = kwargs.pop('sample_format_spacer', _DEFAULT_FMT_SPACER)
        self.dataset_per_sample: int = dataset_per_sample
        super().__init__(
            sample_name_regex=_sample_name_regex,
            sample_must_exist=_sample_must_exist,
            sample_format_regex=_sample_format_regex,
            sample_format_spacer=_sample_format_spacer,
            **kwargs)

    def dir_qualifies(self, dir_path: Path) -> bool:
        if super().dir_qualifies(dir_path=dir_path):
            # self.assay is now init
            assay_info = json.loads(self.assay.info)
            snif: GeneCoreAssaySniffer = GeneCoreAssaySniffer()
            info_run: InstrumentRun = snif.load_run_from_json_obj(data=assay_info, data_dir=dir_path, group="")
            # we test that we have the expect number of dataset
            return len(info_run.assays[0].datasets) % self.dataset_per_sample == 0
        return False

    def sniff_instrument_run_assays(self, dir_path: Path, group: str, username: Optional[str] = None) \
            -> List[InstrumentRun]:
        runs: List[InstrumentRun] = super().sniff_instrument_run_assays(
            dir_path=dir_path, group=group, username=username)
        if len(runs) == 1:
            # validate the dataset number
            the_assay: Assay = runs[0].assays[0]
            dataset_num = len(the_assay.datasets)
            sample_num = len(the_assay.samples)
            if int(dataset_num / sample_num) != self.dataset_per_sample:
                raise AssayStructureError(
                    f"Wrong number of datasets ({dataset_num}) given the plate number: {sample_num}")

        return runs

    @classmethod
    def get_sniffer_description(cls) -> str:
        return ("""This sniffer validates a multiplexed, plate-based, single-cell assay (in the initialized state) transferred from EMBL GeneCore.
A unique sequencing library representing all wells of the input plate is created and all datasets originating from
this plate are linked to this 'POOL' library. Optionally, the Plate must exist in LabID.
This sniffer also expects samples to be named according to a predefined pattern

Note that regex can be passed in the CLI like : -p "sample_name_regex=^([\w]+202\d{5,5}SC\d).+$"
""")

    @classmethod
    def get_sniffer_param_names(cls) -> Dict[str, str]:
        params = {
            'sample_name_regex': f'a regex with one group to extract the pooled table name '
                                 f'(default: {_DEFAULT_SAMPLE_NAME_REGEX})',
            'sample_must_exist': f'True or False. If True the sample must exist in LabID '
                                 f'(default: {_DEFAULT_SAMPLE_MUST_EXIST})',
            'sample_format_regex': f'optional pattern to reformat the extracted pooled sample name. The new name is '
                                   f'obtained by concatenating all matched groups with the sample_format_spacer. '
                                   f'Default: {_DEFAULT_SAMPLE_FMT_REGEX}',
            'sample_format_spacer': f'spacer to use to concatenate all matched groups from sample_format_regex. '
                                    f'Default: {_DEFAULT_FMT_SPACER}',
            'dataset_per_sample': 'expected dataset number per plate (default: 96)',
            'platform': 'the sequencing platform, default is ILLUMINA',
            'producer': 'the producer of the data, default is GeneCore'
        }
        return params

