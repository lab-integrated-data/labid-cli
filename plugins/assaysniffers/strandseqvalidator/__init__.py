import json
import logging
from pathlib import Path
from typing import List, Optional, Dict

from stocks import AssayStructureError
from stocks.assaysniffer.registry import registry
from stocks.assaysniffer.sniffers import PooledIlluminaInitializedAssayValidator, GeneCoreAssaySniffer
from stocks.models import InstrumentRun, Assay

logger = logging.getLogger(__name__)

_DEFAULT_SAMPLE_NAME_REGEX: str = r'(.+)(PE20|iTRU)\w+'
_DEFAULT_SAMPLE_MUST_EXIST: bool = True

@registry.install
class SeqStrandAssayValidator(PooledIlluminaInitializedAssayValidator):
    """
    This sniffer validates a StrandSeq assay performed by the Korbel lab and transferred from EMBL GeneCore.
    A unique sequencing library representing all wells of a plate is created and all datasets originating from
    this plate are linked to this 'POOL' library.
    This parsing expects samples to be named according to the pattern r'(.+)PE20\w+' or r'(.+)iTRU\w+' with
     the sub-string preceding the 'PE' or 'iTRU' common to all wells from a plate.

     Note that regex can be passed in the CLI like : -p "sample_name_regex=(.+)(PE20|iTRU)\w+"
    """
    def __init__(self, dataset_per_sample: int | List[int] | str = [96, 192, 288, 384], **kwargs):
        _sample_name_regex = kwargs.pop('sample_name_regex', _DEFAULT_SAMPLE_NAME_REGEX)
        _sample_must_exist = kwargs.pop('sample_must_exist', _DEFAULT_SAMPLE_MUST_EXIST)

        self.dataset_per_sample: List[int] = list()

        if isinstance(dataset_per_sample, str):
            dataset_per_sample = dataset_per_sample.split(",")
        elif isinstance(dataset_per_sample, int):
            dataset_per_sample = [dataset_per_sample]

        for x in dataset_per_sample:
            self.dataset_per_sample.append(int(x))

        super().__init__(
            sample_name_regex=_sample_name_regex,
            sample_must_exist=_sample_must_exist,
            **kwargs)

    def dir_qualifies(self, dir_path: Path) -> bool:
        if super().dir_qualifies(dir_path=dir_path):
            assay_info = json.loads(self.assay.info)
            snif: GeneCoreAssaySniffer = GeneCoreAssaySniffer()
            info_run: InstrumentRun = snif.load_run_from_json_obj(data=assay_info, data_dir=dir_path, group="")
            # we test that we have the expect number of dataset
            correct_dataset_num = False
            has_expected_name = False
            for nd in self.dataset_per_sample:
                if len(info_run.assays[0].datasets) % nd == 0:
                    correct_dataset_num = True
                    # we check that we have expected sequencing index, get one dataset
                    d = info_run.assays[0].datasets[0]
                    try:
                        self.extract_sample_name(d.samples[0].name)
                    except AttributeError:
                        # the sample name is not in the expected format
                        logger.error(f"Sample name {d.samples[0].name} is not in the expected format. Skipping assay")
                    else:
                        has_expected_name = True
                    finally:
                        # no need to continue
                        break
        return correct_dataset_num and has_expected_name

    def sniff_instrument_run_assays(self, dir_path: Path, group: str, username: Optional[str] = None) \
            -> List[InstrumentRun]:
        runs: List[InstrumentRun] = super().sniff_instrument_run_assays(dir_path=dir_path, group=group,
                                                                        username=username)
        if len(runs) == 1:
            # validate the dataset number
            the_assay: Assay = runs[0].assays[0]
            dataset_num = len(the_assay.datasets)
            sample_num = len(the_assay.samples)
            if int(dataset_num / sample_num) not in self.dataset_per_sample:
                raise AssayStructureError(
                    f"Wrong number of datasets ({dataset_num}) given the plate number: {sample_num}")

        return runs

    @classmethod
    def get_sniffer_description(cls) -> str:
        return "This sniffer validates a standard strand-seq assay receive from GeneCore; thus simplifying operations" \
               " to be performed by the staff."

    @classmethod
    def get_sniffer_param_names(cls) -> Dict[str, str]:
        params = {
            'sample_name_regex': f'a regex with one group to extract the pooled table name '
                                 f'(default: {_DEFAULT_SAMPLE_NAME_REGEX})',
            'sample_must_exist': f'True or False. If True the sample must exist in LabID '
                                 f'(default: {_DEFAULT_SAMPLE_MUST_EXIST})',
            'sample_format_regex': 'optional pattern to reformat the extracted pooled sample name. The new name is '
                                   'obtained by concatenating all matched groups with the sample_format_spacer. '
                                   'Default: no sample name reformatting',
            'sample_format_spacer': 'spacer to use to concatenate all matched groups from sample_format_regex. '
                                    'Default: _',
            'dataset_per_sample': 'expected dataset number per plate (default: 96)',
            'platform': 'the sequencing platform, default is ILLUMINA',
            'producer': 'the producer of the data, default is GeneCore'
        }
        return params

