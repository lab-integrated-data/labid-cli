import glob
from pathlib import Path
from typing import List, Optional

from stocks import AssayStructureError
from stocks.models import InstrumentRun, User, UserGroup
from cli.utils import Technology

from stocks.assaysniffer import AssaySniffer, check_valid_directory
from stocks.assaysniffer.registry import registry

# not ready
#@registry.install
class SerialEMAssaySniffer(AssaySniffer):
    def __init__(self):
        super(SerialEMAssaySniffer, self).__init__()

    def sniff_instrument_run_assays(self, dir_path: Path, group: str, username: Optional[str] = None) \
            -> List[InstrumentRun]:
        if username:
            owner = User(username=username, groups=[UserGroup(name=group)])

        if not check_valid_directory(dir_path):
            mess = f"The dir_path {str(dir_path)} is either empty or does not point to a valid directory."
            raise AssayStructureError(mess)

        runs: List[InstrumentRun] = list()
        return []

    def looks_like_serialem_project_dir(self, dir_path: Path) -> bool:
        # we need to find some .mdoc files and also a sub dir full of TS_XXX_*.mdoc
        p = str(Path(dir_path, "**/TS_*.mdoc"))
        for item in glob.iglob(p, recursive=True):
            try:
                df = mdocfile.read( Path(item) )
            except Exception as e:
                return False
            return True
        return False

    def get_supported_technology(cls) -> Technology:
        """
        :return: the technology this sniffer supports
        """
        return Technology.ELECTRON_MICROSCOPY

    def get_supported_platforms(cls) -> List[str]:
        """
        :return: platforms: the list of Platform this sniffer supports
        """
        return ["OTHER"]

    def is_multi_run_sniffer(cls) -> bool:
        """
        Tells if the sniffer can sniff more than one run.
        :return: true if more than one instrument run can be sniffed by this sniffer
        """
        return False

    def get_sniffer_description(self) -> str:
        return "Loads a TEM assay reflecting a Single Particule or Tomography assay acquired with the " \
               "SerialEM software (https://bio3d.colorado.edu/SerialEM/)"