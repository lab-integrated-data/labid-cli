jsonpickle==3.0.1
jsonschema==4.17.3
mdocfile==0.0.8
numpy==1.24.3
pandas==2.0.1
pydantic==1.10.7
PyJWT==2.7.0
python-dateutil==2.8.2
pytz==2023.3
PyYAML==6.0
requests==2.30.0
typer==0.9.0
responses==0.23.3
# this line does not work in PyCharm
# --extra-index-url https://git.embl.de/api/v4/projects/4393/packages/pypi/simple dma-client==0.0.23
# pip install dma-client --extra-index-url https://git.embl.de/api/v4/projects/4393/packages/pypi/simple
