from setuptools import setup, find_namespace_packages
from pathlib import Path

# read the contents of your README file
this_directory = Path(__file__).parent
long_description = (this_directory / "pypi_description.md").read_text()

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

# init the version from version.py; this adds the __version__ in main_ns
main_ns = {}
with open('cli/version.py') as f:
    exec(f.read(), main_ns)
with open('cli/appname.py') as f:
    exec(f.read(), main_ns)

__app_name__ = main_ns['__app_name__']
__version__ = main_ns['__version__']

setup(
    name=__app_name__,
    version=__version__,
    author="GBCS",
    author_email="gbcs@embl.de",
    packages=find_namespace_packages(where=".",
                                     include=["cli", "stocks", "stocks.assaysniffer", "stocksapi", "plugins"]),
    data_files=[('', ['labidcli.ini.template', 'log_config.ini.template'])],
    include_package_data=True,
    install_requires=requirements,
    extras_require={
        "dev": ["setuptools==67.7.2"]
    },
    entry_points={
        "console_scripts": [
            f"{__app_name__} = cli.__main__:main",
        ],
    },
    url="https://www.embl.org/research/units/genome-biology/genome-biology-computational-support/",
    description="Command Line Interface to the LabID server",
    long_description=long_description,
    long_description_content_type='text/markdown'
)
