# Lab Integrated Data CLI

**labid-cli** - CLI tool for interacting with the Lab Integrated Data (LabID) server.

## Description

labid-cli is a command-line interface (CLI) tool designed to facilitate seamless communication and interaction with the LabID server. The tool provides various commands and options to perform common operations and tasks related to the LabID server, including fetching information of items, data registration, and export data to repositories.

## Features

- **Config Command**: Use the `config` command to manage the configuration settings of the labid-cli tool. Set up server details, authentication credentials, and other parameters to customize the tool according to your needs.

- **Export Command**: The `export` command allows you to export data from the LabID server. Choose specific datasets or define filters to extract the desired data in various formats for further analysis and processing.

- **Fetch Command**: With the `fetch` command, you can retrieve data from the LabID server. Specify the datasets or filters to fetch the required information and receive the data in real-time for immediate use.

- **Register Command**: The `register` command enables you to register new data or resources with the LabID server. Use this command to provide relevant details and metadata for the resources you want to add to the server.

## Installation

You can install labid-cli using pip, the Python package installer.

```shell
pip install labid-cli
```

## Usage

Once installed, you can use the labid-cli tool with various commands and options. Here are some examples:

- To display the available commands and options, use the `labid --help` command.

- To configure the labid-cli tool, run `labid config` and follow the interactive prompts to set up the necessary server details and authentication credentials.

- To export data from the LabID server, use the `labid export` command followed by specific parameters and filters as needed.

- To fetch data from the LabID server, execute the `labid fetch` command with appropriate dataset or filter specifications.

- To register new data or resources with the LabID server, utilize the `labid register` command and provide the required details and metadata.

## Additional Information

For more information, detailed usage instructions, and advanced options, please refer to the [labid-cli documentation](https://labid.embl.org/docs/training/topics/cli/).

## License

This project is licensed under the [MIT License](https://opensource.org/licenses/MIT).
