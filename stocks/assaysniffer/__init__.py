from abc import ABC, abstractmethod
from typing import List, Any, Optional, Set, Dict
from pathlib import Path
import logging

from stocks.models import InstrumentRun, Dataset, DatasetCollection
from cli.utils import Technology
from stocksapi.manager import StocksManager

logger = logging.getLogger(__name__)

def check_valid_directory(path: Path) -> bool:
    if not path or not path.exists() or not path.is_dir():
        return False
    return True


class DatasetSniffer(ABC):
    def __init__(self, stocks_manager: StocksManager = None, **kwargs):
        self.stocks_manager = stocks_manager
    """
    An DatasetSniffer is responsible to extract Dataset description from a target folder, optionally organizing them 
    in DatasetCollection. This is achieved by implementing the sniff_datasets() method. The optional dir_qualifies()
    may be overwritten to first decide if the folder is suitable for the sniffer by returning a boolean. 
    """

    def set_stocks_manager(self, stocks_manager: StocksManager):
        self.stocks_manager = stocks_manager

    @classmethod
    @abstractmethod
    def get_sniffer_description(cls) -> str:
        """
        Describes how this assay sniffer finds datasets and organize then, optionally, in collection(s)
        :return: a string explaining what the plugin expects to find / is looking for
        """
        pass

    @classmethod
    def get_sniffer_param_names(cls) -> Dict[str, str]:
        """
        :return: the dict of supported sniffer parameters i.e. parameters that can be passed to the sniffer using the
        --sniffer_param <param_name>=<param_value>. The dict contains the param name as key and a short help as value
        """
        return {}

    @classmethod
    def get_sniffer_type(cls) -> str:
        """
        :return: either AssaySniffer or DatasetSniffer
        """
        return "DatasetSniffer"

    def dir_qualifies(self, dir_path: Path) -> bool:
        """
        method called before sniff_* method to determine if this sniffer qualifies to extract data from
        the given dir. When this method returns False, the sniffer's sniff_* method should not be called as
        it will most likely raise errors
        When this method is not implemented, it is almost impossible to understand if a raised error is due to:
        - the data in the folder is not expected to be parsed with this Sniffer, or
        - the data in the folder is expected to be parsed with this Sniffer but something does not comply to
        sniffer's expectations (file naming...)

        The default implementation returns True
        """
        return True

    @abstractmethod
    def sniff_datasets(self, dir_path: Path, group: str, username: Optional[str] = None) \
            -> List[Dataset] | List[DatasetCollection]:
        """
        Main method to parse the content of a directory in a list of Dataset or DatasetCollection objects.
        When the sniffer organizes data within collections, the return list must contain the DatasetCollection; else
        a list of Dataset is returned. In the DatasetCollection case, the dataset.collection may remain null or empty.

        :param dir_path: the path to the directory to sniff
        :param group: the unix group_name considered to be the owner of the dir_path content
        :param username: an optional username to use as data owner
        :return the list of discovered Assay
        :except : AssayStructureError if the dir_path structure does not match sniffer's expectations
        """
        pass


class AssaySniffer(DatasetSniffer):
    """
    An AssaySniffer is responsible to parse run folders and extract Assay and Dataset description corresponding
    to one or many InstrumentRun. It extends DatasetSniffer.
    The key method to implement is the 'sniff_instrument_run_assays()' while the 'sniff_datasets()' is optional.
    By default, 'sniff_datasets()' will call 'sniff_instrument_run_assays()' and extract all datasets.
    When an AssaySniffer is executed on folder that does not match the sniffer expectations i.e. folder structure,
    file/folder naming, missing expected information... an AssayStructureError should be raised.
    When an AssaySniffer is executed on folder that does match the sniffer expectations but no assay nor run can be
    extracted (i.e. some filtering conditions on eg ownership ...), an empty list should be returned.
    """

    def __init__(self, stocks_manager: StocksManager = None, **kwargs):
        super().__init__(stocks_manager=stocks_manager, **kwargs)

    @classmethod
    def get_sniffer_type(cls) -> str:
        """
        :return: either AssaySniffer or DatasetSniffer
        """
        return "AssaySniffer"

    @classmethod
    @abstractmethod
    def get_supported_technology(cls) -> Technology:
        """
        :return: the technology this sniffer supports
        """
        pass

    @classmethod
    @abstractmethod
    def get_supported_platforms(cls) -> List[str]:
        """
        :return: platforms: the list of Platform this sniffer supports or None if the sniffer is
        not platform specific. Please use all UPPERCASE eg NANOPORE, ILLUMINA ...
        """
        pass

    @classmethod
    @abstractmethod
    def is_multi_run_sniffer(cls) -> bool:
        """
        Tells if the sniffer can sniff more than one run.
        :return: true if more than one instrument run can be sniffed by this sniffer
        """
        pass

    def is_sniffer_valid_for(self, technology: Technology, platform: str | None = None,
                             enforce_platform_match: bool = False) -> bool:
        """
        Utility method to check if this sniffer can be used for a given Technology and, optionally, a specific platform.
        The platform filtering only happens if a valid platform name is given.
        When a platform is given, a sniffer of requested technology that does not restrict on specific platform is
        considered valid unless enforce_platform_match is True
        considered valid
        :param technology: the technology for which you need a sniffer
        :param platform: an optional platform
        :param enforce_platform_match: if true, sniffer that do not explicitly list the requested platform are
        not considered valid
        """
        res: bool = technology == self.get_supported_technology()
        if res and platform:
            if self.get_supported_platforms():
                # if the sniffer defines platforms, we anyway do the filtering
                res = platform.upper() in self.get_supported_platforms()
            elif enforce_platform_match:
                res = False
        return res

    @abstractmethod
    def sniff_instrument_run_assays(self, dir_path: Path, group: str, username: Optional[str] = None) \
            -> List[InstrumentRun]:
        """
        Main method to parse the content of a directory in a list of assay objects.

        :param dir_path: the path to the directory to sniff
        :param group: the unix group_name considered to be the owner of the dir_path content
        :param username: an optional username to use as data owner
        :return the list of discovered Assay
        :except : AssayStructureError if the dir_path structure does not match sniffer's expectations
        """
        pass

    def sniff_datasets(self, dir_path: Path, group: str, username: Optional[str] = None) \
            -> List[Dataset]:
        """
        Calls sniff_instrument_run_assays() and iterate over the runs and assays to accumulate all assay.datasets
        returns all accumulated datasets
        """
        runs: List[InstrumentRun] = self.sniff_instrument_run_assays(dir_path=dir_path, group=group, username=username)
        if not runs:
            return list()
        all_datasets: List[Dataset] = list()
        for run in runs:
            for assay in run.assays:
                all_datasets.extend( assay.datasets )
        return all_datasets


class JSONAssaySniffer(AssaySniffer):
    def __init__(self, stocks_manager: StocksManager = None, **kwargs):
        super().__init__(stocks_manager=stocks_manager, **kwargs)

    @abstractmethod
    def get_json_schema(self, technology: Technology, platform: str) -> Any:
        """
        Returns the JSON schema this sniffer uses for the technology and platform.
        """
        pass
