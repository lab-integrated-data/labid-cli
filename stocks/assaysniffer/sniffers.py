import fnmatch
import glob
import json
import os
import re
from datetime import datetime
import logging
from pathlib import Path
from time import strftime, gmtime
from typing import Optional, List, Dict, Any, Tuple, Set

from jsonschema import validate
from stocks.models import InstrumentRun, User, Instrument, SequencingLibrary, NanoporeAssay, Dataset, \
    DatasetCollection, DatasetFile, FastqFile, Fast5File, FastqDir, Fast5Dir, DataProducer, SequencingAssay, \
    UserGroup, Sample, Pod5File, Pod5Dir, Project
from stocks import AssayStructureError
from cli.utils import Technology, SequencingReadType, SequencingRunType, NanoporeLiveBaseCallingType, ModelType, \
    ObjectState, is_uuid
from stocks.assaysniffer import JSONAssaySniffer, AssaySniffer, check_valid_directory, DatasetSniffer
from stocks.assaysniffer.registry import registry
import pandas as pd

from stocksapi.exceptions import MultipleObjectMatchedError
from stocksapi.manager import StocksManager

logger = logging.getLogger(__name__)


def list_files_in_dir(dir_path: Path | str, ext: str, gz_tolerant: bool = True) -> List[Path]:
    """
    util method to list files matching given extension (optionally tolerating extra '.gz') in the given dir_path

    :param dir_path:
    :param ext:
    :param gz_tolerant:
    :return:
    """
    dir_path = Path(dir_path)
    file_paths: List[Path] = []
    for path in dir_path.iterdir():
        if path.is_file() and (path.name.endswith(ext) or (gz_tolerant and path.name.endswith(ext + ".gz"))):
            file_paths.append(path)
    if len(file_paths) > 1:
        file_paths.sort()
    return file_paths


def just_one_file_in_dir(dir_path: Path, ext: str, gz_tolerant: bool = True) -> bool:
    """
    util method to check if the given dir_path contains a unique file matching given extension (optionally
    tolerating extra '.gz')

    :param dir_path:
    :param ext:
    :param gz_tolerant:
    :return: True if a single file matching the parameters is found in the dir_path ; else False (0 or >1 file
    found)
    """
    num: int = 0
    for path in dir_path.iterdir():
        if path.is_file() and (path.name.endswith(ext) or (gz_tolerant and path.name.endswith(ext + ".gz"))):
            num += 1
        if num > 1:
            return False

    return num == 1


def read_md5_from_disk(df: DatasetFile):
    """
    checks if a 'df.uri.md5' file exists and if so reads in the md5sum from it
    ie the 'df.uri.md5' is expected to only contain the md5sum
    """
    md5file_path: Path = Path(df.uri, ".md5")
    if md5file_path.exists():
        try:
            with open(str(md5file_path), 'r') as file:
                return file.read().rstrip()
        except Exception:
            return ""

@registry.install
class WildcardDatasetSniffer(DatasetSniffer):

    @staticmethod
    def keep_top_level_directories(paths: List[Path] | List[str]) -> List[str]:
        top_level_dirs: Set[Path] = set()
        s_paths = sorted(paths, key=lambda x: len(str(x)))
        for path in s_paths:
            path = Path(path)
            is_subdir = False
            for parent in top_level_dirs:
                if parent in path.parents:
                    is_subdir = True
                    break
            if not is_subdir:
                top_level_dirs.add(path)
        return [str(path) for path in top_level_dirs]

    @staticmethod
    def find_files(directory: Path | str, pattern: str,
                   recursive: bool = True, search_files: bool = True, search_directories: bool = False):
        """
        Recursively finds files or directories that match a specified pattern in a specified directory.

        Args:
            directory (str): The directory to search in.
            pattern (str): The pattern to match file or directory names against.
            recursive (bool, optional): Whether to search recursively or not. Defaults to True.
            search_files (bool, optional): Whether to search for files or not. Defaults to True.
            search_directories (bool, optional): Whether to search for directories or not. Defaults to False.

        Yields:
            str: The full path of each file or directory that matches the pattern.
        """
        if recursive:
            walker = os.walk(directory)
        else:
            walker = [(directory, [f for f in os.listdir(directory) if os.path.isdir(os.path.join(directory, f))],
                       [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))])]
        for root, dirs, files in walker:
            if search_files and not search_directories:
                for file_name in files:
                    if fnmatch.fnmatch(file_name, pattern):
                        yield os.path.join(root, file_name)
            elif search_directories and not search_files:
                for dir_name in dirs:
                    if fnmatch.fnmatch(dir_name, pattern):
                        yield os.path.join(root, dir_name)
            elif search_directories and search_files:
                for dir_name in files + dirs:
                    if fnmatch.fnmatch(dir_name, pattern):
                        yield os.path.join(root, dir_name)

    @staticmethod
    def remove_extension(path: Path | str) -> Tuple:
        """Remove the extension(s) from a file path.

        If the file name has one of the following compressed file extensions, both extensions will be removed:
        .zip, .gz, .bz2, .xz, .tar, .tar.gz, .tar.bz2, .tar.xz

        Args:
            path (str): The file path to remove the extension(s) from.

        Returns:
            tuple: A tuple containing the file path without any extensions and the stripped extension.
        """
        root, ext = os.path.splitext(path)
        stripped_ext = ""
        if ext in [".zip", ".gz", ".bz2", ".xz", ".tar", ".tar.gz", ".tar.bz2", ".tar.xz"]:
            root, stripped_ext = os.path.splitext(root)
            ext = stripped_ext + ext
        return root, ext

    @classmethod
    def get_sniffer_description(cls) -> str:
        return """
        Search for files (search_files is True) or folders (search_folders is True) which names match a search pattern
        (given in 'pattern'). The search is recursive (ie scans sub-folders) if 'recursive' parameter is True.
        Each file/folder found becomes a Dataset with type 'dataset_type'.
        Finally, the overall Dataset list is return as a unique DatasetCollection named after the 'collection_name' 
        parameter. 
        """

    @classmethod
    def get_sniffer_param_names(cls) -> Dict[str, str]:
        sniffer_params_map = dict()
        sniffer_params_map['collection_name'] = "The dataset collection name."
        sniffer_params_map['pattern'] = "The search pattern"
        sniffer_params_map['recursive'] = "Should we recursively look into sub-folders"
        sniffer_params_map['dataset_type'] = "The dataset type."
        sniffer_params_map['search_files'] = "Are we looking for files?"
        sniffer_params_map['search_folders'] = "Are we looking for folders?"

        return sniffer_params_map

    def __init__(self,
                 collection_name: str = "dataset_collection",
                 pattern: str = "*",
                 recursive: bool = False,
                 dataset_type: str = "generic",
                 search_files: bool = False,
                 search_folders: bool = False,
                 stocks_manager: StocksManager = None,
                 **kwargs):
        super().__init__(stocks_manager=stocks_manager, **kwargs)
        self.collection_name: str = collection_name
        self.pattern: str = pattern
        self.recursive: bool = recursive
        self.dataset_type: str = dataset_type
        self.search_files: bool = search_files
        self.search_folders: bool = search_folders

    def sniff_datasets(self, dir_path: Path, group: str, username: Optional[str] = None) \
            -> List[Dataset] | List[DatasetCollection]:
        now = strftime("%Y-%m-%d_%Hh%Mm", gmtime())
        collection: DatasetCollection = DatasetCollection(
            name=f"{self.collection_name}", description=f"Datasets batch loaded from {str(dir_path)}")
        datasets: List[Dataset] = []

        # need to wrap in list() as find_files() returns a Generator
        path_as_str_lst: List[str] = list(self.find_files(
            directory=dir_path, pattern=self.pattern, recursive=self.recursive,
            search_files=self.search_files, search_directories=self.search_folders
        ))
        if self.search_folders:
            # we need to make sure we dont have nested paths, only keep the top dirs if we do
            logger.debug(path_as_str_lst)
            path_as_str_lst = self.keep_top_level_directories(path_as_str_lst)
            logger.debug(path_as_str_lst)

        for datafile_path in path_as_str_lst:
            p: Path = Path(datafile_path)
            size = 0
            if self.search_files:
                size = os.path.getsize(datafile_path)
            ds_name, stripped_ext = self.remove_extension(p.name)
            stripped_ext = stripped_ext.lstrip(".")
            now = strftime("%Y-%m-%d %H:%M:%S", gmtime())
            descr = f"Imported from {datafile_path} on {now}"
            # we init the filetype to the ext by default
            dataset_file: DatasetFile = DatasetFile(
                name=p.name, uri=datafile_path, is_dir=self.search_folders, mime_type=stripped_ext, byte=size,
                filetype=stripped_ext
            )
            dataset: Dataset = Dataset(name=ds_name, is_raw=False, description=descr, datafiles=[dataset_file],
                                       collection=collection,
                                       dataset_type=self.dataset_type)

            datasets.append(dataset)

        collection.datasets = datasets

        return [collection]

class PooledIlluminaInitializedAssayValidator(AssaySniffer):

    @classmethod
    def get_sniffer_description(cls) -> str:
        return """
        This sniffer validates an assay that is "initialized" in LabID e.g. as initialized upon data transfer from 
        EMBL GeneCore. The assay reflects a pooled assay with dozens of datasets coming from one or more pooled samples.
        A typical example is pool library preparation from material sorted in plates (eg 96 or 384 wells) where all 
        datasets from a given plate should be connected to a unique sequencing library representing the plate.
        The sniffer therefore needs a way to extract the common libray name from the library names saved with the
        initialized assay. This is provided by the sample_name_regex that must define at least one capture group i.e. only
        the first capture group is used to extract the pooled sample name.
        For example, using r'(.+)(PE20|iTRU)\w+' all datasets connected to libraries "Plate1PE20A1" ... "Plate1PE20H12"
        will be connected to the pooled library "Plate1PE20".
        Once the common sample name eg "Plate1PE20" is extracted from, a second optional pattern (sample_format_regex)
        can be used to reformat the sample name by concatenating all matched groups with the sample_format_spacer
        Finally, if sample_must_exist is True, the reformatted sample name is used to fetch the sample UUID from LabID. An
        error is raised if the lookup fails or yields multiple results
        
        Custom sample name extraction and reformatting can also be done by overwriting the extract_sample_name() and
        reformat_sample_name()
        """

    def __init__(self,
                 sample_name_regex: str,
                 sample_must_exist: bool,
                 sample_format_regex: str | None = None,
                 sample_format_spacer: str = "_",
                 pooled_sample: bool = True,
                 sample_search_params: dict | None = None,
                 stocks_manager: StocksManager = None,
                 platform: str = "ILLUMINA",
                 producer: str = "GeneCore",
                 **kwargs):
        """
        sample_search_params: can be used to pass additional parameters to the fetch_item_by_name() call
        e.g. {'owner': 'girardot}. Used only if sample_must_exist is True
        """
        super().__init__(stocks_manager=stocks_manager, **kwargs)
        self.sample_name_regex: str = sample_name_regex
        self.sample_format_regex: str = sample_format_regex
        self.sample_format_spacer: str = sample_format_spacer
        self.sample_must_exist: bool = sample_must_exist
        self.sample_search_params: dict | None = sample_search_params
        self.sample_type: ModelType = ModelType.SEQUENCINGLIBRARY
        # when pooled_sample is True, all barcode are set to POOL
        self.pooled_sample = pooled_sample
        self.name_pattern: re.Pattern = re.compile(sample_name_regex)
        self.fmt_pattern: re.Pattern = re.compile(sample_format_regex) if sample_format_regex else None
        self.assay: SequencingAssay = None
        self.platform = platform
        self.producer = producer
        logger.debug(sample_format_regex)

    @classmethod
    def get_sniffer_param_names(cls) -> Dict[str, str]:
        params = {
            'sample_name_regex': 'a regex with one group to extract the pooled sample name',
            'sample_must_exist': 'True or False. If True the sample must exist in LabID ',
            'sample_format_regex': 'optional pattern to reformat the extracted pooled sample name. The new name is '
                                   'obtained by concatenating all matched groups with the sample_format_spacer',
            'sample_format_spacer': 'spacer to use to concatenate all matched groups from sample_format_regex',
            'platform': 'the sequencing platform, default is ILLUMINA',
            'producer': 'the producer of the data, default is GeneCore'
        }
        return params

    def extract_sample_name(self, sample_name: str) -> str:
        """
        Override this method to adapt how the sample name should be extracted
        @param sample_name: the sample name as registered in the initialized assay
        @raise AttributeError if match is not productive
        """
        results = self.name_pattern.match(sample_name)
        return results.group(1)

    def reformat_sample_name(self, sample_name: str) -> str:
        """
        @param sample_name: the sample name as extracted using the extract_sample_name()
        @raise AttributeError if match is not productive
        """
        results = self.fmt_pattern.match(sample_name)
        return self.sample_format_spacer.join(results.groups())

    def dir_qualifies(self, dir_path: Path) -> bool:
        # we expect to be in a rundir of some registered assay in INITIALIZED state
        self.assay = self.stocks_manager.fetch_assay(run_dir=str(dir_path), load_ownership=True)
        if not self.assay:
            raise AssertionError(f"No Assay found for run dir {str(dir_path)}")
        elif self.assay.state != ObjectState.INITIALIZED:
            raise AssertionError(f"Assay state of {self.assay.name} (id={self.assay.id}) is {self.assay.state.value}"
                                 f" while expecting {ObjectState.INITIALIZED.value}")

        return True

    def sniff_instrument_run_assays(self, dir_path: Path, group: str, username: Optional[str] = None) \
            -> List[InstrumentRun]:

        if not self.assay:
            self.dir_qualifies(dir_path)

        logger.debug(self.assay.as_simple_json())
        the_run: InstrumentRun | str = self.assay.instrumentrun
        # usually the run is not loaded on the assay, we need it to comply to the return contract
        if isinstance(the_run, str) and is_uuid(the_run):
            the_run = self.stocks_manager.fetch_instrument_run(the_run)
            self.assay.instrumentrun = the_run

        the_run.assays = [self.assay]
        # below should not be needed, but it does not hurt
        the_run.managed = True
        the_run.technology = Technology.SEQUENCING
        the_run.platform = self.platform
        the_run.producer = self.producer

        # the assay must have its info set
        assay_info = json.loads(self.assay.info)
        snif: GeneCoreAssaySniffer = GeneCoreAssaySniffer()
        info_run: InstrumentRun = snif.load_run_from_json_obj(
            data=assay_info, data_dir=dir_path, group=group, username=username)

        reduced_samples: dict = dict()

        for d in info_run.assays[0].datasets:
            try:
                pooled_name = self.extract_sample_name(d.samples[0].name)
            except AttributeError:
                # non-productive match -> the .group() call raises the AttributeError
                raise AssayStructureError(
                    f"sample {d.samples[0].name} do not match expected naming convention:{self.sample_name_regex}")

            if self.fmt_pattern:
                try:
                    pooled_name = self.reformat_sample_name(pooled_name)
                    logger.debug(f"reformatted sample name: {pooled_name}")
                except AttributeError:
                    # non-productive match -> the .groups() call raises the AttributeError
                    raise AssayStructureError(
                        f"Extracted sample {pooled_name} name cannot be reformatted using :{self.sample_format_regex}")

            d.samples[0].name = pooled_name
            if self.pooled_sample:
                d.samples[0].barcode = "POOL"

            if self.sample_must_exist:
                if pooled_name in reduced_samples:
                    d.samples[0].id = reduced_samples[pooled_name].id
                    d.samples[0].primary_project = reduced_samples[pooled_name].primary_project
                else:
                    try:
                        o = self.stocks_manager.fetch_item_by_name(
                            name=pooled_name,
                            model=self.sample_type,
                            query_params=self.sample_search_params)
                        if not o:
                            mess = f"No {self.sample_type.name} found for '{pooled_name}': sample must exist in LabID."
                            raise AssayStructureError(mess)
                        else:
                            logger.debug(f"ID {o['id']} found for name {pooled_name}")
                            d.samples[0].id = o['id']
                            # create project from primary_project dict in o
                            logger.debug(o['primary_project'])
                            p: Project = Project(**o['primary_project'])
                            d.samples[0].primary_project = p

                    except MultipleObjectMatchedError as mome:
                        logger.warning(mome)
                        # problem might be that the API uses a starts_with and not an exact match
                        raise AssayStructureError(f"More than one {self.sample_type.name} match name {pooled_name}!")

            if pooled_name not in reduced_samples:
                reduced_samples[pooled_name] = d.samples[0]

            d.owner = self.assay.owner
            d.owned_by = self.assay.owned_by
            # we can also setup the size and md5sum if we have access to it
            for df in d.datafiles:
                if Path(df.uri).exists():
                    df.md5sum = read_md5_from_disk(df)
                    df.byte = Path(df.uri).stat().st_size

        info_run.assays[0].samples = list(reduced_samples.values())

        # transfer original data
        the_run.assays[0].datasets = info_run.assays[0].datasets
        the_run.assays[0].samples = info_run.assays[0].samples

        return [the_run]

    @classmethod
    def get_supported_technology(cls) -> Technology:
        """
        :return: the technology this sniffer supports
        """
        return Technology.SEQUENCING

    @classmethod
    def get_supported_platforms(cls) -> List[str]:
        """
        :return: platforms: the list of Platform this sniffer supports or None if the
        """
        return ["ILLUMINA"]

    @classmethod
    def is_multi_run_sniffer(cls) -> bool:
        """
        Tells if the sniffer can sniff more than one run.
        :return: true if more than one instrument run can be sniffed by this sniffer
        """
        return False


@registry.install
class SimpleIlluminaInitializedAssayValidator(PooledIlluminaInitializedAssayValidator):
    @classmethod
    def get_sniffer_description(cls) -> str:
        return """
            This sniffer validates a sequencing assay that is "initialized" in LabID (e.g. as initialized upon data 
            transfer from EMBL GeneCore) **provided that all the sequencing libraries exist in LabID**. 
            
            Importantly, the sequencing library look up by name must return a unique library, else an error is raised. 
            The lookup can be restricted to a specific sample's owner using the 'sample_owner' parameter.
            The sniffer can be activated using different triggers: 
                - the assay owner or/and
                - a sample name pattern 'sample_name_regex'.  
            
            Once a qualifying assay is identified, the optional "sample_format_regex" pattern is used to reformat the 
            sample name by concatenating all matched groups with the 'sample_format_spacer'. 
            
            Finally, the reformatted sample name is used to fetch the sample UUID from LabID and an error is raised if 
            the lookup fails or yields multiple results. 

            Custom sample name extraction and reformatting can also be done by overwriting the extract_sample_name() and
            reformat_sample_name()
            """

    @classmethod
    def get_sniffer_param_names(cls) -> Dict[str, str]:
        params = {
            'sample_name_regex': 'the pattern *all* sample names must match to qualify the assay',
            'assay_owner': 'only assays with this owner username will qualify. If not set, all assays are considered',
            'sample_owner': 'owner username of the samples to restrict the lookup to',
            'sample_format_regex': 'optional pattern to reformat the extracted pooled sample name. The new name is '
                                   'obtained by concatenating all matched groups with the sample_format_spacer',
            'sample_format_spacer': 'spacer to use to concatenate all matched groups from sample_format_regex',
            'platform': 'the sequencing platform, default is ILLUMINA',
            'producer': 'the producer of the data, default is GeneCore'
        }
        return params

    def __init__(self,
                 sample_name_regex: str | None = None,
                 assay_owner: str | None = None,
                 sample_owner: str | None = None,
                 sample_format_regex: str | None = None,
                 sample_format_spacer: str = "_",
                 platform: str = "ILLUMINA",
                 producer: str = "GeneCore",
                 stocks_manager: StocksManager = None,
                 **kwargs):
        self.assay_owner: str | None = assay_owner
        self.sample_owner: str | None = sample_owner
        sample_qs_params: dict | None = None
        if self.sample_owner:
            sample_qs_params = {'owner': self.sample_owner}
        # check if a sample_name_regex is provided, if so this should be used to detect qualifying assays
        self.qualifying_pattern: re.Pattern | None = None
        if sample_name_regex:
            self.qualifying_pattern: re.Pattern = re.compile(sample_name_regex)

        super().__init__(
            sample_name_regex="(.+)",  # the sample name will be the full name
            sample_must_exist=True,
            sample_format_regex=sample_format_regex,
            sample_format_spacer=sample_format_spacer,
            pooled_sample=False,
            sample_search_params=sample_qs_params if sample_qs_params else None,
            platform=platform,
            producer=producer,
            stocks_manager=stocks_manager,
            **kwargs)

    def dir_qualifies(self, dir_path: Path) -> bool:

        if super().dir_qualifies(dir_path=dir_path):
            # self.assay is now set, its owner slot contains the Owner object
            # check owner if needed
            if self.assay_owner and self.assay.owner.name != self.assay_owner:
                # not the correct owner
                return False

            # do we need to check the sample names?
            if self.qualifying_pattern:
                # load the assay description
                assay_info = json.loads(self.assay.info)
                snif: GeneCoreAssaySniffer = GeneCoreAssaySniffer()
                info_run: InstrumentRun = snif.load_run_from_json_obj(data=assay_info, data_dir=dir_path, group="")
                # all sample names must match the expected pattern
                for d in info_run.assays[0].datasets:
                    if not self.qualifying_pattern.match(d.samples[0].name):
                        return False

        return True


@registry.install
class NanoporeAssaySniffer(AssaySniffer):
    # see https://github.com/nanoporetech/minknow_api for some concept definition (in the README)

    # currently, 12 but announced to be 96 in near future
    MAX_BARCODE_NUMBER: int = 12

    @classmethod
    def is_multi_run_sniffer(cls) -> bool:
        return False

    @classmethod
    def get_supported_technology(cls) -> Technology:
        """
        :return: the technology this sniffer supports ie 'Technology.SEQUENCING'
        """
        return Technology.SEQUENCING

    @classmethod
    def get_supported_platforms(cls) -> List[str]:
        """
        :return: platforms: the list of Platform this sniffer supports
        """
        return ["NANOPORE"]

    @classmethod
    def get_sniffer_description(cls) -> str:
        help_txt = f"""
        This sniffer looks into a directory expecting the native project_id structure created by nanopore sequencer:
            - PROJECT_FOLDER => an option project_id folder regrouping multiple samples & runs
                - LIBRARY/SAMPLE_FOLDER(s) => an optional library (potentially multiplexed) folder containing 
                                            results from 1 or more runs (tech replicates)
                    - RUN_FOLDER(s)
                        - barcode_alignment_* file: contains a single line when the sample is not a multiplexed library.
                                                    MANDATORY
                        - final_summary_* file: key=value summary of main parameters. 
                                                MANDATORY
                        - report_*.md file: a multi-section file with an initial JSON session holding all but more 
                                            params than final_summary_* file
                                            MANDATORY
                        - duty_time_* file: we will assume this optional (as we dont use it)
                        - throughput_* file: we will assume this optional (as we dont use it)
                        - other_reports/*csv: additional files generate during run. We wont assume anything here   
                        
                        If base calling was OFF:
                        - fast5 | pod5 : a directory containing the raw fast5/pod5 signal. MANDATORY
                        
                        If base calling was ON:
                        - sequencing_summary_* file: a big file listing all the reads sequenced and in which fastQ/fast5
                                                    file(s) they are in, this file is multi Gb big.
                                                    MANDATORY when base calling is true

                        - (fast|pod)5_fail & (fast|pod)5_pass: directories containing the fast5/pod5 files for failed
                                                    and passed reads.
                                                    A sub-directory layer 'barcode01-12' and 'unclassified' is present 
                                                    if the library is multiplexed. 
                                                    one of the two is MANDATORY
                        - fastq_fail & fastq_pass: optional directories (present if base calling was done) containing 
                                                    the fastq files for failed and passed reads. 
         
        The run folder detection occurs by looking for the presence of files matching the pattern
        'final_summary_*.txt' and/or 'report_*.md' (both must be found). 
        Each directory containing such a file will be parsed into a nanopore assay. 
        
        When the fast5/pod5 (and fastq) directories contain more than one file, the directories are registered as 
        'multi-fast5'/'multi-pod5'/'multi-fastq' dataset directories. When unique files are found per dir_path, 
        there are registered as pod5/fast5/fastq dataset files. 
         Each '(fast|pod)5', '(fast|pod)5_pass'/'(fast|pod)5_fail', 'fastq_pass' and 'fastq_fail'
         will end up as a different dataset collection when found. 
        
        The 'sequencing_summary_* file' and verbose other metadata files are also registered as a dataset
        
        An Assay is created for each *run*.
        
        The instrument serial number (matching the instrument's code in LabID) is taken from the metadata 
        'sequencer_serial_number' when present. When absent, the serial number will be created as 
        '<device_type>-<host_product_serial_number>' to differenciate between gridion and p2 models (as both would 
        indicate the same 'host_product_serial_number') 
         """
        return help_txt

    def looks_like_nanopore_project_dir(self, dir_path: Path) -> int:
        """
        inspects the given directory for being a Nanopore top project_id dir_path.

        :param dir_path: the path to Nanopore project_id dir_path
        :return int: the run number found (or raises exception)
        :raises: AssayStructureError if expected content is not found
        """
        # we grab all the final_summary_* files. One per run should be found
        run_num = 0
        for path in dir_path.rglob('final_summary_*.txt'):
            run_num += 1
            run_dir: Path = path.parent

            # each run folder must contain a number of files/dir_path
            fast5_pass: bool = Path(run_dir, "fast5_pass").exists() or Path(run_dir, "pod5_pass").exists()
            fast5_fail: bool = Path(run_dir, "fast5_fail").exists() or Path(run_dir, "pod5_fail").exists()
            fast5: bool = Path(run_dir, "fast5").exists() or Path(run_dir, "pod5").exists()

            if not any([fast5, fast5_fail, fast5_pass]):
                raise AssayStructureError(f"Expected fast5* dir_path not found in Nanopore run dir_path {str(run_dir)}")

            for expected in ['barcode_alignment_*.tsv', 'report_*.md']:
                if not self.nanopore_file_exists(run_dir, expected):
                    raise AssayStructureError(
                        f"File matching {expected} pattern not found in Nanopore run folder {str(run_dir)}")

            if Path(run_dir, "fastq_pass").exists():
                if not self.nanopore_file_exists(run_dir, 'sequencing_summary_*.txt'):
                    raise AssayStructureError(
                        "sequencing_summary_*.txt not found while base calling is on (fasq_pass dir_path present) " +
                        f"in Nanopore run folder {str(run_dir)}")

        return run_num

    @staticmethod
    def nanopore_file_exists(a_dir: Path, filename_pattern: str):
        for x in a_dir.glob(pattern=filename_pattern):
            return x.exists()
        return False

    def sniff_instrument_run_assays(self, dir_path: Path, group: str, username: Optional[str] = None) \
            -> List[InstrumentRun]:
        owner: User | None = None
        if username:
            owner = User(username=username, groups=[UserGroup(name=group)])

        if not check_valid_directory(dir_path):
            mess = f"The dir_path {str(dir_path)} is either empty or does not point to a valid directory."
            raise AssayStructureError(mess)

        runs: List[InstrumentRun] = list()
        # validate directory
        try:
            if self.looks_like_nanopore_project_dir(dir_path) == 0:
                return runs
        except AssayStructureError as err:
            mess = f"""The dir_path {str(dir_path)} does not fit the expected Nanopore data structure:
                    {self.get_sniffer_description()}
    
                    Error is : {str(err)}
                    """
            raise AssayStructureError(mess)

        for path in dir_path.rglob('report_*.md'):
            params: Dict[str, str] = self.get_param_dict_from_nanopore_report(path)
            final_params: Dict[str, str] = self.get_param_dict_from_nanopore_final_summary(path.parent)

            sample_id = params['sample_id']

            # instrument details
            # the issue is that the promethION will give exactly the same params['host_product_serial_number'] as
            # a GridION since the main sequencer is still the gridion, with promethion beeing an adapter or
            # addon of sorts, it is not a standalone machine

            instrument_serial = f"{params['device_type']}"
            if 'host_product_serial_number' in params:
                instrument_serial = f"{params['device_type']}-{params['host_product_serial_number']}"
            elif 'sequencer_serial_number' in params:
                # this was added ~2023
                instrument_serial = params['sequencer_serial_number']

            instrument_name = params['hostname']
            model = params['device_type']  # eg gridion, p2
            if 'host_product_code' in params:
                model_code = params['host_product_code']
            else:
                # this is a very old run, we set this to unknown and expect the user to provide the instrument UUID
                model_code = "unknown"
            instrument = Instrument(name=f"{model} {instrument_name}", model=model_code,
                                    serial_number=instrument_serial)
            # run details
            run_name = path.parent.name
            run_id = params['run_id']
            run = InstrumentRun(name=f"{instrument.name} run {run_id}", managed=False, technology=Technology.SEQUENCING,
                                platform="NANOPORE", instrument=instrument)
            if owner:
                run.set_owner(owner=owner, also_set_group=True)
            else:
                run.group = group

            run.add_annotation(annot_key="position", annot_value=params['device_id'])

            flowcell = params['flow_cell_id']
            flowcell_version = params['flow_cell_product_code']
            protocol = final_params['protocol']
            # protocol=sequencing/sequencing_MIN106_DNA:FLO-MIN106:SQK-LSK110
            # here 2 pieces of info seems avail => 'DNA' (as the run_mode) and 'SQK-LSK110' (as the chemistry)
            protocol_splits = protocol.split(":")
            chemistry: str = protocol_splits.pop()
            run_mode: str = protocol_splits.pop()
            # run times
            start_time = datetime.fromisoformat(final_params['started']).replace(microsecond=0)
            end_time = datetime.fromisoformat(final_params['processing_stopped']).replace(microsecond=0)
            run.start_datetime = start_time
            run.end_datetime = end_time
            # td = end_time - start_time
            # run_duration: str = str(td)  ## not used

            # is this sample multiplexed ?
            barcode_df = self.sniff_multiplexing_info(run_dir=path.parent)
            multiplexed = len(barcode_df) > 1
            # if we have fastq files, base calling was on
            with_base_calling = bool(final_params['basecalling_enabled'])
            datasets: List[Dataset] = []
            samples: List[SequencingLibrary] = []

            if multiplexed:
                # we need to find how many multiplexed samples we had
                # create the different dataset
                if Path(path.parent, "fast5").exists():
                    datasets.extend(self.get_nanopore_demultiplexed_datasets(
                        run_dir=path.parent, dir_name="fast5", sample_base_name=sample_id, file_type="fast5",
                        barcode_number=self.MAX_BARCODE_NUMBER))
                elif Path(path.parent, "pod5").exists():
                    datasets.extend(self.get_nanopore_demultiplexed_datasets(
                        run_dir=path.parent, dir_name="pod5", sample_base_name=sample_id, file_type="pod5",
                        barcode_number=self.MAX_BARCODE_NUMBER))

                if Path(path.parent, "fast5_pass").exists():
                    datasets.extend(self.get_nanopore_demultiplexed_datasets(
                        run_dir=path.parent, dir_name="fast5_pass", sample_base_name=sample_id, file_type="fast5",
                        barcode_number=self.MAX_BARCODE_NUMBER))
                elif Path(path.parent, "pod5_pass").exists():
                    datasets.extend(self.get_nanopore_demultiplexed_datasets(
                        run_dir=path.parent, dir_name="pod5_pass", sample_base_name=sample_id, file_type="pod5",
                        barcode_number=self.MAX_BARCODE_NUMBER))

                if Path(path.parent, "fast5_fail").exists():
                    datasets.extend(self.get_nanopore_demultiplexed_datasets(
                        run_dir=path.parent, dir_name="fast5_fail", sample_base_name=sample_id, file_type="fast5",
                        barcode_number=self.MAX_BARCODE_NUMBER))
                elif Path(path.parent, "pod5_fail").exists():
                    datasets.extend(self.get_nanopore_demultiplexed_datasets(
                        run_dir=path.parent, dir_name="pod5_fail", sample_base_name=sample_id, file_type="pod5",
                        barcode_number=self.MAX_BARCODE_NUMBER))

                if Path(path.parent, "fastq_pass").exists():
                    datasets.extend(self.get_nanopore_demultiplexed_datasets(
                        run_dir=path.parent, dir_name="fastq_pass", sample_base_name=sample_id, file_type="fastq",
                        barcode_number=self.MAX_BARCODE_NUMBER))

                if Path(path.parent, "fastq_fail").exists():
                    datasets.extend(self.get_nanopore_demultiplexed_datasets(
                        run_dir=path.parent, dir_name="fastq_fail", sample_base_name=sample_id, file_type="fastq",
                        barcode_number=self.MAX_BARCODE_NUMBER))

                # get unique list of samples
                name2lib: dict[str, Sample] = dict()
                for d in datasets:
                    for _smpl in d.samples:
                        if _smpl.name not in name2lib:
                            name2lib[_smpl.name] = _smpl
                samples = list(name2lib.values())

            else:
                # we have a single assay, a single sample
                sample = SequencingLibrary(name=sample_id, barcode=None)
                samples.append(sample)
                # create the different dataset
                if Path(path.parent, "fast5").exists():
                    datasets.append(self.get_nanopore_dataset(run_dir=path.parent, dir_name="fast5", sample=sample,
                                                              file_type="fast5"))
                elif Path(path.parent, "pod5").exists():
                    datasets.append(self.get_nanopore_dataset(run_dir=path.parent, dir_name="pod5", sample=sample,
                                                              file_type="pod5"))

                if Path(path.parent, "fast5_pass").exists():
                    datasets.append(self.get_nanopore_dataset(run_dir=path.parent, dir_name="fast5_pass", sample=sample,
                                                              file_type="fast5"))
                elif Path(path.parent, "pod5_pass").exists():
                    datasets.append(self.get_nanopore_dataset(run_dir=path.parent, dir_name="pod5_pass", sample=sample,
                                                              file_type="pod5"))
                if Path(path.parent, "fast5_fail").exists():
                    datasets.append(self.get_nanopore_dataset(run_dir=path.parent, dir_name="fast5_fail", sample=sample,
                                                              file_type="fast5"))
                elif Path(path.parent, "pod5_fail").exists():
                    datasets.append(self.get_nanopore_dataset(run_dir=path.parent, dir_name="pod5_fail", sample=sample,
                                                              file_type="pod5"))

                if Path(path.parent, "fastq_pass").exists():
                    datasets.append(self.get_nanopore_dataset(run_dir=path.parent, dir_name="fastq_pass", sample=sample,
                                                              file_type="fastq"))

                if Path(path.parent, "fastq_fail").exists():
                    datasets.append(self.get_nanopore_dataset(run_dir=path.parent, dir_name="fastq_fail", sample=sample,
                                                              file_type="fastq"))

            datasets.extend(self.get_all_metadata_file_as_datasets(run_dir=path.parent))
            # create the assay for this run dir_path
            assay = NanoporeAssay(name=run_name, flowcell=flowcell, flowcell_version=flowcell_version,
                                  datasets=datasets, samples=samples, instrumentrun=run,
                                  chemistry=chemistry, run_mode=run_mode,
                                  multiplexed=False, demultiplexed=False)
            if owner:
                assay.set_owner(owner=owner, also_set_group=True)
            else:
                assay.group = group

            assay.live_base_calling = NanoporeLiveBaseCallingType.OTHER if with_base_calling \
                else NanoporeLiveBaseCallingType.NONE
            run.add_assay(assay)
            # add run in result list
            runs.append(run)

        return runs

    def get_nanopore_demultiplexed_datasets(self, run_dir: Path, dir_name: str, sample_base_name: str, file_type: str,
                                            barcode_number: int, ignore_empty_dir: bool = True) -> List[Dataset]:
        """
        :param run_dir: the base dir_path
        :param dir_name: the dir_path name to inspect (this dir_path is in run_dir)
        :param sample_base_name: the multiplexed library name
        :param file_type: either fast5 or fastq
        :param barcode_number : the number of barcode to expect (12 or 96 as announced)
        :return:
        """
        if file_type not in ['fastq', 'fast5', 'pod5']:
            raise ValueError(f"file_type must be one of 'fastq', 'fast5', 'pod5' but was {file_type}")

        col = DatasetCollection(name=dir_name)
        base_dir = Path(run_dir, dir_name)
        # user may have chosen to output all reads in a single file, in which case we ll register the file as a dataset
        # else we register the directory as a dataset
        single_file: bool = just_one_file_in_dir(Path(base_dir, "barcode01"), file_type)
        datasets: List[Dataset] = []
        barcode_names: List[str] = [f"barcode{n:02d}" for n in range(1, barcode_number + 1)]
        barcode_names.append('unclassified')
        for barcode in barcode_names:
            the_dir = Path(base_dir, barcode)
            if not the_dir.exists():
                raise AssayStructureError(f"Multiplexed result directory missing : {str(the_dir)}")
            if ignore_empty_dir and not list(the_dir.iterdir()):
                continue
            lib: SequencingLibrary = SequencingLibrary(name=f"{sample_base_name}_{barcode}", barcode=barcode)
            data_file: DatasetFile
            if single_file:
                the_file = Path(the_dir, list_files_in_dir(the_dir, file_type)[0])
                if file_type == 'fastq':
                    data_file = FastqFile(name=f"{dir_name} file", read_type=SequencingReadType.READ1,
                                          byte=os.path.getsize(str(the_file)), mime_type="application/gzip",
                                          uri=str(the_file))
                elif file_type == 'fast5':
                    data_file = Fast5File(name=f"{dir_name} file", byte=os.path.getsize(str(the_file)),
                                          uri=str(the_file))
                else:
                    data_file = Pod5File(name=f"{dir_name} file", byte=os.path.getsize(str(the_file)),
                                         uri=str(the_file))
            else:
                if file_type == 'fastq':
                    data_file = FastqDir(name=f"{dir_name} directory", read_type=SequencingReadType.READ1,
                                         mime_type="application/gzip", uri=str(the_dir))
                elif file_type == 'fast5':
                    data_file = Fast5Dir(name=f"{dir_name} directory", uri=str(the_dir))
                else:
                    data_file = Pod5Dir(name=f"{dir_name} directory", uri=str(the_dir))

            datasets.append(Dataset(name=data_file.name, is_raw=True,
                                    datafiles=[data_file], samples=[lib], collection=col))

        return datasets

    @staticmethod
    def get_all_metadata_file_as_datasets(run_dir: Path) -> List[Dataset]:
        """
        registers all file found in the indicated run_dir as datasets without link to samples. If present, the
        sub dir_path 'other_reports' is also scanned.
        Skips hidden files (name starting with a dot '.').

        :param run_dir:
        :return: a list of Datasets groups into a DatasetCollection; or an empty list
        """
        datasets: List[Dataset] = list()
        col: DatasetCollection = DatasetCollection(name="metadata files")
        for f in [p for p in run_dir.iterdir() if p.is_file() and not p.name.startswith(".")]:
            df: DatasetFile = DatasetFile(name=f.name, mime_type="", byte=os.path.getsize(str(f)),
                                          filetype=f.suffix[1:],
                                          uri=str(f))
            datasets.append(Dataset(name=f.name, is_raw=False, datafiles=[df], collection=col))

        # other_reports subdir
        if Path(run_dir, 'other_reports').exists():
            for f in [p for p in Path(run_dir, 'other_reports').iterdir() if p.is_file()]:
                df: DatasetFile = DatasetFile(name=f"other_reports/{f.name}", mime_type="",
                                              byte=os.path.getsize(str(f)),
                                              filetype=f.suffix[1:], uri=str(f))
                datasets.append(Dataset(name=f.name, is_raw=False, datafiles=[df], collection=col))

        return datasets

    def get_nanopore_dataset(self, run_dir: Path, dir_name: str, sample: SequencingLibrary, file_type: str) -> Dataset:
        """

        :param run_dir: the base dir_path
        :param dir_name: the dir_path name to inspect (this dir_path is in run_dir)
        :param sample: the sample name
        :param file_type: either fast5 or fastq
        :return:
        """
        if file_type not in ['fastq', 'fast5']:
            raise ValueError(f"file_type must be one of 'fastq', 'fast5' but was {file_type}")

        col = DatasetCollection(name=dir_name)
        the_dir = Path(run_dir, dir_name)
        data_file: DatasetFile
        single_file: bool = just_one_file_in_dir(the_dir, file_type)

        if single_file:
            the_file = Path(the_dir, list_files_in_dir(the_dir, file_type)[0])
            if file_type == 'fastq':
                data_file = FastqFile(name=f"{dir_name} file", read_type=SequencingReadType.READ1,
                                      byte=os.path.getsize(str(the_file)), mime_type="application/gzip",
                                      uri=str(the_file))
            else:
                data_file = Fast5File(name=f"{dir_name} file", byte=os.path.getsize(str(the_file)), uri=str(the_file))
        else:
            if file_type == 'fastq':
                data_file = FastqDir(name=f"{dir_name} directory", read_type=SequencingReadType.READ1,
                                     mime_type="application/gzip", uri=str(the_dir))
            else:
                data_file = Fast5Dir(name=f"{dir_name} directory", uri=str(the_dir))

        return Dataset(name=data_file.name, is_raw=True, datafiles=[data_file], samples=[sample],
                       collection=col)

    @staticmethod
    def get_param_dict_from_nanopore_report(report_path: Path) -> Dict[str, str]:
        """
        Loads the json params from the Tracking ID section of the given report

        :param report_path:
        :return:
        """

        json_str = ""
        keep = False
        with open(report_path) as sum_file:
            for line in sum_file:
                if keep or line.startswith('{'):
                    keep = True
                    json_str += line
                if keep and line.startswith('}'):
                    break
        params = json.loads(json_str)
        return params

    @staticmethod
    def get_param_dict_from_nanopore_final_summary(run_dir: Path) -> Dict[str, str]:
        """
        Load the key/value params from the final_summary_*.txt file

        :param run_dir:
        :return:
        """
        params: Dict[str, str] = dict()
        found_it: bool = False
        for sum_file_path in run_dir.glob('final_summary_*.txt'):
            if found_it:
                raise AssayStructureError(f"Found more than one final_summary_*.txt file in run dir_path {run_dir}")
            with open(str(sum_file_path)) as sum_file:
                for line in sum_file:
                    name, var = line.partition("=")[::2]
                    params[name.strip()] = var.strip()

        return params

    @staticmethod
    def sniff_multiplexing_info(run_dir: Path) -> Any:
        """
        returns the barcode file (barcode_alignment_*.tsv) content as a panda dataframe
        :param run_dir:
        :return:
        """
        # finds the barcode file
        bc_file: Optional[Path] = None
        for x in run_dir.glob(pattern='barcode_alignment_*.tsv'):
            bc_file = x
        if not bc_file or not bc_file.exists():
            raise FileNotFoundError("Barcode file not found (pattern 'barcode_alignment_*.tsv')")
        return pd.read_csv(str(bc_file), sep='\t')


@registry.install
class GeneCoreAssaySniffer(JSONAssaySniffer):
    JSON_NAME_PATTERN: str = '*_lane[0-9].json'
    JSON_SPECS_URL: str = 'https://git.embl.de/grp-gbcs/stocks-server/-/issues/558'

    def __init__(self, **kwargs):
        super(GeneCoreAssaySniffer, self).__init__(**kwargs)

    @classmethod
    def get_sniffer_description(cls) -> str:
        help_txt = f"""
         This sniffer looks for a json file matching {GeneCoreAssaySniffer.JSON_NAME_PATTERN} describing:
           - a unique Illumina assay and its details
           - the input samples  
           - the different FastQ files to be imported and their relationship with samples 
          The described FastQ files must exist in the same directory.
          The complete JSON description can be found at {GeneCoreAssaySniffer.JSON_SPECS_URL}
         """
        return help_txt

    @classmethod
    def is_multi_run_sniffer(cls) -> bool:
        return False

    @classmethod
    def get_supported_technology(cls) -> Technology:
        """
        :return: the technology this sniffer supports ie 'Technology.SEQUENCING'
        """
        return Technology.SEQUENCING

    @classmethod
    def get_supported_platforms(cls) -> List[str]:
        """
        :return: platforms: the list of Platform this sniffer supports
        """
        return ["ILLUMINA"]

    def get_json_schema(self, technology: Technology, platform: str) -> Any:

        if not self.is_sniffer_valid_for(technology, platform):
            raise ValueError(f"Unsupported technology/platform combination: {technology}/{platform}")

        return {
            "$schema": "https://json-schema.org/draft/2020-12/schema",
            "$id": "http://www.embl.org/stocks/schemas/genecore-illumina.json",
            "title": "GeneCore Sequencing Data",
            "description": "This is a schema that describes the sequencing data released by EMBL GeneCore.",
            "type": "object",
            "properties": {
                "managed": {"type": "string"},
                "data": {
                    "description": "the different instrument runs",
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "flowcell": {"type": "string"},
                            "runid": {"type": "string"},
                            "sequencer": {
                                "type": "object",
                                "properties": {
                                    "name": {"type": "string"},
                                    "model": {"type": "string"},
                                    "serialnumber": {"type": "string"},
                                    "runid": {"type": "string"}
                                },
                                "required": ["name", "model"]
                            },
                            "producer": {
                                "type": "object",
                                "properties": {
                                    "name": {"type": "string"}
                                },
                                "required": ["name"]
                            },
                            "lanes": {
                                "description": "the different lanes of the flowcell",
                                "type": "array",
                                "items": {
                                    "description": "each item is a lane that will become an Assay in LabID",
                                    "type": "object",
                                    "properties": {
                                        "name": {"type": "string"},
                                        "lane": {"type": ["integer", "string"]},
                                        "user": {"type": "string"},
                                        "email": {"type": "string", "format": "email"},
                                        "type": {"enum": ["single-end", "paired-end", "multi-end"]},
                                        "multiplexed": {"enum": ["true", "false", "True", "False", True, False]},
                                        "readlength": {"type": ["integer", "string"]},
                                        "runmode": {"type": "string"},
                                        "demultiplexed": {"enum": ["true", "false", "True", "False", True, False]},
                                        "samples": {
                                            "type": "object",
                                            "patternProperties": {
                                                "^.+$": {
                                                    "type": "object",
                                                    "properties": {
                                                        "barcode": {"type": "string"},
                                                        "genecoreid": {"type": "string"},
                                                        "genecorereaction": {"type": "string"},
                                                        "sampletype": {"type": "string"},
                                                        "applicationname": {"type": "string"}
                                                    },
                                                    "required": ["barcode"]
                                                }
                                            }
                                        },
                                        "filelist": {
                                            "type": "array",
                                            "items": {
                                                "type": "object",
                                                "properties": {
                                                    "name": {"type": "string"},
                                                    "format": {"type": "string"},
                                                    "sample": {"type": "string"},
                                                    "readtype": {
                                                        "enum": ["read_1", "read_2", "read_3", "read_4", "index_1",
                                                                 "index_2"]},
                                                    "mimetype": {"type": "string"},
                                                    "bytes": {"type": "integer"}
                                                },
                                                "required": ["name", "format", "sample", "readtype"]
                                            },
                                            "minItems": 1
                                        }
                                    },
                                    "required": ["user", "type", "multiplexed", "demultiplexed", "samples", "filelist"]
                                },
                                "minItems": 1,
                                "maxItems": 1
                            }
                        },
                        "required": ["flowcell", "runid", "sequencer", "producer", "lanes"]
                    },
                    "minItems": 1,
                    "maxItems": 1
                }
            },
            "required": ["managed", "data"]
        }

    @staticmethod
    def __decode_samples(sample_dict: dict) -> Dict[str, SequencingLibrary]:
        """
        converts the JSON representation in a list of SequencingLibrary

        :param sample_dict:
        :return:
        """
        samples: Dict[str, SequencingLibrary] = {}
        for key, value in sample_dict.items():
            lib: SequencingLibrary = SequencingLibrary(key, value['barcode'])
            if 'sampletype' in value:
                lib.sample_type = value['sampletype']
            if 'genecoreid' in value:
                lib.provider_sample_name = value['genecoreid']
            if 'genecorereaction' in value:
                lib.provider_sample_id = value['genecorereaction']
            if 'applicationname' in value:
                lib.application = value['applicationname']
            samples[key.replace("'", "")] = lib
        return samples

    @staticmethod
    def __decode_datasets(file_list: [], samples: Dict[str, SequencingLibrary], run_type: SequencingRunType,
                          data_dir: Path) -> List[Dataset]:
        datasets: Dict[str, Dataset] = {}  # map sample name to dataset
        collections: Dict[str, DatasetCollection] = {}  # map name to collection
        for values in file_list:
            file_format: str = values['format']
            mime_type = ""
            if 'mimetype' in values:
                mime_type = values['mimetype']
            byte = 0
            if 'bytes' in values:
                byte = values['bytes']
            if file_format == "fastq":
                df: DatasetFile = FastqFile(name=values['name'],
                                            uri=str(Path(data_dir, values['name'])),
                                            read_type=values['readtype'],
                                            mime_type=mime_type,
                                            byte=byte)
            else:
                df: DatasetFile = DatasetFile(name=values['name'],
                                              uri=str(Path(data_dir, values['name'])),
                                              mime_type=mime_type,
                                              byte=byte,
                                              filetype=file_format)

            sample = samples[values['sample']]
            collection_name: str = file_format + ' files'
            if collection_name not in collections:
                collections[collection_name] = DatasetCollection(name=collection_name)
            collection = collections[collection_name]
            # get or create a dataset
            if sample.name not in datasets:
                datasets[sample.name] = Dataset(name=f"{sample.name} {str(run_type)} {file_format} dataset",
                                                is_raw=(file_format == "fastq"),
                                                samples=[sample],
                                                collection=collection)

            dataset: Dataset = datasets[sample.name]
            dataset.add_datafile(df)
            datasets[sample.name] = dataset

        return list(datasets.values())

    def sniff_instrument_run_assays(self, dir_path: Path, group: str, username: Optional[str] = None) \
            -> List[InstrumentRun]:
        logger.debug(f"looking in {str(dir_path)}")
        # we expect JSON files like <flowcell>_lane<x>.json
        json_filenames_list = glob.glob(str(Path(dir_path, GeneCoreAssaySniffer.JSON_NAME_PATTERN)))
        logger.debug(json_filenames_list)
        if not len(json_filenames_list):
            raise AssayStructureError(
                f"Sniffer {self.__class__}: No JSON file found in directory {str(dir_path)}.")

        runs: List[InstrumentRun] = []
        for json_path in json_filenames_list:
            try:
                runs.append(self.load_run_from_json_file(Path(json_path), group, username=username))
            except ValueError:
                mess = f"AssaySniffer {self.__class__}: Parsing JSON file {json_path} failed."
                logger.exception(mess)
                raise AssayStructureError(mess)

        return runs

    def load_run_from_json_file(self, json_path: Path, group: str, username: str | None = None) -> InstrumentRun:
        # load the file as a dict
        with open(json_path) as json_file:
            data = json.load(json_file)
            return self.load_run_from_json_obj(data=data, data_dir=json_path.parent, group=group, username=username)

    def load_run_from_json_obj(self, data: Any, data_dir: Path, group: str, username: str | None = None) \
            -> InstrumentRun:
        """
        parses the GeneCore json payload into a InstrumentRun
        :param data: the json object
        :param data_dir: the dir (aka run dir) containing the data described in this json
        :param group: the group the data belongs to
        :param username: the group the data belongs to
        """

        # validate JSON with json schema
        validate(data, schema=self.get_json_schema(Technology.SEQUENCING, "ILLUMINA"))

        owner: Optional[User] = None
        if username:
            owner = User(username=username, groups=[UserGroup(name=group)])

        managed = bool(data['managed'])
        payload = data['data']
        # the payload potentially holds many runs
        for run_data in payload:
            sn: str = None
            if 'serialnumber' in run_data['sequencer']:
                sn = run_data['sequencer']['serialnumber']
            flowcell: str = run_data['flowcell']
            run_id: str = run_data['runid']
            instrument: Instrument = Instrument(name=run_data['sequencer']['name'],
                                                model=run_data['sequencer']['model'],
                                                serial_number=sn
                                                )
            run = InstrumentRun(name=run_id,
                                managed=managed,
                                technology=Technology.SEQUENCING,
                                platform="ILLUMINA",
                                instrument=instrument,
                                producer=DataProducer(run_data['producer']['name']))
            if owner:
                run.set_owner(owner=owner, also_set_group=True)
            else:
                run.group = group

            for assay_dict in run_data['lanes']:
                assay_lane: int = assay_dict['lane'] if 'lane' in assay_dict else 1
                assay_name: str = assay_dict['name'] if 'name' in assay_dict else f"{flowcell}_lane{assay_lane}"
                user: str = assay_dict['user']
                email: str = assay_dict['email'] if 'email' in assay_dict else ""
                if not owner:
                    owner = User(username=user, groups=[UserGroup(name=group)])
                owner.email = email

                run_type: SequencingRunType = SequencingRunType(assay_dict['type'])
                multiplexed: bool = bool(assay_dict['multiplexed'])
                demultiplexed: bool = bool(assay_dict['demultiplexed'])
                read_length: str = assay_dict['readlength'] if 'readlength' in assay_dict else ""
                run_mode: str = assay_dict['runmode'] if 'runmode' in assay_dict else ""
                samples: Dict[str, SequencingLibrary] = self.__decode_samples(assay_dict['samples'])
                datasets: List[Dataset] = self.__decode_datasets(assay_dict['filelist'],
                                                                 samples, run_type, data_dir=data_dir)
                # create an SequencingAssay
                assay: SequencingAssay = SequencingAssay(name=assay_name,
                                                         flowcell=flowcell,
                                                         platform=run.platform,
                                                         datasets=datasets,
                                                         samples=list(samples.values()),
                                                         chemistry="",
                                                         instrumentrun=run,
                                                         lane=assay_lane,
                                                         multiplexed=multiplexed,
                                                         demultiplexed=demultiplexed,
                                                         runtype=run_type,
                                                         runmode=run_mode,
                                                         readlength=read_length
                                                         )
                if owner:
                    assay.set_owner(owner=owner, also_set_group=True)
                else:
                    assay.group = group

                run.add_assay(assay)

        return run
