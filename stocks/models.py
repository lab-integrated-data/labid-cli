# -*- coding: utf-8 -*-
"""
Defines the LabID models
Here we define a few coding convention:
- do not init list or dict props with empty list or dict. They must be None until a value is added
This will allow to get these attributes exported to JSON if empty
- nested objects should always accept either the object or a str so one can either indicate the object or its ID.
In teh case of an ID, it must be a valid LabID UUID


"""
import logging
from datetime import datetime, date
from pathlib import Path
from typing import List, Optional, Dict, Union, Any, Set, Tuple
from jsonpickle import encode
from cli.utils import Technology, SequencingRunType, SequencingReadType, UserRole, ObjectState, \
    ExperimentStatus, NanoporeLiveBaseCallingType, NanoporeAdaptiveMode, ModelType, StorageBackend

logger = logging.getLogger(__name__)


class CreatableMixin:
    def __init__(self, created: Optional[datetime] = None, created_by: Optional["User"] | str = None, **kwargs):
        self.created: datetime | None = created
        self.created_by: Optional["User"] | str = created_by
        super().__init__(**kwargs)


class ModifiableMixin:
    def __init__(self, modified: Optional[datetime] = None, modified_by: Optional["User"] | str = None, **kwargs):
        self.modified: datetime | None = modified
        self.modified_by: Optional["User"] | str = modified_by
        super().__init__(**kwargs)


class DeletableMixin:
    def __init__(self, deleted: bool = False, deleted_by: Optional["User"] | str = None,
                 deleted_date: Optional[datetime] = None, **kwargs):
        self.deleted: bool = deleted
        self.deleted_by: Optional["User"] | str = deleted_by
        self.deleted_date: Optional[datetime] = deleted_date
        super().__init__(**kwargs)


class ProtocolableMixin:
    def __init__(self, protocols: List["Protocol"] | None = None, **kwargs):
        self.protocols: List["Protocol"] | None = protocols
        super().__init__(**kwargs)


class ProtectableMixin:
    # TODO: implement permission support when needed
    def __init__(self, permissions=None, **kwargs):
        self.permissions = permissions
        super().__init__(**kwargs)


class OwnableMixin:
    """
    Mixin for object that can be owned. An Owned object has a owner and is also owned_by a group_name
    """

    def __init__(self, owner: Optional["User"] | str = None, owned_by: str | None = None, **kwargs):
        self.owner: Optional["User"] | str = owner
        self.owned_by: str = owned_by
        if not self.owned_by and isinstance(owner, User) and owner.groups:
            self.owned_by = next(iter([grp.name for grp in owner.groups.values() if grp.is_primary_group]),
                                 list(owner.groups.values())[0].name)
        super().__init__(**kwargs)

    def set_owner(self, owner: "User", also_set_group=True):
        """
        sets the owner and also optionally resets the group_name according to the owner's group_name
        :param owner:
        :param also_set_group: if True the owner's group_name prop propagates to the owned_by field
        :return:
        """
        self.owner = owner
        if also_set_group and owner.groups:
            self.owned_by = next(iter([grp.name for grp in owner.groups.values() if grp.is_primary_group]),
                                 list(owner.groups.values())[0].name)


class AnnotableMixin:
    """
    Mixin for object that can be annotated.
    """

    def __init__(self, annotations: Dict[str, str|dict] | None = None, **kwargs):
        self.annotations: Dict[str, str|dict] | None = annotations
        super().__init__(**kwargs)

    def add_annotation(self, annot_key: str, annot_value: str | dict):
        if not self.annotations:
            self.annotations = {}
        self.annotations[annot_key] = annot_value


###
# Classes that are not yet in LabID
###

class Ontology:
    """
    Class representing an ontology, it has a name and a URL. Both are supposed to be unique and can be used as unique
     keys.
    """

    def __init__(self, name: str, url: Optional[str] = None, description: Optional[str] = None, **kwargs):
        super().__init__(**kwargs)
        self.name: str = name
        self.description: str = description
        self.url: Optional[str] = url


class OntologyTerm:
    """
    A term from an ontology.
    id, name, description, owner and annotations inherited and represent the CV instance from LabID,
    and where name can be different from term_name
    """

    def __init__(self, name: str, term_id: str, ontology: Ontology, description: Optional[str] = None, **kwargs):
        """
        :param name: the term's name in the ontology
        :param term_id: the term's id in the ontology
        :param ontology: the ontology this term belongs to
        """
        super().__init__(**kwargs)
        self.name: str = name
        self.term_id: str = term_id
        self.ontology: Ontology = ontology
        self.description = description


###
# Classes that are in LabID
###

class Note:
    """
    A class representing a Note
    """

    def __init__(self, content: str, is_readonly: bool, id: str | None, object_id: str | None, owner: str | None = None,
                 **kwargs):
        self.content: str = content
        self.is_readonly: bool = is_readonly
        self.id: str | None = id
        self.object_id: str | None = object_id
        self.owner: str | None = owner


class StocksBaseItem:
    """
    Base class for a LabID object exposing basic id, name and description properties.
    """
    # for classes with properties which value is a StocksCVTerm, this dict[str,str] should be populated with
    # the class_name -> [property_name, cv_category_name]
    property_to_cvcategory: Dict[str, Dict[str, str]] = dict()

    @classmethod
    def add_values_to_property_to_cvcategory(cls, key, value):
        if not cls.__name__ in cls.property_to_cvcategory:
            cls.property_to_cvcategory[cls.__name__] = dict()
        cls.property_to_cvcategory[cls.__name__][key] = value

    @classmethod
    def get_property_to_cvcategory(cls) -> Dict[str, str]:
        """
        return the [property_name, cv_category_name] for this class
        """
        if cls.__name__ in cls.property_to_cvcategory:
            return cls.property_to_cvcategory[cls.__name__]
        return dict()

    def __init__(self, name: str, id: Optional[str] = None, description: Optional[str] = None, **kwargs):
        self.name: str = name
        self.id: str = id
        self.description: str = description
        # map to LabID model type
        self.stocks_model_type: str = "DEFAULT"
        if kwargs:
            # when creating object from API response parsed into Pydantic Object, we may have 'model_type'
            if 'model_type' in kwargs:
                self.stocks_model_type = kwargs['model_type']
                kwargs.pop('model_type')
            # kwargs is still not empty, we need to warn this as this maybe not normal
            logger.debug(f"Potential typo/bug: kwargs not empty one ce reached StocksBaseItem => {str(kwargs)}")

        # we do not propagate kwargs to object class
        super().__init__()

    def as_simple_json(self):
        return encode(self, unpicklable=False, indent=2, make_refs=False)


class UserGroup(StocksBaseItem):

    def __init__(self, name: str, id: Optional[str] = None, description: Optional[str] = None,
                 is_primary_group: bool = False, **kwargs):
        """
        :param name: the name of this group_name
        :param id: the LabID UUID
        """
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = ModelType.GROUP.value.upper()
        self.is_primary_group: bool = is_primary_group


class User(StocksBaseItem):
    """
    A base class for a user
    """

    def __init__(self, username: str, last_name: str = None, id: str = None,
                 groups: List['UserGroup'] | Dict[str, 'UserGroup'] = None, email: Optional[str] = None,
                 institution: Optional[str] = None, date_joined: Optional[datetime] = None,
                 is_superuser: Optional[bool] = None, is_staff: Optional[bool] = None, is_active: Optional[bool] = None,
                 middle_name: Optional[str] = None, first_name: str = None, full_name: str = None, name: str = None,
                 **kwargs):
        if not name:
            name = last_name
        super().__init__(name=name, id=id, **kwargs)
        self.stocks_model_type = ModelType.USER.value.upper()
        self.username: str = username
        self.first_name: Optional[str] = first_name
        self.last_name: Optional[str] = last_name
        self.middle_name: Optional[str] = middle_name
        self.unix_group: Optional[str] = None  # unix group_name name
        self.unix_name: Optional[str] = None  # unix user name
        self.email: Optional[str] = email  # user email
        self.orcid: Optional[str] = None  # user ORCID ID

        self.groups: Dict[str, 'UserGroup'] = {}
        if groups:
            self.groups = {}
            logger.debug(f"groups => {type(groups)}")
            if isinstance(groups, list):
                for grp in groups:
                    logger.debug(f"grp {grp} of type \n {type(grp)}")
                    self.groups[grp.name] = grp
            elif isinstance(groups, dict):
                self.groups = groups
        assert (isinstance(self.groups, dict))

        self.institution: Optional[str] = institution  # user institution
        self.date_joined: Optional[datetime] = date_joined
        self.is_superuser: Optional[bool] = is_superuser
        self.is_staff: Optional[bool] = is_staff
        self.is_active: Optional[bool] = is_active
        self.full_name = full_name

    def add_user_group(self, group: UserGroup):
        if not self.groups:
            self.groups = {}
        self.groups[group.name] = group

    def get_primary_group(self) -> UserGroup | None:
        for group in self.groups.values():
            if group.is_primary_group:
                return group
        return None


class UserMember(User):
    """
    A user with a list membership roles
    """

    def __init__(self, user: User, roles: Set[UserRole], **kwargs):
        super().__init__(username=user.username, id=user.id, groups=user.groups, email=user.email,
                         first_name=user.first_name, middle_name=user.middle_name, last_name=user.last_name,
                         institution=user.institution, **kwargs)
        self.roles: Set[UserRole] = roles


class StorageVolume(OwnableMixin, CreatableMixin, ModifiableMixin, StocksBaseItem):
    def __init__(self, id: str, name: str, base_path: str, is_primary_volume: bool, is_dropbox_volume: bool,
                 is_modifyable: bool = True, storagebackend: StorageBackend = StorageBackend.LOCAL,
                 **kwargs):
        super().__init__(name=name, id=id, **kwargs)
        self.stocks_model_type = ModelType.STORAGE_VOLUME.value.upper()
        self.base_path = base_path
        self.is_primary_volume = is_primary_volume
        self.is_dropbox_volume = is_dropbox_volume
        self.storagebackend = storagebackend
        self.is_modifyable = is_modifyable
        if self.storagebackend == StorageBackend.ARCHIVE:
            self.is_modifyable = False


class Project(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, AnnotableMixin, StocksBaseItem):
    """
    A class representing a Project
    """

    def __init__(self, name: str, description: Optional[str] = None, id: Optional[str] = None,
                 user_roles: Optional[List[UserMember]] = None,
                 studies: List["Study"] | None = None, **kwargs):
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type: str = ModelType.PROJECT.value.upper()
        self.user_roles: List[UserMember] = user_roles
        self.studies: List["Study"] | None = studies


class Experiment(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, StocksBaseItem):
    def __init__(self, name: str, status: ExperimentStatus, project: Project | str, is_frozen: bool = False,
                 summary: Optional[str] = None, description: Optional[str] = None,
                 protocol: Optional["Protocol"] = None,
                 start_date: Optional[datetime] = None, completed_date: Optional[datetime] = None,
                 estimated_completion_date: Optional[datetime] = None, freeze_date: Optional[datetime] = None,
                 id: Optional[str] = None, **kwargs):
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type: str = ModelType.EXPERIMENT.value.upper()
        self.status: ExperimentStatus = status
        self.project: "Project" | str = project
        self.is_frozen: bool = is_frozen
        self.summary: str = summary
        self.description: str = description
        self.protocol: Optional["Protocol"] = protocol
        self.start_date: datetime | date | None = start_date
        self.completed_date: datetime | date | None = completed_date
        self.estimated_completion_date: datetime | date | None = estimated_completion_date
        self.freeze_date: datetime | date | None = freeze_date


class StocksCVCategory(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, StocksBaseItem):
    """
    A CV category in LabID
    id, name, description, owner and annotations inherited and represent the CV instance from LabID,
    and where name can be different from term_name
    """

    def __init__(self, name: str, id: Optional[str] = None, description: Optional[str] = None,
                 default_ontology: Optional[Ontology] = None,
                 allowed_ontologies: Optional[List[Ontology]] = None, **kwargs):
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = ModelType.CVCATEGORY.value.upper()
        self.default_ontology = default_ontology  # source in LabID?
        self.allowed_ontologies = allowed_ontologies


class StocksCVTerm(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, StocksBaseItem):
    """
    A CV term in LabID
    """

    def __init__(self, name: str, id: Optional[str] = None, description: Optional[str] = None,
                 category: StocksCVCategory | str | None = None,
                 ontology_mappings: Optional[Dict[str, OntologyTerm]] = dict(),
                 **kwargs):
        """
        :param name: the LabID term name
        :param category: the LabID term category, or a UUID for the category or none
        :param ontology_mappings: optional mappings to ontologies as a dict keyed by Ontology
        """
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = "TERMS"
        self.category: StocksCVCategory = category
        self.ontology_mappings: Optional[Dict[str, OntologyTerm]] = ontology_mappings


class InstrumentModel(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, StocksBaseItem):
    def __init__(self, name: str, technology: Technology, platform: str,
                 id: Optional[str] = None, description: Optional[str] = None, **kwargs):
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = ModelType.INSTRUMENTMODEL.value.upper()
        self.technology: Technology = technology
        self.platform: str = platform


class Instrument(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, AnnotableMixin, StocksBaseItem):
    def __init__(self, name: str, model: str, serial_number: Optional[str] = None,
                 id: Optional[str] = None, description: Optional[str] = None, **kwargs):
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = ModelType.EQUIPMENT.value.upper()
        self.model: InstrumentModel | str = model
        self.serial_number: str = serial_number


class Sample(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, AnnotableMixin, StocksBaseItem):
    def __init__(self, name: str, id: Optional[str] = None, description: Optional[str] = None,
                 primary_project: Project | None = None, **kwargs):
        # sample is sometimes used to store a minimal ref to sample sub-class (eg SEQLIB), we indicate this with the
        # correct stocks_model_type if model_type is in kwargs
        _model_type = kwargs.pop('model_type') if 'model_type' in kwargs else ModelType.GENERICSAMPLE.value
        if _model_type not in [ModelType.GENERICSAMPLE.value, ModelType.SEQUENCINGLIBRARY.value,
                               ModelType.EMSAMPLE.value]:
            _model_type = ModelType.GENERICSAMPLE.value
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = _model_type
        self.sample_type: Optional[str] = None
        self.application: Optional[str] = None
        self.provider_sample_name: Optional[str] = None  # eg genecoreid
        self.provider_sample_id: Optional[str] = None  # eg genecorereaction
        self.primary_project: Project = primary_project


class SequencingLibrary(Sample):
    def __init__(self, name: str, barcode: Optional[str], id: Optional[str] = None, description: Optional[str] = None,
                 **kwargs):
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = ModelType.SEQUENCINGLIBRARY.value
        self.barcode: str = barcode


class EMSample(Sample):
    def __init__(self, name: str, id: Optional[str] = None, description: Optional[str] = None,
                 **kwargs):
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = ModelType.EMSAMPLE.value


class DatasetFile(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, AnnotableMixin, StocksBaseItem):
    def __init__(self, name: str, mime_type: str, byte: int, filetype: str, is_dir: bool = False,
                 uri: Optional[str] = None, md5sum: Optional[str] = None,
                 id: Optional[str] = None, description: Optional[str] = None, **kwargs):
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = "DATAFILE"
        self.uri: str = uri
        self.md5sum: str = md5sum
        self.byte: int = byte
        self.mime_type: str = mime_type
        self.filetype: str = filetype
        self.is_dir: bool = is_dir


class DatasetFileCopy(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, StocksBaseItem):
    def __init__(self, name: str, uri: str, is_primary_copy: bool, shortname: Optional[str] = None,
                 id: Optional[str] = None, **kwargs):
        self.uri: str = uri
        self.shortname: str = shortname
        self.is_primary_copy: bool = is_primary_copy
        self.dataset: DatasetFile | str | None = None
        super().__init__(name=name, id=id, **kwargs)


class FastqFile(DatasetFile):
    def __init__(self, name: str, read_type: SequencingReadType, mime_type: str, byte: int,
                 filetype: str = "fastq",
                 uri: Optional[str] = None, md5sum: Optional[str] = None,
                 id: Optional[str] = None, description: Optional[str] = None, **kwargs):
        super().__init__(name=name, mime_type=mime_type, byte=byte, filetype=filetype,
                         uri=uri, md5sum=md5sum, is_dir=False,
                         id=id, description=description, **kwargs)
        self.read_type: SequencingReadType = read_type


class FastqDir(DatasetFile):
    def __init__(self, name: str, read_type: SequencingReadType, mime_type: str,
                 uri: Optional[str] = None, id: Optional[str] = None, description: Optional[str] = None, **kwargs):
        super().__init__(name=name, mime_type=mime_type, byte=0, filetype="fastq", is_dir=True,
                         uri=uri, md5sum="", id=id, description=description, **kwargs)
        self.read_type: SequencingReadType = read_type


class Fast5File(DatasetFile):
    def __init__(self, name: str, byte: int, uri: Optional[str] = None, md5sum: Optional[str] = None,
                 id: Optional[str] = None, description: Optional[str] = None, **kwargs):
        super().__init__(name=name, mime_type="application/x-hdf5", byte=byte, filetype="fast5",
                         uri=uri, md5sum=md5sum, id=id, description=description, is_dir=False, **kwargs)


class Pod5File(DatasetFile):
    def __init__(self, name: str, byte: int, uri: Optional[str] = None, md5sum: Optional[str] = None,
                 id: Optional[str] = None, description: Optional[str] = None, **kwargs):
        super().__init__(name=name, mime_type="application/x-pod5", byte=byte, filetype="pod5",
                         uri=uri, md5sum=md5sum, id=id, description=description, is_dir=False, **kwargs)


class Fast5Dir(DatasetFile):
    def __init__(self, name: str, uri: Optional[str] = None, id: Optional[str] = None,
                 description: Optional[str] = None, **kwargs):
        super().__init__(name=name, mime_type="application/x-hdf5", byte=0, filetype="fast5", uri=uri,
                         md5sum="", id=id, description=description, is_dir=True, **kwargs)


class Pod5Dir(DatasetFile):
    def __init__(self, name: str, uri: Optional[str] = None, id: Optional[str] = None,
                 description: Optional[str] = None, **kwargs):
        super().__init__(name=name, mime_type="application/x-pod5", byte=0, filetype="pod5", uri=uri,
                         md5sum="", id=id, description=description, is_dir=True, **kwargs)


class Dataset(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, AnnotableMixin, StocksBaseItem):
    """
    datafiles
    """

    def __init__(self, name: str, is_raw: bool, id: Optional[str] = None, description: Optional[str] = None,
                 datafiles: List[DatasetFile | str] | None = None, dataset_type: str = "generic",
                 samples: List[Sample] | None = None,
                 collection: Optional["DatasetCollection"] = None,
                 **kwargs):
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = "DATASET"
        self.dataset_type: str = dataset_type
        self.is_raw = is_raw
        self.datafiles: List[DatasetFile] | List[str] | None = datafiles if datafiles else None
        self.samples: List[Sample] = samples
        self.collection: DatasetCollection = collection  # in stocks a dataset can belong to many collections

    def add_datafile(self, df: DatasetFile):
        if not self.datafiles:
            self.datafiles = []
        self.datafiles.append(df)


class DatasetCollection(StocksBaseItem):
    """
    A class representing a collection of dataset. It adds the dataset_id_list to store, in an ordered-preserved way,
    the dataset IDs.
    """

    def __init__(self, name: str, id: Optional[str] = None, description: Optional[str] = None,
                 datasets: List[Dataset | str] | None = list(), **kwargs):
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = "DATASETCOLLECTION"
        self.datasets: List[Dataset | str] | None = datasets


class Protocol(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, AnnotableMixin, StocksBaseItem):
    """
    A class representing a Protocol. Adds a type as an ontological term
    id, name, description, owner and annotations inherited
    TODO: add support for protocol parameters
    """

    def __init__(self, name: str, protocol_type: OntologyTerm, id: Optional[str] = None,
                 description: Optional[str] = None, summary: Optional[str] = None, **kwargs):
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = "PROTOCOL"
        self.protocol_type: OntologyTerm = protocol_type
        self.summary: str = summary


class AnnotationType(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, StocksBaseItem):

    def __init__(self, name: str, id: Optional[str] = None, description: Optional[str] = None, **kwargs):
        """
        :param name: the name of this annotation type
        :param id: the LabID UUID
        """
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = "ANNOTATIONTYPE"


class Study(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, AnnotableMixin, DatasetCollection):
    """
    A class representing a Study.
    """

    def __init__(self, name: str, id: Optional[str] = None, description: Optional[str] = None,
                 datasets: Optional[List[Dataset]] = None,
                 user_roles: Optional[Dict[str, UserMember]] = None,
                 experimental_design_terms: Optional[List[StocksCVTerm]] = None,
                 experimental_factors: Optional[List[str]] = None,
                 protocols: List[Protocol | str] | None = None, **kwargs):
        """
        :param name: the study name
        :param datasets: the optional list of datasets grouped in this study
        :param user_roles: the role(s) of each study's participant
        :param experimental_design_terms: the CV terms describing this study design
        :param experimental_factors: the ProtocolType, AnnotationType or str representing the experimental factors
        When a string is given, it represents the key(s) to look up in the annotation dict of the datasets
        :param protocols:
        """
        super().__init__(name=name, id=id, description=description, datasets=datasets, **kwargs)
        self.stocks_model_type = "STUDY"
        self.user_roles: Dict[str, UserMember] = user_roles
        self.experimental_design_terms: Optional[List[StocksCVTerm]] = experimental_design_terms  # design in STCOKS
        self.experimental_factors: Optional[List[str]] = experimental_factors
        self.protocols: Optional[List[Protocol | str]] = protocols  # unique set of protocol used across this study
        self.assays: List[Union[NanoporeAssay, SequencingAssay, Assay]] | None = None
        # TODO: add ref to Project

    def add_experimental_design_term(self, term: StocksCVTerm):
        if not self.experimental_design_terms:
            self.experimental_design_terms = []
        self.experimental_design_terms.append(term)

    def add_experimental_factor(self, exp_factor: str):
        if not self.experimental_factors:
            self.experimental_factors = []
        self.experimental_factors.append(exp_factor)

    def add_protocol(self, protocol: Protocol):
        if not self.protocols:
            self.protocols = []
        self.protocols.append(protocol)

    def add_user(self, user: UserMember) -> None:
        if not self.user_roles:
            self.user_roles = {user.username: user}
        else:
            is_user = self.user_roles.get(user.username)
            if is_user:
                self.user_roles[user.username].roles.update(user.roles)
            else:
                self.user_roles[user.username] = user


class DataProducer:
    def __init__(self, name: str, **kwargs):
        super().__init__(**kwargs)
        self.name: str = name


class SimpleInstrumentRun(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, AnnotableMixin,
                          StocksBaseItem):
    def __init__(self, name: str, id: Optional[str] = None, description: Optional[str] = None,
                 instrument: Optional[Instrument | dict] = None, producer: Optional[DataProducer] = None,
                 start_datetime: Optional[Any] = None, end_datetime: Optional[Any] = None,
                 run_duration: Optional[str] = "", assays: List["Assay"] | List[str] | None = None,
                 **kwargs):
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = "INSTRUMENTRUN"
        if instrument and isinstance(instrument, dict):
            instrument = Instrument(**instrument)
        self.instrument: Instrument = instrument
        self.producer: DataProducer = producer
        self.start_datetime: Any = start_datetime
        self.end_datetime: Any = end_datetime
        self.run_duration: str = run_duration
        self.assays: List["Assay"] | List[str] | None = assays

    def add_assay(self, assay: "Assay"):
        if not self.assays:
            self.assays = list()
        self.assays.append(assay)


class InstrumentRun(SimpleInstrumentRun):
    def __init__(self, name: str, managed: bool, technology: Technology, platform: str,
                 id: Optional[str] = None, description: Optional[str] = None,
                 instrument: Optional[Instrument] = None, producer: Optional[DataProducer] = None,
                 start_datetime: Optional[Any] = None, end_datetime: Optional[Any] = None,
                 run_duration: Optional[str] = "", assays: List["Assay"] | List[str] | None = None,
                 **kwargs):
        super().__init__(name=name, id=id, description=description, instrument=instrument, producer=producer,
                         start_datetime=start_datetime, end_datetime=end_datetime, run_duration=run_duration,
                         assays=assays, **kwargs)
        self.managed: bool = managed  # stocks => is_managed
        self.technology: Technology = technology
        self.platform: str = platform


class Assay(OwnableMixin, CreatableMixin, ModifiableMixin, DeletableMixin, AnnotableMixin, StocksBaseItem):
    technology: Technology = Technology.NA,
    platform: str = "OTHER"
    stocks_model_type: str = ModelType.ASSAY.value

    def __init__(self, name: str,
                 technology: Technology,
                 id: Optional[str] = None,
                 platform: str | None = None,
                 description: Optional[str] = None,
                 datasets: List[Dataset] | None = None, samples: List[Sample] | None = None,
                 instrumentrun: InstrumentRun | str = None, instrumentmodel: InstrumentModel | str = None,
                 multiplexed: bool = None,
                 run_dir: Optional[Path] = None, state: Optional[ObjectState] = None,
                 **kwargs):
        super().__init__(name=name, id=id, description=description, **kwargs)
        self.stocks_model_type = Assay.stocks_model_type
        self.technology: Technology = technology
        self.platform: str = platform if platform else Assay.platform
        if not multiplexed and samples:
            self.multiplexed = len(samples) > 1
        elif not multiplexed:
            self.multiplexed = False
        else:
            self.multiplexed: bool = multiplexed
        self.instrumentrun: InstrumentRun | str | None = instrumentrun
        self.instrumentmodel: InstrumentModel | str | None = instrumentmodel
        self.run_dir: Optional[Path] = run_dir
        self.state: Optional[ObjectState] = state
        if multiplexed is None and samples:
            self.multiplexed = len(samples) > 1
        self.datasets: List[Dataset] = datasets  # datasets_out
        self.samples: Dict[str, Sample] | None = None  # samples_in
        if samples and len(samples) > 0:
            self.add_samples(samples)

    def add_sample(self, sample: Sample):
        if not self.samples:
            self.samples = []
        self.samples[sample.name] = sample

    def add_samples(self, samples: List[Sample]):
        if not self.samples:
            self.samples = samples
        else:
            self.samples = {}
            for smpl in samples:
                self.samples[smpl.name] = smpl


class GenericAssay(Assay):
    platform: str = "UNKNOWN"
    technology: Technology = Technology.OTHER
    stocks_model_type: str = ModelType.GENERICASSAY.value

    def __init__(self, name: str,
                 technology: Technology,
                 generic_assay_type: str,
                 id: Optional[str] = None,
                 platform: str | None = None,
                 description: Optional[str] = None,
                 datasets: Optional[List[Dataset]] = None, samples: Optional[List[Sample]] = None,
                 instrumentrun: InstrumentRun | str = None, multiplexed: bool = None,
                 **kwargs):
        # remove technology from kwargs is present
        kwargs.pop('technology', None)
        super().__init__(name=name, id=id, description=description,
                         technology=technology if technology else GenericAssay.technology,
                         platform=platform if platform else GenericAssay.platform,
                         datasets=datasets, samples=samples, instrumentrun=instrumentrun,
                         multiplexed=multiplexed, **kwargs)
        self.stocks_model_type = GenericAssay.stocks_model_type
        self.generic_assay_type = generic_assay_type


# TODO: replace the Sample type with SequencingLibrary
class SequencingAssay(Assay):
    platform: str = "ILLUMINA"
    technology: Technology = Technology.SEQUENCING
    stocks_model_type: str = ModelType.NGSILLUMINAASSAY.value

    def __init__(self, name: str, flowcell: str,
                 id: Optional[str] = None,
                 platform: str | None = None,
                 description: Optional[str] = None,
                 datasets: Optional[List[Dataset]] = None,
                 samples: Optional[List[Sample]] = None,
                 chemistry: str = "",
                 instrumentrun: InstrumentRun | str = None,
                 lane: int = 1,
                 multiplexed: bool = None,
                 demultiplexed: bool = None,
                 runtype: SequencingRunType = SequencingRunType.SINGLE_END,
                 runmode: str = "",
                 readlength: str = None,
                 info: str = None,
                 **kwargs):
        # remove technology from kwargs is present
        kwargs.pop('technology', None)
        super().__init__(name=name,
                         technology=SequencingAssay.technology,
                         id=id,
                         description=description,
                         platform=platform if platform else SequencingAssay.platform,
                         datasets=datasets,
                         samples=samples,
                         instrumentrun=instrumentrun,
                         multiplexed=multiplexed,
                         **kwargs)
        self.stocks_model_type = SequencingAssay.stocks_model_type

        if demultiplexed is None:
            if multiplexed is False:
                self.demultiplexed = False
            elif datasets and samples and len(datasets) == len(samples) and len(samples) > 1:
                self.demultiplexed = True
            else:
                self.demultiplexed = False
        else:
            self.demultiplexed: bool = demultiplexed

        self.flowcell: str = flowcell
        self.flowcell_version: str = ""
        try:
            self.lane: int = int(lane)
        except ValueError:
            self.lane: int = 1
        self.runtype: SequencingRunType = runtype
        self.runmode: str = runmode
        self.readlength: str = readlength
        self.chemistry: str = chemistry
        self.info: str = info  # this slot is only avail on INITIALIZED ASSAY FROM GENECORE BRIDGE


class AvitiSequencingAssay(SequencingAssay):
    platform: str = "Element Biosciences"
    stocks_model_type: str = ModelType.AVITISEQUENCINGASSAY.value

    def __init__(self,
                 name: str,
                 flowcell: str,
                 id: Optional[str] = None,
                 description: Optional[str] = None,
                 datasets: Optional[List[Dataset]] = None,
                 samples: Optional[List[Sample]] = None,
                 chemistry: str = "",
                 instrumentrun: InstrumentRun | str = None,
                 lane: int = 1,
                 multiplexed: bool = None,
                 demultiplexed: bool = None,
                 runtype: SequencingRunType = SequencingRunType.SINGLE_END,
                 run_mode: str = None,
                 readlength: str = None,
                 info: str = None,
                 **kwargs
                 ):
        super().__init__(name=name,
                         flowcell=flowcell,
                         platform=AvitiSequencingAssay.platform,
                         id=id,
                         description=description,
                         datasets=datasets,
                         samples=samples,
                         chemistry=chemistry,
                         instrumentrun=instrumentrun,
                         lane=lane,
                         multiplexed=multiplexed,
                         demultiplexed=demultiplexed,
                         runtype=runtype,
                         runmode=run_mode,
                         readlength=readlength,
                         info=info,
                         **kwargs)
        self.stocks_model_type = AvitiSequencingAssay.stocks_model_type


class NanoporeAssay(SequencingAssay):
    platform: str = "NANOPORE"
    stocks_model_type: str = ModelType.NANOPOREASSAY.value

    def __init__(self,
                 name: str,
                 flowcell: str,
                 flowcell_version: str,
                 id: Optional[str] = None,
                 description: Optional[str] = None,
                 datasets: Optional[List[Dataset]] = None,
                 samples: Optional[List[Sample]] = None,
                 chemistry: str = "",
                 instrumentrun: InstrumentRun | str = None,
                 multiplexed: bool = None,
                 demultiplexed: bool = None,
                 run_mode: str = None,
                 **kwargs):
        super().__init__(name=name, id=id, description=description,
                         platform=NanoporeAssay.platform,
                         flowcell=flowcell,
                         datasets=datasets,
                         samples=samples,
                         chemistry=chemistry,
                         instrumentrun=instrumentrun,
                         lane=1,
                         multiplexed=multiplexed,
                         demultiplexed=demultiplexed,
                         runtype=SequencingRunType.SINGLE_END,
                         runmode=run_mode,
                         **kwargs)
        self.stocks_model_type = NanoporeAssay.stocks_model_type
        self.flowcell_version = flowcell_version
        self.live_base_calling: NanoporeLiveBaseCallingType = NanoporeLiveBaseCallingType.NONE
        self.live_read_mapping: bool = False
        self.genome_reference: Optional[str] = None  # genome_reference
        self.adaptive_mode: NanoporeAdaptiveMode = NanoporeAdaptiveMode.NONE
        self.adaptive_mode_details: str = ""


class IntegerRange:
    def __init__(self, lower: int, upper: int, **kwargs):
        if lower and upper and lower > upper:
            raise ValueError(f"in a range, the lower value must be <= upper value. "
                             f"Got lower={lower} and upper={upper}")
        self.lower: int = lower
        self.upper: int = upper


class ChannelVisualizedEntity(StocksBaseItem):
    """
    We initially modeled that a channel measures the signal from a single 'visualized' entity
    With technos like STORM (https://git.embl.de/grp-gbcs/stocks-technical-documentation/-/issues/181), we realisex that
    one needs to describe several 'visualized' entities for a channel. We decided to keep the current model ie not
    refactor the model to e.g. having 1 'Channel' containing a list of 'VisualizedEntity'
    This means that the channel related information ie (channel) number, (channel) name, *_wavelength should be
    identical for all the entities visualized in the same channel (ie same "number"); this is your responsibility
    to make sure these channel's properties have the same values (i am not sure how mauch the server will complain if
     not the case).
    """

    def __init__(self,
                 number: int, target: str, name: str | None = None,
                 excitation_wavelength: IntegerRange | None = None,
                 emission_wavelength: IntegerRange | None = None,
                 target_type: StocksCVTerm | None = None,
                 label: StocksCVTerm | None = None, label_object: StocksBaseItem | None = None,
                 probe_description: str | None = None, probe_type: StocksCVTerm | None = None,
                 probe_object: StocksBaseItem | None = None, **kwargs):
        super().__init__(name=name, **kwargs)
        # fixed info for the channel
        self.number: int = number
        self.name: str = name if name else number
        self.excitation_wavelength: IntegerRange | None = excitation_wavelength
        self.emission_wavelength: IntegerRange | None = emission_wavelength
        # visualized entity info
        self.target: str = target
        self.target_type: StocksCVTerm | None = target_type
        self.label: StocksCVTerm | None = label
        self.label_object: StocksBaseItem | None = label_object
        self.probe_type: StocksCVTerm | None = probe_type
        self.probe_description: str | None = probe_description
        self.probe_object: StocksBaseItem | None = probe_object


class LightMicroscopyAssay(Assay):
    technology: Technology = Technology.LIGHT_MICROSCOPY
    platform: str = "UNKNOWN"
    stocks_model_type: str = ModelType.LIGHTMICROSCOPYASSAY.value

    def __init__(self,
                 name: str,
                 id: Optional[str] = None,
                 description: Optional[str] = None,
                 platform: str | None = None,
                 datasets: Optional[List[Dataset]] = None,
                 samples: Optional[List[Sample]] = None,
                 instrumentrun: InstrumentRun | str = None,
                 multiplexed: bool = None,
                 assay_type: Optional[StocksCVTerm] = None,
                 imaging_method: List[StocksCVTerm] | None = None,
                 objective: Optional[StocksCVTerm] = None,
                 x_size: int = 0,
                 y_size: int = 0,
                 pixel_size: float = 0.0,
                 z_planes: int = 1,
                 z_step: float = 0.0,
                 time_points: int = 1,
                 time_step: float = 0.0,
                 channels: List[ChannelVisualizedEntity] | None = None,
                 **kwargs):
        # remove technology from kwargs is present
        kwargs.pop('technology', None)
        super().__init__(
            name=name,
            id=id,
            description=description,
            technology=LightMicroscopyAssay.technology,
            platform=platform if platform else LightMicroscopyAssay.platform,
            datasets=datasets,
            samples=samples,
            instrumentrun=instrumentrun,
            multiplexed=multiplexed,
            **kwargs)
        self.stocks_model_type = LightMicroscopyAssay.stocks_model_type
        self.assay_type: StocksCVTerm | None = assay_type
        self.imaging_method: List[StocksCVTerm] = imaging_method if imaging_method else list()
        self.objective: StocksCVTerm = objective
        self.x_size: int = x_size
        self.y_size: int = y_size
        self.pixel_size: float = pixel_size
        # Z dim
        self.z_planes: int = z_planes
        self.z_step: float = z_step
        # time dim
        self.time_points: int = time_points
        self.time_step: float = time_step
        # channels dim
        self.channels: List[ChannelVisualizedEntity] | None = channels


class LightMicroscopyScreenAssay(LightMicroscopyAssay):
    stocks_model_type: str = ModelType.LIGHTMICROSCOPYSCREENASSAY.value

    def __init__(self,
                 name: str,
                 id: Optional[str] = None,
                 description: Optional[str] = None,
                 platform: str | None = None,
                 datasets: Optional[List[Dataset]] = None,
                 samples: Optional[List[Sample]] = None,
                 instrumentrun: InstrumentRun | str = None,
                 multiplexed: bool = None,
                 screen_type: Optional[StocksCVTerm] = None,
                 screening_library: Optional[StocksCVTerm] = None,
                 library_manufacturer: str = "",
                 library_version: str = "",
                 field_of_view: Optional[int] = None,
                 assay_type: Optional[StocksCVTerm] = None,
                 imaging_method: List[StocksCVTerm] | None = None,
                 objective: Optional[StocksCVTerm] = None,
                 x_size: int = 0,
                 y_size: int = 0,
                 pixel_size: float = 0.0,
                 z_planes: int = 1,
                 z_step: float = 0.0,
                 time_points: int = 1,
                 time_step: float = 0.0,
                 channels: List[ChannelVisualizedEntity] | None = None,
                 **kwargs):
        super().__init__(name=name,
                         id=id,
                         description=description,
                         platform=platform if platform else LightMicroscopyScreenAssay.platform,
                         datasets=datasets,
                         samples=samples,
                         instrumentrun=instrumentrun,
                         multiplexed=multiplexed,
                         assay_type=assay_type,
                         imaging_method=imaging_method,
                         objective=objective,
                         x_size=x_size,
                         y_size=y_size,
                         pixel_size=pixel_size,
                         z_planes=z_planes,
                         z_step=z_step,
                         time_points=time_points,
                         time_step=time_step,
                         channels=channels,
                         **kwargs)

        self.stocks_model_type = LightMicroscopyScreenAssay.stocks_model_type
        self.screen_type: Optional[StocksCVTerm] = screen_type
        self.screening_library: Optional[StocksCVTerm] = screening_library
        self.library_manufacturer: str = library_manufacturer
        self.library_version: str = library_version
        self.field_of_view: Optional[int] = field_of_view


class TransmissionEMAssay(Assay):
    """
    VolumeEMAssay shoudl be used when TEMAssay does not fit
    """
    technology: Technology = Technology.ELECTRON_MICROSCOPY
    platform: str = "UNKNOWN"
    stocks_model_type: str = ModelType.TRANSMISSIONEMASSAY.value

    def __init__(self,
                 name: str,
                 id: Optional[str] = None,
                 description: Optional[str] = None,
                 platform: str | None = None,
                 datasets: Optional[List[Dataset]] = None,
                 samples: Optional[List[EMSample]] = None,
                 instrumentrun: InstrumentRun | str = None,
                 multiplexed: bool = None,
                 assay_type: Optional[StocksCVTerm] = None,
                 imaging_method: List[StocksCVTerm] = [
                     StocksCVTerm(name="TEM", category=StocksCVCategory(name="transmission_em_imaging_method"))],
                 tem_type: Optional[StocksCVTerm] =
                 StocksCVTerm(name="Single Particle", category=StocksCVCategory(name="transmission_em_tem_type")),
                 assay_temperature: Optional[StocksCVTerm] =
                 StocksCVTerm(name="Cryo", category=StocksCVCategory(name="transmission_em_assay_temperature")),
                 correlative_assay: bool = False,
                 pixel_size: Optional[float] = None,
                 nominal_magnification: Optional[float] = None,
                 c2_aperture: Optional[float] = None,
                 objective_aperture: Optional[float] = None,
                 phase_plate: Optional[bool] = None,
                 exposure_time: Optional[float] = None,
                 electron_dose: Optional[float] = None,
                 image_format: Optional[StocksCVTerm] = None,
                 movie_fraction: Optional[float] = None,
                 beam_size: Optional[float] = None,
                 spot_size: Optional[float] = None,
                 beam_mode: Optional[StocksCVTerm] = None,
                 energy_filter: bool = True,
                 slit_width: Optional[float] = None,
                 montage: bool = True,
                 tilting_method: Optional[StocksCVTerm] = None,
                 series_size: Optional[float] = None,
                 angle_step: Optional[float] = None,
                 tilt_range: Optional[IntegerRange] = None,
                 **kwargs):
        # remove technology from kwargs is present
        kwargs.pop('technology', None)
        super().__init__(name=name,
                         id=id,
                         description=description,
                         technology=TransmissionEMAssay.technology,
                         platform=platform if platform else TransmissionEMAssay.platform,
                         datasets=datasets,
                         samples=samples,
                         instrumentrun=instrumentrun,
                         multiplexed=multiplexed,
                         **kwargs)

        self.stocks_model_type = TransmissionEMAssay.stocks_model_type
        self.imaging_method: List[StocksCVTerm] = imaging_method
        self.tem_type: Optional[StocksCVTerm] = tem_type
        self.assay_temperature: Optional[StocksCVTerm] = assay_temperature
        self.correlative_assay: bool = correlative_assay
        self.pixel_size: Optional[float] = pixel_size
        self.nominal_magnification: Optional[float] = nominal_magnification
        self.c2_aperture: Optional[float] = c2_aperture
        self.objective_aperture: Optional[float] = objective_aperture
        self.phase_plate: Optional[bool] = phase_plate
        self.exposure_time: Optional[float] = exposure_time
        self.electron_dose: Optional[float] = electron_dose
        self.image_format: Optional[StocksCVTerm] = image_format
        self.movie_fraction: Optional[float] = movie_fraction
        self.beam_size: Optional[float] = beam_size
        self.spot_size: Optional[float] = spot_size
        self.beam_mode: Optional[StocksCVTerm] = beam_mode
        self.energy_filter: bool = energy_filter
        self.slit_width: Optional[float] = slit_width
        self.montage: bool = montage
        # tomography fields
        self.tilting_method: Optional[StocksCVTerm] = tilting_method
        self.series_size: Optional[float] = series_size
        self.angle_step: Optional[float] = angle_step
        self.tilt_range: Optional[IntegerRange] = tilt_range


class VolumeEMAssay(Assay):
    """
    VolumeEMAssay should be used when TEMAssay does not fit
    """
    technology: Technology = Technology.ELECTRON_MICROSCOPY
    platform: str = "UNKNOWN"
    stocks_model_type: str = ModelType.VOLUMEEMASSAY.value

    def __init__(self,
                 name: str,
                 id: Optional[str] = None,
                 description: Optional[str] = None,
                 platform: str | None = None,
                 datasets: Optional[List[Dataset]] = None,
                 samples: Optional[List[EMSample]] = None,
                 instrumentrun: InstrumentRun | str = None,
                 multiplexed: bool = None,
                 assay_type: Optional[StocksCVTerm] = None,
                 imaging_method: List[StocksCVTerm] = [
                     StocksCVTerm(name="FIB-SEM", category=StocksCVCategory(name="volume_em_imaging_method"))],
                 is_cryo_fib_sem: bool = False,
                 correlative_assay: bool = False,
                 detector_type: Optional[StocksCVTerm] = None,
                 pixel_size_value: Optional[float] = None,
                 pixel_size_unit: Optional[StocksCVTerm] = None,
                 high_tension: Optional[float] = None,
                 primary_beam_current: Optional[float] = None,
                 tile_x_size: Optional[float] = None,
                 tile_y_size: Optional[float] = None,
                 tile_overlap: Optional[float] = None,
                 amount_of_images: Optional[int] = None,
                 dwell_time: Optional[float] = None,
                 line_average: Optional[float] = None,
                 slice_thickness: Optional[float] = None,
                 fibsem_milling_current: Optional[float] = None,
                 **kwargs):
        # remove technology from kwargs is present
        kwargs.pop('technology', None)
        super().__init__(name=name,
                         id=id,
                         description=description,
                         technology=VolumeEMAssay.technology,
                         platform=platform if platform else VolumeEMAssay.platform,
                         datasets=datasets,
                         samples=samples,
                         instrumentrun=instrumentrun,
                         multiplexed=multiplexed,
                         **kwargs)

        self.stocks_model_type = VolumeEMAssay.stocks_model_type
        self.imaging_method: List[StocksCVTerm] = imaging_method
        self.is_cryo_fib_sem: bool = is_cryo_fib_sem
        self.correlative_assay: bool = correlative_assay
        self.detector_type: Optional[StocksCVTerm] = detector_type
        self.pixel_size_value: Optional[float] = pixel_size_value
        self.pixel_size_unit: Optional[StocksCVTerm] = pixel_size_unit
        self.high_tension: Optional[float] = high_tension
        self.primary_beam_current: Optional[float] = primary_beam_current
        self.tile_x_size: Optional[float] = tile_x_size
        self.tile_y_size: Optional[float] = tile_y_size
        self.tile_overlap: Optional[float] = tile_overlap
        self.amount_of_images: Optional[int] = amount_of_images
        self.dwell_time: Optional[float] = dwell_time
        self.line_average: Optional[float] = line_average
        self.slice_thickness: Optional[float] = slice_thickness
        self.fibsem_milling_current: Optional[float] = fibsem_milling_current


# define all property to CVCategory mappings
Study.add_values_to_property_to_cvcategory('experimental_design_terms', 'efo_study_design')
ChannelVisualizedEntity.add_values_to_property_to_cvcategory('target_type', 'imaging_channel_target_types')
ChannelVisualizedEntity.add_values_to_property_to_cvcategory('label', 'imaging_channel_labels')
ChannelVisualizedEntity.add_values_to_property_to_cvcategory('probe_type', 'imaging_channel_probe_types')
LightMicroscopyAssay.add_values_to_property_to_cvcategory('assay_type', 'imaging_assay_types')
LightMicroscopyAssay.add_values_to_property_to_cvcategory('imaging_method', 'imaging_methods')
LightMicroscopyAssay.add_values_to_property_to_cvcategory('objective', 'imaging_microscope_objectives')
LightMicroscopyScreenAssay.add_values_to_property_to_cvcategory('screen_type', 'imaging_screen_types')
LightMicroscopyScreenAssay.add_values_to_property_to_cvcategory('screening_library', 'imaging_screen_library_types')
TransmissionEMAssay.add_values_to_property_to_cvcategory('imaging_method', 'transmission_em_imaging_method')
TransmissionEMAssay.add_values_to_property_to_cvcategory('tem_type', 'transmission_em_tem_type')
TransmissionEMAssay.add_values_to_property_to_cvcategory('assay_temperature', 'transmission_em_assay_temperature')
TransmissionEMAssay.add_values_to_property_to_cvcategory('image_format', 'transmission_em_image_format')
TransmissionEMAssay.add_values_to_property_to_cvcategory('beam_mode', 'transmission_em_beam_mode')
TransmissionEMAssay.add_values_to_property_to_cvcategory('tilting_method', 'transmission_em_tilting_method')
VolumeEMAssay.add_values_to_property_to_cvcategory('imaging_method', 'volume_em_imaging_method')
VolumeEMAssay.add_values_to_property_to_cvcategory('detector_type', 'volume_em_detector_type')
VolumeEMAssay.add_values_to_property_to_cvcategory('pixel_size_unit', 'volume_em_pixel_size_unit')
